
AddCSLuaFile()

SWEP.PrintName = "Defibrillator"
SWEP.Author = "Vac"
SWEP.Purpose = "Defibrillate people with left click, charge with right click."

SWEP.Slot = 2
SWEP.SlotPos = 3

SWEP.Spawnable = true

SWEP.ViewModel = Model( "models/weapons/c_medkit.mdl" )
SWEP.WorldModel = Model( "models/weapons/w_medkit.mdl" )
SWEP.ViewModelFOV = 80

SWEP.UseHands = true

SWEP.Category = "Impact"

SWEP.Primary.ClipSize = 1
SWEP.Primary.DefaultClip = 0
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = "none"
SWEP.isCharged = 0

SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"

SWEP.UsageDelay = 1

local HealSound = Sound( "HealthKit.Touch" )
local DenySound = Sound( "WallHealth.Deny" )

if SERVER then
	CVAR_DEFIB_TIME = CreateConVar( "defib_defibtime", "0", { FCVAR_SERVER_CAN_EXECUTE, FCVAR_REPLICATED, FCVAR_NOTIFY }, "Amount of time using the defib on a body should take, in seconds." );
	CVAR_DEFIB_WAKE = CreateConVar( "defib_wake", "1", { FCVAR_SERVER_CAN_EXECUTE, FCVAR_REPLICATED, FCVAR_NOTIFY }, "1 to enable the wake-up animation on respawn, 0 to disable." );
	CVAR_DEFIB_HEALTH = CreateConVar( "defib_health", "100", { FCVAR_SERVER_CAN_EXECUTE, FCVAR_REPLICATED, FCVAR_NOTIFY }, "The amount of health the defibrillated player should spawn with." );
	CVAR_DEFIB_DAMAGE = CreateConVar( "defib_damage", "0", { FCVAR_SERVER_CAN_EXECUTE, FCVAR_REPLICATED, FCVAR_NOTIFY }, "Amount of damage the defib should do if used on players." );
	CVAR_DEFIB_DELAY = CreateConVar( "defib_delay", "0", { FCVAR_SERVER_CAN_EXECUTE, FCVAR_REPLICATED, FCVAR_NOTIFY }, "Delay between being able to re-charge the defibrillator." );
end

function SWEP:Initialize()

	self:SetHoldType( "slam" )
	if ( CLIENT ) then return end
end

function SWEP:PrimaryAttack()

	if ( CLIENT ) then return end

	if (self.isCharged != 1) then self.Owner:PrintMessage(HUD_PRINTCENTER,"Defibrillator is not charged, press right click!") return end

	if ( self.Owner:IsPlayer() ) then
		self.Owner:LagCompensation( true )
	end

	local tr = util.TraceLine( {
		start = self.Owner:GetShootPos(),
		endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * 64,
		filter = self.Owner
	} )
	if ( self.Owner:IsPlayer() ) then
		self.Owner:LagCompensation( false )
	end

	local ent = tr.Entity -- Entity looked at
	if( ent != nil and ent:IsValid() and ent:GetClass() == "prop_ragdoll" ) then

		--Respawn
		if ( ent.Ply == NULL ) then return end

		self.Owner:EmitSound("items/smallmedkit1.wav", 100,100)

		defibTimer = GetConVarNumber("defib_defibtime")

		if defibTimer > 0 then
			timeLeft = defibTimer

			timer.Create("defibWait_" .. self.Owner:Name(), 1, defibTimer, function()
				self.Owner:EmitSound("items/smallmedkit1.wav", 100,100)
				local tr = util.TraceLine( {
					start = self.Owner:GetShootPos(),
					endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * 64,
					filter = self.Owner
				} )

				if tr.Entity == nil or tr.Entity == NULL then
					
					self.Owner:PrintMessage(HUD_PRINTTALK,"You failed the defibrillation!")
					timer.Remove("defibWait_" .. self.Owner:Name())
				else
					timeLeft = timeLeft - 1
					self.Owner:PrintMessage(HUD_PRINTTALK,"Time left: "..tostring(timeLeft))
				end

				if timeLeft == 0 then
					DefibRespawnPlayer(ent.Ply, ent, self)
				end
			end)

		else
			DefibRespawnPlayer(ent.Ply, ent, self)
		end
	end
	if ent:IsPlayer() and GetConVarNumber("defib_damage") > 0 then
		ent:SetHealth(ent:Health() - GetConVarNumber("defib_damage"))
		self.Owner:EmitSound("npc/combine_soldier/pain1.wav", 100,100)
		self.isCharged = 0
		if ent:Health() <= 0 then
			ent:Kill()
		end
	end
end

function DefibRespawnPlayer(deadGuy, ent, self)
		deadGuy = ent.Ply
		
		deadGuy:UnSpectate()
		deadGuy:Spawn()

		deadGuy:SetHealth(GetConVarNumber("defib_health"))
		deadGuy:SetPos(deadGuy.defibRagdoll:GetPos() + Vector(0,0,20))
		if GetConVarNumber("defib_wake") == 1 then deadGuy:ScreenFade( SCREENFADE.IN, Color( 0,0,0, 255 ), 0.5, 0.5 ) end
		-- Give weps back
		if not deadGuy.WepTbl == NULL then

			table.ForEach( deadGuy.WepTbl , function ( k, v )

				deadGuy:Give(deadGuy.WepTbl[k])

			end)
			deadGuy.WepTbl = NULL
		end

		
		ent:Remove()

		self.isCharged = 0
		self.Owner:PrintMessage(HUD_PRINTCENTER,"Defibrillator charge used!")
end

function SWEP:SecondaryAttack()

	if (CLIENT) then return end

	if GetConVarNumber("defib_delay") > 0 then
		a = CurTime()
		if !(a > self.UsageDelay) then 
			self.Owner:PrintMessage(HUD_PRINTCENTER,"You need to wait "..math.Round(self.UsageDelay - CurTime()).." seconds!") self.Owner:EmitSound("items/suitchargeno1.wav", 100, 100)
			return
		end
	end
	if (self.isCharged == 0) then

		self.isCharged = 1
		self.Owner:EmitSound("items/medshot4.wav", 100, 100)
		self.Owner:PrintMessage(HUD_PRINTCENTER,"Defibrillator now charged!")
		self.UsageDelay = CurTime() + GetConVarNumber("defib_delay")

	end

end

function SWEP:OnRemove()

	timer.Stop( "medkit_ammo" .. self:EntIndex() )
	timer.Stop( "weapon_idle" .. self:EntIndex() )

end

function SWEP:Holster()

	timer.Stop( "weapon_idle" .. self:EntIndex() )

	return true

end

function SWEP:CustomAmmoDisplay()

	self.AmmoDisplay = self.AmmoDisplay or {}
	self.AmmoDisplay.Draw = true
	self.AmmoDisplay.PrimaryClip = self:Clip1()

	return self.AmmoDisplay

end

-- Spawn ragdoll on player on death
hook.Add( "PlayerDeath", "[Impact] Replace default ragdoll on death", function( victim, inflictor, attacker)
	if (victim.preDeathWeapons != NULL) then victim.preDeathWeps = NULL end
	if (!(victim:IsPlayer())) then return end
	if (victim:GetCharacter()) then
		local fac = GetFactionObject(victim:GetCharacter():GetFaction())
		if (fac.isDroid) then
			return
		end
	end
	local deathRagdoll = ents.Create( "prop_ragdoll" )

	if ( !IsValid( deathRagdoll ) ) then return end // Check whether we successfully made an entity, if not - bail
	deathRagdoll:SetPos( Vector( victim:GetPos() ) )

	deathRagdoll:SetModel( victim:GetModel() )
	deathRagdoll:SetSkin( victim:GetSkin() )
	
	deathRagdoll.Ply = victim
	deathRagdoll:SetNWBool("defib_show", true)
	deathRagdoll:SetCollisionGroup(COLLISION_GROUP_WEAPON )
	deathRagdoll:Spawn()

	victim.defibRagdoll = deathRagdoll
	victim:EmitSound("HL1\fvox\flatline.wav", 100, 100)
	victim:ScreenFade( SCREENFADE.IN, Color( 200,0,0, 70 ), 3, 15 )

	local plyvel = victim:GetVelocity()

	for i = 1, deathRagdoll:GetPhysicsObjectCount() do
		local bone = deathRagdoll:GetPhysicsObjectNum(i)
		
		if bone and bone.IsValid and bone:IsValid() then
			local bonepos, boneang = victim:GetBonePosition(deathRagdoll:TranslatePhysBoneToBone(i))
			
			bone:SetPos(bonepos)
			bone:SetAngles(boneang)
			bone:SetVelocity(plyvel)
		end
	end

	--Remove normal ragdoll
	timer.Simple(0.01, function()
		if(victim:GetRagdollEntity() != nil and	victim:GetRagdollEntity():IsValid()) then
			victim:GetRagdollEntity():Remove()
		end
	end )

	victim.WepTbl = {}
	-- Get weapons and store them
	for k,v in pairs(victim:GetWeapons()) do
		victim.WepTbl[table.Count(victim.WepTbl) + 1] = v:GetClass()
	end

end)


hook.Add ( "PlayerSpawn", "[Impact] Player Ragdoll Spawn", function( player )

	if (player.defibRagdoll == NULL or player.defibRagdoll == nil) then return end

	player:UnSpectate()
	player.defibRagdoll:Remove()

end)

hook.Add( "PlayerDisconnected", "[Impact] Player Ragdoll DC", function( ply )

	if (ply.defibRagdoll == NULL or ply.defibRagdoll == nil) then return end

	player:UnSpectate()
	ply.defibRagdoll:Remove()
	ply.preDeathWeps = NULL

end)