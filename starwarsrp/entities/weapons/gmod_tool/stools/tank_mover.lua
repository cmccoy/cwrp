
TOOL.Category = "Gamemaster"
TOOL.Name = "Tank Mover"
TOOL.Command = nil
TOOL.ConfigName = ""

TOOL.ClientConVar[ "vj_targetpath_x" ] = 0
TOOL.ClientConVar[ "vj_targetpath_y" ] = 0
TOOL.ClientConVar[ "vj_targetpath_z" ] = 0
TOOL.ClientConVar[ "nostopdriving" ] = 0
TOOL.ClientConVar[ "nostopdrivingwhenfrontblocked" ] = 0

--TOOL.ClientConVar[ "fx" ] = "0"

TOOL.Information = {
	{ name = "left" },
	{ name = "right" },
	{ name = "reload" }
}




local function SetColour( ply, ent, data )

	--
	-- If we're trying to make them transparent them make the render mode
	-- a transparent type. This used to fix in the engine - but made HL:S props invisible(!)
	--
	if ( data.Color && data.Color.a < 255 && data.RenderMode == 0 ) then
		data.RenderMode = 1
	end

	if ( data.Color ) then ent:SetColor( Color( data.Color.r, data.Color.g, data.Color.b, data.Color.a ) ) end
	if ( data.RenderMode ) then ent:SetRenderMode( data.RenderMode ) end
	if ( data.RenderFX ) then ent:SetKeyValue( "renderfx", data.RenderFX ) end

	if ( SERVER ) then
		duplicator.StoreEntityModifier( ent, "colour", data )
	end

end
--duplicator.RegisterEntityModifier( "colour", SetColour )


local function SendErrorMessage( ply, ent, message )

	--if ( CLIENT ) then

	ply:PrintMessage( HUD_PRINTCONSOLE, message )

	--end

end

function TOOL:LeftClick( trace )

	local ent = trace.Entity
	if ( !IsValid( ent ) ) then return false end
	if ( IsValid( ent.AttachedEntity ) ) then ent = ent.AttachedEntity end
	if ( !IsValid( ent ) ) then return false end -- The entity is valid and isn't worldspawn
	if ( CLIENT ) then return true end

	/*
	local r = self:GetClientNumber( "r", 0 )
	local g = self:GetClientNumber( "g", 0 )
	local b = self:GetClientNumber( "b", 0 )
	local a = self:GetClientNumber( "a", 0 )
	local fx = self:GetClientNumber( "fx", 0 )
	local mode = self:GetClientNumber( "mode", 0 )

	SetColour( self:GetOwner(), ent, { Color = Color( r, g, b, a ), RenderMode = mode, RenderFX = fx } )
	*/
	
	local xx = self:GetClientNumber( "vj_targetpath_x", 0 )
	local yy = self:GetClientNumber( "vj_targetpath_y", 0 )
	local zz = self:GetClientNumber( "vj_targetpath_z", 0 )
	
	local tank_nostopdriving = self:GetClientNumber( "nostopdriving", 0 )
	local tank_nostopdrivingblocked = self:GetClientNumber( "nostopdrivingwhenfrontblocked", 0 )
	
	if ent != nil && ent:IsValid() then
	if (!ent:IsPlayer() && ent:IsNPC() && ent:Health() > 0) then
	if ent.IsVJBaseSNPC == true && ent.Dead != true && ent.IsVJBaseSNPC_Tank == true then
	
	if ent.driving != nil then
	
	local targetpath_vector = Vector(xx,yy,zz)
	
	ent.targetpath1 = targetpath_vector
	
	ent.nostopdriving = tank_nostopdriving
	ent.nostopdrivingwhenfrontblocked = tank_nostopdrivingblocked
	
	else
	
	SendErrorMessage( self:GetOwner(), ent, "ERROR." )
	
	end
	
	else
	
	SendErrorMessage( self:GetOwner(), ent, "The NPC is not an VJ SNPC using VJ tank base." )
	
	end

	else
	SendErrorMessage( self:GetOwner(), ent, "The entity is not an SNPC or it is dead." )
	
	end
	end
	
	return true

end

function TOOL:RightClick( trace )


	
	
	local pos = trace.HitPos
	if !( isvector( pos ) ) then return false end
	
	if ( CLIENT ) then return true end
	
	
	self:GetOwner():ConCommand( "vjww2tankmover_vj_targetpath_x " .. pos.x )
	self:GetOwner():ConCommand( "vjww2tankmover_vj_targetpath_y " .. pos.y )
	self:GetOwner():ConCommand( "vjww2tankmover_vj_targetpath_z " .. pos.z )
	
	
	return true

end

function TOOL:Reload( trace )

	local ent = trace.Entity
	if ( !IsValid( ent ) ) then return false end
	if ( IsValid( ent.AttachedEntity ) ) then ent = ent.AttachedEntity end
	if ( !IsValid( ent ) ) then return false end -- The entity is valid and isn't worldspawn
	if ( CLIENT ) then return true end

	
	--local xx = self:GetClientNumber( "targetpath_x", 0 )
	--local yy = self:GetClientNumber( "targetpath_y", 0 )
	--local zz = self:GetClientNumber( "targetpath_z", 0 )
	
	
	local tank_nostopdriving = self:GetClientNumber( "nostopdriving", 0 )
	local tank_nostopdrivingblocked = self:GetClientNumber( "nostopdrivingwhenfrontblocked", 0 )
	
	if ent != nil && ent:IsValid() then
	if (!ent:IsPlayer() && ent:IsNPC() && ent:Health() > 0) then
	if ent.IsVJBaseSNPC == true && ent.Dead != true && ent.IsVJBaseSNPC_Tank == true then
	
	if ent.driving != nil then
	
	--local targetpath_vector = Vector(xx,yy,zz)
	
	ent.targetpath1 = 0
	
	ent.nostopdriving = tank_nostopdriving
	ent.nostopdrivingwhenfrontblocked = tank_nostopdrivingblocked
	
	else
	
	SendErrorMessage( self:GetOwner(), ent, "ERROR." )
	
	end
	
	else
	
	SendErrorMessage( self:GetOwner(), ent, "The NPC is not an VJ SNPC using VJ tank base." )
	
	end

	else
	SendErrorMessage( self:GetOwner(), ent, "The entity is not an SNPC or it is dead." )
	
	end
	end
	
	return true

end

--local ConVarsDefault = TOOL:BuildConVarList()

function TOOL.BuildCPanel( panel )

	--CPanel:AddControl( "Header", { Description = "#tool.colour.desc" } )

	panel:AddControl("Header", { Text = "Tank Mover Tool", Description = "If it doesn't work, it doesn't work!." })
	
	
	--CPanel:AddControl( "ComboBox", { MenuButton = 1, Folder = "colour", Options = { [ "#preset.default" ] = ConVarsDefault }, CVars = table.GetKeys( ConVarsDefault ) } )

	--CPanel:AddControl( "Color", { Label = "#tool.colour.color", Red = "colour_r", Green = "colour_g", Blue = "colour_b", Alpha = "colour_a" } )

	--CPanel:AddControl( "ListBox", { Label = "#tool.colour.mode", Options = list.Get( "RenderModes" ) } )
	--CPanel:AddControl( "ListBox", { Label = "#tool.colour.fx", Options = list.Get( "RenderFX" ) } )

	panel:AddControl("Slider", {
	    Label = "Don't stop to engage its enemies, if visible",
	    --Type = "Float",
		Min = "0",
	    Max = "1",
	    Command = "vjww2tankmover_nostopdriving"
	})
	
	panel:AddControl("Slider", {
	    Label = "Don't stop when a friendly SNPC is in its front.",
	    --Type = "Float",
		Min = "0",
	    Max = "1",
	    Command = "vjww2tankmover_nostopdrivingwhenfrontblocked"
	})
	
	
	
	
end

if CLIENT then
language.Add( "tool.vjww2tankmover.left", "Select a VJ tank SNPC to move." )
language.Add( "tool.vjww2tankmover.right", "Select a position to move to." )
language.Add( "tool.vjww2tankmover.reload", "Make the tank SNPC stop." )

language.Add( "tool.vjww2tankmover.name", "Tank Mover" )
language.Add( "tool.vjww2tankmover.desc", "" )
end