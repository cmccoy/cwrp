AddCSLuaFile()

if (CLIENT) then
	SWEP.PrintName = "Gonker"
	SWEP.Slot = 0
	SWEP.SlotPos = 5
	SWEP.DrawAmmo = false
	SWEP.DrawCrosshair = false
end

SWEP.Category = "IG"
SWEP.Author = "Vac"
SWEP.Instructions = "GONK GUN GO GONKKKKKKKKKKKKKKK"
SWEP.Purpose = "GONK"
SWEP.Drop = false

SWEP.HoldType = "melee"

SWEP.Spawnable = true
SWEP.AdminOnly = true

SWEP.ViewModelFOV = 65
SWEP.ViewModelFlip = false
SWEP.AnimPrefix	 = "melee"

SWEP.Primary.ClipSize	= 0
SWEP.Primary.Automatic = false
SWEP.Primary.DefaultClip = 0
SWEP.Primary.Ammo = "none"
SWEP.DrawCrosshair = false
SWEP.Primary.Delay = 0
SWEP.IsAlwaysRaised = true

SWEP.ViewModel = "models/weapons/v_hands.mdl"
SWEP.WorldModel = ""

function SWEP:Precache()
	util.PrecacheSound("weapon/ig_cwrp/gonk.mp3")
end

function SWEP:Initialize()
    self:SetHoldType(self.HoldType)
end

function SWEP:OnLowered()

end

function SWEP:Holster(nextWep)
	return true
end

function SWEP:PrimaryAttack()
    self:EmitSound( "weapon/ig_cwrp/gonk.mp3" )
    self:SetNextPrimaryFire(CurTime() + 5)
end

function SWEP:SecondaryAttack()
	
end
