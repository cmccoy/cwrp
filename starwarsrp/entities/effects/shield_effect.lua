local matColor  = Material("Models/effects/comball_tape")
local Zero		= Vector(0,0,0)
local EN = {
	Vector(1,0,0),
	Vector(-1,0,0),
	Vector(0,1,0),
	Vector(0,-1,0),
	Vector(0,0,1),
	Vector(0,0,-1),
}

function EFFECT:Init( data )
    self.Target = data:GetEntity()
	self.StartTime = CurTime()
	self.Length = 1
end

function EFFECT:Think()
	return self.StartTime + self.Length > CurTime()
end

function EFFECT:Render()
	render.OverrideDepthEnable(true,false)
		self.Target:DisableMatrix("RenderMultiply")
		self.Target:SetRenderAngles(self.Target:GetRenderAngles())
		render.MaterialOverride(matColor)
		render.SetColorModulation(2,5,5)
		self.Target:DrawModel()
		render.SetBlend(1)
		render.MaterialOverride(nil)
		render.SetColorModulation(1,1,1)
		self.Target:SetRenderOrigin(Zero)
	render.OverrideDepthEnable(false,false)
end