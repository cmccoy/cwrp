include('shared.lua')

local  Laser = Material( "trails/laser" )

local lastThink = CurTime()
local VectorStart
local Vector1
local Vector2
local Vector3
local Vector4

function ENT:Draw()
	
    self:DrawModel()
	
	if lastThink < CurTime() then
		
		VectorStart = self:LocalToWorld( Vector( 0, 0, 0 ) ) 
		Vector1 = self:LocalToWorld( Vector( 0, 0, -200 ) + Vector(math.random(-250,250),math.random(-250,250),0) )
		Vector2 = self:LocalToWorld( Vector( 0, 0, -200 ) + Vector(math.random(-250,250),math.random(-250,250),0) )
		Vector3 = self:LocalToWorld( Vector( 0, 0, -200 ) + Vector(math.random(-250,250),math.random(-250,250),0) )
		Vector4 = self:LocalToWorld( Vector( 0, 0, -200 ) + Vector(math.random(-250,250),math.random(-250,250),0) )
		
		render.SetMaterial( Laser )
		render.DrawBeam( Vector1, VectorStart, 100, 1, 1, Color( math.random(0,255), math.random(0,255), math.random(0,255), 255 ) )
		render.DrawBeam( Vector2, VectorStart, 100, 1, 1, Color( math.random(0,255), math.random(0,255), math.random(0,255), 255 ) ) 
		render.DrawBeam( Vector3, VectorStart, 100, 1, 1, Color( math.random(0,255), math.random(0,255), math.random(0,255), 255 ) ) 
		render.DrawBeam( Vector4, VectorStart, 100, 1, 1, Color( math.random(0,255), math.random(0,255), math.random(0,255), 255 ) ) 
		
		lastThink = CurTime() + .2
		
	else
	
		render.SetMaterial( Laser )
		render.DrawBeam( Vector1, VectorStart, 100, 1, 1, Color( math.random(0,255), math.random(0,255), math.random(0,255), 255 ) ) 
		render.DrawBeam( Vector1, VectorStart, 100, 1, 1, Color( math.random(0,255), math.random(0,255), math.random(0,255), 255 ) )
		render.DrawBeam( Vector2, VectorStart, 100, 1, 1, Color( math.random(0,255), math.random(0,255), math.random(0,255), 255 ) ) 
		render.DrawBeam( Vector3, VectorStart, 100, 1, 1, Color( math.random(0,255), math.random(0,255), math.random(0,255), 255 ) ) 
		render.DrawBeam( Vector4, VectorStart, 100, 1, 1, Color( math.random(0,255), math.random(0,255), math.random(0,255), 255 ) ) 
	
	end
	
end