AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
 
include('shared.lua')

function ENT:Initialize()

        self:SetModel("models/props/starwars/medical/health_droid.mdl")

        self:PhysicsInit(SOLID_VPHYSICS)
        self:SetMoveType(MOVETYPE_VPHYSICS)
        self:SetSolid(SOLID_VPHYSICS)
		self:Activate()

       
end


function ENT:Think()

	for k, v in ipairs(ents.FindInSphere(self:GetPos(), 100)) do
		if v:IsPlayer() && v:Alive() then
			if(v:Health() < v:GetMaxHealth()) then
				if((v:Health() + 10) > v:GetMaxHealth()) then
					v:SetHealth(v:GetMaxHealth())
				else
					v:SetHealth(v:Health() + 10)
				end
				self:EmitSound("items/medshot4.wav")
			end
		end
	end
	self:NextThink(CurTime() + 1)
	return true
	
end