include('shared.lua')

local function Draw3DText( pos, ang, scale, text, flipView )
	if ( flipView ) then
		-- Flip the angle 180 degrees around the UP axis
		ang:RotateAroundAxis( Vector( 0, 0, 1 ), 180 )
	end

	cam.Start3D2D( pos, ang, scale )
		-- Actually draw the text. Customize this to your liking.
		draw.DrawText( text, "DermaLarge", 0, 0, Color( 255, 255, 255, 150 ), TEXT_ALIGN_CENTER )
	cam.End3D2D()
end

function ENT:Draw()

    self:DrawModel()

    if LocalPlayer():GetPos():DistToSqr(self:GetPos()) > 600*600 then return end

	local text = "Hackable Computer" -- The text to display

	local mins, maxs = self:GetModelBounds()
	local pos = self:GetPos() + Vector( 0, 0, 80 )

	local direction = self:GetPos() - LocalPlayer():GetPos()
	local x_d = direction.x
	local y_d = direction.y
	local ang = Angle(0, math.deg(math.atan(y_d/x_d))+90/(x_d/-math.abs(x_d)), 90)


	Draw3DText( pos, ang, 0.2, text, false )
 
end

function ENT:Think()

    
       
end