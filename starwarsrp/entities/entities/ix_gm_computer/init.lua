AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
 
include('shared.lua')

function ENT:Initialize()

	self:SetModel("models/props/consolebox.mdl")

	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	self:SetUseType(SIMPLE_USE)
	self:Activate()

	self.uses = 1

	self.InUse = false

end

function ENT:Use(activator)

	if (activator:IsPlayer()) then
		if (self.InUse) then activator:ChatPrint("Terminal is already in use") return end
		if (self.uses <= 0 ) then activator:ChatPrint("This device has already been hacked") return end
		local rand = math.random(60, 120)
		self.InUse = true
		activator:SetAction("Hacking...", rand)
		activator:DoStaredAction(self, function()
			if (math.random(1, 2) == 1) then

				if (math.random(1, 2) == 1) then
					activator:GetCharacter():GetInventory():Add("cooling_unit")
					activator:ChatPrint("You were able to snag a cooling unit from the computer!")
				end

				local money = rand * 5
				activator:GetCharacter():GiveMoney(money)
				activator:ChatPrint("You got " .. money .. " RC for hacking the computer")

				for k, v in ipairs (player.GetAll()) do
					v:ChatPrint("[EVENT] " .. activator:GetName() .. " successfully hacked a computer")
				end
			else
				for k, v in ipairs (player.GetAll()) do
					v:ChatPrint("[EVENT] " .. activator:GetName() .. " was unable to hack a computer")
				end
			end
			self.uses = self.uses - 1
			self.InUse = false
		end, rand, function()
			activator:SetAction("Hacking...", 0.01)
			self.InUse = false
		end)
	end

end

local nextThink = CurTime()

function ENT:Think()
	if (CurTime() > nextThink) then
		if (self.InUse) then
			local vPoint = self:GetPos() + Vector(0,0, 40)
			local effectdata = EffectData()
			effectdata:SetOrigin( vPoint )
			util.Effect( "cball_bounce", effectdata )

			self:EmitSound("ambient/energy/spark" .. math.random(1,6) ..".wav")
		end
		nextThink = CurTime() + 2
	end
end