
AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )

include('shared.lua')

/*---------------------------------------------------------
Initialize
---------------------------------------------------------*/
function ENT:Initialize()

	self.Entity:SetModel("models/weapons/w_eq_fraggrenade_thrown.mdl")
	self.Entity:PhysicsInit( SOLID_VPHYSICS )
	self.Entity:SetMoveType( MOVETYPE_VPHYSICS )
	self.Entity:SetSolid( SOLID_VPHYSICS )
	self.Entity:DrawShadow( false )
	self.Entity:SetFriction(-9)
	
	-- Don't collide with the player
	self.Entity:SetCollisionGroup( COLLISION_GROUP_WEAPON )
	self.Entity:SetNetworkedString("Owner", "World")
	
	local phys = self.Entity:GetPhysicsObject()
	
	if (phys:IsValid()) then
		phys:Wake()
	end
	self:EmitSound("weapons/explosives_cannons_superlazers/sw_detonator_startup.ogg")
	timer.Simple(3, function()
		self:EmitSound("weapons/explosives_cannons_superlazers/sw_detonator_explode.ogg")
	end)
	self.timer = CurTime() + 4
end

local exp

/*---------------------------------------------------------
Think
---------------------------------------------------------*/
function ENT:Think()
	if self.timer < CurTime() then

	self:Explosion()
	self.Entity:Remove()
	end
end

/*---------------------------------------------------------
HitEffect
---------------------------------------------------------*/
function ENT:HitEffect()
	for k, v in pairs ( ents.FindInSphere( self.Entity:GetPos(), 800 ) ) do
		if v:IsValid() && v:IsPlayer() then
			ent:EmitSound( "ambient/explosions/explode_3.wav", 180, 100 );
		end	
	end
end

/*---------------------------------------------------------
Explosion
---------------------------------------------------------*/
function ENT:Explosion()

	ParticleEffect( "shock_explode_ch_after", self:GetPos() + Vector(0, 0, 30), Angle( 0, 0, 0 ) )
	ParticleEffect( "shock_explode_ch_glow", self:GetPos() + Vector(0, 0, 30), Angle( 0, 0, 0 ) )
	ParticleEffect( "shock_explode_ch_sparks", self:GetPos() + Vector(0, 0, 30), Angle( 0, 0, 0 ) )
	ParticleEffect( "electrical_arc_01_system", self:GetPos() + Vector(0, 0, 30), Angle( 0, 0, 0 ) )
	sound.Play("weapons/explosives_cannons_superlazers/sw_detonator_explosion.ogg", self:GetPos() + Vector(0, 0, 30))
	local shake = ents.Create( "env_shake" )
		shake:SetOwner( self.Owner )
		shake:SetPos( self.Entity:GetPos() )
		shake:SetKeyValue( "amplitude", "2000" )	-- Power of the shake
		shake:SetKeyValue( "radius", "900" )	-- Radius of the shake
		shake:SetKeyValue( "duration", "2.5" )	-- Time of shake
		shake:SetKeyValue( "frequency", "2550" )	-- How har should the screenshake be
		shake:SetKeyValue( "spawnflags", "4" )	-- Spawnflags( In Air )
		shake:Spawn()
		shake:Activate()
		shake:Fire( "StartShake", "", 0 )




	for k, v in pairs ( ents.FindInSphere( self.Entity:GetPos(), 250 ) ) do
		v:Fire( "EnableMotion", "", math.random( 0, 0.5 ) )

		if (v:IsNPC()) then
			v:TakeDamage(500, self.Owner, self)
		end
	end
	
end