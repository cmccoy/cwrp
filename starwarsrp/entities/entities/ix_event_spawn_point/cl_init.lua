include('shared.lua')

ENT.Category			= "Impact"
ENT.Spawnable			= true
ENT.AdminSpawnable		= false
ENT.RenderGroup 		= RENDERGROUP_OPAQUE

function ENT:Think()

end

function ENT:Draw()
	self:DrawModel()
end

function ENT:Think()
	self:SetAngles(self:GetAngles() + Angle(0, 1, 0))
end

local mat = Material("models/props_combine/tprings_globe")

hook.Add( "PostDrawTranslucentRenderables", "[Impact] Draw Spawn Range", function()

	for k, v in ipairs(ents.FindByClass("ix_event_spawn_point")) do
		render.SetMaterial(mat)

		render.DrawSphere( v:GetPos(), -250, 30, 30, Color( 0, 175, 175, 100 ) )
		render.DrawSphere( v:GetPos(), 250, 30, 30, Color( 0, 175, 175, 100 ) )
	end

end )