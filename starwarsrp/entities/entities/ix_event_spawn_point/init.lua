AddCSLuaFile( "cl_init.lua" ) 
AddCSLuaFile( "shared.lua" ) 
include('shared.lua')

event_spawner_active = false
event_spawner_point = Vector(0,0,0)

sound.Add( {
	name = "enzo_engine_idle",
	channel = CHAN_STATIC,
	volume = 1.0,
	level = 80,
	pitch = {95, 110},
	sound = "vehicles/enzo/idle.wav"
} )

function ENT:Initialize()

	self:SetModel( "models/starwars/syphadias/props/sw_tor/bioware_ea/props/nar_shadda/nar_street_light_02.mdl" )
	self:SetModelScale(.25,0)
	self:SetSkin( 0 ) 
	self:SetUseType( SIMPLE_USE )
	self:SetCollisionGroup(COLLISION_GROUP_DEBRIS)
	self:AddEFlags( EFL_FORCE_CHECK_TRANSMIT )
	self:SetMaterial("models/wireframe")
	local phys = self:GetPhysicsObject()
	if (phys:IsValid()) then
		phys:Wake()
	end
	self:EmitSound("spawn_point_loop")

	event_spawner_active = true
	event_spawner_point = self:GetPos()
	
end

function ENT:Use( ply, caller )
	
end

function ENT:UpdateTransmitState()
	return TRANSMIT_PVS
end

local nextThink = CurTime()

function ENT:Think()
	if (CurTime() > nextThink) then
		for k, v in ipairs(ents.FindInSphere(self:GetPos(), 250)) do
			if v:IsPlayer() && !v.spawn_shield then
				v.spawn_shield = true
				timer.Simple(3, function()
					v.spawn_shield = false
				end)
			end
		end
		nextThink = CurTime() + 3
	end
end

function ENT:OnRemove()
	if(#ents.FindByClass("ix_event_spawn_point") <= 0) then
		event_spawner_active = false
		event_spawner_point = nil
	end 
	self:StopSound("spawn_point_loop")
end

hook.Add("PlayerShouldTakeDamage", "[Impact] Disable damage around spawn point", function(client, attacker)

	if(client.spawn_shield) then
		return false
	end

end)