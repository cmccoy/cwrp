AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
 
include('shared.lua')

function ENT:Initialize()

	self:SetModel("models/props/swscrap1.mdl")

	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	self:SetUseType(SIMPLE_USE)
	self:Activate()

	self.uses = 20

end

function ENT:Use(activator)

	if (activator:IsPlayer()) then
		if(self.uses <= 0 ) then activator:ChatPrint("There's nothing left in this pile") return end
		activator:SetAction("Searching...", 5)
		activator:DoStaredAction(self, function()
			if (math.random(1, 10) == 5) then
				activator:GetCharacter():GetInventory():Add("scrap_metal")
				activator:ChatPrint("You found some scrap")
			else
				activator:ChatPrint("You didn't find anything")
			end
			self.uses = self.uses - 1

			if (self.uses <= 0) then
				timer.Simple(300, function()
					self.uses = 20
				end)
			end
		end, 5, function()
			activator:SetAction("Searching...", 0.01)
		end)
	end

end

function ENT:Think()
	
end