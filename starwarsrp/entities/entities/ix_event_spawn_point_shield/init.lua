AddCSLuaFile( "cl_init.lua" ) 
AddCSLuaFile( "shared.lua" ) 
include('shared.lua')

function ENT:Initialize()

	self:SetModel( "models/props_phx/construct/metal_dome360.mdl" )
	self:SetModelScale(5.4,0)
	self:SetUseType( SIMPLE_USE )
	self:PhysicsInit( SOLID_VPHYSICS ) 
	self:SetMoveType( MOVETYPE_VPHYSICS ) 
	self:SetSolid( SOLID_VPHYSICS ) 
	self:SetCollisionGroup(COLLISION_GROUP_PASSABLE_DOOR)
	self:SetColor(Color(0, 255, 255, 1))
	self:SetRenderMode(RENDERMODE_TRANSCOLOR)
	-- self:SetMaterial("models/props_combine/tprings_globe")
	local phys = self:GetPhysicsObject()
	if (phys:IsValid()) then
		phys:Wake()
	end
	
end

function ENT:Use( ply, caller )
	
end

function ENT:OnTakeDamage(dmg)
	local pos = dmg:GetDamagePosition()

	ParticleEffect("st_elmos_fire_cp0", pos, Angle(0,0,0), self)
end

function ENT:Think()
	
end

function ENT:OnRemove()
	
end