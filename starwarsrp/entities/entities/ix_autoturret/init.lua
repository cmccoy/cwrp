AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
 
include('shared.lua')
 
local function printf(...)
        print(Format(...))
end

function ENT:Initialize()
       
        util.PrecacheSound("npc/turret_floor/shoot1.wav")
        util.PrecacheSound("npc/turret_floor/shoot2.wav")
        util.PrecacheSound("npc/turret_floor/shoot3.wav")
       
        self:SetModel("models/props/swautoturrets.mdl")

        self:PhysicsInit(SOLID_VPHYSICS)
        self:SetMoveType(MOVETYPE_VPHYSICS)
        self:SetSolid(SOLID_VPHYSICS)
		self:Activate()
		-- self:PrintBoneData()
       
        self:SetEngageDistance(3000)
        self:SetDamage(100)
        self:SetSpread(0.25)
        self:SetFireDelay(.1)
       
        self:SetTargetClasses({
			"npc_vj_rancor_medium",
			"npc_vj_rancor_huge"
        })
       
end

sound.Add( { name = "railgun_idle", channel = CHAN_AUTO, volume = 1.0, sound = "weapons/explosives_cannons_superlazers/wpn_atte_turret_fire.ogg" } )

local bullet = {}
bullet.Src = nil
bullet.Dir = nil
bullet.Spread = nil
bullet.Damage = nil
bullet.Num = 1
bullet.Force = 100
bullet.AmmoType = "rifle"
bullet.Attacker = nil
bullet.Hull = HULL_TINY
bullet.Tracer = 3
bullet.TracerName = "Tracer"
bullet.Callback = nil

function ENT:PrintBoneData()
        local boneCount = self:GetBoneCount()
       
        printf("Listing %d bones:", boneCount)
        for i=0, boneCount-1 do
                printf("\t%d. %s", i, self:GetBoneName(i))
        end
end

local revUp = false
local revSide = false

function ENT:GetBarrelPos()
	return self:GetBoneMatrix(2):GetTranslation()
end

local counter = 0

function ENT:Think()
	if !(ix.config.Get("autoTurrets")) then return end

	if not self.nextSearch or CurTime() >= self.nextSearch then
			self.target = self:FindTarget()
			self:SetNWEntity("target", self.target or Entity(-1))
			self.nextSearch = CurTime() + 0.3
	end
   
	local target = self:GetTarget()
	if target then
			if not self.nextShot or CurTime() >= self.nextShot then
			
				local barrelPos = self:GetBarrelPos()
				local targetPos = self:GetTargetPos(target)
			   
				self.nextShot = CurTime() + self.fireDelay
				
				bullet.Src = barrelPos
				bullet.Dir = (targetPos-barrelPos):GetNormalized()
				bullet.Damage = self.damage
				bullet.Spread = self.spread
				bullet.Attacker = self
				bullet.Tracer = 1
				bullet.TracerName  = "rw_sw_laser_blue"

				self:FireBullets(bullet)
				self:EmitSound("weapons/explosives_cannons_superlazers/wpn_atte_turret_fire.ogg")
				
			end
	end
   
	self:NextThink(CurTime()+self.fireDelay/2)

end