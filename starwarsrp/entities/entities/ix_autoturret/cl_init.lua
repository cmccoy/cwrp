include('shared.lua')

local revUp = false
local revSide = false

function ENT:Draw()

    self:DrawModel()
 
end


function ENT:GetBarrelPos()
        return self:GetBoneMatrix(2):GetTranslation()
end

local counter = 0

function ENT:Think()

	local target = self:GetTarget()
	
	if target then
		
		local targetPos = self:GetTargetPos(target)
                       
		local ang = self:GetAngles()
		local boneMatrix = self:GetBoneMatrix(1)
		
		if boneMatrix then
			local bonePos = boneMatrix:GetTranslation()
			local desiredAng = (targetPos-bonePos):GetNormalized():Angle()
			self:ManipulateBoneAngles(1, Angle(desiredAng.y + 90, 0, 0))
		end
		
		local boneMatrix = self:GetBoneMatrix(2)
		if boneMatrix then
			local bonePos = boneMatrix:GetTranslation()
			local desiredAng = (targetPos-bonePos):Angle()
			self:ManipulateBoneAngles(2, Angle(0, 0, -desiredAng.p-ang.p))
		end
		
		   
	else
		
		
		local curAng = self:GetManipulateBoneAngles(1)
		curAng.x = math.Round(curAng.x, 0)
		if curAng.x < 360 then
			self:ManipulateBoneAngles(1, Angle(curAng.x+1, 0, 0))
		elseif curAng.x > 360 then
			self:ManipulateBoneAngles(1, Angle(curAng.x-1, 0, 0))
		end
		
	
	end
       
end