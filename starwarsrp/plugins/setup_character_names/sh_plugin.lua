local PLUGIN = PLUGIN

PLUGIN.name = "Naming convention adherence"
PLUGIN.author = "Vac"
PLUGIN.description = "Makes sure player names adhere to their battalion"

function PLUGIN:OnCharacterCreated(client, character)
    if (character) then
        SetupName(character)
    end
end

ix.char.RegisterVar("name_prefix", {
    field = "name_prefix",
    fieldType = ix.type.string,
	default = "",
	isLocal = true,
    bNoDisplay = true
})

ix.char.RegisterVar("char_name", {
    field = "char_name",
    fieldType = ix.type.string,
	default = "",
	isLocal = true,
    bNoDisplay = true
})

ix.char.RegisterVar("name_postfix", {
    field = "name_postfix",
    fieldType = ix.type.string,
	default = "",
	isLocal = true,
    bNoDisplay = true
})

function PLUGIN:CanPlayerCreateCharacter(client, payload)
    local name = payload.name
    if (name:match("[^%a]")) then
        return false, "Your name can only contain letters"
    end
end

local noNumberFactions ={
    FACTION_JEDI,
    FACTION_CLONE,
    FACTION_COMBAT_DROID,
    FACTION_UTILITY_DROID,
    FACTION_RECRUIT
}

local function findAndReplace(toFind, STR)
    local start, stop = string.find(STR:lower(), toFind:lower())
    if (start) then
        local changed = string.sub(STR, 1, start -1) .. "" .. string.sub(STR, stop + 1)
        return changed
    end
end

--TODO refactor this beast
function SetupName(character)
    
    if(character) then
        local name = character:GetChar_name()
        local rank = character:GetRank()
        local faction = ix.faction.indices[character:GetFaction()]
        local id = character:GetID()
        local specialty = character:GetSpecialty()
        local name_prefix
        local newName

        -- Set the Players rank ABV to faction name for factions that have no ranks
        if(faction.index == FACTION_RECRUIT) then
            name_prefix = faction.name
        elseif(faction.index == FACTION_CLONE) then
            name_prefix = faction.name
        elseif(faction.index == FACTION_MEDICAL) then
            name_prefix = "Chief"
        elseif(faction.index == FACTION_PILOT) then
            name_prefix = "Chief"
        elseif(faction.index == FACTION_COMBAT_DROID) then
            name_prefix = "Combat"
        elseif(faction.index == FACTION_UTILITY_DROID) then
            name_prefix = "Utility"
        elseif(rank == nil) then
            name_prefix = "PVT"
        else
            name_prefix = rankToClass(rank, character:GetFaction()).currentRankAbv || "ERROR"
        end

        if(!table.HasValue(noNumberFactions, faction.index)) then
            name_prefix = (faction.name .. " " .. name_prefix)
        elseif(faction.index == FACTION_JEDI) then
            name_prefix = (faction.name .. " Initiate ")
        elseif(faction.index == FACTION_COMBAT_DROID || faction.index == FACTION_UTILITY_DROID) then
            name_prefix = (faction.name .. " Droid ")
        elseif(faction.index == FACTION_CLONE) then
            name_prefix = (faction.name .. " " .. id)
        else
            name_prefix = (faction.name .. " " .. id)
        end

        newName = name_prefix + " " + character:GetCharname()

        local query = mysql:Update("ix_characters")
            query:Where("id", id)
            query:Update("name", newName)
            query:Limit(1)
        query:Execute()

        character:SetName(newName)

        return newName
    end
end