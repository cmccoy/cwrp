
ENT.Type = "anim"
ENT.PrintName = "Broken Droid"
ENT.Category = "IG"
ENT.Spawnable = true
ENT.AdminOnly = true
ENT.bNoPersist = true

function ENT:SetupDataTables()
	self:NetworkVar("String", 0, "DisplayName")
	self:NetworkVar("String", 1, "Description")
end

function ENT:Initialize()
	if (SERVER) then
		self:SetModel("models/kryptonite/ig88/ig88.mdl")
		self:SetModelScale(1.25, 0)
		self:SetUseType(SIMPLE_USE)
		self:SetMoveType(MOVETYPE_NONE)
		self:DrawShadow(true)
		self:SetSolid(SOLID_BBOX)
		self:PhysicsInit(SOLID_BBOX)

		self:SetDisplayName("Broken Droid")
		self:SetDescription("This droid appears to be broken, perhaps it can be repaired?")

		self.receivers = {}

		local physObj = self:GetPhysicsObject()

		if (IsValid(physObj)) then
			physObj:EnableMotion(false)
			physObj:Sleep()
		end
	end

	timer.Simple(1, function()
		if (IsValid(self)) then
			self:SetAnim()
		end
	end)

end

function ENT:SetupParts()
	local t = {
		{ name = "Processor", mdl = "models/Items/combine_rifle_ammo01.mdl", uniqueID = "hk_droid_processor", installed = false }, 
		{ name = "Vocabulator", mdl = "models/Items/battery.mdl", uniqueID = "hk_vocabulator", installed = false }, 
		{ name = "Control Cluster", mdl = "models/props_lab/reciever01d.mdl", uniqueID = "hk_control_cluster", installed = false }, 
		{ name = "Chassis", mdl = "models/props_c17/TrapPropeller_Engine.mdl", uniqueID = "hk_chassis", installed = false }
	}

	self.parts = t;
end

function ENT:SetParts(t)
	self.parts = t
end

function ENT:GetParts()
	return self.parts
end

function ENT:SetAnim()
	for k, v in ipairs(self:GetSequenceList()) do
		if (v:lower():find("idle") and v != "idlenoise") then
			return self:ResetSequence(k)
		end
	end

	if (self:GetSequenceCount() > 1) then
		self:ResetSequence(4)
	end
end

if (SERVER) then
	util.AddNetworkString("ixOpenHkMenu")
	util.AddNetworkString("ixRepairHk47Part")
	local PLUGIN = PLUGIN

	function ENT:SpawnFunction(client, trace)
		local angles = (trace.HitPos - client:GetPos()):Angle()
		angles.r = 0
		angles.p = 0
		angles.y = angles.y + 180

		local entity = ents.Create("ix_hk47_broken")
		entity:SetPos(trace.HitPos)
		entity:SetAngles(angles)
		entity:SetupParts()
		entity:Spawn()

		PLUGIN:SaveData()

		return entity
	end

	function ENT:Use(activator)
		if (activator:IsPlayer()) then
			local character = activator:GetCharacter()
			if(character) then
				net.Start("ixOpenHkMenu")
					net.WriteTable(self.parts)
					net.WriteEntity(self)
				net.Send(activator)
			end
		end
	end

	net.Receive("ixRepairHk47Part", function(len, pl)
		local character = pl:GetCharacter()

		if	(character) then
			local inv = character:GetInventory()
			local partName = net.ReadString()
			local partUniqueId = net.ReadString()
			local droid = net.ReadEntity()
			--TODO refactor this to not use two tables
			if (inv:HasItem(partUniqueId)) then
				for k, v in pairs(droid:GetParts()) do
					if(table.HasValue(v, partUniqueId)) then
						if(v.installed == false) then
							v.installed = true

							for k, v in pairs(inv:GetItems()) do
								if(v.uniqueID == partUniqueId) then
									v:Remove()
								end
							end

							for k, v in pairs(player.GetAll()) do
								v:NotifyLocalized(character:GetName() .. " fixed the broken droid's " .. partName .. "!")
							end
						else
							pl:NotifyLocalized("Part is already installed!")
						end
					end
				end
			end
		end

	end)

else


	function ENT:Draw()
		self:DrawModel()
	end

	function ENT:Think()
		
	end

	function ENT:OnRemove()
		
	end

	net.Receive("ixOpenHkMenu", function(len, pl)
		local parts = net.ReadTable()
		local droid = net.ReadEntity()

		ix.gui.hk47_repiar = vgui.Create("ixHk47Menu")
		ix.gui.hk47_repiar:Setup(parts || {}, droid)
	end)

	ENT.PopulateEntityInfo = true

	function ENT:OnPopulateEntityInfo(container)
		local name = container:AddRow("name")
		name:SetImportant()
		name:SetText(self:GetDisplayName())
		name:SetBackgroundColor(Color(225 ,0 , 0, 255))
		name:SizeToContents()

		local descriptionText = self:GetDescription()

		if (descriptionText != "") then
			local description = container:AddRow("description")
			description:SetText(self:GetDescription())
			description:SizeToContents()
		end
	end
end
