
local PANEL = {}

function PANEL:Init()
	self:SetSize(256, 280)
	self:Center()
	self:MakePopup()
	self:SetTitle("Repair Menu")
	self.scroll = self:Add("DScrollPanel")
	self.scroll:Dock(FILL)
	self.scroll:DockPadding(0, 0, 0, 4)
	
end


function PANEL:Setup(t, droid)
	self.parts = t
	for k, v in pairs(self.parts) do

		if (v.installed != true) then

			local panel = self.scroll:Add("DPanel")
			panel:Dock(TOP)
			panel:SetTall(self:GetTall() * .2) 	
			panel:DockMargin(0, 0, 0, 5)

			local part = panel:Add("DButton")
			part:Dock(FILL)
			part:SetText(v.name)
			-- part:DockMargin(0, 0, 0, 4)
			part.DoClick = function()
				net.Start("ixRepairHk47Part")
					net.WriteString(v.name)
					net.WriteString(v.uniqueID)
					net.WriteEntity(droid)
				net.SendToServer()
			end

			local partHeight = part:GetTall()
			part:SetTextInset(partHeight + 4, 0)
			part:SetWide(part:GetWide() + partHeight + 4)


			partImage = part:Add("SpawnIcon")
			partImage.rotating = true
			partImage:Dock(LEFT)
			partImage:SetSize(part:GetTall() * 2, part:GetTall())
			partImage:SetModel(Model(v.mdl))
		
		end

	end

end

vgui.Register("ixHk47Menu", PANEL, "DFrame")
