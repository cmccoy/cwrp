
local PLUGIN = PLUGIN

ITEM.name = "HK Chassis"
ITEM.description = "Rusty, but a droid could use it."
ITEM.price = 2
ITEM.model = Model("models/props_c17/TrapPropeller_Engine.mdl")
ITEM.width = 2
ITEM.height = 3

function ITEM:GetDescription()
	return self.description
end