ix.command.Add("makePilot", {
    arguments = {ix.type.player},
    privilege = "pilot whitelist",
    description = "Allows the target to become a pilot",

    OnRun = function(self, client, target)
        local character = client:GetCharacter()
        local tcharacter = target:GetCharacter()

        if(character && tcharacter) then
            if (character:GetFaction() == FACTION_PILOT || client:IsStaff()) then
                tcharacter:SetData("pilot", true)
                client:NotifyLocalized("You have whitelisted " .. tcharacter:GetName() .. " to become a pilot!")
                target:NotifyLocalized(character:GetName() .. " has whitelisted you to become a pilot! Type /pilot to become one!")

                hook.Run("SwrpOnSetPilot", client, target)
            else
                client:NotifyLocalized("You can't make someone a pilot!")
            end
        end
    end
})

ix.command.Add("removePilot", {
    arguments = {ix.type.player},
    privilege = "Remove pilot whitelist",
    description = "Removes pilot from player",

    OnRun = function(self, client, target)
        local character = client:GetCharacter()
        local tcharacter = target:GetCharacter()

        if(character && tcharacter) then
            if (character:GetFaction() == FACTION_PILOT || client:IsStaff()) then
                tcharacter:SetData("pilot", false)
                tcharacter:SetSpecialty("none")
                client:NotifyLocalized("You have removed " .. tcharacter:GetName() .. " from pilot!")
                target:NotifyLocalized(character:GetName() .. " has removed you from pilot")

                hook.Run("SwrpOnRemovePilot", client, target)
            else
                client:NotifyLocalized("You can't remove someone's pilot status!")
            end
        end
    end
})

ix.command.Add("pilot", {
    arguments = {},
    privilege = "Sets your specialty to pilot",
    description = "Sets your specialty to pilot, if you change your specialty after this you'll have to be retrained!",

    OnRun = function(self, client)
        local character = client:GetCharacter()

        if (character) then
            if (character:GetData("pilot", false)) then
                character:SetSpecialty("pilot")
                character:SetData("pilot", false)
                client:NotifyLocalized("You have specialized as a pilot!")
            else
                client:NotifyLocalized("You have not passed pilot training!")
            end
        end
    end
})

ix.command.Add("writePilotReferral", {
    arguments = {ix.type.player},
    privilege = "Writes a pilot referral",
    description = "Writes a referral with the given troopers name stating that they can take pilot training",

    OnRun = function(self, client, target)
        local character = client:GetCharacter()
        local tcharacter = target:GetCharacter()
        if (character && tcharacter) then
            local faction = ix.faction.indices[character:GetFaction()]

            if (character:GetRank() == faction.commanderRank) then
                local inv = character:GetInventory()
                inv:Add("pilot_referral", 1, {
                    signer = character:GetName(),
                    trooper = tcharacter:GetName()
                })
            else
                client:NotifyLocalized("Only commanders can write pilot referrals!")
            end
        end
    end
})