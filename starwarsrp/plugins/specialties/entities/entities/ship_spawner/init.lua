AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")

include("shared.lua")

util.AddNetworkString( "shipspawnnpc" )
util.AddNetworkString( "closeddpanel" )
util.AddNetworkString( "spawnShip" )

function ENT:Initialize()
	
	self:SetModel("models/props/doorpanel1.mdl")
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	
	local phys = self:GetPhysicsObject()
	
	if phys:IsValid() then
	
		phys:Wake()
		
	end
	
	self:SetIsDOpen(false)
	self:SetOpenedby("")
	
	net.Receive( "closeddpanel", function(len, ply)	
		self:SetIsDOpen(false)
	end)
	
end

local function CanUseSSCP( ply )
	return (ply:IsGameMaster() || ply:IsStaff() || ply:GetCharacter() && (ply:GetCharacter():GetSpecialty() == "pilot" || ply:GetCharacter():GetFaction() == FACTION_PILOT))
end

local MainBaseSpawnTable = {
	["MP"] = {pos = Vector(-10309, 240, 244),   ang = Angle(0,270,0)},
	["AP1"] = {pos = Vector(-10641, 2354, 4595),   ang = Angle(0,270,0)},
	["AP2"] = {pos = Vector(-5187, 2409, 4488),   ang = Angle(0,270,0)}
}

local ships = {
	["arc170"] = {class = "arc170", maxaval = 3, pname="Arc-170's", cost = 2000},
	["laat"] = {class = "cgilaat", maxaval = 2, pname="LAAT/i's", cost = 1000}
}

net.Receive("spawnShip", function(len, pl)

	local ship = net.ReadString()
	local class = ships[ship].class
	local pos = net.ReadString()
	
	if not(CanUseSSCP(pl)) then pl:ChatPrint("[Ship Storage CP] Error, Incorrect Clearence Provided") return end
	
	local character = pl:GetCharacter()

	if (character:GetSpecialty() == "pilot" && !character:HasMoney(ships[ship].cost)) then
		pl:ChatPrint("[Ship Storage CP] You can't afford this!")
		return
	end

	if (#ents.FindByClass(class) >= ships[ship].maxaval) then
		pl:ChatPrint("[Ship Storage CP] Error, There are no more "..ships[ship].pname.." in storage.")
		return
	end
		
	local spawnPos = MainBaseSpawnTable[pos].pos
	local spawnAng = MainBaseSpawnTable[pos].ang
	
	--[[ Check if there are any ships at the pos ]]
	for k,v in pairs(ents.FindInSphere(spawnPos, 128)) do
		for l,b in pairs(ships) do
			if v:GetClass() == b.class then
				pl:ChatPrint("[Ship Storage CP] Error, No room for ship at "..pos)
				return
			end
		end
	end
	
	local ent = ents.Create(class)
	ent:SetPos( spawnPos )
	ent:SetAngles( spawnAng )
	ent:SetOwner(pl)

	ent:Spawn()
	ent:Activate()

	if (character:GetSpecialty() == "pilot") then
		character:TakeMoney(ships[ship].cost)
	end
	
	hook.Run("SwrpOnPilotSpawn", pl, ship)
end)

function ENT:Use(activator, caller)

	if caller:IsPlayer() and not(self:GetIsDOpen()) then
	
		self:SetIsDOpen(true)
		self:SetOpenedby(caller:Nick())
		net.Start("shipspawnnpc")
		net.Send(caller)
		
	elseif caller:IsPlayer() and self:GetIsDOpen() then
	
		if caller:Nick() ~= self:GetOpenedby() then
			if self:GetOpenedby() == "" then
				caller:ChatPrint("[Ship Storage CP] Error, Already being used.")
			else
				caller:ChatPrint("[Ship Storage CP] Error, being Used by: "..self:GetOpenedby())
			end
		end
		
	end

end