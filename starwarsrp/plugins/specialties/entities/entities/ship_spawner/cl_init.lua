include("shared.lua")

local function Draw3DText( pos, ang, scale, text, flipView )
	if ( flipView ) then
		-- Flip the angle 180 degrees around the UP axis
		ang:RotateAroundAxis( Vector( 0, 0, 1 ), 180 )
	end

	cam.Start3D2D( pos, ang, scale )
		-- Actually draw the text. Customize this to your liking.
		draw.DrawText( text, "DermaLarge", 0, 0, Color( 255, 255, 255, 150 ), TEXT_ALIGN_CENTER )
	cam.End3D2D()
end

local blur = Material("pp/blurscreen")
local function DrawBlur(panel, amount)
	local x, y = panel:LocalToScreen(0, 0)
	local scrW, scrH = ScrW(), ScrH()
	surface.SetDrawColor(255, 255, 255)
	surface.SetMaterial(blur)
	for i = 1, 3 do
		blur:SetFloat("$blur", (i / 3) * (amount or 6))
		blur:Recompute()
		render.UpdateScreenEffectTexture()
		surface.DrawTexturedRect(x * -1, y * -1, scrW, scrH)
	end
end

local MainBaseSpawns = {
	["MP"] 	= "Main Pad",
	["AP1"] = "Auxiliary Pad #1",
	["AP2"] = "Auxiliary Pad #2"
}


net.Receive( "shipspawnnpc", function()

	local function sendclosedpanel()
		net.Start("closeddpanel")
		net.SendToServer()
	end

	local mainPanel = vgui.Create( "DFrame" )
	mainPanel:SetPos( ScrW()/3, ScrH()/3 )
	mainPanel:SetSize( 500, 305 )
	mainPanel:SetTitle( "" )
	mainPanel:SetVisible( true )
	mainPanel:SetDraggable( false )
	mainPanel:ShowCloseButton( false )
	mainPanel:MakePopup()
	mainPanel.Paint = function( self, w, h )
		DrawBlur(self, 2)
		draw.RoundedBox( 5, 0, 0, w, h, Color( 0, 0, 0, 200 ) )
	end
	
	local function choosepos(ship)
	
	
		local function sendclosepanel()
			net.Start("closeddpanel")
			net.SendToServer()
		end
		
		local posBox = vgui.Create( "DFrame", mainPanel )
		posBox:SetPos( 10 , 25 )
		posBox:SetSize( 170, 300 )
		posBox:SetTitle( "" )
		posBox:SetVisible( true )
		posBox:SetDraggable( false )
		posBox:ShowCloseButton( false )
		posBox.Paint = function( self, w, h )
			draw.RoundedBox( 5, 0, 0, w, h, Color( 0, 0, 0, 0 ) )
		end
		
		local choosetext = vgui.Create( "DLabel", posBox )
		choosetext:SetPos( 10, 10 )
		choosetext:SetFont("Trebuchet18")
		choosetext:SetText("Select Location for Ship.")
		choosetext:SizeToContents()
		
		local PosSelect = vgui.Create( "DScrollPanel", posBox )
		PosSelect:SetPos( 0, 40 )
		PosSelect:SetSize( 170, 280)
		for k,v in pairs(MainBaseSpawns) do
			local PosButton = PosSelect:Add( "DButton" )
			PosButton:SetText( v )
			PosButton:Dock( TOP )
			PosButton:DockMargin( 0, 0, 0, 5 )
			PosButton.DoClick = function()
				sendclosepanel()
				mainPanel:Close()
				net.Start("spawnShip")
				net.WriteString(ship)
				net.WriteString(k)
				net.SendToServer()
			end
		end
		
	end
	
	local titletext = vgui.Create( "DLabel", mainPanel )
	titletext:SetPos( 170, 10 )
	titletext:SetFont("Trebuchet24")
	titletext:SetText("Ship Storage CP")
	titletext:SizeToContents()
	
	local arc170 = vgui.Create( "DButton", mainPanel )
	arc170:SetText( "Spawn Arc 170 - $2000" )	
	arc170:SetPos( 225, 40 )		
	arc170:SetSize( 250, 30 )	
	arc170.DoClick = function()
		choosepos("arc170")
	end
	arc170.Paint = function( self, w, h )
		draw.RoundedBox( 5, 0, 0, w, h, Color( 64, 64, 64, 200 ) )
	end
	
	local laat = vgui.Create( "DButton", mainPanel )
	laat:SetText( "Spawn LAAT - $1000" )	
	laat:SetPos( 225, 90 )		
	laat:SetSize( 250, 30 )	
	laat.DoClick = function()
		choosepos("laat")
	end
	laat.Paint = function( self, w, h )
		draw.RoundedBox( 5, 0, 0, w, h, Color( 64, 64, 64, 200 ) )
	end
	
	local goodbye = vgui.Create( "DButton", mainPanel )
	goodbye:SetText( "Nothing, Goodbye." )	
	goodbye:SetPos( 225, 140 )		
	goodbye:SetSize( 250, 30 )	
	goodbye.DoClick = function()
		sendclosedpanel()
		mainPanel:Close()
	end
	goodbye.Paint = function( self, w, h )
		draw.RoundedBox( 5, 0, 0, w, h, Color( 64, 64, 64, 200 ) )
	end
	
	-- local clearships = vgui.Create( "DButton", mainPanel )
	-- clearships:SetText( "Remove your ship" )	
	-- clearships:SetPos( 225, 190 )		
	-- clearships:SetSize( 250, 30 )	
	-- clearships.DoClick = function()
	-- 	net.Start("remShips")
	-- 	net.SendToServer()
	-- 	sendclosedpanel()
	-- 	mainPanel:Close()
	-- end
	-- clearships.Paint = function( self, w, h )
	-- 	draw.RoundedBox( 5, 0, 0, w, h, Color( 64, 64, 64, 200 ) )
	-- end

end)

function ENT:Draw()

	self:DrawModel()
	
	if LocalPlayer():GetPos():Distance( self:GetPos() ) > 600 then end

	local text = "Ship Storage CP" -- The text to display

	local mins, maxs = self:GetModelBounds()
	local pos = self:GetPos() + Vector( 10, 0, 10 )

	local direction = self:GetPos() - LocalPlayer():GetPos()
	local x_d = direction.x
	local y_d = direction.y
	local ang = Angle(0, math.deg(math.atan(y_d/x_d))+90/(x_d/-math.abs(x_d)), 90)


	Draw3DText( pos, ang, 0.2, text, false )
	
end