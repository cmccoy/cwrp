
ITEM.name = "Medical Training Referral"
ITEM.model = Model("models/props_lab/clipboard.mdl")
ITEM.description = "A referral for %s to take medic training, signed by %s"
ITEM.category = "Referrals"

function ITEM:GetDescription()
	return string.format(self.description, self:GetData("trooper", "ERROR"), self:GetData("signer", "ERROR"))
end

ITEM.functions.Destroy = {
	OnRun = function(itemTable)
		return true
	end,
}
