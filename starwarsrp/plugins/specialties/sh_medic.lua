ix.command.Add("makeMedic", {
    arguments = {ix.type.player},
    privilege = "Medic whitelist",
    description = "Allows the target to become a medic",

    OnRun = function(self, client, target)
        local character = client:GetCharacter()
        local tcharacter = target:GetCharacter()

        if(character && tcharacter) then
            if (character:GetFaction() == FACTION_MEDICAL || client:IsStaff()) then
                tcharacter:SetData("medic", true)
                client:NotifyLocalized("You have whitelisted " .. tcharacter:GetName() .. " to become a medic!")
                target:NotifyLocalized(character:GetName() .. " has whitelisted you to become a medic! Type /medic to become one!")

                hook.Run("SwrpOnSetMedic", client, target)
            else
                client:NotifyLocalized("You can't make someone a medic!")
            end
        end
    end
})

ix.command.Add("removeMedic", {
    arguments = {ix.type.player},
    privilege = "Remove Medic whitelist",
    description = "Removes medic from player",

    OnRun = function(self, client, target)
        local character = client:GetCharacter()
        local tcharacter = target:GetCharacter()

        if(character && tcharacter) then
            if (character:GetFaction() == FACTION_MEDICAL || client:IsStaff()) then
                tcharacter:SetData("medic", false)
                tcharacter:SetSpecialty("none")
                client:NotifyLocalized("You have removed " .. tcharacter:GetName() .. " from medic!")
                target:NotifyLocalized(character:GetName() .. " has removed you from medic")

                target:StripMedicWeps()

                hook.Run("SwrpOnRemoveMedic", client, target)
            else
                client:NotifyLocalized("You can't remove someone's medic status!")
            end
        end
    end
})

ix.command.Add("medic", {
    arguments = {},
    privilege = "Sets your specialty to medic",
    description = "Sets your specialty to medic, if you change your specialty after this you'll have to be retrained!",

    OnRun = function(self, client)
        local character = client:GetCharacter()

        if (character) then
            if (character:GetData("medic")) then
                character:SetSpecialty("medic")
                character:SetData("medic", false)
                client:NotifyLocalized("You have specialized as a medic!")
                client:GiveMedicWeps()
            else
                client:NotifyLocalized("You have not passed medical training!")
            end
        end
    end
})

ix.command.Add("writeMedicReferral", {
    arguments = {ix.type.player},
    privilege = "Writes a medic referral",
    description = "Writes a referral with the given troopers name stating that they can take medical training",

    OnRun = function(self, client, target)
        local character = client:GetCharacter()
        local tcharacter = target:GetCharacter()
        if (character && tcharacter) then
            local faction = ix.faction.indices[character:GetFaction()]

            if (character:GetRank() == faction.commanderRank) then
                local inv = character:GetInventory()
                inv:Add("medic_referral", 1, {
                    signer = character:GetName(),
                    trooper = tcharacter:GetName()
                })
            else
                client:NotifyLocalized("Only commanders can write medical referrals!")
            end
        end
    end
})

if (CLIENT) then
    local textColor = Color(250, 25, 25)
    local shadowColor = Color(14, 14, 14)
    
    hook.Add("PostDrawTranslucentRenderables", "[Impact] Show dead bodies to medics", function()
        if(!LocalPlayer():Alive()) then return end
        if(!IsValid(LocalPlayer():GetActiveWeapon())) then return end
        if(!(LocalPlayer():GetActiveWeapon():GetClass() == "weapon_defib")) then return end
        
        for k, v in ipairs(ents.FindByClass("prop_ragdoll")) do
            if(v:GetNWBool("defib_show", false)) then
                local angle = EyeAngles()
                angle:RotateAroundAxis(angle:Forward(), 90)
                angle:RotateAroundAxis(angle:Right(), 90)

                local text = "REVIVE"

                cam.Start3D2D((v:GetPos() + Vector(0,0,80)), Angle(0, angle.y, 90), 0.05)
                    surface.SetFont("ixTypingIndicator")

                    local _, textHeight = surface.GetTextSize(text)

                    draw.SimpleTextOutlined(text, "ixTypingIndicator", 0,
                        -textHeight * 0.5,
                        ColorAlpha(textColor, 255),
                        TEXT_ALIGN_CENTER,
                        TEXT_ALIGN_CENTER, 4,
                        ColorAlpha(shadowColor, 255)
                    )
                cam.End3D2D()
            end
        end    
    end)
end