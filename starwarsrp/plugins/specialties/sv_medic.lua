local PLUGIN = PLUGIN

function PLUGIN:PostPlayerLoadout(client)
    local character = client:GetCharacter()
    if (character) then
        if(character:GetSpecialty() == "medic") then
            client:GiveMedicWeps()
            if(client:HasWeapon("ix_hands")) then
                client:StripWeapon("ix_hands")
            end
        end
    end
end