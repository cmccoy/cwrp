local playersToDraw = {}
local playersNextDistanceCheck = {}
local playersPurgeCheck = {}
local nextPurgeDelay = 30
local nextPurge = CurTime() + nextPurgeDelay
local curtime = CurTime()


local maxDrawDistance = math.pow(500, 2) --Square distance for use with DistanceToSquare to avoid square roots monkaS
local textColor = Color(250, 50, 50)
local shadowColor = Color(66, 66, 66)

local function checkStillOnline() --Cleaning the tables out if not online
	for pid, checkTime in pairs(playersPurgeCheck) do
		if checkTime >= CurTime() then continue end

		if not IsValid( Player(pid) ) then 
			playersToDraw[pid] = nil
			playersNextDistanceCheck[pid] = nil
			playersPurgeCheck[pid] = nil
		else
			playersPurgeCheck[pid] = curtime + nextPurgeDelay
		end
	end
end
timer.Create("ArrestWarrant_RenderTablePurge", nextPurgeDelay, 0, checkStillOnline )

local function shouldRenderOnPlayer(ply)
	local dist = ply:GetPos():DistToSqr(LocalPlayer():GetPos())
	local moveType = ply:GetMoveType()
	
	if dist > maxDrawDistance or (moveType != MOVETYPE_WALK and moveType != MOVETYPE_NONE) then
		return false
	end
	return true
end

function PLUGIN:PostPlayerDraw(ply)
	local pid = ply:UserID()
	
	playersPurgeCheck[pid] = playersPurgeCheck[pid] or curtime + nextPurgeDelay

	if playersPurgeCheck[pid] < curtime then 
		playersPurgeCheck[pid] = curtime + nextPurgeDelay
	end

	playersNextDistanceCheck[pid] = playersNextDistanceCheck[pid] or 0
	local cache = playersNextDistanceCheck[pid] 

	if cache < curtime then
		playersNextDistanceCheck[pid] = curtime + .5 --Delay before we check distance again
		playersToDraw[pid] = shouldRenderOnPlayer(ply)
	end

	if playersToDraw[pid] then
        if (!ply:GetNWBool("arrested") && !ply:GetNWBool("wanted")) then return end

        local angle = EyeAngles()
        angle:RotateAroundAxis(angle:Forward(), 90)
        angle:RotateAroundAxis(angle:Right(), 90)

        local text = "Wanted"
        
        if ply:GetNWBool("arrested") then
            text = "Arrested"
        end

        cam.Start3D2D((ply:GetPos() + Vector(0,0,80)), Angle(0, angle.y, 90), 0.05)
            surface.SetFont("ixTypingIndicator")

            local _, textHeight = surface.GetTextSize(text)

            draw.SimpleTextOutlined(text, "ixTypingIndicator", 0,
                -textHeight * 0.5,
                ColorAlpha(textColor, 255),
                TEXT_ALIGN_CENTER,
                TEXT_ALIGN_CENTER, 4,
                ColorAlpha(shadowColor, 255)
            )
        cam.End3D2D()
	end
end

local arrestTimer = 0

net.Receive("ixUpdateClientArrestTimer", function(len, pl)

	arrestTimer = net.ReadInt(32)

	if (arrestTimer <= 0) then
		if (timer.Exists("ixLocalArrestTimer")) then
			timer.Remove("ixLocalArrestTimer")
		end
		return
	end

	if (timer.Exists("ixLocalArrestTimer")) then
		timer.Remove("ixLocalArrestTimer")
		timer.Create("ixLocalArrestTimer", 1, arrestTimer, function()
			arrestTimer = arrestTimer - 1
		end)
	else
		timer.Create("ixLocalArrestTimer", 1, arrestTimer, function()
			arrestTimer = arrestTimer - 1
		end)
	end

end)

hook.Add("HUDPaint", "[Impact] Draw arrest on screen timer", function()
	if (arrestTimer <= 0) then return end

	surface.SetFont( "ix3D2DSmallFont" )
	surface.SetTextColor( 255, 255, 255 )
	surface.SetTextPos( 5, ScrH() * .2 ) 
	surface.DrawText( "Arrest Timer: " .. arrestTimer .. " seconds left." )
end)