AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
include('shared.lua')

util.AddNetworkString("Bailout_npc")

function ENT:Initialize()

	self:SetModel("models/galactic/supnpc/shopdroid/shopdroid.mdl")

	self:SetHullSizeNormal()
 
	self:SetSolid( SOLID_BBOX ) 
	self:SetMoveType( MOVETYPE_STEP )

	self:SetUseType( SIMPLE_USE )

end


function ENT:AcceptInput(name,activator,caller)
	if !activator:IsPlayer() then return end
	if name == "Use" and caller:IsPlayer() then
		
		local tbl = {}
		for k, v in ipairs(player.GetAll()) do
			local character = v:GetCharacter()
			if (character && character:IsArrested()) then
				table.insert(tbl, {player = v, cost = character:GetBailCost(), sev = character:GetArrestSeverity()})
			end
		end

		net.Start("Bailout_npc")
			net.WriteTable(tbl)
		net.Send(activator)

	end
end

net.Receive("Bailout_npc", function(len, pl)

	local toBail = net.ReadEntity()

	local character = pl:GetCharacter()
	local tcharacter = toBail:GetCharacter()

	if (character && tcharacter && tcharacter:IsArrested()) then
		local cost = tcharacter:GetBailCost()
		if (character:HasMoney(cost)) then
			character:TakeMoney(cost)
			tcharacter:Unarrest()

			net.Start("ixBailMessage")
				net.WriteString(character:GetName())
				net.WriteString(tcharacter:GetName())
			net.Broadcast()

			hook.Run("SwrpOnBail", pl, toBail, cost)
		end
	end

end)