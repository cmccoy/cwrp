include('shared.lua')
ENT.Category			= "IG"
ENT.Spawnable			= true
ENT.AdminOnly			= true
ENT.AdminSpawnable		= true
ENT.RenderGroup 		= RENDERGROUP_BOTH

function ENT:OnPopulateEntityInfo(container)
	local name = container:AddRow("name")
	name:SetImportant()
	name:SetText("SD-420 Bailouts")
	name:SetBackgroundColor(Color(255, 100, 100, 255))
	name:SizeToContents()

	local description = container:AddRow("description")
	description:SetText("Allows you to bail out arrested people")
	description:SizeToContents()
end

net.Receive("Bailout_npc", function()
	local frame = vgui.Create("DFrame")
		frame:ShowCloseButton(true)
		frame:SetTitle("Bailout")
		frame:SetSize(425, 600)
		frame:Center()
		frame:MakePopup()

	local scroll = vgui.Create("DScrollPanel", frame)
		scroll:Dock(FILL)

	local character = LocalPlayer():GetCharacter()

	local arrested = net.ReadTable()

	for k, v in pairs(arrested) do
		local pan = vgui.Create("DPanel")
		pan:SetTall(90)
		pan:Dock(TOP)
		scroll:AddItem(pan)

		local mdl = pan:Add("DModelPanel")
		mdl:SetModel(v.player:GetModel())
		mdl:Dock(LEFT)

		function mdl:LayoutEntity(ent)
			ent:SetSequence(ent:LookupSequence("idle_all_01"))
			mdl:RunAnimation()		
			return
		end

		local eyepos = mdl.Entity:GetBonePosition(mdl.Entity:LookupBone("ValveBiped.Bip01_Head1"))

		eyepos:Add(Vector(10, 0, 2))	-- Move up slightly

		mdl:SetLookAt(eyepos)

		mdl:SetCamPos(eyepos-Vector(-12, 0, 0))	-- Move cam in front of eyes

		mdl.Entity:SetEyeTarget(eyepos-Vector(-12, 0, 0))

		local name = pan:Add("DLabel")
		name:SetText(v.player:GetName())
		name:SetFont("ix3D2DSmallFont")
		name:DockMargin(10,10,0,0)
		name:Dock(TOP)

		local sev = pan:Add("DLabel")
		sev:SetText("Severity: " .. v.sev)
		sev:SetFont("ix3D2DSmallFont")
		sev:DockMargin(10,10,0,0)
		sev:Dock(TOP)

		local cost = pan:Add("DLabel")
		cost:SetText("Cost: $" .. v.cost)
		cost:SetFont("ix3D2DSmallFont")
		cost:DockMargin(10,10,0,0)
		cost:Dock(TOP)


		local view = pan:Add("DButton")
		view:SetText("Bailout Player")
		view:SetFont("ix3D2DSmallFont")
		view:DockMargin(0,-70,10,30)
		view:Dock(RIGHT)
		view:SizeToContents()
		view.DoClick = function(self)
			net.Start("Bailout_npc")
				net.WriteEntity(v.player)
			net.SendToServer()
			frame:Close()
		end
	end

end)