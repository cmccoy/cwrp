AddCSLuaFile()
if SERVER then
    AddCSLuaFile("cl_init.lua")
end

/* 
---------------------------------------------------------------------------------------------------------------------------------------------
				Downloading content from workshop
---------------------------------------------------------------------------------------------------------------------------------------------
*/	

if SERVER then resource.AddWorkshop("615887479"); end

/* 
---------------------------------------------------------------------------------------------------------------------------------------------
				Config
---------------------------------------------------------------------------------------------------------------------------------------------
*/

SWEP.hitRequireForStun = 1;
SWEP.stunTime = 4;
SWEP.primaryFireDamage = 10;

SWEP.primaryFireDelay = 0.5;
SWEP.secondaryFireDelay = 2;
/* 
---------------------------------------------------------------------------------------------------------------------------------------------
				Default SWEP config
---------------------------------------------------------------------------------------------------------------------------------------------
*/

SWEP.reloadCooldown = CurTime();
SWEP.DrawAmmo = false
SWEP.DrawCrosshair = false
SWEP.Author = "Vac"
SWEP.Contact = ""
SWEP.Instructions = "Left Click: Stun | Right Click: Arrest"
SWEP.IconLetter = ""
SWEP.PrintName = "Beating Stick"
SWEP.ViewModelFOV = 62
SWEP.ViewModelFlip = false
SWEP.AnimPrefix = "physgun"
SWEP.HoldType ="physgun"
SWEP.Spawnable = true
SWEP.AdminOnly = true
SWEP.Category = "IG" 


SWEP.UseHands = true;
SWEP.ViewModel = Model("models/drover/baton.mdl");
SWEP.WorldModel = Model("models/drover/w_baton.mdl");

local SwingSound = Sound( "WeaponFrag.Throw" );
local HitSound = Sound( "Flesh.ImpactHard" );



SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = 0
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = ""

SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = ""



/* 
---------------------------------------------------------------------------------------------------------------------------------------------
				Initialize
---------------------------------------------------------------------------------------------------------------------------------------------
*/

function SWEP:Initialize()
    self:SetHoldType("melee");
end



function SWEP:SetupShield()

end

/* 
---------------------------------------------------------------------------------------------------------------------------------------------
				Hook Can Drop Weapon
---------------------------------------------------------------------------------------------------------------------------------------------
*/

hook.Add("canDropWeapon", "NoDropPoliceBaton",function(ply, ent)
	if IsValid(ent) and ent:GetClass() == "weapon_policebaton" then
		return false;
	end
end)

/* 
---------------------------------------------------------------------------------------------------------------------------------------------
				Reload
---------------------------------------------------------------------------------------------------------------------------------------------
*/

function SWEP:Reload()
	if self.reloadCooldown + 2 > CurTime() then return end;
	self.reloadCooldown = CurTime();
	if CLIENT then return end
	self.Weapon:SendWeaponAnim( ACT_VM_RELOAD);
end	


function SWEP:Draw()
	self:SendWeaponAnim(ACT_VM_DRAW);
	self.Weapon:SendWeaponAnim(ACT_VM_DRAW);
end	


/* 
---------------------------------------------------------------------------------------------------------------------------------------------
				Baton Stun / Unstun
---------------------------------------------------------------------------------------------------------------------------------------------
*/
function SWEP:Stun(ply)
	local ang = ply:GetAngles();
	ply:SetEyeAngles(Angle(60,ang.y,ang.r));
	ply:Freeze(true);
	ply.stunnedBaton = true;
	ply:SetNWInt('batonstuntime',CurTime());
	net.Start("batonstunanim") net.WriteEntity(ply) net.WriteBool(true) net.Broadcast();
	timer.Create("unstunbatonstun"..tostring(ply:EntIndex()),self.stunTime,1,function()
		if IsValid(ply) then 
			ply:Freeze(false);
			ply.stunnedBaton = false;
			net.Start("batonstunanim") net.WriteEntity(ply) net.WriteBool(false) net.Broadcast();
		end
	end)
end


/* 
---------------------------------------------------------------------------------------------------------------------------------------------
				Attack Player
---------------------------------------------------------------------------------------------------------------------------------------------
*/

function SWEP:StunPlayer(ply)
	if not IsValid(ply) or not ply:IsPlayer() then return end;
	if(ply:GetCharacter()) then
		if(ply:GetCharacter():GetFaction() == FACTION_CG) then return end
	end
	self.Owner:EmitSound(Sound("Flesh.ImpactHard"));
	ply:SetVelocity((ply:GetPos() - self:GetOwner():GetPos()) * 2);
	if ply.stunnedBaton == true then return end;
	local hits = ply.hitByBaton or 0;
	local lTime = ply.lastBatonHit or CurTime();
	if CurTime() > lTime + 3 then 
		hits = 0; 
	end
	local numb = 1;
	ply.hitByBaton = hits + numb;
	ply.lastBatonHit = CurTime();
	if hits + numb >= self.hitRequireForStun then
		self:Stun(ply);
	end
end

function SWEP:ArrestPlayer(ply)
	if not IsValid(ply) or not ply:IsPlayer() then return end;
	if(ply:GetCharacter()) then
		if(ply:GetCharacter():GetFaction() == FACTION_CG) then return end

		self.Owner:EmitSound(Sound("Flesh.ImpactHard"));
		if (ply:GetCharacter():IsArrested()) then
			self:Stun(ply);
			if (ix.config.arrest.jailPos) then
				ply:SetPos(table.Random(ix.config.arrest.jailPos))
			else
				ply:Freeze(true)
			end
			self.Owner:NotifyLocalized(ply:GetName() .. " has been returned to jail!")
		elseif(ply:GetCharacter():GetWanted()) then
			ix.log.Add(self.Owner, "arrest", ply:GetName())
			
			ply:GetCharacter():Arrest()

			net.Start("ixArrestReason")
				net.WriteEntity(ply)
			net.Send(self.Owner)

			hook.Run("SwrpOnArrest", self.Owner, ply)
		else
			self:Stun(ply);
			self.Owner:NotifyLocalized(ply:GetName() .. " is not AOS!")
		end
	end
end

/* 
---------------------------------------------------------------------------------------------------------------------------------------------
				Attack Entity
---------------------------------------------------------------------------------------------------------------------------------------------
*/

function SWEP:AttackEnt(ent,dmg)
	self.Owner:EmitSound(Sound("physics/wood/wood_box_impact_hard3.wav"));
	ent:TakeDamage(dmg, self:GetOwner(), self);
end
/* 
---------------------------------------------------------------------------------------------------------------------------------------------
				Make Hit
---------------------------------------------------------------------------------------------------------------------------------------------
*/
local entMeta = FindMetaTable("Entity");
function SWEP:MakeHit(dmg)
	self:GetOwner():SetAnimation(PLAYER_ATTACK1);
    if CLIENT then return end;
    local trace = util.QuickTrace(self:GetOwner():EyePos(), self:GetOwner():GetAimVector() * 90, {self:GetOwner()});
    
    if IsValid(trace.Entity) and trace.Entity:GetClass() == "func_breakable_surf" then
        trace.Entity:Fire("Shatter"); 
        return;
    end

	local ent = self:GetOwner():getEyeSightHitEntity(100, 15);
    if not IsValid(ent) then return end;
    if ent:IsPlayer() and not ent:Alive() then return end;

    

    if ent:IsPlayer() then		
		if dmg > 0 then
			self:ArrestPlayer(ent);
		else
			self:StunPlayer(ent)
		end
    elseif !ent:IsNPC() or !ent:IsVehicle() then
        self:AttackEnt(ent,dmg);
    end
end

/* 
---------------------------------------------------------------------------------------------------------------------------------------------
				Primary attack
---------------------------------------------------------------------------------------------------------------------------------------------
*/

function SWEP:PrimaryAttack()
	self:SetHoldType("melee");
	self:SetNextPrimaryFire(CurTime() + self.primaryFireDelay);
	self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK );
	self:GetOwner():EmitSound(SwingSound);
	self:MakeHit(0);
	
end


/* 
---------------------------------------------------------------------------------------------------------------------------------------------
				Secondary attack / Deploy static shield
---------------------------------------------------------------------------------------------------------------------------------------------
*/
function SWEP:SecondaryAttack()
	self:SetNextSecondaryFire(CurTime() + self.secondaryFireDelay);
	self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK );
	self:GetOwner():EmitSound(SwingSound);
	self:MakeHit(self.primaryFireDamage)
end

/* 
---------------------------------------------------------------------------------------------------------------------------------------------
				Network initialize && Client Receive
---------------------------------------------------------------------------------------------------------------------------------------------
*/

if SERVER then
	util.AddNetworkString("batonstunanim");
	util.AddNetworkString("batonsendfunc");
	util.AddNetworkString("batondrawbut");
	
	
	net.Receive("batonsendfunc",function(leng,ply)
		local id = net.ReadInt(3);
		local enemy = net.ReadEntity();
		if not IsValid(enemy) or not enemy:IsPlayer() or not enemy:Alive() then return end;
		if ply:GetActiveWeapon():GetClass() != "weapon_policebaton" then return end;
		ply:GetActiveWeapon().menuButtons[id].func(ply,enemy);
	end)
	
	net.Receive("batondrawbut",function(leng,ply)
		local enemy = net.ReadEntity();
		if IsValid(ply:GetActiveWeapon():GetClass() != "weapon_policebaton") then return end;
		ply:GetActiveWeapon():Draw();
	end)
end

if CLIENT then
	net.Receive("batonstunanim",function()
		local ply = net.ReadEntity();
		local enable = net.ReadBool();
		if IsValid(ply) and ply:IsPlayer() and ply:Alive() then
			if enable then
				ply:AnimRestartGesture( GESTURE_SLOT_CUSTOM,ACT_HL2MP_IDLE_SLAM, false);   
			else
				ply:AnimResetGestureSlot(GESTURE_SLOT_CUSTOM );
			end
		end	
	end)	
end


/* 
---------------------------------------------------------------------------------------------------------------------------------------------
				Deploy && Holster && Drop && Remove
---------------------------------------------------------------------------------------------------------------------------------------------
*/




function SWEP:Deploy()
	return true
end

function SWEP:Holster()
	return true;
end


function SWEP:OnDrop()
	return true;
end

function SWEP:OnRemove()
	return true;
end


/* 
---------------------------------------------------------------------------------------------------------------------------------------------
				Draw circles
---------------------------------------------------------------------------------------------------------------------------------------------
*/

