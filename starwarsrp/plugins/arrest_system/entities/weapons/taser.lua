SWEP.PrintName = "Taser"
SWEP.Author = ""
SWEP.Purpose = ""
SWEP.Instructions = "Left click to tase the player."
SWEP.Contact = ""

SWEP.Slot = 3
SWEP.SlotPos = 1
SWEP.Weight = 2
SWEP.AutoSwitchTo = true
SWEP.AutoSwitchFrom = true

SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Category = "IG"

SWEP.ViewModel			= "models/weapons/v_pistol.mdl"
SWEP.WorldModel			= "models/weapons/w_pistol.mdl"

SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= 1
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= ""

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= true
SWEP.Secondary.Ammo			= ""
SWEP.TaserCooldown = 20
SWEP.TaserFireSound = "weapons/stunstick/spark1.wav"
SWEP.WepIconBounceRate = 6
SWEP.WepIconBounceOffset = 4

if ( SERVER ) then

	function SWEP:PrimaryAttack( )
		self.nextTase = self.nextTase or CurTime( )
		if ( self.nextTase > CurTime( ) ) then
            local taseDelay = string.NiceTime( math.Round( self.nextTase - CurTime( ) ) )
            self.Owner:NotifyLocalized("Your taser is recharging .." .. taseDelay .. " left!")
			return
		end
		local range = 225
		self.Owner:LagCompensation( true )
		local traceRes = self.Owner:RangeEyeTrace( range, { self } )
		self.Owner:LagCompensation( false )
		if ( !IsValid( traceRes.Entity ) ) then return end

		if ( !traceRes.Entity:IsPlayer( ) ) then return end
		local traceEnt = traceRes.Entity
		if(traceEnt:GetCharacter()) then
			if(traceEnt:GetCharacter():GetFaction() == FACTION_CG || traceEnt:GetCharacter():GetFaction() == FACTION_JEDI || traceEnt:GetMoveType() == MOVETYPE_NOCLIP || traceEnt:Health() > 500) then
				self.Owner:NotifyLocalized("You cannot taze that!")
				return
			end
		end

		ix.log.Add(self.Owner, "taser", traceEnt:GetName())
		self.nextTase = CurTime( ) + self.TaserCooldown
		self.Owner:EmitSound( self.TaserFireSound )
		traceEnt:ParalyzePlayer( 10, self.Owner )
	end

end

function SWEP:Initialize( )
	util.PrecacheSound( self.TaserFireSound )
	self:SetHoldType( "pistol" )
end