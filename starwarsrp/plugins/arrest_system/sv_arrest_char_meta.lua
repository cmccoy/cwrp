util.AddNetworkString("ixUpdateClientArrestTimer")

local CHAR = ix.meta.character

local mapSpawns = {Vector(-5724, 14381, -196)}

for k, v in pairs(ents.GetAll()) do
    if (v:GetClass() == "info_player_start") then
        table.insert(mapSpawns, v:GetPos())
    end
end

function CHAR:Arrest()
    if (self:GetPlayer() && self:GetPlayer():InVehicle()) then
        self:GetPlayer():ExitVehicle()
    end
    
    self:SetData("wanted", false)
    self:GetPlayer():SetNWBool("wanted", false)

    self:SetData("arrested", true)
    self:GetPlayer():SetNWBool("arrested", true)
    
    local arrestTime = 600
    if(self:GetData("arrest_time") == 0) then
        self:SetData("arrest_time", 600)
    else
        arrestTime = self:GetData("arrest_time", 600)
    end

    net.Start("ixUpdateClientArrestTimer")
        net.WriteInt(tonumber(arrestTime), 32)
    net.Send(self:GetPlayer())

    self:GetPlayer():StripWeapons()
    if (ix.config.arrest.jailPos) then
        self:GetPlayer():SetPos(table.Random(ix.config.arrest.jailPos))
    else
        self:GetPlayer():Freeze(true)
    end
    self:GetPlayer():NotifyLocalized("You've been arrested for " .. arrestTime .. " seconds!")
    self:GetPlayer():SetWalkSpeed(ix.config.Get("walkSpeed") / 2)
    self:GetPlayer():SetRunSpeed(ix.config.Get("runSpeed") / 2)
    timer.Create(self:GetName() .. self:GetPlayer():SteamID64() .. "ArrestTimer", arrestTime, 1, function()
        if (self:GetPlayer()) then
            self:Unarrest()
        end
    end)
end

function CHAR:UpdateArrest(arrester, time, bail, severity, reason, comment)
    self:SetArrestTime(time)
    self:SetBailCost(bail)
    self:SetArrestSeverity(severity)

    self:GetPlayer():NotifyLocalized("Your arrest time has been updated to " .. time .. " seconds!")

    timer.Remove(self:GetName() .. self:GetPlayer():SteamID64() .. "ArrestTimer")

    timer.Create(self:GetName() .. self:GetPlayer():SteamID64() .. "ArrestTimer", time, 1, function()
        if (self:GetPlayer()) then
            self:Unarrest()
        end
    end)

    net.Start("ixUpdateClientArrestTimer")
        net.WriteInt(tonumber(time), 32)
    net.Send(self:GetPlayer())

    local query = mysql:Insert("ix_arrests")
        query:Insert("clone_id", self:GetID())
        query:Insert("name", self:GetName())
        query:Insert("arrester", arrester:GetName())
        query:Insert("arresterSteamID", arrester:GetPlayer():SteamID())
        query:Insert("severity", reason)
        query:Insert("comment", comment || "No Comment")
        query:Insert("bail", bail)
        query:Insert("duration", math.Round(time / 60, 0))
        query:Insert("date", os.time())
    query:Execute()
end

function CHAR:SetArrestTime(jailTime)
    self:SetData("arrest_time", jailTime)
end

function CHAR:GetArrestTime()
    return self:GetData("arrest_time")
end

function CHAR:SetBailCost(bailCost)
    self:SetData("bail_cost", bailCost)
end

function CHAR:GetBailCost()
    return self:GetData("bail_cost")
end

function CHAR:SetArrestSeverity(severity)
    self:SetData("arrest_severity", severity)
end

function CHAR:GetArrestSeverity()
    return self:GetData("arrest_severity")
end

function CHAR:SetWanted()
    self:SetData("wanted", true)
    self:GetPlayer():SetNWBool("wanted", true)

    for k, v in pairs(player.GetAll()) do
        v:NotifyLocalized(self:GetName() .. " is AOS!")
    end
end

function CHAR:SetUnwanted()
    self:SetData("wanted", false)
    self:GetPlayer():SetNWBool("wanted", false)

    for k, v in pairs(player.GetAll()) do
        v:NotifyLocalized(self:GetName() .. " is no longer AOS!")
    end
end

function CHAR:GetWanted()
    return self:GetData("wanted") || false
end

function CHAR:GetSpawnPos()
    if (ix.plugin.Get("spawns").spawns and !table.IsEmpty(ix.plugin.Get("spawns").spawns) and self) then
		local class = self:GetClass()
		local points
		local className = "default"

		for k, v in ipairs(ix.faction.indices) do
			if (k == self:GetFaction()) then
				points = ix.plugin.Get("spawns").spawns[v.uniqueID] or {}

				break
			end
		end

		if (points) then
			for _, v in ipairs(ix.class.list) do
				if (class == v.index) then
					className = v.uniqueID

					break
				end
			end

			points = points[className] or points["default"]

			if (points and !table.IsEmpty(points)) then
				local position = table.Random(points)

				return position
            end
        end
    end
    return table.Random(mapSpawns)
end

function CHAR:Unarrest()
    if (self:IsArrested()) then
        self:SetData("arrested", false)
        self:SetData("arrest_time", 0)
        self:SetData("arrest_severity", nil)
        self:GetPlayer():SetNWBool("arrested", false)
        self:SetData("wanted", false)
        self:GetPlayer():SetNWBool("wanted", false)
        self:GetPlayer():SetWalkSpeed(ix.config.Get("walkSpeed"))
        self:GetPlayer():SetRunSpeed(ix.config.Get("runSpeed"))
        self:GetPlayer():NotifyLocalized("You've been unarrested.")
        self:GetPlayer():SetPos(self:GetSpawnPos())
        self:EquipNewLoadout(self:GetPlayer(), nil, self:GetLoadout())
        self:GetPlayer():Give("tfa_swch_dc15a")
        if (self:GetSpecialty() == "Medic") then
            self:GetPlayer():GiveMedicWeps()
        end
        self:GetPlayer():Freeze(false)
        net.Start("ixUpdateClientArrestTimer")
            net.WriteInt(0, 32)
        net.Send(self:GetPlayer())
        hook.Run("SwrpOnUnarrest", self)
    end
end

function CHAR:IsArrested()
    return self:GetData("arrested")
end