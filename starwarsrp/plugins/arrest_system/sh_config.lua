ix.config.arrest = {}

ix.config.arrest.lowTime = 300
ix.config.arrest.mediumTime = 600
ix.config.arrest.highTime = 900
ix.config.arrest.severeTime = 1800

if (SERVER) then
    if(game.GetMap() == "rp_anaxes_impact") then
        ix.config.arrest.jailPos = {
            Vector(-6983, 14994, 1028),
            Vector(-6989, 14388, 1028),
            Vector(-7374, 14994, 1028),
            Vector(-7365, 14399, 1028),
            Vector(-7764, 15012, 1028),
            Vector(-7766, 14400, 1028),
            Vector(-8143, 14994, 1028),
            Vector(-8152, 14424, 1029),
            Vector(-8523, 14715, 1028)
        }
    end
end