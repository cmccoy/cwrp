
local PANEL = {}

surface.CreateFont("ixArrestFormSmall", {
	font = font,
	size = 10,
	weight = 1000
})

surface.CreateFont("ixArrestFormMedium", {
	font = font,
	size = 15,
	weight = 1000
})

surface.CreateFont("ixArrestFormBig", {
	font = font,
	size = 25,
	weight = 1000
})

local subLabel = "Add warning and arrests to a player. This will be stored for 1 month and can be viewed by \ncommanders and all CG."

local trainingCitation = Color(255, 255, 255)
local lowCitation = Color(45, 130, 186)
local mediumCiation = Color(34, 166, 92)
local highCitation = Color(224, 183, 11)
local severeCitiation = Color(195, 107, 31)

local reasons = {
	{severity = "training", text = "SEV 0 | Training Citation", color = trainingCitation},
	{severity = "low", text = "SEV 1 | Weapon Pointed", color = lowCitation},
	{severity = "low", text = "SEV 1 | Jumping on Rails", color = lowCitation},
	{severity = "low", text = "SEV 1 | Breaking PTS", color = lowCitation},
	{severity = "medium", text = "SEV 2 | Mic / Chat spam", color = mediumCitation},
	{severity = "medium", text = "SEV 2 | Bunny Hopping", color = mediumCitation},
	{severity = "medium", text = "SEV 2 | Abuse of Force Powers (Minor)", color = mediumCitation},
	{severity = "high", text = "SEV 3 | Minging/FailRP", color = highCitation},
	{severity = "high", text = "SEV 3 | Repeat Offender", color = highCitation},
	{severity = "high", text = "SEV 3 | Illegal Training", color = highCitation},
	{severity = "high", text = "SEV 3 | AOS Area Violation", color = highCitation},
	{severity = "high", text = "SEV 3 | Refusing Orders", color = highCitation},
	{severity = "high", text = "SEV 3 | Minuse of Communications", color = highCitation},
	{severity = "high", text = "SEV 3 | DB Violation", color = highCitation},
	{severity = "severe", text = "SEV 4 | Abuse of Force Powers", color = severeCitation},
	{severity = "severe", text = "SEV 4 | Breaking Brotherhood Code", color = severeCitation},
	{severity = "severe", text = "SEV 4 | Disrespect to Battalion or Superior", color = severeCitation},
	{severity = "severe", text = "SEV 4 | Partaking or Instigating a Riot", color = severeCitation},
}


function PANEL:Init()
	self:SetSize(600, 400)
	self:Center()
	self:MakePopup()
	self:ShowCloseButton(false)
	self:SetTitle("Citation Menu")
end

function PANEL:Setup(player)

	local arrestLabel = self:Add("DLabel")
	arrestLabel:SetText("Issue Arrest")
	arrestLabel:SetFont("ixArrestFormBig")
	arrestLabel:SizeToContents()
	arrestLabel:Dock(TOP)

	local arrestSubLabel = self:Add("DLabel")
	arrestSubLabel:SetText(subLabel)
	arrestSubLabel:SetFont("ixArrestFormMedium")
	arrestSubLabel:SizeToContents()
	arrestSubLabel:Dock(TOP)
	arrestSubLabel:DockMargin(0, 5, 0, 20)

	local reasonLabel = self:Add("DLabel")
	reasonLabel:SetText("Reason")
	reasonLabel:SetFont("ixArrestFormBig")
	reasonLabel:SizeToContents()
	reasonLabel:Dock(TOP)

	local reasonSubLabel = self:Add("DLabel")
	reasonSubLabel:SetText("A predefined reason for arresting. You can also add a comment to help other CG.")
	reasonSubLabel:SetFont("ixArrestFormMedium")
	reasonSubLabel:SizeToContents()
	reasonSubLabel:Dock(TOP)

	local reasonSelection = self:Add("DComboBox")
	reasonSelection:SetFont("ixArrestFormMedium")
	reasonSelection:SizeToContents()
	reasonSelection:Dock(TOP)

	for k, v in ipairs(reasons) do
		reasonSelection:AddChoice(v.text, "", false, "icon16/error.png")
	end
	reasonSelection.Paint = function( self, w, h )
		surface.SetDrawColor( Color( 255, 255, 255, 255 ) )
		surface.DrawOutlinedRect(0, 0, w, h)
	end
	
	local commentLabel = self:Add("DLabel")
	commentLabel:SetText("Comment")
	commentLabel:SetFont("ixArrestFormMedium")
	commentLabel:SizeToContents()
	commentLabel:Dock(TOP)
	commentLabel:DockMargin(0, 5, 0, 0)

	local commentValue = self:Add("DTextEntry")
	commentValue:SetFont("ixArrestFormMedium")
	commentValue:SizeToContents()
	commentValue:Dock(TOP)

	local jailLabel = self:Add("DLabel")
	jailLabel:SetText("Jail Time")
	jailLabel:SetFont("ixArrestFormBig")
	jailLabel:SizeToContents()
	jailLabel:Dock(TOP)
	jailLabel:DockMargin(0, 20, 0, 0)

	local jailSubLabel = self:Add("DLabel")
	jailSubLabel:SetText("In minutes. Time should reflect their crime. Unrealistic times will get you removed")
	jailSubLabel:SetFont("ixArrestFormMedium")
	jailSubLabel:SizeToContents()
	jailSubLabel:Dock(TOP)

	local jailTimeValue = self:Add("DTextEntry")
	jailTimeValue:SetFont("ixArrestFormMedium")
	jailTimeValue:SizeToContents()
	jailTimeValue:Dock(TOP)
	jailTimeValue:DockMargin(0, 5, 400, 0)

	local bailLabel = self:Add("DLabel")
	bailLabel:SetText("Bail Amount")
	bailLabel:SetFont("ixArrestFormMedium")
	bailLabel:SizeToContents()
	bailLabel:Dock(TOP)

	local bailValue = self:Add("DTextEntry")
	bailValue:SetFont("ixArrestFormMedium")
	bailValue:SizeToContents()
	bailValue:Dock(TOP)
	bailValue:DockMargin(0, 5, 400, 0)

	local submitButton = self:Add("DButton")
	submitButton:SizeToContents()
	submitButton:SetText("Submit")
	submitButton:SetTextColor(Color(255,255,255))
	submitButton:Dock(FILL)

	submitButton:DockMargin(200, 20, 200, 0)

	submitButton.Paint = function( self, w, h )
		surface.SetDrawColor( Color( 40, 200, 40) )
		surface.DrawRect(0, 0, w, h)
	end

	submitButton.DoClick = function()
		if(reasonSelection:GetSelected() && commentValue:GetValue() && jailTimeValue:GetValue() != "" && bailValue:GetValue() != "") then
			net.Start("ixArrestReason")
				net.WriteString(reasonSelection:GetSelected())
				net.WriteString(commentValue:GetValue() || "No Comment")
				net.WriteString((jailTimeValue:GetValue()) * 60)
				net.WriteString(bailValue:GetValue())
				net.WriteEntity(player)
			net.SendToServer()
			self:Remove()
		else
			LocalPlayer():NotifyLocalized("You are missing a value!")
		end
	end
	
end

vgui.Register("ixCitationMenu", PANEL, "DFrame")

net.Receive("ixArrestReason", function(len, pl)

	local player = net.ReadEntity()
	if (player) then
		ix.gui.openArrestMenu = vgui.Create("ixCitationMenu")
		ix.gui.openArrestMenu:Setup(player)
	end

end)