
local PANEL = {}

surface.CreateFont("ixArrestFormMedium", {
	font = font,
	size = 15,
	weight = 1000
})

local subLabel = "Add warning and arrests to a player. This will be stored for 1 month and can be viewed by \ncommanders and all CG."


function PANEL:Init()
	self:SetSize(600, 400)
	self:Center()
	self:MakePopup()
	self:SetTitle("Previous Citations")
end

function PANEL:Setup(arrests)

	local scroll = self:Add("DScrollPanel")
		scroll:Dock(FILL)

	for k, v in pairs(arrests) do
		local pan = vgui.Create("DPanel")
		pan:SetTall(90)
		pan:Dock(TOP)
		scroll:AddItem(pan)

		local name = pan:Add("DLabel")
		name:SetText(v.name)
		name:SetFont("ixArrestFormMedium")
		name:SizeToContents()
		name:DockMargin(10,10,0,0)
		name:Dock(LEFT)

		local arrester = pan:Add("DLabel")
		arrester:SetText(v.arrester .. "\n" .. v.arresterSteamID)
		arrester:SetFont("ixArrestFormMedium")
		arrester:SizeToContents()
		arrester:DockMargin(10,10,0,0)
		arrester:Dock(LEFT)

		local sev = pan:Add("DLabel")
		sev:SetText("Severity: " .. v.severity .. "\n" .. v.comment || "No Comment")
		sev:SetFont("ixArrestFormMedium")
		sev:SizeToContents()
		sev:DockMargin(10,10,0,0)
		sev:Dock(LEFT)

		local cost = pan:Add("DLabel")
		cost:SetText("Bail: $" .. v.bail)
		cost:SetFont("ixArrestFormMedium")
		cost:SizeToContents()
		cost:DockMargin(10,10,0,0)
		cost:Dock(LEFT)

		local duration = pan:Add("DLabel")
		duration:SetText(v.duration .. " minutes\n" .. (string.NiceTime(os.time() - v.date) .. " ago"))
		duration:SetFont("ixArrestFormMedium")
		duration:SizeToContents()
		duration:DockMargin(10,10,0,0)
		duration:Dock(LEFT)


	end
	
end

vgui.Register("ixPreviousArrests", PANEL, "DFrame")

net.Receive("ixArrestLookup", function(len, pl)

	local previousArrests = net.ReadTable()

	ix.gui.openArrestMenu = vgui.Create("ixPreviousArrests")
	ix.gui.openArrestMenu:Setup(previousArrests)

end)