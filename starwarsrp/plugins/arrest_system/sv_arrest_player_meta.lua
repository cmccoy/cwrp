local PLAYER = FindMetaTable("Player")

function PLAYER:RangeEyeTrace( range, filter )
	local filterTable = filter or { }
	table.insert( filterTable, self )
	local traceData = { }
	traceData.start = self:GetShootPos( )
	traceData.endpos = self:GetShootPos( ) + self:EyeAngles( ):Forward( ) * range
	traceData.filter = filterTable
	local traceRes = util.TraceLine( traceData )
	return traceRes
end

function PLAYER:ParalyzePlayer( length, attacker )
	self.tasedBody = ents.Create( "prop_ragdoll" )
	self.tasedBody:SetModel( self:GetModel( ) )
	self.tasedBody:SetKeyValue( "origin", self:GetPos( ).x .. " " .. self:GetPos( ).y .. " " .. self:GetPos( ).z )
	self.tasedBody:SetAngles( self:GetAngles( ) )
	if ( self:Team( ) == TEAM_CRAB ) then
		self.tasedBody.playerModelScale = self:GetModelScale( )
	end
	self:StripWeapons( )
	self:DrawViewModel( false )
	self:DrawWorldModel( false )
	self:Spectate( OBS_MODE_CHASE )
	self:SpectateEntity( self.tasedBody )
	self.isTasered = true
	self.tasedBody:Spawn( )
	self.tasedBody:Activate( )
	self.tasedBody:GetPhysicsObject( ):SetVelocity( 4 * self:GetVelocity( ) )
	if ( string.find( self:GetModel( ), "group01/female" ) and self.femaleClothingMaterial and string.len( self.femaleClothingMaterial ) > 0 ) then
		self.tasedBody:SetSubMaterial( self:GetClothingIndex( ), self.femaleClothingMaterial )
	elseif ( string.find( self:GetModel( ), "group01/male" ) and self.maleClothingMaterial and string.len( self.maleClothingMaterial ) > 0 ) then
		self.tasedBody:SetSubMaterial( self:GetClothingIndex( ), self.maleClothingMaterial )
	end
	local tasedBodyIndex = self.tasedBody:EntIndex( )
	local tasedBodyColor = self:GetPlayerColor( )
	self.tasedBody.GetPlayerColor = function( )
		return tasedBodyColor
	end
	-- I hate using SendLua or BroadcastLua, but it seemed like a waste of lines to write a net message for this.
	timer.Simple( 1, function( )
		BroadcastLua( "Entity( " .. tasedBodyIndex.. " ).GetPlayerColor = function( ) return Vector( " .. tasedBodyColor.x ..", " .. tasedBodyColor.y .. ", " .. tasedBodyColor.z .. " ) end" )
	end )
	local entIndex = self:EntIndex( )
	local tasedBody = self.tasedBody
	timer.Create( entIndex .. ":ParalysisRecover", length, 1, function( )
		local standPos = standPos or tasedBody:GetPos( )
		local modelScale = nil
		if ( IsValid( tasedBody ) and tasedBody.playerModelScale ) then
			modelScale = tasedBody.playerModelScale
		end
		if not ( IsValid( self ) ) then
			SafeRemoveEntity( tasedBody )
			return
		end
		self:UnSpectate( )
		self:Spectate( OBS_MODE_NONE )
		self:DrawViewModel( true )
		self:DrawWorldModel( true )
		local oldHealth = self:Health( )
		local oldArmor = self:Armor( )
		self:Spawn( )
		self:SetPos(standPos)
		self:SetHealth( oldHealth )
		self:SetArmor( oldArmor )
		SafeRemoveEntity( tasedBody )
		self.isTasered = false
		
		self:SetRunSpeed( ix.config.Get("runSpeed") / 2 )
		self:SetWalkSpeed( ix.config.Get("walkSpeed") / 2 )
		self:SetColor(Color(100, 0, 255))
		timer.Simple(5, function()
			self:SetColor(Color(255, 255, 255))
			if (self:GetCharacter():IsArrested()) then return end
			self:SetWalkSpeed(ix.config.Get("walkSpeed"))
			self:SetRunSpeed(ix.config.Get("runSpeed"))
		end)
	end )
end

function PLAYER:getEyeSightHitEntity(searchDistance, hitDistance, filter)
    searchDistance = searchDistance or 100
    hitDistance = (hitDistance or 15) * (hitDistance or 15)

    filter = filter or function(p) return p:IsPlayer() and p ~= self end

    self:LagCompensation(true)

    local shootPos = self:GetShootPos()
    local entities = ents.FindInSphere(shootPos, searchDistance)
    local aimvec = self:GetAimVector()

    local smallestDistance = math.huge
    local foundEnt

    for _, ent in pairs(entities) do
        if not IsValid(ent) or filter(ent) == false then continue end

        local center = ent:GetPos()

        -- project the center vector on the aim vector
        local projected = shootPos + (center - shootPos):Dot(aimvec) * aimvec

        if aimvec:Dot((projected - shootPos):GetNormalized()) < 0 then continue end

        -- the point on the model that has the smallest distance to your line of sight
        local nearestPoint = ent:NearestPoint(projected)
        local distance = nearestPoint:DistToSqr(projected)

        if distance < smallestDistance then
            local trace = {
                start = self:GetShootPos(),
                endpos = nearestPoint,
                filter = {self, ent}
            }
            local traceLine = util.TraceLine(trace)
            if traceLine.Hit then continue end

            smallestDistance = distance
            foundEnt = ent
        end
    end

    self:LagCompensation(false)

    if smallestDistance < hitDistance then
        return foundEnt, math.sqrt(smallestDistance)
    end

    return nil
end