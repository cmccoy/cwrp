local PLUGIN = PLUGIN

PLUGIN.name = "Arrest System"
PLUGIN.author = "Vac"
PLUGIN.description = "Sets up a system to allow arresting of players"

ix.util.Include("cl_draw_arrest.lua")
ix.util.Include("cl_net_rec.lua")
ix.util.Include("sh_config.lua")
ix.util.Include("sh_handcuffs.lua")
ix.util.Include("sv_arrest.lua")
ix.util.Include("sv_handcuffs.lua")
ix.util.Include("sv_arrest_char_meta.lua")
ix.util.Include("sv_arrest_player_meta.lua")

ix.command.Add("aos", {
	description = "Makes a clone AOS.",
	arguments = {ix.type.character},
    OnRun = function(self, client, target)
        local character = client:GetCharacter()

        if (character && target) then
            if (client:IsStaff() || character:GetFaction() == FACTION_CG) then
                target:SetWanted()
            end
        end
	end
})

ix.command.Add("unaos", {
	description = "Makes a clone no longer AOS.",
	arguments = {ix.type.character},
    OnRun = function(self, client, target)
        local character = client:GetCharacter()

        if (character && target) then
            if (client:IsStaff() || character:GetFaction() == FACTION_CG) then
                target:SetUnwanted()
            end
        end
	end
})

ix.command.Add("location", {
	description = "Prints the current area the target is in.",
	arguments = {ix.type.character},
    OnRun = function(self, client, target)
        local character = client:GetCharacter()

        if (character && target) then
            if (client:IsStaff() || character:GetFaction() == FACTION_CG) then
                client:ChatPrint("[LOCATION] " .. target:GetName() .. " is in " .. target:GetPlayer():GetArea())
            end
        end
	end
})

ix.command.Add("citations", {
	description = "Looks up a clone's prior citations.",
	arguments = {ix.type.character},
    OnRun = function(self, client, target)
        local character = client:GetCharacter()

        if (character && target) then
            local class = rankToClass(character:GetRank(), character:GetFaction())

            if (client:IsStaff() || character:GetFaction() == FACTION_CG || class && class.canPromote) then
                local query = mysql:Select("ix_arrests")
                    query:Where("clone_id", target:GetID())
                    query:Callback(function(result)
                        if (istable(result) and #result > 0) then
                            net.Start("ixArrestLookup")
                                net.WriteTable(result)
                            net.Send(client)
                        else
                            client:NotifyLocalized("Player has no past citations!")
                        end
                    end)
                query:Execute()
            end
        end
	end
})

ix.command.Add("unjail", {
	description = "Unjails a clone",
	arguments = {ix.type.character},
    OnRun = function(self, client, target)
        local character = client:GetCharacter()

        if (character && target) then
            if (client:IsStaff()) then
                target:Unarrest()
            end
        end
	end
})