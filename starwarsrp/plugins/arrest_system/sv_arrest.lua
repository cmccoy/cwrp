local PLUGIN = PLUGIN

util.AddNetworkString("ixArrestReason")
util.AddNetworkString("ixGuardMessage")
util.AddNetworkString("ixBailMessage")
util.AddNetworkString("ixArrestLookup")

function PLUGIN:PostPlayerLoadout(client)
    local character = client:GetCharacter()
    if (character) then
        if (character:IsArrested()) then
            if (timer.Exists(character:GetName() .. client:SteamID64() .. "ArrestTimer")) then
                if (ix.config.arrest.jailPos) then
                    client:SetPos(table.Random(ix.config.arrest.jailPos))
                else
                    client:Freeze(true)
                end
                client:SetWalkSpeed(ix.config.Get("walkSpeed") / 2)
                client:SetRunSpeed(ix.config.Get("runSpeed") / 2)
                client:StripWeapons()
            else
                character:Arrest()
            end
        elseif(client:GetNWBool("arrested", false)) then
            client:NotifyLocalized("Did you really think I didn't think of that?")
            timer.Simple(1, function()
                client:StripWeapons()
            end)
            character:Arrest()
        end
    end
end

function PLUGIN:OnCharacterDisconnect(client, character)
    if character:IsArrested() then
        if (timer.Exists(character:GetName() .. client:SteamID64() .. "ArrestTimer")) then
            timer.Remove(character:GetName() .. client:SteamID64() .. "ArrestTimer")
        end
    end
end

function PLUGIN:PreCharacterDeleted(client, character)
    if(character:IsArrested()) then
        client:NotifyLocalized("You can not delete this chracter as they are arrested.")
        return false
    end
end

function PLUGIN:PreCharacterDeleted(client, character)
    if(character:IsArrested()) then
        client:NotifyLocalized("You can not delete this chracter as they are arrested.")
        return false
    end
end

net.Receive("ixArrestReason", function(len, pl)

    local character = pl:GetCharacter()

    if (character && character:GetFaction() == FACTION_CG || pl:IsStaff()) then
        local reason = net.ReadString() || "No Reason"
        local comment = net.ReadString() || "No Comment"
        local jailTime = tonumber(net.ReadString()) || 600
        local bail = tonumber(net.ReadString()) || 5000
        local player = net.ReadEntity()

        local severity = string.sub(reason, 1, 5)

        if (player) then
            local tcharacter = player:GetCharacter()
            tcharacter:UpdateArrest(character, jailTime, bail, severity, reason, comment)

            net.Start("ixGuardMessage")
                net.WriteString(tcharacter:GetName())
                net.WriteString(character:GetName())
                net.WriteString(reason)
                net.WriteInt(bail, 20)
            net.Broadcast()

        end

    end

end)