local White = Color(255, 255, 255)
local Red = Color( 213, 68, 51 )
local DarkRed = Color(138, 10, 10)
local Orange =  Color(217, 119, 26)

net.Receive("ixGuardMessage", function(len, pl)
    local arrested = net.ReadString()
    local arrester = net.ReadString()
    local reason = net.ReadString()
    local bail = net.ReadInt(20)
    local name = "[GUARDS] "
    chat.AddText(Red, name, Color( 0, 240, 240 ), arrested, White," has been arrested by ", DarkRed, arrester, White, " for ", Orange, reason, White,". Bail set at: " .. bail)
end)

net.Receive("ixBailMessage", function(len, pl)
    local bailer = net.ReadString()
    local arrested = net.ReadString()
    local name = "[GUARDS] "
    chat.AddText(Red, name, Color( 0, 240, 240 ), bailer, White, " bailed out ", DarkRed, arrested)
end)