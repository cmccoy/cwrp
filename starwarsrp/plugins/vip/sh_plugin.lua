local PLUGIN = PLUGIN

PLUGIN.name = "VIP module"
PLUGIN.author = "Vac"
PLUGIN.description = "Sets up things for VIPs"

ix.util.Include("sh_vip_meta.lua")

function PLUGIN:GetMaxPlayerCharacter(client)
    print(client:IsGameMaster())
    if(client:IsStaff() || client:IsGameMaster()) then
        return 5
    elseif(client:IsVip()) then
        return 3
    end
    return 1
end