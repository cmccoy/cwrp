local pl = FindMetaTable("Player")

function pl:IsVip()
    return self:GetUserGroup() == "vip"
end