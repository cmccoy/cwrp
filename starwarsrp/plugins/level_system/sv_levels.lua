--[[------------------------------------------------------------------------------
 *  Copyright (C) Fluffy(76561197976769128 - STEAM_0:0:8251700) - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
--]]------------------------------------------------------------------------------

local CHAR = FindMetaTable("Player")
local SQL = Sublime.GetSQL();
util.AddNetworkString("Sublime.ExperienceNotification");

function CHAR:SL_AddExperience(amount, source, notify, shouldMultiply)
    local amount = (amount or 50);
    local character = self:GetCharacter()

    if (not isnumber(tonumber(amount))) then
        Sublime.Print("Argument 'Amount' in function SL_AddExperience needs to be a number.");

        return false;
    end

    shouldMultiply = shouldMultiply == nil and true or shouldMultiply;
    
    --if (player.GetCount() < 4) then
        --return false;
    --end

    local max = Sublime.Settings.Get("other", "max_level", "number");
    if (self:SL_GetLevel() >= max) then
        return;
    end

    local sData = Sublime.GetSkill("experienced_player");
    if (sData and sData.Enabled) then
        local points = character:GetData(sData.Identifier, 0);

        if (points > 0 and shouldMultiply) then
            local modifier = 1 + ((points * sData.AmountPerPoint) / 100);

            amount = math.Round(amount * modifier);
        end
    end

    -- VIP bonus.
    local vModifier = Sublime.Settings.Get("other", "vip_modifier", "number");

    if ((self:IsVip() || self:IsStaff()) && shouldMultiply) then
        amount = amount * vModifier;
    end

    local new       = character:GetData("sl_experience", 0) + amount;
    local needed    = self:SL_GetNeededExperience();

    character:SetTotalxp(character:GetTotalxp() + amount)

    if (new >= needed) then
        self:SL_LevelUp();
    else
        character:SetData("sl_experience", new);
        character:SetXp(new)
    end
    
    if (shouldMultiply) then
        Sublime.Query(SQL:FormatSQL("UPDATE Sublime_Data SET ExperienceGained = ExperienceGained + '%s'", amount));
    end

    if (not source or source == "") then
        source = "from an unknown source.";
    end

    if (notify == nil) then
        notify = true;
    end

    if (notify) then
        net.Start("Sublime.ExperienceNotification")
            net.WriteString(source);
            net.WriteUInt(amount, 32);
        net.Send(self);
    end

    local left = new - needed;
    if (left > 0) then
        self:SL_AddExperience(left, source, false, false);
    end

    hook.Run("SL.PlayerReceivedExperience", self, amount);

    return true;
end

function CHAR:SL_LevelUp(amount)
    local amount    = amount or 1;
    local character = self:GetCharacter()
    local new       = character:GetData("sl_level", 1) + amount;

    character:SetData("experience", 0);
    character:SetData("sl_level", new);
    character:SetData("sl_experience", 0);
    character:SetData("neededexperience", self:SL_GetNeededExperience())

    character:SetLevel(new)
    character:SetXp(0)

    self:SL_SetInteger("ability_points", character:GetData("ability_points", 0))

    Sublime.Query("UPDATE Sublime_Data SET LevelsGained = LevelsGained + " .. amount);

    hook.Run("SL.PlayerLeveledUp", self, new, amount)
end

---
--- SL_SetLevel
---
--- A function to hard set a level instead of leveling up.
--- added due to customer demand.
---
function CHAR:SL_SetLevel(level, give_points)
    if (not level or not isnumber(level)) then
        Sublime.Print("The argument 'level' is either nil or not a number. Its type is '%s'", type(level));

        return false;
    end

    if (level <= 0) then
        Sublime.Print("Level can not be 0 or below.");

        return false;
    end

    local character = self:GetCharacter()

    character:SetData("experience", character:GetData("experience", 0));
    character:SetData("sl_level", level);
    character:SetData("sl_experience", character:GetData("sl_experience", 0));

    character:SetLevel(level)
    character:SetXp(character:GetXp())

    -- Should the player receive ability points after we set his level?
    -- This is sometimes useful, but I would not recommend it.
    -- If you set the players level to 10 then he will receive 10 ability points regardless of his level.
    if (give_points) then
        hook.Run("SL.PlayerLeveledUp", self, level, level)
    end
end

function CHAR:SL_AddSkillPoint(amount)
    local amount = (amount or 1);
    local character = self:GetCharacter()

    if (not isnumber(tonumber(amount))) then
        Sublime.Print("Argument 'Amount' in function SL_AddSkillPoint needs to be a number.");

        return false;
    end

    local new = character:GetData("ability_points", 0) + amount;
    character:GetData("ability_points", new);

    Sublime.Print("%s has now %i skill points to use.", self:Nick(), new);
    hook.Run("SL.PlayerReceivedSkillPoint", self, amount, new);
end