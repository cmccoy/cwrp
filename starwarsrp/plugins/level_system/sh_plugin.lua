local PLUGIN = PLUGIN

PLUGIN.name = "Level System - Helix"
PLUGIN.author = "Vac"
PLUGIN.desc = "Level System - Helix"


ix.util.Include("sh_levels.lua")
ix.util.Include("sv_levels.lua")
ix.util.Include("sv_resets.lua")

ix.char.RegisterVar("level", {
    field = "level",
    fieldType = ix.type.number,
	default = 1,
	isLocal = true,
    bNoDisplay = true
})

ix.char.RegisterVar("xp", {
    field = "xp",
    fieldType = ix.type.number,
	default = 0,
	isLocal = true,
    bNoDisplay = true
})

ix.char.RegisterVar("totalxp", {
    field = "totalxp",
    fieldType = ix.type.number,
	default = 0,
	isLocal = true,
    bNoDisplay = true
})

ix.char.RegisterVar("neededxp", {
    field = "neededxp",
    fieldType = ix.type.number,
	default = 0,
	isLocal = true,
    bNoDisplay = true
})