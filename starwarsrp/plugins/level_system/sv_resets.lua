local PLUGIN = PLUGIN

function PLUGIN:OnCharacterCreated(client, character)

    if (character) then

        local query = mysql:Update("ix_characters")
            query:Where("id", character:GetID())
            query:Update("level", 1)
            query:Update("xp", 0)
            query:Update("totalxp", 0)
            query:Update("neededxp", 1571)
        query:Execute()
    end

end

function PLUGIN:PlayerLoadedCharacter(client, character, currentChar)

    if(currentChar) then
        local oldData = currentChar:GetData("skill_data", {})
        if (oldData) then
            for k, v in pairs(oldData) do
                client:SL_SetInteger(k, 0);
            end
        end
    end

    client:SL_SetLevel(character:GetLevel())
    client:SL_SetInteger("ability_points", character:GetData("ability_points", 0))

    local data = character:GetData("skill_data", {})

    if (data) then
        for k, v in pairs(data) do
            if v != 0 then
                client:SL_SetInteger(k, v);
            end
        end
    end

end