local CLIENT = FindMetaTable("Player")

function CLIENT:SL_GetNeededExperience()
    local character = self:GetCharacter()
    if (character) then
        return math.Round((character:GetData("sl_level", 1) * Sublime.Config.BaseExperience) * Sublime.Config.ExperienceTimes); 
    end
    return 1571
end

---
--- GetExperience
---
function CLIENT:SL_GetExperience()
    local character = self:GetCharacter()
    if (character) then
        return character:GetData("sl_experience", 0);
    end
    return 0
end

function CLIENT:SL_GetLevel()
    local character = self:GetCharacter()
    if (character) then
        return character:GetData("sl_level", 1);
    end
    return 1
end

function CLIENT:SL_GetAbilityPoints()
    local character = self:GetCharacter()
    if (character) then
        return character:GetData("ability_points", 0);
    end
    return 0
end