local PLUGIN = PLUGIN

PLUGIN.name = "CustomHUD"
PLUGIN.author = "Vac"
PLUGIN.description = "Sets up a custom HUD for players."

if (CLIENT) then

	function PLUGIN:LoadFonts(font, genericFont)
		surface.CreateFont( "exo_compass_Numbers", {
			font = "Exo",
			size = 15,
			antialias = true
		} )

		surface.CreateFont( "exo_compass_Letters", {
			font = "Exo",
			size = ScrH() * .025,
			antialias = true
		} )
	end
	
	function PLUGIN:CanDrawAmmoHUD(weapon)
		return false
	end

	local adv_compass_tbl = {
		[0] = "N",
		[45] = "NE",
		[90] = "E",
		[135] = "SE",
		[180] = "S",
		[225] = "SW",
		[270] = "W",
		[315] = "NW",
		[360] = "N"
	}

	local function custom_compass_GetTextSize( font, text )
		surface.SetFont( font )
		local w, h = surface.GetTextSize( text )
		return w, h
	end

	local function custom_compass_DrawLineFunc( mask1, mask2, line, color )

		render.ClearStencil() -- This is being ran alot
		render.SetStencilEnable( true )

			render.SetStencilFailOperation( STENCILOPERATION_KEEP )
			render.SetStencilZFailOperation( STENCILOPERATION_KEEP )
			render.SetStencilPassOperation( STENCILOPERATION_REPLACE)
			render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_ALWAYS )

			render.SetStencilWriteMask( 1 )
			render.SetStencilReferenceValue( 1 )

			surface.SetDrawColor( Color( 0, 0, 0, 1 ) )
			surface.DrawRect( mask1[1], mask1[2], mask1[3], mask1[4] ) -- left
			surface.DrawRect( mask2[1], mask2[2], mask2[3], mask2[4] ) -- right

			render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_EQUAL )
			render.SetStencilTestMask( 1 )

			surface.SetDrawColor( color )
			surface.DrawLine( line[1], line[2], line[3], line[4] )

		render.SetStencilEnable( false )

	end

	function formatMoney(n)
		if not n then return attachCurrency("0") end
	
		if n >= 1e14 then return attachCurrency(tostring(n)) end
		if n <= -1e14 then return "-" .. attachCurrency(tostring(math.abs(n))) end
	
		local negative = n < 0
	
		n = tostring(math.abs(n))
		local dp = string.find(n, "%.") or #n + 1
	
		for i = dp - 4, 1, -3 do
			n = n:sub(1, i) .. "," .. n:sub(i + 1)
		end
	
		-- Make sure the amount is padded with zeroes
		if n[#n - 1] == "." then
			n = n .. "0"
		end
	
		return (negative and "-" or n)
	end

	local function OutlinedBox( x, y, w, h, thickness, clr )
		surface.SetDrawColor( clr )
		for i=0, thickness - 1 do
			surface.DrawOutlinedRect( x + i, y + i, w - i * 2, h - i * 2 )
		end
	end

	local function unRoundedBoxNumber(size, x, y, text)

		local w = surface.GetTextSize( text )

		local boxWidth = w + size * 2

		return boxWidth

	end

	local function unRoundedBox(size, x, y, upper, lower, font, color )

		local fontcolor = Color(255,255,255,255)

		surface.SetFont( font )
		local w, h = surface.GetTextSize( upper )
		local w2, h2 = surface.GetTextSize( lower )

		local boxWidth = w + size * 2
		local boxHeight = h + size

		if(x + boxWidth > ScrW()) then 
		 	x = ScrW() - 2 - boxWidth
		end

		if(upper == "HEALTH") then
			surface.SetDrawColor(Color(16,16,16, 190))
			surface.DrawRect(x, y, boxWidth, boxHeight )
			surface.SetDrawColor(color)
			local n = LocalPlayer():GetMaxHealth() - (LocalPlayer():GetMaxHealth() - LocalPlayer():Health())
			local max = n / LocalPlayer():GetMaxHealth()
			local animate = math.Clamp(boxHeight * max, 0, boxHeight)
			surface.DrawRect(x, y, boxWidth, animate )
			OutlinedBox(x, y, boxWidth, boxHeight, 2, Color(64,64,64,255))
		elseif(upper == "ARMOR") then
			surface.SetDrawColor(Color(16,16,16, 190))
			surface.DrawRect(x, y, boxWidth, boxHeight )
			surface.SetDrawColor(color)
			local n = tonumber(lower)
			local max = (n *.01)
			local animate = math.Clamp(boxHeight * max, 0, boxHeight)
			surface.DrawRect(x, y, boxWidth, animate )
			OutlinedBox(x, y, boxWidth, boxHeight, 2, Color(64,64,64,255))
		else
			surface.SetDrawColor(color)
			surface.DrawRect(x, y, boxWidth, boxHeight )
			OutlinedBox(x, y, boxWidth, boxHeight, 2, Color(64,64,64,255))
		end


		surface.SetTextColor( fontcolor.r, fontcolor.g, fontcolor.b, fontcolor.a )
		surface.SetTextPos( x + size, y + size * .25 )
		surface.DrawText( upper )

		if(lower != "" && lower != nil) then
			surface.SetTextColor( fontcolor.r, fontcolor.g, fontcolor.b, fontcolor.a )
			surface.SetTextPos( x + ((boxWidth - w2) *.5), y + size * .75 )
			surface.DrawText( lower )
		end

		return w + size * 2
	end

	function PLUGIN:HUDPaint()

		local ply = LocalPlayer()

		if (!ply:GetCharacter()) then
			return
		end

		local hud = {
			{name = "HEALTH", value = ply:Health() , color = Color(100,15,16,200)},
			{name = "ARMOR", value = ply:Armor(), color = Color(70,102,255, 200)},
			{name = "MONEY", value = ply:GetCharacter():GetMoney(), color = Color(16,16,16, 190)},
			{name = "TIME", value = os.date("%H:%M:%S"), color = Color(16,16,16, 190)}
		}

		surface.SetFont( "exo_compass_Numbers" )
		local lastRight = 2
		local boxSizes = unRoundedBoxNumber(32,1,1,hud[3].name) + unRoundedBoxNumber(32,1,1,hud[4].name)
		local lastLeft = ScrW() - boxSizes
		local boxSize = 0

		for i=1,#hud do
			if (i >= 3) then
				if(hud[i].name == "MONEY") then
					local toAdd = unRoundedBox(32, lastLeft - 2 , 2, hud[i].name, formatMoney(hud[i].value) .. " RC", "exo_compass_Numbers", hud[i].color) 
					lastLeft = lastLeft + toAdd + 2	
				else
					local toAdd = unRoundedBox(32, lastLeft - 2 , 2, hud[i].name, hud[i].value, "exo_compass_Numbers", hud[i].color) 
					lastLeft = lastLeft + toAdd + 2
				end
			else
				local toAdd = unRoundedBox(32, lastRight, 2, hud[i].name, hud[i].value, "exo_compass_Numbers", hud[i].color) 
				lastRight = lastRight + toAdd + 2
			end
		end
		


		--===========================================================================================--
											-- Compass stuff below --
		--===========================================================================================--
		if(LocalPlayer():GetActiveWeapon() != NULL) then
			if (string.match(LocalPlayer():GetActiveWeapon():GetClass(), "^weapon_lightsaber_.+")) then return end
		end


		local ang = ply:GetAngles()
		local compassX, compassY, width, height, multiplier, offset, spacing, numOfLines, fadeDistMultiplier, ratio
		local displayHeading, fadeDistance, color, compassBearingW, compassBearing, maxMarkerSize, minMarkerSize

		compassX, compassY = ScrW()*.5, ScrH()*.95
		width, height = ScrW()*.25, ScrH()*.03
		multiplier = 2.5
		ratio = 4
		color = Color(255, 255, 255, 255)
		minMarkerSize, maxMarkerSize = ScrH() * ( 0.5 / 45 ), ScrH() * ( 1 / 45 )
		displayHeading = 1

		offset = 180


		spacing = ( width * multiplier ) / 360
		numOfLines = width / spacing
		fadeDistMultiplier = 1
		fadeDistance = (width/2) / fadeDistMultiplier

		surface.SetFont( "exo_compass_Numbers" )

		local text = math.Round( 360 - ( ang.y % 360 ) )
		local font = "exo_compass_Numbers"
		compassBearingW, compassBearingH = custom_compass_GetTextSize( font, text )

		for i = ( math.Round( ang.y ) - numOfLines/2 ) % 360, ( ( math.Round( ang.y ) - numOfLines/2 ) % 360 ) + numOfLines do

			local x = ( compassX + ( width/2 * multiplier ) ) - ( ( ( i - ang.y - offset ) % 360 ) * spacing )
			local value = math.abs( x - compassX )
			local calc = 1 - ( ( value + ( value - fadeDistance ) ) / ( width/2 ) )
			local calculation = 255 * math.Clamp( calc, 0.001, 1 )

			if i % 15 == 0 && i > 0 then

				local text = adv_compass_tbl[360 - (i % 360)] && adv_compass_tbl[360 - (i % 360)] || 360 - (i % 360)
				local font = type( text ) == "string" && "exo_compass_Letters" || "exo_compass_Numbers"
				local w, h = custom_compass_GetTextSize( font, text )

				surface.SetDrawColor( Color( color.r, color.g, color.b, calculation ) )
				surface.SetTextColor( Color( color.r, color.g, color.b, calculation ) )
				surface.SetFont( font )

				surface.DrawLine( x, compassY, x, compassY + height * 0.5 )
				surface.SetTextPos( x - w/2, compassY + height * 0.5 )
				surface.DrawText( text )

			end

			if i % 5 == 0 && i % 15 != 0 then

				local mask1 = { compassX - width/2 - fadeDistance, compassY, width/2 + fadeDistance - compassBearingW, height }
				local mask2 = { compassX + compassBearingW, compassY, width/2 + fadeDistance - compassBearingW, height }
				local col = Color( color.r, color.g, color.b, calculation )

				local line = { x, compassY, x, compassY + height * 0.35 }
				custom_compass_DrawLineFunc( mask1, mask2, line, col )

				surface.DrawLine( x, compassY, x, compassY + height * 0.3 )

			end

		end


		-- Middle Triangle
		local triangleSize = 8
		local triangleHeight = compassY

		local triangle = {
			{ x = compassX - triangleSize/2, y = triangleHeight - ( triangleSize * 2 ) },
			{ x = compassX + triangleSize/2, y = triangleHeight - ( triangleSize * 2 ) },
			{ x = compassX, y = triangleHeight - triangleSize },
		}
		surface.SetDrawColor( 255, 255, 255 )
		draw.NoTexture()
		surface.DrawPoly( triangle )

		if displayHeading then
			local text = math.Round( 360 - ( ang.y % 360 ) )
			local font = "exo_compass_Numbers"
			local w, h = custom_compass_GetTextSize( font, text )
			surface.SetFont( font )
			surface.SetTextColor( Color( 255, 255, 255 ) )
			surface.SetTextPos( compassX - w/2, compassY - h - ( triangleSize * 2 ) )
			surface.DrawText( text )
		end

		if (GetGlobalInt("defcon") != 5) then
			local text = ("Defcon: " .. GetGlobalInt("defcon"))
			local font = "exo_compass_Letters"
			local w, h = custom_compass_GetTextSize( font, text )
			surface.SetTextPos( compassX - w/2, compassY - h * 2 - ( triangleSize * 2 ) )
			surface.DrawText(text)
		end

	end
end