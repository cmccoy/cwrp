PLUGIN.name = "Even Better Description Display"
PLUGIN.author = "Vac"
PLUGIN.description = "An even better description display for when you look at people."

if (CLIENT) then

	function PLUGIN:LoadFonts(font, genericFont)
		surface.CreateFont("ixMenuButtonFontThick", {
			font = "Roboto",
			size = ScreenScale(14),
			extended = true,
			weight = 300
		})
	end

	function PLUGIN:PopulateCharacterInfo(client, character, tooltip)

		if (character) then
			local class = rankToClass(character:GetRank(), character:GetFaction())
			local faction = ix.faction.indices[character:GetFaction()]

			if (class == nil) then
				class = {
					currentRankFull = "Trooper",
					rankLogo = "materials/ig_cwrp/ranks/trp/pvt.png"
				}
			elseif(faction.isDroid) then
				class = {
					currentRankFull = "Droid",
					rankLogo = "materials/ig_cwrp/ranks/droid.png"
				}
			end

			local name = tooltip:GetRow("name")
			name:SetText(character:GetName())
			name:SetFont("ixMenuButtonFontThick")
			name:SetBackgroundColor(team.GetColor(client:Team()))
			name:SizeToContents()	

			local factionName = tooltip:AddRow("faction")
			factionName:SetText(" " .. faction.name)
			factionName:SetBackgroundColor(faction.color)
			factionName:SizeToContents()

			local factionNameHeight = factionName:GetTall()
			factionName:SetTextInset(factionNameHeight + 4, 0)
			factionName:SetWide(factionName:GetWide() + factionNameHeight + 4)

			local factionImage = factionName:Add("DImage")
			factionImage:Dock(LEFT)
			factionImage:SetPlayer(client, factionNameHeight)
			factionImage:SetSize(factionName:GetTall(), factionName:GetTall())
			factionImage:SetImage(faction.logo)

			local rank = tooltip:AddRow("rank")
			rank:SetText(" " .. class.currentRankFull)
			rank:SetBackgroundColor(faction.color)
			rank:SizeToContents()

			local rankHeight = rank:GetTall()
			rank:SetTextInset(rankHeight + 4, 0)
			rank:SetWide(rank:GetWide() + rankHeight + 4)

			local rankImage = rank:Add("DImage")
			rankImage:Dock(LEFT)
			rankImage:SetPlayer(client, rankHeight)
			rankImage:SetSize(rank:GetTall(), rank:GetTall())
			rankImage:SetImage(class.rankLogo)

			if (character:GetSpecialty() != "none") then
				local specialty = tooltip:AddRow("specialty")
				local specName = string.Explode("", character:GetSpecialty())
				specName[1] = string.upper(specName[1])
				specName = string.Implode("", specName)

				specialty:SetText(" " .. specName)
				specialty:SetBackgroundColor(faction.color)
				specialty:SizeToContents()

				local specialtyHeight = specialty:GetTall()
				specialty:SetTextInset(specialtyHeight + 4, 0)
				specialty:SetWide(specialty:GetWide() + specialtyHeight + 4)

				local specialtyImage = specialty:Add("DImage")
				specialtyImage:Dock(LEFT)
				specialtyImage:SetPlayer(client, specialtyHeight)
				specialtyImage:SetSize(specialty:GetTall(), specialty:GetTall())
				specialtyImage:SetImage("materials/ig_cwrp/ranks/specialty/" .. character:GetSpecialty() .. ".png")
			end
		end

	end

	function PLUGIN:PopulatePlayerTooltip(client, tooltip)
		
	end

	function GetPlayerEntityMenu(client, options)
	end
end