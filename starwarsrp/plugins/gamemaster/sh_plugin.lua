local PLUGIN = PLUGIN

PLUGIN.name = "Gamemaster Commands"
PLUGIN.author = ""
PLUGIN.description = "Add commands that make gamemaster jobs easier"

ix.util.Include("cl_gm_net.lua")
ix.util.Include("sh_meta.lua")
ix.util.Include("sv_gm_hooks.lua")
ix.util.Include("sv_gmsql.lua")
ix.util.Include("sv_events.lua")

if SERVER then
    util.AddNetworkString("ixGamemasterEventChat")
end

local function displayMessage(client, target, rank)
    if(client == target) then
        client:NotifyLocalized("Your gamemaster rank was set to " .. rank .. "!")
        return
    end        
    client:NotifyLocalized("You set " .. target:GetName() .. "'s gamemaster rank to " .. rank .."!")
    target:NotifyLocalized("Your gamemaster rank was set to " .. rank .. "!")
end

local t = {
    {abv = "none", rank = 0, name = "None"},
    {abv = "tgm", rank = 1, name = "Trial Gamemaster"},
    {abv = "gm", rank = 2, name = "Gamemaster"},
    {abv = "vgm", rank = 3, name = "Veteran Gamemaster"},
    {abv = "hgm", rank = 4, name = "Head Gamemaster"}
}


ix.command.Add("setGamemasterRank", {
    description = "Promotes a player to the specified gamemaster rank. NONE | TGM | GM | VGM | HGM",
    arguments = {ix.type.player, ix.type.string},
    OnRun = function(self, client, target, rankLiteral)
        if (SERVER) then
            if(!client:IsSuperAdmin()) then return "You're not a high enough rank!" end
            for k, v in pairs(t) do
                if (v.abv == string.lower(rankLiteral)) then
                    if(target:GetGamemasterRank() == v.rank) then
                        client:NotifyLocalized(target:GetName() .. " is already a " .. v.name)
                    else
                        target:SetGamemasterRank(v.rank)
                        displayMessage(client, target, v.name)

                        hook.Run("SwrpOnSetGamemaster", client, target, v.abv)
                        break;
                    end
                end
            end
        end
    end
})

ix.command.Add("setGamemasterRankByID", {
    description = "Set's a gamemaster rank by steam 64 ID. NONE | TGM | GM | VGM | HGM",
    arguments = {ix.type.string, ix.type.string},
    OnRun = function(self, client, steamid, rankLiteral)
        if (SERVER) then
            if(!client:IsSuperAdmin()) then return "You're not a high enough rank!" end

            if(string.len(steamid) != 17) then
                return "Steam ID not valid, it should look like "
            end

            for k, v in pairs(t) do
                if (v.abv == string.lower(rankLiteral)) then
                    setGmBySteamID(steamid, v.rank)
                    hook.Run("SwrpOnSetGamemasterByID", client, steamid, v.abv)
                    break;
                end
            end
        end
    end
})

ix.command.Add("gamemasterMenu", {
    description = "Opens up the gamemaster menu",
    arguments = {},
    OnRun = function(self, client)
        if (SERVER) then
            retrieveGmData(client)
        end
    end
})

ix.command.Add("startEvent", {
    description = "Starts an event",
    arguments = {},

    OnRun = function(self, client)
        net.Start("ixStartEvent")
        net.Send(client)
        hook.Run("SwrpOnStartEvent", client)
    end
})

ix.command.Add("endEvent", {
    description = "Ends the current event",
    arguments = {},
    OnRun = function(self, client)
        if (client:IsGameMaster() || client:IsSuperAdmin()) then
            endCurrentEvent(client)
            hook.Run("SwrpOnEndEvent", client)
        end
    end
})

ix.command.Add("gamemasterMode", {
    description = "Give you access to gamemaster commands",
    arguments = {},
    OnRun = function(self, client)
        if(client:IsGameMaster()) then
            if(client:GetGamemasterMode()) then
                client:SetGamemasterMode(false)
                client:GetCharacter():TakeFlags("n")
                client:GetCharacter():TakeFlags("e")
                client:GetCharacter():TakeFlags("r")
                client:GetCharacter():TakeFlags("C")
                client:GetCharacter():TakeFlags("t")
                client:GetCharacter():TakeFlags("p")
                client:GetCharacter():TakeFlags("E")
                client:GetCharacter():TakeFlags("w")
                client:SetNoTarget(false)
                hook.Run("SwrpOnGmMode", client, false)
            else
                client:SetGamemasterMode(true)
                client:GetCharacter():GiveFlags("n")
                client:GetCharacter():GiveFlags("e")
                client:GetCharacter():GiveFlags("r")
                client:GetCharacter():GiveFlags("C")
                client:GetCharacter():GiveFlags("t")
                client:GetCharacter():GiveFlags("p")
                client:GetCharacter():GiveFlags("E")
                client:GetCharacter():GiveFlags("w")
                client:SetNoTarget(true)
                hook.Run("SwrpOnGmMode", client, true)
            end
        end
    end
})

-- Goto

ix.command.Add("gamemasterBring", {
    description = "Brings the specified player to you",
    arguments = {ix.type.player},
    OnRun = function(self, client, target)
        if (client:GetGamemasterMode()) then
            if not client:IsValid() then
                Msg( "If you brought someone to you, they would instantly be destroyed by the awesomeness that is console.\n" )
                return
            end
        
            if not client:Alive() then
                return
            end
        
            if client:InVehicle() then
                return
            end
        
            local t = {
                start = client:GetPos(),
                filter = { client },
                endpos = client:GetPos(),
            }
            local tr = util.TraceEntity( t, client )
        
            if tr.Hit then
                return
            end
        
            if (!target:Alive()) then
                return
            end
        
            local t = {}
                t.start = client:GetPos() + Vector( 0, 0, 32 ) -- Move them up a bit so they can travel across the ground
                t.endpos = t.start + Vector(50, 50, 0)
            local tr = util.TraceEntity( t, target )
        
            target:SetPos( t.endpos )
            target:SetEyeAngles( (client:GetPos() - t.endpos):Angle() )
            target:SetLocalVelocity( Vector( 0, 0, 0 ) )
            hook.Run("SwrpOnGmBring", client, target)
        end
    end
})

ix.command.Add("gamemasterGoto", {
    description = "Goto the specified player",
    arguments = {ix.type.player},
    OnRun = function(self, client, target)
        if (client:GetGamemasterMode()) then
            if not client:IsValid() then
                return
            end
        
            if not client:Alive() then
                return
            end
        
            if client:InVehicle() then
                return
            end
        
            if (!target:Alive()) then
                return
            end

            client:SetPos(target:GetPos() + Vector(50, 50, 30))
            target:SetLocalVelocity( Vector( 0, 0, 0 ) )
            client:SetMoveType( MOVETYPE_NOCLIP )

            hook.Run("SwrpOnGmGoto", client, target)
        end
    end
})

ix.command.Add("gamemasterGodmode", {
    description = "Puts you into godmode",
    arguments = {},
    OnRun = function(self, client)
        if(client:GetGamemasterMode()) then
            if (client.hasGodMode) then
                client:GodDisable()
                client.hasGodMode = false
                client:NotifyLocalized("You disabled godmode for yourself")
                hook.Run("SwrpOnGmGod", client, false)
            else
                client:GodEnable()
                client.hasGodMode = true
                client:NotifyLocalized("You enabled godmode for yourself")
                hook.Run("SwrpOnGmGod", client, true)
            end
        end
    end
})

ix.command.Add("gamemasterCloak", {
    description = "Puts you in noclip mode",
    arguments = {},
    OnRun = function(self, client)
        if (client:GetGamemasterMode()) then
            if (client:GetInvisible()) then
                client:SetInvisible(false)
                client:NotifyLocalized("You are now visible")
                hook.Run("SwrpOnGmCloak", client, false)
            else
                client:SetInvisible(true)
                client:NotifyLocalized("You are now invisible")
                hook.Run("SwrpOnGmCloak", client, true)
            end
        end
    end
})

ix.command.Add("gamemasterSetModel", {
	description = "Allows you to set the model of a player.",
	arguments = {
        ix.type.string,
        bit.bor(ix.type.character, ix.type.optional)
	},
    OnRun = function(self, client, model, tcharacter)
        if(client:GetGamemasterMode()) then
            if (tcharacter) then

                if(!tcharacter:GetData("hadPreviousModel", false)) then
                    tcharacter:SetData("hadPreviousModel", true)
                    tcharacter:SetData("previousModel", tcharacter:GetModel())
                    
                    tcharacter:GetPlayer():SetModel(model)
                    tcharacter:GetPlayer():SetupHands()
                else
                    tcharacter:GetPlayer():SetModel(model)
                    tcharacter:GetPlayer():SetupHands()
                end
                hook.Run("SwrpOnGmMdl", client, target, model)
            else
                local character = client:GetCharacter()
                if(!character:GetData("hadPreviousModel", false)) then
                    character:SetData("hadPreviousModel", true)
                    character:SetData("previousModel", character:GetModel())
                    
                    client:SetModel(model)
                    client:SetupHands()
                    hook.Run("SwrpOnGmMdl", client, nil, model)
                else
                    client:SetModel(model)
                    client:GetPlayer():SetupHands()
                end
                hook.Run("SwrpOnGmMdl", client, nil, model)
            end
        end
	end
})

ix.command.Add("gamemasterScale", {
	description = "Allows you to scale the model of a player.",
	arguments = {
        ix.type.string,
        bit.bor(ix.type.player, ix.type.optional)
	},
    OnRun = function(self, client, value, target)
        if(client:GetGamemasterMode()) then
            if (target) then
                target:SetModelScale(value, 0)
                hook.Run("SwrpOnMdlScale", client, target, value)
            else
                client:SetModelScale(value, 0)
                hook.Run("SwrpOnMdlScale", client, nil, value)
            end
        end
	end
})

ix.command.Add("gamemasterSetOwnName", {
	description = "Allows you to set the name of yourself.",
	arguments = {
		ix.type.text
	},
    OnRun = function(self, client, name)
        if (client:GetGamemasterMode()) then
            character = client:GetCharacter()
            if(character:GetData("hadPreviousName")) then
                character:SetName(name)
            else 
                character:SetData("hadPreviousName", true)
                character:SetData("previousName", character:GetName())
                character:SetName(name)
            end
            hook.Run("SwrpOnGmName", client, nil, name)
        end
	end
})

ix.command.Add("gamemasterSetPlayerName", {
	description = "Allows you to set the name of a player.",
	arguments = {
		ix.type.player,
		ix.type.text
	},
    OnRun = function(self, client, target, name)
        if (client:GetGamemasterMode()) then
            if (target && name) then
                tcharacter = target:GetCharacter()

                if(tcharacter:GetData("hadPreviousName")) then
                    tcharacter:SetName(name)
                else
                    tcharacter:SetData("hadPreviousName", true)
                    tcharacter:SetData("previousName", tcharacter:GetName())
                    tcharacter:SetName(name)
                end
                hook.Run("SwrpOnGmName", client, target, name)
            end
        end
	end
})

function PLUGIN:OnCharacterDisconnect(client, character) 
    if (character:GetData("hadPreviousName")) then
        character:SetName(character:GetData("previousName"))
        character:SetData("hadPreviousName", nil)
        character:SetData("previousName", nil)
    end

    if (character:GetData("hadPreviousModel", false)) then
        character:SetModel(character:GetData("previousModel"))
        character:SetData("hadPreviousModel", nil)
        character:SetData("previousModel", nil)
    end

end

ix.command.Add("gamemasterSetHealth", {
    description = "Sets a players health",
    arguments = {ix.type.player, ix.type.number},
    OnRun = function(self, client, target, value)
        if (client:GetGamemasterMode()) then
            if (target) then
                target:SetHealth(value)
                hook.Run("SwrpOnGmHp", client, target, value)
            end
        end
    end
})

ix.command.Add("gamemasterNoclip", {
    description = "Puts you in noclip mode",
    arguments = {},
    OnRun = function(self, client)
        if (client:GetGamemasterMode()) then
            if client:GetMoveType() == MOVETYPE_WALK then
                client:SetMoveType( MOVETYPE_NOCLIP )
                hook.Run("SwrpOnGmNoclip", client, true)
            elseif client:GetMoveType() == MOVETYPE_NOCLIP then
                client:SetMoveType( MOVETYPE_WALK )
                hook.Run("SwrpOnGmNoclip", client, false)
            end
        end
    end
})

ix.command.Add("gamemasterEventChat", {
    description = "Sends a event chat message",
    arguments = {ix.type.string, ix.type.text},
    OnRun = function(self, client, name, text)
        if (client:IsGameMaster() || client:IsStaff()) then
            net.Start("ixGamemasterEventChat")
                net.WriteString(name)
                net.WriteString(text)
            net.Broadcast()
            hook.Run("SwrpOnGmEventChat", client, name, text)
        end
    end
})

ix.command.Add("gamemasterBroadcast", {
    description = "Allows the entire server to hear you",
    arguments = {},
    OnRun = function(self, client)
        if (client:IsGameMaster() || client:IsStaff()) then
            if(client.AllTalk) then
                client:ChatPrint("Disabled broadcast")
                client.AllTalk = nil
                hook.Run("SwrpOnGmEnableAllTalk", client, false)
            else
                client:ChatPrint("Enabled broadcast")
                client.AllTalk = true
                hook.Run("SwrpOnGmEnableAllTalk", client, true)
            end
        end
    end
})

ix.command.Add("gamemasterChangeMap", {
    description = "Changes the map to the specified one",
    arguments = {ix.type.string},
    OnRun = function(self, client, map)
        if (client:IsGameMaster() || client:IsStaff()) then
            RunConsoleCommand("ulx", "map", map, "starwarsrp")
            hook.Run("SwrpOnGmMapChange", client, map)
        end
    end
})

if SERVER then
    util.AddNetworkString("ixLeaveEventServer")
end

ix.command.Add("leaveEventServer", {
    description = "Forces clients to connect to the main server",
    arguments = {},
    OnRun = function(self, client)
        if(client:IsGameMaster() || client:IsStaff()) then
            net.Start("ixLeaveEventServer")
            net.Broadcast()

            hook.Run("SwrpOnEventServerLeave", client)
        end
    end
})

if CLIENT then
    net.Receive("ixLeaveEventServer", function(len,pl)
        LocalPlayer():ConCommand("connect 74.91.121.150:27050")
    end)
end