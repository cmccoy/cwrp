util.AddNetworkString("OpenGmMenu")

function retrieveGmData(client)
    if (client:IsGameMaster()) then
        local query = mysql:Select("ix_players")
            query:WhereGT("gamemaster", "0")
            query:Select("gamemaster_data")
            query:Select("gamemaster")
            query:Callback(function(result)
                if  (istable(result) and #result > 0 and result[1].gamemaster_data != nil) then
                    local totalGM = 0
                    for k, v in pairs(player.GetAll()) do
                        if(v:IsGameMaster()) then
                            totalGM = totalGM + 1
                        end
                    end
                    net.Start("OpenGmMenu")
                        net.WriteTable(result)
                        net.WriteInt(totalGM , 8)
                    net.Send(client)
                else
                    net.Start("OpenGmMenu")
                        net.WriteTable({})
                    net.Send(client)
                end
            end)
        query:Execute()
    end
end

function setGmBySteamID(steamid, rank)
    local query = mysql:Update("ix_players")
        query:Where("steamid", steamid)
        query:Update("gamemaster", rank)
    query:Execute()
end


hook.Add("PlayerInitialSpawn", "[IG] Update ix_players table to have gm data", function(pl)
    local query = mysql:Select("ix_players")
			query:Where("steamid", pl:SteamID64())
            query:Callback(function(result)
                local t = {
                    name = pl:GetName(),
                    events_done = 0,
                    last_seen = os.time(),
                    last_event = 0,
                    steamid = pl:SteamID64()
                }

                if (istable(result) and #result > 0 and result[1].gamemaster_data != nil) then
                    local rank = result[1].gamemaster
                    pl.gmRank = rank
                elseif(istable(result) and #result > 0 and result[1].gamemaster_data == nil) then
                    t = util.TableToJSON(t)
                    local query = mysql:Update("ix_players")
                        query:Where("steamid", pl:SteamID64())
                        query:Update("gamemaster_data", t)
                    query:Execute()
                    pl.gmRank = 0
                else
                    -- If column doesn't exist, make it
                    t = util.TableToJSON(t)
                    local query = mysql:Update("ix_players")
                        query:Where("steamid", pl:SteamID64())
                        query:Update("gamemaster_data", t)
                        query:Callback(function()
                            pl.gmRank = 0
                        end)
                    query:Execute()
                end
            end)
    query:Execute()
end)

hook.Add("InitPostEntity", "[IG] Add gamemaster columns to ix_players", function()
    --[[
    local query = mysql:Alter("ix_players")
        query:Add("gamemaster_data", "TEXT DEFAULT NULL")
    query:Execute()

    local query2 = mysql:Alter("ix_players")
        query2:Add("gamemaster", "INT(8) DEFAULT 0")
    query2:Execute()
    ]]
end)