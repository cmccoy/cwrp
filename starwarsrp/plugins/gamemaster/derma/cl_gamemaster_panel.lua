
local PANEL = {}
	
local mouseX = gui.MouseX()
local mouseY = gui.MouseY()

function PANEL:Init()
	self:SetSize(800, 600)
	self:Center()
	self:MakePopup()
	self:SetTitle("Gamemaster Menu")
	self.base = self:Add("DPanel")
	self.base:Dock(FILL)	
end


function PANEL:Setup(t, totalEventNumber, lastEventDate, totalGM)

	local stats = self.base:Add("DPanel")
	stats:Dock(TOP)
	stats:SetTall(50)

	local totalEvents = stats:Add("DLabel")
	totalEvents:Dock(LEFT)
	totalEvents:SetFont("ixWeaponSelectFont")
	totalEvents:SetText("Total Events: " .. totalEventNumber)
	totalEvents:SizeToContents()

	local onlineGms = stats:Add("DLabel")
	onlineGms:Dock(RIGHT)
	onlineGms:SetFont("ixWeaponSelectFont")
	onlineGms:SetText("Online GMs: " .. totalGM)
	onlineGms:SizeToContents()

	local lastEvent = stats:Add("DLabel")
	lastEvent:Dock(FILL)
	lastEvent:DockMargin(110, 0, 0, 0)
	lastEvent:SetFont("ixWeaponSelectFont")
	lastEvent:SetText("Last Event: " .. lastEventDate)
	lastEvent:SizeToContents()

	local list = self.base:Add("DListView")
	list:DockMargin(5, 5, 5, 5)
	list:Dock(FILL)
	list:AddColumn("Name")
	list:AddColumn("Rank")
	list:AddColumn("Events Done")
	list:AddColumn("Last Seen")
	list:AddColumn("Last Event")
	list:AddColumn("Steam ID")
	for k, v in pairs(t) do
		for k2, v2 in pairs(v) do
			list:AddLine(v2.name, v2.rank, v2.events_done, v2.last_seen, v2.last_event, v2.steamid)
		end
	end
	list.OnRowRightClick = function(self, lineID, line)
		local menu = list:Add("DMenu")
		menu:SetPos(mouseX, mouseY)
		menu:AddOption("Test")
	end

	function list:Paint()
    	draw.RoundedBoxEx( 6, 0, 0, list:GetWide(), list:GetTall(), Color( 45, 50, 53 ) ) 
	end

	for k, v in pairs( list.Lines ) do
		function v:Paint()
			if (k % 2 == 0) then
            	draw.RoundedBoxEx( 6, 0, 0, list:GetWide(), 25, Color( 57, 55, 54 ) ) 
        	else
				draw.RoundedBoxEx( 6, 0, 0, list:GetWide(), 25, Color( 69, 69, 69 ) )
			end
		end
	end

end

local function prettyTime(number)
	if (os.time() - number < 86400) then
		return "Today"
	else
		if(math.Round((os.time() - number) / 86400, 0) > 3000) then
			return "Never"
		else
			return math.Round((os.time() - number) / 86400, 0) .. " days ago"
		end
	end
end

local gmRanks = {
	"Trial Gamemaster",
	"Gamemaster",
	"Verteran Gamemaster",
	"Head Gamemaster"
}

net.Receive("OpenGmMenu", function(len, pl)
	if(LocalPlayer():IsAdmin()) then
		local t = net.ReadTable()
		local totalGM = net.ReadInt(8)
		local totalEvents = 0
		local lastEvent = 0

		for k, v in pairs(t) do
			t[k].gamemaster_data = util.JSONToTable(t[k].gamemaster_data)
			print(t[k].gamemaster)
			totalEvents = t[k].gamemaster_data.events_done + totalEvents
			if(t[k].gamemaster_data.last_event == 0) then
				t[k].gamemaster_data.last_event = "Never"
			else
				if(lastEvent < t[k].gamemaster_data.last_event) then
					lastEvent = t[k].gamemaster_data.last_event
				end
				t[k].gamemaster_data.last_event = prettyTime(t[k].gamemaster_data.last_event)
			end
			t[k].gamemaster_data.rank = gmRanks[t[k].gamemaster]
			t[k].gamemaster_data.last_seen = prettyTime(t[k].gamemaster_data.last_seen)

			t[k].gamemaster = nil
		end
		
		lastEvent = prettyTime(lastEvent)


		ix.gui.gamemasterMenu = vgui.Create("ixGamemasterMenu")
		ix.gui.gamemasterMenu:Setup(t || {}, totalEvents, lastEvent, totalGM)
	end

end)

vgui.Register("ixGamemasterMenu", PANEL, "DFrame")
