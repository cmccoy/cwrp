
local PANEL = {}
	
function PANEL:Init()

	self:SetTitle("Event Creation")
	self:SetBackgroundBlur(true)
	self:SetDeleteOnClose(true)
	self:SetSize(400, 400)

	self.label = self:Add("DLabel")
	self.label:Dock(TOP)
	self.label:SetText("Event Name")


	self.name = self:Add("DTextEntry")
	self.name:Dock(TOP)
	self.name:SetTall(30)
	self.name.SetRealValue = function(self, text)
		self:SetText(text)
		self:SetCaretPos(text:len())
	end

	self.name.Think = function(self)
		local text = self:GetValue()

		if (text:len() > 50) then
			self:SetRealValue(text:sub(1, 50))
			surface.PlaySound("common/talk.wav")
		end
	end

	self.label = self:Add("DLabel")
	self.label:Dock(TOP)
	self.label:SetText("Approver Steam ID if TGM")

	self.approver = self:Add("DTextEntry")
	self.approver:Dock(TOP)
	self.approver:SetTall(30)
	self.approver.SetRealValue = function(this, text)
		this:SetText(text)
		this:SetCaretPos(text:len())
	end

	self.approver.Think = function(this)
		local text = this:GetValue()
		if (text:len() > 18) then
			this:SetRealValue(text:sub(1, 18))
			surface.PlaySound("common/talk.wav")
		end
	end

	self.label = self:Add("DLabel")
	self.label:Dock(TOP)
	self.label:SetText("Event Description")

	self.textEntry = self:Add("DTextEntry")
	self.textEntry:SetMultiline(true)
	self.textEntry:Dock(FILL)
	self.textEntry.SetRealValue = function(this, text)
		this:SetText(text)
		this:SetCaretPos(text:len())
	end

	self.textEntry.Think = function(this)
		local text = this:GetValue()
		if (text:len() > 1024) then
			this:SetRealValue(text:sub(1, 1024))
			surface.PlaySound("common/talk.wav")
		end
	end

	self.button = self:Add("DButton")
	self.button:SetText(L("Submit"))
	self.button:Dock(BOTTOM)
	self.button.DoClick = function(this)
		net.Start("ixSubmitEvent")
			net.WriteString(self.name:GetValue():sub(1, 50))
			net.WriteString(self.approver:GetValue():sub(1, 18))
			net.WriteString(self.textEntry:GetValue():sub(1, 1024))
		net.SendToServer()
		self:Close()
	end

	self:MakePopup()
	self:Center()

end

net.Receive("ixStartEvent", function()

	ix.gui.startEvent = vgui.Create("ixGamemasterCreateEvent")

end)

vgui.Register("ixGamemasterCreateEvent", PANEL, "DFrame")
