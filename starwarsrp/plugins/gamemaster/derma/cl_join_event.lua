
local PANEL = {}
	
function PANEL:Init()

	self:SetTitle("Join Event")
	self:SetBackgroundBlur(true)
	self:SetDeleteOnClose(true)
	self:SetSize(400, 400)

	self:MakePopup()
	self:Center()

end

function PANEL:Setup(name, description, requesterName)

	self.label = self:Add("DLabel")
	self.label:Dock(TOP)
	self.label:SetText("Event")

	self.name = self:Add("DTextEntry")
	self.name:Dock(TOP)
	self.name:SetText(name)
	self.name:SetTall(30)
	self.name.AllowInput = function(self, value)
		return true
	end

	self.label = self:Add("DLabel")
	self.label:Dock(TOP)
	self.label:SetText("Ran by")

	self.approver = self:Add("DTextEntry")
	self.approver:Dock(TOP)
	self.approver:SetText(requesterName)
	self.approver:SetTall(30)
	self.approver.AllowInput = function(self, value)
		return true
	end

	self.label = self:Add("DLabel")
	self.label:Dock(TOP)
	self.label:SetText("Event Description")

	self.textEntry = self:Add("DTextEntry")
	self.textEntry:SetMultiline(true)
	self.textEntry:SetText(description)
	self.textEntry:Dock(FILL)
	self.textEntry.AllowInput = function(self, value)
		return true
	end

	self.approve = self:Add("DButton")
	self.approve:SetText(L("Join Event Server"))
	self.approve:Dock(BOTTOM)
	self.approve.DoClick = function(this)
		LocalPlayer():ConCommand("connect 74.91.121.150:27100")
		self:Close()
	end

	self.decline = self:Add("DButton")
	self.decline:SetText(L("Decline"))
	self.decline:Dock(BOTTOM)
	self.decline.DoClick = function(this)
		self:Close()
	end

end

net.Receive("ixOpenEventServer", function(len, pl)

	local name = net.ReadString()
	local description = net.ReadString()
	local requesterName = net.ReadString()

	ix.gui.ixJoinEvent = vgui.Create("ixJoinEvent")
	ix.gui.ixJoinEvent:Setup(name, description, requesterName)
	
end)

vgui.Register("ixJoinEvent", PANEL, "DFrame")
