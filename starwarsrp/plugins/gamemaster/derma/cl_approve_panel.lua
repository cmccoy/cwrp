
local PANEL = {}
	
function PANEL:Init()

	self:SetTitle("Event Approval")
	self:SetBackgroundBlur(true)
	self:SetDeleteOnClose(true)
	self:SetSize(400, 400)

	self:MakePopup()
	self:Center()

end

function PANEL:Setup(name, approver, description, requesterID, requesterName)

	self.label = self:Add("DLabel")
	self.label:Dock(TOP)
	self.label:SetText("Event Name")

	self.name = self:Add("DTextEntry")
	self.name:Dock(TOP)
	self.name:SetText(name)
	self.name:SetTall(30)
	self.name.AllowInput = function(self, value)
		return true
	end

	self.label = self:Add("DLabel")
	self.label:Dock(TOP)
	self.label:SetText("You were selected to approve this event")

	self.approver = self:Add("DTextEntry")
	self.approver:Dock(TOP)
	self.approver:SetText(approver)
	self.approver:SetTall(30)
	self.approver.AllowInput = function(self, value)
		return true
	end

	self.label = self:Add("DLabel")
	self.label:Dock(TOP)
	self.label:SetText("Requester name and steamID")

	self.approver = self:Add("DTextEntry")
	self.approver:Dock(TOP)
	self.approver:SetText(requesterName .. " | " .. requesterID)
	self.approver:SetTall(30)
	self.approver.AllowInput = function(self, value)
		return true
	end

	self.label = self:Add("DLabel")
	self.label:Dock(TOP)
	self.label:SetText("Event Description")

	self.textEntry = self:Add("DTextEntry")
	self.textEntry:SetMultiline(true)
	self.textEntry:SetText(description)
	self.textEntry:Dock(FILL)
	self.textEntry.AllowInput = function(self, value)
		return true
	end

	self.approve = self:Add("DButton")
	self.approve:SetText(L("Approve"))
	self.approve:Dock(BOTTOM)
	self.approve.DoClick = function(this)
		net.Start("ixApproveEvent")
			net.WriteString(self.name:GetValue():sub(1, 50))
			net.WriteString(self.approver:GetValue():sub(1, 18))
			net.WriteString(self.textEntry:GetValue():sub(1, 1024))
			net.WriteString(requesterID)
			net.WriteString(requesterName)
		net.SendToServer()
		self:Close()
	end

	self.decline = self:Add("DButton")
	self.decline:SetText(L("Deny"))
	self.decline:Dock(BOTTOM)
	self.decline.DoClick = function(this)
		self:Close()
	end

end

net.Receive("ixApproveEvent", function(len, pl)

	local name = net.ReadString()
	local approver = net.ReadString()
	local description = net.ReadString()
	local requesterID = net.ReadString()
	local requesterName

	for k, v in pairs(player.GetAll()) do
		if(v:SteamID() == requesterID) then
			requesterName = v:GetName()
		end
	end

	ix.gui.ixGamemasterApproveEvent = vgui.Create("ixGamemasterApproveEvent")
	ix.gui.ixGamemasterApproveEvent:Setup(name, approver, description, requesterID, requesterName)
	
end)

vgui.Register("ixGamemasterApproveEvent", PANEL, "DFrame")
