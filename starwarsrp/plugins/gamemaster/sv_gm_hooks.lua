util.AddNetworkString("ixSetGmRank")

hook.Add("PlayerCanHearPlayersVoice", "[Impact] Enable GM alltalk", function(listener, talker)
    if talker.AllTalk then 
        return true 
    end
end)

hook.Add("PlayerFullLoad", "[Impact] Set GM rank", function(pl)

    local query = mysql:Select("ix_players")
        query:Where("steamid", pl:SteamID64())
        query:Select("gamemaster", rank)
        query:Callback(function(result)
            if (istable(result) && #result > 0 and result[1].gamemaster != nil) then
                pl.gmRank = result[1].gamemaster
                net.Start("ixSetGmRank")
                    net.WriteInt(result[1].gamemaster, 8)
                net.Send(pl)
            end
        end)
    query:Execute()

end)

hook.Add("PlayerInitialSpawn","FullLoadSetup",function(ply)
    hook.Add("SetupMove",ply,function(self,ply,_,cmd)
        if self == ply and not cmd:IsForced() then hook.Run("PlayerFullLoad",self) hook.Remove("SetupMove",self) end
    end)
end)