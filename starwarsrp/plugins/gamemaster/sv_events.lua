util.AddNetworkString("ixSubmitEvent")
util.AddNetworkString("ixApproveEvent")
util.AddNetworkString("ixStartEvent")
util.AddNetworkString("ixOpenEventServer")

local currentEventName = ""
local currentEventDesc = ""
local currentEventRunner = ""

--TODO refactor
net.Receive("ixSubmitEvent", function(len, pl)
    if (pl:IsGameMaster()) then
        local name = net.ReadString()
        local approver = net.ReadString()
        local description = net.ReadString()
        local requester = pl:SteamID()

        local query = mysql:Select("ix_events")
            query:WhereGT("status", "0")
            query:Callback(function(result)
                if (istable(result) and #result > 0) then
                    if (result[1].status != 1) then
                        if (pl:GetGamemasterRank() == 1) then                
                            for k, v in pairs(player.GetAll()) do
                                if (v:SteamID() == approver) then
                                    if(v:GetGamemasterRank() > 2 || v:IsSuperAdmin()) then
                                        net.Start("ixApproveEvent")
                                            net.WriteString(name)
                                            net.WriteString(approver)
                                            net.WriteString(description)
                                            net.WriteString(requester)
                                        net.Send(v)
                                        break;
                                    else
                                        break;
                                    end
                                end
                            end
                        else
                            currentEventName = name
                            currentEventDesc = description
                            currentEventRunner = pl:GetName()
                            insertEventLog(pl, name, description, nil, nil, nil)
                        end
                    else
                        pl:NotifyLocalized(result[1].creator .. " is currently running an event!")
                    end
                else
                    if (pl:GetGamemasterRank() == 1) then            
                        for k, v in pairs(player.GetAll()) do
                            if (v:SteamID() == approver) then
                                if(v:GetGamemasterRank() > 2 || v:IsSuperAdmin()) then
                                    net.Start("ixApproveEvent")
                                        net.WriteString(name)
                                        net.WriteString(approver)
                                        net.WriteString(description)
                                        net.WriteString(requester)
                                    net.Send(v)
                                    break;
                                else
                                    break;
                                end
                            end
                        end
                    else
                        currentEventName = name
                        currentEventDesc = description
                        currentEventRunner = pl:GetName()
                        insertEventLog(pl, name, description, nil, nil, nil)
                    end
                end

            end)
        query:Execute()
    end
end)

net.Receive("ixApproveEvent", function(len, pl)
    if (pl:GetGamemasterRank() > 2 || pl:IsSuperAdmin()) then
        local name = net.ReadString()
        local approver = net.ReadString()
        local description = net.ReadString()
        local requesterID = net.ReadString()
        local requesterName = net.ReadString()

        currentEventName = name
        currentEventDesc = description
        currentEventRunner = requesterName

        insertEventLog(pl, name, description, approver, requesterID, requesterName)

        hook.Run("SwrpEventApproved", approver, requesterName)
    end
end)

ix.command.Add("eventServer", {
    description = "Opens a menu to join the event server on clients",
    arguments = {},
    OnRun = function(self, client)
        if(client:IsGameMaster() || client:IsStaff()) then
            if (currentEventName == "") then return end
            
            net.Start("ixOpenEventServer")
                net.WriteString(currentEventName)
                net.WriteString(currentEventDesc)
                net.WriteString(currentEventRunner)
            net.Broadcast()

            currentEventName = ""
            currentEventDesc = ""
            currentEventRunner = ""

            hook.Run("SwrpOnOpenEventServerJoin", client)
        end
    end
})

function endCurrentEvent(client)
    local query = mysql:Update("ix_events")
        query:WhereGT("status", "0")
        query:Update("status", 0)
        query:Update("ended_by", client:GetName())
        query:Update("ended_by_id", client:SteamID64())
    query:Execute()

    client:NotifyLocalized("You ended the current event")
end

-- Util function for submitting events
function insertEventLog(client, name, description, approver, requesterID, requesterName)
    if(!client:IsGameMaster()) then return end

    if(client:GetGamemasterRank() == 1) then
        local query = mysql:Insert("ix_events")
            query:Insert("creator", requesterName)
            query:Insert("creator_id", util.SteamIDTo64(requesterID))
            query:Insert("approver", client:GetName())
            query:Insert("approver_id", client:SteamID64())
            query:Insert("name", name)
            query:Insert("description", description)
        query:Execute()
    else
        local query = mysql:Insert("ix_events")
            query:Insert("creator", client:GetName())
            query:Insert("creator_id", client:SteamID64())
            query:Insert("name", name)
            query:Insert("description", description)
        query:Execute()
    end
end

-- Setup new table if it doesn't exist
hook.Add("InitPostEntity", "[IG] Setup events table", function()
    local query = mysql:Create("ix_events")
        query:Create("id", "INT(11) UNSIGNED NOT NULL AUTO_INCREMENT")
        query:PrimaryKey("id")
        query:Create("creator", "VARCHAR(50) DEFAULT NULL")
        query:Create("creator_id", "VARCHAR(50) DEFAULT NULL")
        query:Create("approver", "VARCHAR(50) DEFAULT NULL")
        query:Create("approver_id", "VARCHAR(50) DEFAULT NULL")
        query:Create("name", "VARCHAR(50) DEFAULT NULL")
        query:Create("description", "TEXT")
        query:Create("status", "INT (3) DEFAULT 1")
        query:Create("rating", "INT (7) DEFAULT 0")
        query:Create("ended_by", "VARCHAR(50) DEFAULT NULL")
        query:Create("ended_by_id", "VARCHAR(50) DEFAULT NULL")
    query:Execute()

    -- Server crashed and an event wasn't able to be ended properly
    -- local query = mysql:Update("ix_events")
    --     query:WhereGT("status", "0")
    --     query:Update("status", 0)
    -- query:Execute()
end)