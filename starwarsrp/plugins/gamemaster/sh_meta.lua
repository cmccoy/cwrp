local meta = FindMetaTable("Player")

function meta:IsGameMaster()
	print(self.gmRank)
	if(self.gmRank == nil) then return false end
	
    if (self.gmRank > 0) then
        return true
    end
    return false
end

function meta:SetGamemasterRank(rank)
    if(rank >= 0 && rank <= 4) then
        self.gmRank = rank
        local query = mysql:Update("ix_players")
            query:Where("steamid", self:SteamID64())
            query:Update("gamemaster", rank)
        query:Execute()
    end
end

function meta:GetGamemasterRank()
    return self.gmRank || 0
end


function meta:SetGamemasterMode(value)
    self.gamemasterMode = value
end

function meta:GetGamemasterMode()
    return self.gamemasterMode || false
end

local function doInvis()
	local remove = true
	for _, v in pairs( player.GetAll() ) do
		local t = v:GetTable()
		if t.invis then
			remove = false
			if v:Alive() and v:GetActiveWeapon():IsValid() then
				if t.invis.wep && v:GetActiveWeapon() ~= t.invis.wep then

					if t.invis.wep and IsValid( t.invis.wep ) then		-- If changed weapon, set the old weapon to be visible.
						t.invis.wep:SetRenderMode( RENDERMODE_NORMAL )
						t.invis.wep:Fire( "alpha", 255, 0 )
						t.invis.wep:SetMaterial( "" )
					end

					t.invis.wep = v:GetActiveWeapon()
				end
			end
		end
	end

	if remove then
		hook.Remove( "Think", "InvisThink" )
	end
end

function meta:SetInvisible(value)
    if (value) then
        self:DrawShadow( false )
		self:SetMaterial( "models/effects/vol_light001" )
		self:SetRenderMode( RENDERMODE_TRANSALPHA )
		self:Fire( "alpha", visibility, 0 )
		self:GetTable().invis = { vis=visibility, wep=self:GetActiveWeapon() }

		if IsValid( self:GetActiveWeapon() ) then
			self:GetActiveWeapon():SetRenderMode( RENDERMODE_TRANSALPHA )
			self:GetActiveWeapon():Fire( "alpha", visibility, 0 )
			self:GetActiveWeapon():SetMaterial( "models/effects/vol_light001" )
			if self:GetActiveWeapon():GetClass() == "gmod_tool" then
				self:DrawWorldModel( false ) -- tool gun has problems
			else
				self:DrawWorldModel( true )
			end
        end
        self.invis = true

        hook.Add( "Think", "InvisThink", doInvis )
    else
        self:DrawShadow( true )
		self:SetMaterial( "" )
		self:SetRenderMode( RENDERMODE_NORMAL )
		self:Fire( "alpha", 255, 0 )
		local activeWeapon = self:GetActiveWeapon()
		if IsValid( activeWeapon ) then
			activeWeapon:SetRenderMode( RENDERMODE_NORMAL )
			activeWeapon:Fire( "alpha", 255, 0 )
			activeWeapon:SetMaterial( "" )
        end
        
        self.invis = false

    end
end

function meta:GetInvisible()
    return self.invis || false
end