local red = Color(255,0,0)
local green = Color(100,255,100)

net.Receive("ixGamemasterEventChat", function(len, pl)
    local name = net.ReadString()
    local text = net.ReadString()

    chat.AddText(red, "[" .. name .. "] ", green, text)
end)

net.Receive("ixSetGmRank", function(len, pl)

    local rank = net.ReadInt(8)

    LocalPlayer().gmRank = rank

end)