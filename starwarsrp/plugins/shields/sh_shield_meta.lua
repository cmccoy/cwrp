 
local PLAYER = FindMetaTable( "Player" )

function PLAYER:HasShield()
    if (self:GetNWBool("shielded", false)) then
        return true
    end
    return false
end

function PLAYER:GetShieldHealth()
    return self.ShieldHealth
end

function PLAYER:GetShieldTime()
    return self.ShieldTime
end

function PLAYER:SetShieldTime(value)
    if(self.ShieldTime <= 1) then
        timer.Simple(1, function()
            self:RemoveShield()
            self.ShieldTime = 0
        end)
    end
    self.ShieldTime = value
end

function PLAYER:RemoveShield()
    self.ShieldHealth = 0
    self.ShieldTime = 0
    self:SetNWBool("shielded", false)
    if(timer.Exists(self:GetName().."_shield_time")) then
        timer.Remove(self:GetName().."_shield_time")
    end
    if(self:Alive()) then
        self:EmitSound("npc/roller/mine/rmine_explode_shock1.wav")
    end
end

function PLAYER:SetShieldValues(int, time)
    if int < 0 then 
        int = 0
    end
    self.ShieldHealth = int
    self.ShieldTime = time || 60
    self:SetNWBool("shielded", true)
    if(timer.Exists(self:GetName().."_shield_time")) then
        timer.Remove(self:GetName().."_shield_time")
    end
    timer.Create(self:GetName() .. "_shield_time", 1, int, function()
        self:SetShieldTime(self:GetShieldTime() - 1)
    end)
end

if CLIENT then

    timer.Create("[IG] Show Shields on clients", 1, 0, function()
        for k, v in pairs(player.GetAll()) do
            if (v:GetNWBool("shielded", false)) then
                local effectdata = EffectData()
                effectdata:SetOrigin( v:GetPos())
                effectdata:SetEntity( v )
                util.Effect( "shield_effect", effectdata, true, true )
            end
        end
    end)

end