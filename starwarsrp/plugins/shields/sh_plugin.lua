local PLUGIN = PLUGIN

PLUGIN.name = "Shield Display"
PLUGIN.author = "Vac"
PLUGIN.description = "Displays a shield effect when someone has an active shield"

ix.util.Include("sh_shield_meta.lua")
ix.util.Include("sv_shield_setup.lua")