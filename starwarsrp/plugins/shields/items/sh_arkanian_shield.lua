
ITEM.name = "Arkanian Energy Shield"
ITEM.model = Model("models/props_wasteland/prison_toiletchunk01f.mdl")
ITEM.description = "Even 2000 years after the designs were pioneered, Arkanian technology remains desirable. When equipped and activated, this forearm shield protects against a variety of combat conditions, though it must be replaced often due to burnout."
ITEM.category = "Shields"
ITEM.price = 30000

ITEM.functions.Equip = {
	sound = "items/medshot4.wav",
	OnRun = function(itemTable)
		
	end
}
