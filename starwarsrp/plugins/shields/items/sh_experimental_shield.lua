
ITEM.name = "Verpine Prototype Shield"
ITEM.model = Model("models/props_wasteland/prison_toiletchunk01f.mdl")
ITEM.description = "Though manufactured by the Verpine, these forearm shields are based on highly modified Arkanian designs. They are must-have items for the professional soldier, though they have to be replaced when the maximum number of activations are expended."
ITEM.category = "Shields"
ITEM.price = 30000

ITEM.functions.Equip = {
	sound = "items/medshot4.wav",
	OnRun = function(itemTable)
		
	end
}
