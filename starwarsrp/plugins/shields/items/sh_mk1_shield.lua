
ITEM.name = "MK1 Shield"
ITEM.model = Model("models/gibs/shield_scanner_gib5.mdl")
ITEM.description = "When equipped, this items project an energy shield around the wearer. The small power source burns out when used, requiring replacement of the entire unit. "
ITEM.category = "Shields"
ITEM.price = 200

ITEM.functions.Equip = {
	OnRun = function(itemTable)
		local client = itemTable.player
		client:EmitSound("items/suitchargeok1.wav")
		client:SetShieldValues(50, 120)
	end
}