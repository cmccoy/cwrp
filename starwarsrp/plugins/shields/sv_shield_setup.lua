hook.Add( "PlayerSpawn", "[IG] Give player object the sheild health attribute", function(pl)
    pl.ShieldHealth = 0
    pl.ShieldTime = 0
    pl:SetNWBool("shielded", false)
end )

hook.Add( "EntityTakeDamage", "[IG] Shield take damage", function( target, dmginfo )
    if (target:IsPlayer()) then
        if ( target:HasShield() ) then
            local shield = target:GetShieldHealth()
            local dmg = dmginfo:GetDamage()
            if(dmg > shield + target:Health()) then
                target:Kill()
                target:RemoveShield()            
            elseif(dmg > shield) then
                dmg = dmg - shield
                target:RemoveShield()
                dmginfo:SetDamage(dmg)
            else
                dmginfo:SetDamage(0)
                local vPoint = target:GetPos() + Vector(0, 0, 32)
                local effectdata = EffectData()
                effectdata:SetOrigin( vPoint )
                effectdata:SetMagnitude(.5)
                effectdata:SetScale(1)
                effectdata:SetRadius(15)
                util.Effect( "Sparks", effectdata )
                target:SetShieldValues(target:GetShieldHealth() - dmg, target:GetShieldTime())
                target:EmitSound("ambient/energy/zap" .. math.random(1, 9) .. ".wav")
            end
        end
    end
end )

hook.Add("PlayerDeath", "[IG] Remove Shield", function(pl)
    if (pl:HasShield()) then
        pl:RemoveShield()
    end
end)