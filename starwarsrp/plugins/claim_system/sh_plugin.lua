local PLUGIN = PLUGIN

PLUGIN.name = "Room claiming"
PLUGIN.author = ""
PLUGIN.description = "Add commands that allow officers to claim rooms."

local rooms = {
    citadel = {claimed = false, claimedBy = "", niceName = "Citadel" },
    sim = {claimed = false, claimedBy = "", niceName = "Sim Room"}
}

ix.util.Include("sv_claim.lua")
ix.util.Include("cl_claim.lua")

ix.command.Add("claim", {
    description = "Claims a room for a reason.",
    arguments = {ix.type.string, ix.type.text},
    OnRun = function(self, client, room, reason)
        local character = client:GetCharacter()
        if (character) then
            if(client.hasClaimed) then
                return client:NotifyLocalized("You already have a room claimed!")
            end

            room = string.lower(room)

            if(!rooms[room]) then return end

            if( reason == "") then 
                return client:NotifyLocalized("You need to enter a reason for claiming!")
            end

            local tblValue = rooms[room]

            if(tblValue && tblValue["claimed"]) then
                return client:NotifyLocalized("Room already claimed by " .. tblValue["claimedBy"])
            end

            local currentRank = character:GetRank()
            local class = rankToClass(currentRank, character:GetFaction())
            if (class && class.canPromote || client:IsStaff()) then
                local niceName = tblValue["niceName"]

                client.hasClaimed = true
                client.claimedRoom = room

                tblValue["claimed"] = true
                tblValue["claimedBy"] = character:GetName()

                client.niceName = niceName

                net.Start("ixClaimText")
                    net.WriteString(character:GetName() .. " claims " .. niceName .. " for " .. reason)
                net.Broadcast()

                client:ChatPrint("You claimed a room and have been given the ability to spawn npcs | props | use toolgun." ..
                 "\nAbusing this privledge will result in either a character wipe or ban.")

                hook.Run("SwrpOnRoomClaimed", client, niceName, reason)
            end
        end
    end
})

ix.command.Add("unclaim", {
    description = "Unclaims your current claimed room.",
    arguments = {},
    OnRun = function(self, client)
        local character = client:GetCharacter()
        if (character) then
            if (client.hasClaimed) then

                local tblValue = rooms[client.claimedRoom]

                tblValue["claimed"] = false
                tblValue["claimedBy"] = ""

                net.Start("ixClaimText")
                    net.WriteString(character:GetName() .. " unclaimed " .. client.niceName)
                net.Broadcast()

                hook.Run("SwrpOnRoomUnclaim", client, niceName)

                local character = client:GetCharacter()
                character:TakeFlags("e")
                character:TakeFlags("n")
                character:TakeFlags("p")
                character:TakeFlags("t")

                client.hasClaimed = nil
                client.claimedRoom = nil
                client.niceName = nil
            end
        end
    end
})

ix.command.Add("unclaimByRoom", {
    description = "Unclaims your current claimed room.",
    arguments = {ix.type.string},
    OnRun = function(self, client, room)
        if (client:IsStaff() && room != "") then
            room = string.lower(room)

            if(room != "simone" && room != "simtwo") then return end

            local tblValue = rooms[room]

            tblValue["claimed"] = false
            tblValue["claimedBy"] = ""
            local niceName = tblValue["niceName"]

            net.Start("ixClaimText")
                net.WriteString(client:GetName() .. " force unclaimed " .. niceName)
            net.Broadcast()

            hook.Run("SwrpOnAdminRoomUnclaim", client, niceName)
        end
    end
})

ix.command.Add("fly", {
    description = "Allows you to fly.",
    arguments = {},
    OnRun = function(self, client, room)
        local character = client:GetCharacter()
        if (character) then
            if (client.hasClaimed) then

                if (string.lower(client.claimedRoom) != string.lower(client:GetArea())) then
                    return "You must be in your claimed room to use this command"
                end

                if client:GetMoveType() == MOVETYPE_FLY then
                    client:SetMoveType(MOVETYPE_WALK)
                    hook.Run("SwrpOnFly", client, false)
                else
                    client:SetMoveType(MOVETYPE_FLY)
                    hook.Run("SwrpOnFly", client, true)
                end
            end
        end
    end
})

function PLUGIN:OnCharacterDisconnect(client, character)
    if (client.hasClaimed) then
        local tblValue = rooms[client.claimedRoom]

        tblValue["claimed"] = false
        tblValue["claimedBy"] = ""

        character:TakeFlags("e")
        character:TakeFlags("n")
        character:TakeFlags("p")
        character:TakeFlags("t")

        net.Start("ixClaimText")
            net.WriteString(character:GetName() .. " has disconnected and " .. client.niceName .. " has been unclaimed.")
        net.Broadcast()

        hook.Run("SwrpOnRoomUnclaimDisconnect", client, niceName)
    end
end