AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
include( 'shared.lua')

util.AddNetworkString("MinecraftSignTextChange")
net.Receive("MinecraftSignTextChange", function( len, pl )
	if(pl.hasClaimed || pl:IsStaff()) then
		local sign = net.ReadEntity()
		local newText = net.ReadString()
		SaveText( nil, sign, { text = newText } )
		pl:NotifyLocalized("Board updated!")

		hook.Run("SwrpOnBoardEdit", pl, newText)
	else
		pl:NotifyLocalized("You must claim a room or be staff to use this board!")
	end
end )

function ENT:Initialize()
	self:SetModel( "models/galactic/me3fix/wall_cover01_l.mdl" )
	self:SetModelScale(.75)
    self:PhysicsInit( SOLID_VPHYSICS )
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid( SOLID_VPHYSICS )   
	self:SetColor(Color(0,0,0,255))
	self:SetMaterial("debug/env_cubemap_model")

	local phys = self.Entity:GetPhysicsObject()
	if (phys:IsValid()) then
		phys:EnableMotion( false ) //freeze the block
		phys:Wake()
	end
	
	self:SetUseType( SIMPLE_USE )
end

function ENT:SpawnFunction( ply, tr )

	if ( !tr.Hit ) then return end

	local SpawnPos = tr.HitPos

	local ent = ents.Create( ClassName )
	ent:SetPos( SpawnPos )
	
	//orient facing the player
	local base = Vector( -1, 0, 0 ) //North vector
	local thevector = SpawnPos - ply:GetPos()
	local angle = GetAngleBetweenVectors( base, thevector )
	ent:SetAngles( Angle( 0, angle, 0) )
	
	ent:Spawn()
	ent:Activate()

	return ent
end

function ENT:OnRemove()
	-- timer.Simple( 0, function()
	-- 	local dissolver = ents.Create( "env_entity_dissolver" )
	-- 	dissolver:SetPos( self:GetPos() )
	-- 	dissolver:Spawn()
	-- 	dissolver:Activate()
	-- 	dissolver:SetKeyValue( "target", self:GetName() )
	-- 	dissolver:SetKeyValue( "magnitude", 100 )
	-- 	dissolver:SetKeyValue( "dissolvetype", 0 )
	-- 	dissolver:Fire( "Dissolve" )
	-- 	dissolver:Remove()
	-- end)
end

function ENT:OnSpawn( ID, hitEntity )
	self.spawned = true
end
