local PLUGIN = PLUGIN

util.AddNetworkString("ixClaimText")

local rooms = {
    citadel = {claimed = false, claimedBy = "", niceName = "Citadel" },
    sim = {claimed = false, claimedBy = "", niceName = "Sim Room"}
}

function PLUGIN:OnPlayerAreaChanged(client, oldID, newID)
    if (client.hasClaimed) then
        local old = string.lower(oldID)
        local new = string.lower(newID)
        local character = client:GetCharacter()
        
        if (rooms[old]) then
            character:TakeFlags("e")
            character:TakeFlags("n")
            character:TakeFlags("p")
            character:TakeFlags("t")
            client:SetNoTarget(false)
            client:SetMoveType(MOVETYPE_WALK)
        elseif (rooms[new]) then
            character:GiveFlags("e")
            character:GiveFlags("n")
            character:GiveFlags("p")
            character:GiveFlags("t")
            client:SetNoTarget(true)
        end
    end
end