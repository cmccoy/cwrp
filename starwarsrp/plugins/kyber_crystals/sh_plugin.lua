local PLUGIN = PLUGIN

PLUGIN.name = "Kyber Crystal Interaction"
PLUGIN.author = "Mr. Sandbox"
PLUGIN.description = "Adds methods to make specific map props have collisions and hooks to +use interact with them. For now, is being used for jedis to harvest crystals for quest."
PLUGIN.schema = "CWRP RP"

ix.util.Include("sv_plugin.lua")
ix.util.Include("cl_plugin.lua")

PLUGIN.ValidKyberCrystalsModels = {
	["models/props_abandoned/crystals/crystal_damaged/crystal_cluster_wall_damaged_huge.mdl"]	=	true,
	["models/props_abandoned/crystals/crystal_default/crystal_cluster_wall_huge_a.mdl"]			=	true,
	["models/props_abandoned/crystals/crystal_default/crystal_cluster_wall_huge_b.mdl"]			=	true,
	["models/props_abandoned/crystals/crystal_default/crystal_cluster_wall_small_a.mdl"]		=	true
}

-- if CLIENT then
-- 	local material = Material("models/debug/debugwhite.vtf")
-- 	local function KyberCollisionDraw(depth, skybox)
-- 		for k, ent in pairs(ents.FindByClass("prop_dynamic")) do
-- 			if PLUGIN.ValidKyberCrystalsModels[ent:GetModel()] then
-- 				local min, max = ent:GetCollisionBounds()
-- 				local ang = ent:GetAngles()
-- 				ang = Angle(0,0,0)
-- 				render.SetMaterial(material)
-- 				render.DrawWireframeBox( ent:GetPos(), ang, min, max, Color(250,250,250), true )
-- 				-- render.DrawWireframeBox(Vector position,Angle angle,Vector mins,Vector maxs,table color,boolean writeZ)
-- 			end
-- 		end
-- 	end
-- 	hook.Add("PostDrawTranslucentRenderables", "Kyber Collision Test", KyberCollisionDraw )
-- end
