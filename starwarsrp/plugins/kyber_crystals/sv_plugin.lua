local PLUGIN = PLUGIN
PLUGIN.CrystalAlwaysInteractable = true --Only for testing as it has no other purpose. True if anyone can use kyber crystals,

--[[ crystal props on rp_anaxes 4/5/2020
	models/props_abandoned/crystals/crystal_damaged/crystal_cluster_wall_damaged_huge.mdl	=	9
	models/props_abandoned/crystals/crystal_default/crystal_cluster_wall_huge_a.mdl	=	35
	models/props_abandoned/crystals/crystal_default/crystal_cluster_wall_huge_b.mdl	=	18
	models/props_abandoned/crystals/crystal_default/crystal_cluster_wall_small_a.mdl	=	77
]]

PLUGIN.KyberCrystalSkinInfo = {
	[0] = { color = Color(250, 0, 0), name = "Red" },
	[1] = { color = Color(0, 0, 205), name = "Blue" },
	[2] = { color = Color(0, 205, 205), name = "Cyan" },
	[3] = { color = Color(0, 205, 0), name = "Green" },
	[4] = { color = Color(255, 125, 0), name = "Orange" },
	[5] = { color = Color(205, 0, 205), name = "Pink" },
	[6] = { color = Color(102, 0, 204), name = "Purple" },
	[7] = { color = Color(0, 205, 0), name = "Yellow" },
	[8] = { color = function() return ColorRand() end, name = "Assorted" }
}
local KyberCrystalColorKey = PLUGIN.KyberCrystalSkinInfo

PLUGIN.KyberCrystalMapIDTable = {
	--long string table is long, figured data folder saving would be unncessary
	["rp_anaxes_impact"] = '{"2618":true,"2594":true,"2602":true,"2770":true,"2778":true,"2754":true,"2762":true,"2738":true,"2746":true,"2722":true,"2730":true,"2714":true,"2690":true,"2698":true,"2675":true,"2627":true,"2611":true,"2619":true,"2595":true,"2603":true,"2803":true,"2779":true,"2707":true,"2715":true,"2691":true,"2699":true,"2684":true,"2660":true,"2668":true,"2628":true,"2612":true,"2620":true,"2596":true,"2604":true,"2804":true,"2772":true,"2756":true,"2764":true,"2740":true,"2748":true,"2724":true,"2732":true,"2708":true,"2716":true,"2700":true,"2677":true,"2685":true,"2669":true,"2629":true,"2613":true,"2621":true,"2597":true,"2605":true,"2589":true,"2805":true,"2773":true,"2757":true,"2741":true,"2709":true,"2693":true,"2678":true,"2662":true,"2670":true,"2630":true,"2614":true,"2622":true,"2598":true,"2606":true,"2590":true,"2814":true,"2782":true,"2766":true,"2742":true,"2750":true,"2726":true,"2734":true,"2718":true,"2702":true,"2671":true,"2631":true,"2615":true,"2623":true,"2599":true,"2607":true,"2591":true,"2775":true,"2783":true,"2759":true,"2767":true,"2727":true,"2711":true,"2719":true,"2695":true,"2672":true,"2680":true,"2664":true,"2624":true,"2632":true,"2608":true,"2616":true,"2592":true,"2600":true,"2800":true,"2784":true,"2768":true,"2776":true,"2752":true,"2736":true,"2744":true,"2728":true,"2704":true,"2712":true,"2688":true,"2696":true,"2673":true,"2681":true,"2665":true,"2625":true,"2633":true,"2609":true,"2617":true,"2593":true,"2601":true,"2801":true,"2785":true,"2753":true,"2761":true,"2737":true,"2745":true,"2729":true,"2705":true,"2713":true,"2689":true,"2697":true,"2674":true,"2682":true,"2666":true,"2626":true,"2610":true}'
}

for k, v in pairs(PLUGIN.KyberCrystalMapIDTable) do --from string to usable table
	PLUGIN.KyberCrystalMapIDTable[k] = util.JSONToTable(v)
end

function PLUGIN:GetMapCrystalTable()
	return PLUGIN.KyberCrystalMapIDTable[game.GetMap()] or {}
end

function PLUGIN:getCrystalColorTable(skin)
	return self.KyberCrystalSkinInfo[skin] or nil
end

function PLUGIN:isCrystal(ent)
	if ent:CreatedByMap() and self:GetMapCrystalTable()[ent:MapCreationID()] then --If entity was made by map, and in our list of map ents
		return true
	else 
		return false 
	end
end

function PLUGIN:GetSavedMapCrystalDataTable()
	local info = file.Read("cwrp_crystals/"..game.GetMap()..".txt")
	if info then return util.JSONToTable(info) end
end

function PLUGIN:DumpCrystalMapIDS()
	self:cacheKyberCrystalMapIDS("cwrp_crystals/")
	print( string.format( "Saved crystals to %s%s.txt", "cwrp_crystals/", game.GetMap() ) )
end

function PLUGIN:CacheKyberCrystalMapIDS(dir)
	local cryIds = {}
	for _, ent in pairs(ents.GetAll()) do
		if not ent:CreatedByMap() then continue end

		local mapID = ent:MapCreationID()
		--mapcreationid of -1 means it's not a map entity
		if not mapID or mapID == -1 then continue end

		if self:isCrystal(ent) then
			cryIds[mapID] = true
		end
	end

	if not file.IsDir("cwrp_crystals", "DATA") then
		file.CreateDir("cwrp_crystals")
	end
	file.Write(dir..game.GetMap()..".txt", util.TableToJSON(cryIds) )
end

--Enables basic collision bounds of the kyber crystals in the cave
function PLUGIN:InitPostEntity()
	for entID, v in pairs( self:GetMapCrystalTable() ) do
		local targetCrystal = ents.GetMapCreatedEntity(entID)
		if not targetCrystal then continue end

		if self:isCrystal(targetCrystal) then
			targetCrystal:SetSolid(SOLID_BBOX)
			targetCrystal:SetUseType(SIMPLE_USE) --Set use type doesn't work for the prop_dynamics, has to be lua entities :(
		end
	end
end


function PLUGIN:PlayerUse(ply, ent)
    if not ent.Kyber_NextUse or ent.Kyber_NextUse < CurTime() then

        if self:isCrystal(ent) && self.CrystalAlwaysInteractable then
            ent.Kyber_NextUse = CurTime() + .5 --Anti spam specifically for crystals

            ply:SetAction("Searching...", 5) -- for displaying the progress bar
            ply:DoStaredAction(ent, function()
                if (math.random(1, 10) == 5) then
                    ply:GetCharacter():GetInventory():Add("broken_crystal")
                    ply:ChatPrint("You found a broken crystal")
                else
                    ply:ChatPrint("You didn't find anything")
                end
            end, 5)

            hook.Call("KyberCrystalUse", nil, ply, plyChar, ent)
        end
    end
end

ix.command.Add("kybertest", {
    description = "command for kyber testing",
    OnCheckAccess = function(self, client)
        if client:IsSuperAdmin() then
            return true
        end
        return false
    end,

	OnRun = function(self, client)
		local ply = client
		local tr = util.TraceLine( {
			start = ply:EyePos(),
			endpos = ply:EyePos() + ply:EyeAngles():Forward() * 500,
			filter = {ply},
		} )

		print("Start Kyber Test")
		local crystal = tr.Entity
		if tr.Hit and IsValid(crystal) and PLUGIN:isCrystal(crystal) then
			print(crystal:GetAngles(), crystal:GetCollisionBounds())
			--Consider using an entity:GetUp(), using that to then rotatearoundaxis on angle change
			local min, max = crystal:GetCollisionBounds()
			local rotateAng = Angle(0,40,0)
				-- min:Rotate(rotateAng)
				-- max:Rotate(rotateAng)
			-- crystal:SetCollisionBounds(min, max)
			print(min, max)
			min, max = crystal:GetRotatedAABB(min, max)

			min = min * .75
			max = max * .75
			crystal:SetCollisionBounds(min, max)
			print(min, max)
			min, max = crystal:GetModelBounds()
			crystal:SetCollisionBounds(min, max)

		end
	end
} )

local kyberCycle = 0
local kyberMax = 8
ix.command.Add("kybertest_cycle", {
    description = "command for kyber testing skins",
    OnCheckAccess = function(self, client)
        if client:IsSuperAdmin() then
            return true
        end
        return false
    end,

	OnRun = function(self, client)
		if not client or not IsValid(client) then return false end

		if timer.Exists(client:GetCreationID().."kyber cycle") then
			timer.Destroy(client:GetCreationID().."kyber cycle")
			return true
		end

		timer.Create(client:GetCreationID().."kyber cycle", .2, 0, function()
			local ply = client
			local tr = util.TraceLine( {
				start = ply:EyePos(),
				endpos = ply:EyePos() + ply:EyeAngles():Forward() * 500,
				filter = {ply},
			} )
			local ent = tr.Entity
			if IsValid(ent) and ent:CreatedByMap() then
				local model = ent:GetModel()
				if model and string.find(model, "crystals/") then
					if ent:GetSkin() < kyberMax then
						ent:SetSkin(ent:GetSkin() + 1)
					else
						ent:SetSkin(0)
					end
					local kyberCol = KyberCrystalColorKey[ent:GetSkin()]
					if kyberCol then
						ply:ChatPrint(ent:GetSkin() .." " .. tostring(kyberCol.name) )
					else
						ply:ChatPrint(ent:GetSkin())
					end
				end
			end
		end)
	end
})

ix.command.Add("kybertest_rave", {
    description = "command for kyber testing skins",
    OnCheckAccess = function(self, client)
        if client:IsSuperAdmin() then
            return true
        end
        return false
    end,

	OnRun = function(self, client)
		if not client or not IsValid(client) then return false end

		if timer.Exists(client:GetCreationID().."kyber rave") then
			timer.Destroy(client:GetCreationID().."kyber rave")
			return true
		end

		timer.Create(client:GetCreationID().."kyber rave", .2, 0, function()
			for k, ent in pairs(ents.GetAll()) do
				if IsValid(ent) and ent:CreatedByMap() then
					local model = ent:GetModel()
					if model and string.find(model, "crystals/") then
						if ent:GetSkin() < kyberMax then
							ent:SetSkin(ent:GetSkin() + 1)
						else
							ent:SetSkin(0)
						end
						
					end
				end
			end
		end)
	end
})

ix.command.Add("kybertest_setskin", {
    description = "command for kyber testing skins",
    OnCheckAccess = function(self, client)
        if client:IsSuperAdmin() then
            return true
        end
        return false
    end,
	OnRun = function(self, client, args)
		if not client or not IsValid(client) then return false end

		local ply = client
		local tr = util.TraceLine( {
			start = ply:EyePos(),
			endpos = ply:EyePos() + ply:EyeAngles():Forward() * 500,
			filter = {ply},
		} )
		local ent = tr.Entity

		if IsValid(ent) and ent:CreatedByMap() then
			local model = ent:GetModel()
			if model and string.find(model, "crystals/") then
				
				ent:SetSkin(args[1] or ent:GetSkin())

				local kyberCol = KyberCrystalColorKey[ent:GetSkin()]
				if kyberCol then
					ply:ChatPrint(ent:GetSkin() .." " .. tostring(kyberCol.name) )
				else
					ply:ChatPrint(ent:GetSkin())
				end
			end
		end
	end
})

-- PrintTable(PLUGIN)