RXF4_Adjust = {} 
GeneralDB = {} 
OfficerDB = {} 
RuleDB = {} 
GamemasterDB = {} 
StaffDB = {} 

function ixRule_Title(Text) 
	table.insert(RuleDB,{Type="Title",Text=Text}) 
end
function ixRule_Text(Text) 
	table.insert(RuleDB,{Type="Text",Text=Text}) 
end

function ixGamemaster_Title(Text) 
	table.insert(GamemasterDB,{Type="Title",Text=Text}) 
end
function ixGamemaster_Text(Text) 
	table.insert(GamemasterDB,{Type="Text",Text=Text}) 
end

function ixStaff_Title(Text) 
	table.insert(StaffDB,{Type="Title",Text=Text}) 
end
function ixStaff_Text(Text) 
	table.insert(StaffDB,{Type="Text",Text=Text}) 
end

function ixOfficer_Title(Text) 
	table.insert(OfficerDB,{Type="Title",Text=Text}) 
end
function ixOfficer_Text(Text) 
	table.insert(OfficerDB,{Type="Text",Text=Text}) 
end

function ixGeneralCom_Title(Text) 
	table.insert(GeneralDB,{Type="Title",Text=Text}) 
end
function ixGeneralCom_Text(Text) 
	table.insert(GeneralDB,{Type="Text",Text=Text}) 
end
			
	-- VIPs
		-- VIP Group. ( I think  you need ULX anyway. these group are able to access VIP Shop or VIP Job )
			RXF4_Adjust.VIPGroup = {"owner","superadmin","admin"}

		-- VIP Shop ( if you dont want to make VIP shop, set ' true ' to ' false ' )
			RXF4_Adjust.Main_EnableVIPShop = true 
			
		-- VIP Job ( if you dont want to make VIP Job, set ' true ' to ' false ' )
			RXF4_Adjust.Main_EnableVIPJob = true
			
		
	
	-- Appearance
		-- Main Screen Size
			RXF4_Adjust.Main_Size_X = 0.8 -- 1 Mean 100% fit to your screen. screen will cover your screen. if you want half size. set this to 0.5
			RXF4_Adjust.Main_Size_Y = 0.7 -- 1 Mean 100% fit to your screen. screen will cover your screen. if you want half size. set this to 0.5
		-- Main Screen Main Text.
			RXF4_Adjust.Main_MainText = "Impact Networks CWRP"
			
			
	-- Menu Panel
		-- HTML Panel
			-- Set home url
				RXF4_Adjust.HTML_URL = "www.google.com"


	-- General Commands
		ixGeneralCom_Title("/join")
		ixGeneralCom_Text[[Requires: Battalion Name
						Joins the specified battalion]]
		ixGeneralCom_Title("/leave")
		ixGeneralCom_Text[[Requires: None
						Leaves your current battalion]]

	-- Officer Commands
		ixOfficer_Title("/claim")
		ixOfficer_Text[[Requires: Room Name | Reason
						Default values: sim | citadel
						Claims the specified room for the specified reason
						Allows you to edit the room board and spawn entities/props in the room
						Must be unclaimed when done]]
		ixOfficer_Title("/unclaim")
		ixOfficer_Text[[Requires: None
						Unclaims your currently claimed room]]
		ixOfficer_Title("/invite")
		ixOfficer_Text[[Requires: Character name
						Invites the specified character to your battalion]]
		ixOfficer_Title("/kick")
		ixOfficer_Text[[Requires: Character name
						Kicks the specified character from your battalion]]
		ixOfficer_Title("/kickByID")
		ixOfficer_Text[[Requires: Clone ID
						Kicks the specified clone ID from your battalion]]
		ixOfficer_Title("/promote")
		ixOfficer_Text[[Requires: Character name
						Promotes the specified clone]]
		ixOfficer_Title("/demote")
		ixOfficer_Text[[Requires: Character name
						Demotes the specified clone to their previous rank]]
		ixOfficer_Title("/xo")
		ixOfficer_Text[[Requires: Character name
						Promotes the specified clone to Executive Officer]]
	-- Staff Commands
		ixStaff_Title("/sit")
		ixStaff_Text[[Sends you to the sit room]]
		ixStaff_Title("/leaveSit")
		ixStaff_Text[[Leaves the sit room]]
		ixStaff_Title("/unclaimByRoom")
		ixStaff_Text[[Requires: Room Name
						Default values: sim | citadel
						Forcefully unclaims a claimed room]]
		ixStaff_Title("/commander")
		ixStaff_Text[[Requires: Character name | Battalion name
						Promotes the specified clone to Commander of the specified battalion]]
		ixStaff_Title("/promoteCommanderByID")
		ixStaff_Text[[Requires: STEAM64ID | Battalion
					Promotes the specified player to commander of specified battalion]]
		ixStaff_Title("/clearCommander")
		ixStaff_Text[[Requires: Battalion name
					Demotes all commanders from specified battalion]]

	-- Custom Rule ( You may know what ' RXFR_Rule_AddTitle ' and ' RXF4_Rule_AddText '  thing does )
		ixRule_Title("Rule")
		ixRule_Text[[ This is Rule 1
							Hmmmmmmm.....
							Dont Kill Ohter!
							Dont Spam!
							Have Fun!]]
		ixRule_Title("Rule 2")
		ixRule_Text[[ RocketMania's Black & Blue Style F4 Menu
							 Available at Coderhire.com
							Thank you!]]

		-- Gamemaster Commands
		ixGamemaster_Title("/gamemasterMenu")
		ixGamemaster_Text[[Requires: None
							Returns a list of all gamemasters and information]]
		ixGamemaster_Title("/startEvent")
		ixGamemaster_Text[[Requires: None
							Opens a panel to begin an event]]
		ixGamemaster_Title("/endEvent")
		ixGamemaster_Text[[Requires: None
							Ends the currently running event]]
		ixGamemaster_Title("/defcon")
		ixGamemaster_Text[[Requires: Number
							Sets the DEFCON to the specified number]]
		ixGamemaster_Title("/gamemasterMode")
		ixGamemaster_Text[[Requires: None
							Puts your character into gamemastermode
							Allows you to access physgun | toolgun | spawn entities
							Type it again to exit]]
		ixGamemaster_Title("/gamemasterBring")
		ixGamemaster_Text[[Requires: Character name
							Brings the specified player to you]]
		ixGamemaster_Title("/gamemasterGoto")
		ixGamemaster_Text[[Requires: Character name
							Goes the specified player to you]]
		ixGamemaster_Title("/gamemasterGodMode")
		ixGamemaster_Text[[Requires: None
							Enables godmode on you]]
		ixGamemaster_Title("/gamemasterCloak")
		ixGamemaster_Text[[Requires: None
							Turns your character invisible]]
		ixGamemaster_Title("/gamemasterSetModel")
		ixGamemaster_Text[[Requires: Model-Name | OPTIONAL: Character name
							No target: Sets your character model to the specified model
							With target: Sets target's character to model specified]]
		ixGamemaster_Title("/gamemasterSetOwnName")
		ixGamemaster_Text[[Requires: Name
							Sets your characters name to specified text.
							Should automatically fix if you disconnect]]
		ixGamemaster_Title("/gamemasterSetPlayerName")
		ixGamemaster_Text[[Requires: Target | Name
							Sets target's name to specified text.
							Should automatically fix if they disconnect]]
		ixGamemaster_Title("/gamemasterSetHealth")	
		ixGamemaster_Text[[Requires: Number
							Sets your health to the number specified]]
		ixGamemaster_Title("/gamemasterNoclip")	
		ixGamemaster_Text[[Requires: None
							Enables noclip mode for you]]
		ixGamemaster_Title("/gamemasterScale")	
		ixGamemaster_Text[[Requires: Number | OPTIONAL : Character name
							No target: Scales your character model to number, default scale is 1
							With target: Scales your target's model to number, default scale is 1]]