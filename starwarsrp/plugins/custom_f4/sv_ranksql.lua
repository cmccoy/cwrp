util.AddNetworkString("ixBattalionMenu")
util.AddNetworkString("ixOpenBattalionMenu")
util.AddNetworkString("ixKickPlayer")
util.AddNetworkString("ixOpenJoinBattalionMenu")

net.Receive("ixKickPlayer", function(len, pl)
    local name = net.ReadString()
    local ID = net.ReadString()
    ix.command.Run(pl, "kickByID", {ID})
end)

net.Receive("ixBattalionMenu", function(len, pl)
    if(pl.nextQueryTime == nil || pl.nextQueryTime < os.time()) then
        local index = net.ReadInt(8)
        local fac = ix.faction.indices[index]

        local query = mysql:Select("ix_characters")
            query:Select("name")
            query:Select("rank")
            query:Select("faction")
            query:Select("last_join_time")
            query:Select("model")
            query:Select("id")
            query:Where("faction", fac.uniqueID)
            query:OrderByDesc("rank")
            query:Limit(100)
            query:Callback(function(result)
                net.Start("ixBattalionMenu")
                    net.WriteTable(result)
                    net.WriteInt(index, 8)
                net.Send(pl)
            end)
        query:Execute()

        pl.nextQueryTime = os.time() + 2
    end
end)

function retrieveGmData(client)
    if (client:IsGameMaster()) then
        local query = mysql:Select("ix_characters")
            query:WhereGT("gamemaster", "0")
            query:Select("gamemaster_data")
            query:Select("gamemaster")
            query:Callback(function(result)
                if  (istable(result) and #result > 0 and result[1].gamemaster_data != nil) then
                    local totalGM = 0
                    for k, v in pairs(player.GetAll()) do
                        if(v:IsGameMaster()) then
                            totalGM = totalGM + 1
                        end
                    end
                    net.Start("OpenGmMenu")
                        net.WriteTable(result)
                        net.WriteInt(totalGM , 8)
                    net.Send(client)
                else
                    net.Start("OpenGmMenu")
                        net.WriteTable({})
                    net.Send(client)
                end
            end)
        query:Execute()
    end
end