local PLUGIN = PLUGIN

PLUGIN.name = "Custom F4 Menu"
PLUGIN.author = "Vac"
PLUGIN.description = ""


ix.util.Include("sh_config.lua")
ix.util.Include("sv_font_include.lua")
ix.util.Include("sv_ranksql.lua")

if SERVER then
    util.AddNetworkString("ixOpenCustomMenu")
end

hook.Add("ShowSpare2", "[IG] Show Menus", function(pl)
    net.Start("ixOpenCustomMenu")
    net.Send(pl)
end)