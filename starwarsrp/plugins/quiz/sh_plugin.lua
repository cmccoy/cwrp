PLUGIN.name = "Quiz"
PLUGIN.author = "Vac"
PLUGIN.desc = "A clone whitelisting quiz"
PLUGIN.players = {}

ix.util.Include("sh_config.lua")
ix.util.Include("cl_quiz.lua")

if SERVER then
	util.AddNetworkString("ixCloneWhitelist")
	util.AddNetworkString("ixOpenQuiz")
end

function PLUGIN:SaveData()
	local data = {}

	for _, entity in ipairs(ents.FindByClass("ix_whitelister")) do
		local bodygroups = {}

		for _, v in ipairs(entity:GetBodyGroups() or {}) do
			bodygroups[v.id] = entity:GetBodygroup(v.id)
		end

		data[#data + 1] = {
			name = entity:GetDisplayName(),
			description = entity:GetDescription(),
			pos = entity:GetPos(),
			angles = entity:GetAngles(),
			model = entity:GetModel(),
		}
	end

	self:SetData(data)
end

function PLUGIN:LoadData()
	for _, v in ipairs(self:GetData() or {}) do
		local entity = ents.Create("ix_whitelister")
		entity:SetPos(v.pos)
		entity:SetAngles(v.angles)
		entity:Spawn()

		entity:SetModel(v.model)
		entity:SetSkin(v.skin or 0)
		entity:SetSolid(SOLID_BBOX)
		entity:PhysicsInit(SOLID_BBOX)

		local physObj = entity:GetPhysicsObject()

		if (IsValid(physObj)) then
			physObj:EnableMotion(false)
			physObj:Sleep()
		end

		entity:SetDisplayName(v.name)
		entity:SetDescription(v.description)
	end
end

ix.char.RegisterVar("passed", {
    field = "passed",
    fieldType = ix.type.bool,
	default = 0,
	isLocal = true,
    bNoDisplay = true,
	OnGet = function(self, default)
		return self.vars.cttest || 0
    end
})

net.Receive("ixCloneWhitelist", function(len, client)
	if (!client) then return end
	local result = net.ReadBool()
	-- Oh wow you read the lua and saw you can just spoof this message you're a pro
	if (result) then
		local character = client:GetCharacter()
		if (character) then
			if(character:GetPassed() == 0 && character:GetFaction() == FACTION_RECRUIT) then
				client:SetWhitelisted(FACTION_CLONE, true)
				client:NotifyLocalized("You passed the quiz and have been made a Clone Trooper!")
				character:SetFaction(FACTION_CLONE)
				SetupName(character)
				character:SetPassed(1)
				local loadout = ix.plugin.Get("loadout_selection").defaultCloneLoadout || {}
				for category, entclass in pairs(loadout) do
					character:UnlockLoadoutWeapon(entclass)
				end
				character:EquipNewLoadout(client, ix.plugin.Get("loadout_selection").defaultRecruitLoadout, ix.plugin.Get("loadout_selection").defaultCloneLoadout)
				client:Give("tfa_swch_dc15a")
			else
				client:NotifyLocalized("You already passed the quiz!")
			end
		end
	else
		client:NotifyLocalized("You didn't pass the quiz")
	end
end)