local PANEL = {}
function PANEL:Init()
	self:SetPos(ScrW() * 0.250, ScrH() * 0.125)
	self:SetSize(700, 400)
	self:MakePopup()
	self:ShowCloseButton(false)
	self:SetDraggable(false)
	self:SetDrawOnTop(true)
	self:SetTitle("Whitelist quiz")
	
	local panel = self:Add("DScrollPanel")
	panel:Dock(FILL)

	local answers = {}	
	for k, question in ipairs(ix.config.quiz.questions) do
		answers[k] = {}

		local text = panel:Add("DLabel")
		text:Dock(TOP)
		text:DockMargin(4, 0, 4, 0)
		text:SetFont("ixWeaponLockerFont_Small")
		text:SetText(question.question)
		text:SetTall(30)
		text.Paint = function(self, w, h)
			draw.RoundedBox(0, 0, 0, w, h, Color(100,100,200))
		end

		local options = panel:Add("DComboBox")
		options:Dock(TOP)
		options:DockMargin( 4, 0, 4, 10 )
		options:SetValue(question.text and question.text or ix.config.quiz.defaultText)		
		options.OnSelect = function(panel, index, value)
			answers[k].correct = question.correct == index
			answers[k].selected = true
		end

		for _, option in ipairs(question.options) do
			options:AddChoice(option)
		end
	end

	local submit = self:Add("DButton")
	submit:Dock(BOTTOM)
	submit:DockMargin(0, 5, 0, 0)
	submit:SetText("Submit quiz")
	submit.DoClick = function()
		for _, answer in pairs(answers) do
			answer.correct = answer.correct or false
			answer.selected = answer.selected or false

			if (!answer.correct or !answer.selected) then
				net.Start("ixCloneWhitelist")
					net.WriteBool(false)
				net.SendToServer()
				self:Close()
				return
			end		    		
		end

		net.Start("ixCloneWhitelist")
			net.WriteBool(true)
		net.SendToServer()

		self:Close()
	end
end
vgui.Register("ix_CloneQuiz", PANEL, "DFrame")

net.Receive("ixOpenQuiz", function(len, client)
	ix.gui.data = vgui.Create("ix_CloneQuiz")
end)