-- Config table for the quiz plugin.
ix.config.quiz = {}
-- Questions for the quiz plugin.
ix.config.quiz.questions = {
	[1] = {
		question = "Which rank is highest?",
		options = {
			[1] = "Executive Officer",
			[2] = "Major",
			[3] = "SFC",
			[4] = "Commander",
		},
		correct = 4
	},
	[2] = {
		question = "What does defcon 3 mean?",
		options = {
			[1] = "Protect engine room, brig and command area.",
			[2] = "Report to battlestations",
			[3] = "High alert patrol the base",
		},
		correct = 2
	},
	[3] = {
		question = "Which way would you be facing after doing Left, Right, About",
		options = {
			[1] = "Left",
			[2] = "Right",
			[3] = "About",
			[4] = "Front"
		},
		correct = 3
	},
	[4] = {
		question = "What is the golden rule?",
		options = {
			[1] = "RDM as soon as you get whitelisted",
			[2] = "Never shoot another brother",
			[3] = "Scream in voice chat",
			[4] = "Become a commander"
		},
		correct = 2
	},
	[5] = {
		question = "At what rank do you salute to?",
		options = {
			[1] = "2ndLT+",
			[2] = "SGT+",
			[3] = "CPL+",
			[4] = "WO+"
		},
		correct = 1
	},
	[6] = {
		question = "What does PTS stand for?",
		options = {
			[1] = "Player To SuperAdmin",
			[2] = "Plug The Snorkel",
			[3] = "Permission To Speak",
			[4] = "Play The Server"
		},
		correct = 3
	},
	[7] = {
		question = "What does AOS stand for?",
		options = {
			[1] = "Admin Official Sit",
			[2] = "Attack On Sight",
			[3] = "Alpha On Sight",
			[4] = "Arrest On Sight"
		},
		correct = 4
	},
}
-- The default text to be shown for questions which a player have not yet selected an answer for.
ix.config.quiz.defaultText = "Select one"
-- How wide the quiz menu is. This is a ratio for the screen's width. (0.5 = half of the screen's width)
ix.config.quiz.menuWidth = 0.5
-- How tall the quiz menu is. This is a ratio for the screen's height. (0.5 = half the screen's height)
ix.config.quiz.menuHeight = 0.75

