--[[
	Weapon HUD Replacement
	Copyright 2017 Torque Software (www.torquesoft.com)
]]--

if( CLIENT ) then return end

-- shorthand
local tbl = ix.weaponHud

--[[
	Analytics ConVar
	Comment out if you wish to not provide information that you have this addon running
	It helps me determine how popular each script is
--]]
local function createAnalytics()
	if( not ConVarExists( "ix_addons" ) ) then
		CreateConVar( "ix_addons", "weaponmenu", { FCVAR_UNLOGGED, FCVAR_ARCHIVE } );
		return 
	end
	
	local c = GetConVar( "ix_addons" ):GetString() or ""
	
	if( not c:find( "weaponmenu" ) ) then
		local updateString = c .. ";weaponmenu"
		GetConVar("ix_addons"):SetString( updateString );
	end
end
createAnalytics();

--[[
	This generates a GUID unique to each server
	So slot configurations don't conflict with other servers running this script
--]]
local function createSlotConfig()
	local q = sql.Query( "CREATE TABLE IF NOT EXISTS ix_weaponhud (guid INTEGER PRIMARY KEY);" );
	
	if( q and q == false ) then
		tbl.print( "There was a problem creating the SQL table. Error: '" .. sql.LastError() .. "'" );
		return
	end
	
	local q = sql.Query( "SELECT GUID FROM ix_weaponhud" );
	
	if( q and q == false ) then
		tbl.print( "There was a problem obtaining Server GUID. Error: '" .. sql.LastError() .. "'" );
		return
	end
	
	if( not q ) then
		local guid = util.CRC( GetHostName() .. util.DateStamp() );
		tbl.print( "Generated GUID for this Server '" .. guid .. "'" );
		
		local q = sql.Query( "INSERT INTO ix_weaponhud (guid) VALUES(" .. guid .. ")" );
	
		if( q and q == false ) then
			tbl.print( "There was a problem inserting generated GUID Error: '" .. sql.LastError() .. "' GUID: '" .. guid .. "'" );
			return
		end
		
		tbl.guid = guid
		tbl.slotFile = "ix/weaponhud/slot_config_" .. guid .. ".txt"
		
		-- replicated, client will know this servers GUID
		-- needed, so the client can load the correct slot config file
		GetConVar( "ix_weaponhud_guid"):SetString( guid );
		
		return
	end
	
	local guid = q[1]["guid"]
	local fileName = "ix/weaponhud/slot_config_" .. guid .. ".txt"
	
	tbl.guid = guid
	tbl.slotFile = fileName
	
	-- replicated, client will know this servers GUID
	-- needed, so the client can load the correct slot config file
	GetConVar( "ix_weaponhud_guid"):SetString( guid );
	
	if( file.Exists( fileName, "DATA" ) ) then
		tbl.print( "Found slot configuration: " .. fileName );
	end
end
createSlotConfig();

--[[
	Received when a player requests or sends a modified slot configuration
--]]
net.Receive( "ix_w_slotconfig", function( length, player )
	if( not IsValid( player ) or not player:IsPlayer( player ) ) then
		return
	end
	
	CAMI.PlayerHasAccess( player, tbl.config.priv, function( hasAccess, reason )
		if( hasAccess == true ) then
			if( not player:ixNetCooldown() ) then
				return
			end
			
			local bit = net.ReadBit() or 0
			
			-- we're sending over our configuration
			if( bit == 0 ) then
				local json = net.ReadString();
				tbl.print( "Received slot data from player '" .. player:GetName() .. "' length: " .. length );
				
				local t = util.JSONToTable( json );
				if( not t or #t < 1 ) then
					ErrorNoHalt( "failed to convert json into table, please submit a support ticket. json: '" .. json  .. "'" );
					return
				end
				
				if( not tbl.slotFile ) then
					ErrorNoHalt( "no slot file has been generated, check for SQL errors and submit a support ticket" );
					return
				end
				
				file.Write( tbl.slotFile, json );
				tbl.print( "Wrote config file '" .. tbl.slotFile .. "'" );
				
				-- !TODO!
				-- Submit this data to all connected players now?
				
			-- we want the servers configuration
			elseif( bit == 1 ) then
				if( not tbl.slotFile ) then
					ErrorNoHalt( "Unable to obtain server slot file (is nil) check for SQL errors." );
					return
				end
				
				local fileExists = file.Exists( tbl.slotFile, "DATA" );
				
				net.Start( "ix_w_slotconfig" );
				net.WriteBit( 1 ); -- 1 = have access
				net.WriteBool( fileExists );
				
				if( fileExists ) then
					local fileData = file.Read( tbl.slotFile );
					local dataLen = string.len( fileData );
					
					if( dataLen >= 65533 ) then
						ErrorNoHalt( "Slot configuration too large to send to client, please submit a support ticket. Size: '" .. datalen .. "'" );
						return 
					end
					
					net.WriteString( fileData ); -- WriteString() contains less data than WriteTable()
				end
				net.Send( player );
			end
		else
			if( player.ixNotified ) then return end -- don't let us send spam
			
			net.Start( "ix_w_slotconfig" );
				net.WriteBit( 0 ); -- 0 = no access
			net.Send( player );
			
			player.ixNotified = true
		end
	end )
end )
