local PLUGIN = PLUGIN

PLUGIN.name = "Custom Weapon Selection"
PLUGIN.author = "Vac"
PLUGIN.description = "Sets up custom weapon selection for players."

ix.weaponHud = ix.weaponHud or {}

ix.util.Include("sh_cami.lua")
ix.util.Include("sh_config.lua")
ix.util.Include("sh_util.lua")
ix.util.Include("cl_settings.lua")
ix.util.Include("cl_themes.lua")
ix.util.Include("cl_weaponhud.lua")
ix.util.Include("sv_weaponhud.lua")

local files, dir = file.Find( "themes/*.lua", "LUA" );
for k, v in pairs( files ) do
    ix.util.Include( "themes/" .. v );
end

if SERVER then
    util.AddNetworkString( "ix_w_slotconfig" );
end