--[[
	Blank theme. 
	This contains all the functions and information avaiable to themes.
--]]

-- shorthand
local tbl = ix.weaponHud
local theme = {} -- MAKE SURE THIS IS LOCAL

-- called when the theme is being ran for the first time
-- this is called only once
function theme.begin( self )
	--[[
		self = our local theme table
	--]]
	
	-- overrides default config stuff here
	-- tbl.config.selectColor 	=  { r = 0, g = 0, b = 0, a = 150 }
	-- tbl.config.showTitles 	= false
	
	-- allow clients to modify some aspects of this theme (such as colors)
	--self.allowCustomize = false
	
	-- pre-opening the main parent panel
	-- this contains all the sub panels (slots)
	-- return FALSE to disallow the panel from being opened
	--self.open = function( panel ) end
	
	-- the main parent panel played the animation and is fully "opened"
	-- all sub panels have been created.
	--self.opened = function( panel ) end
	
	-- when the drawer begins to close
	-- this does NOT guarantee the panel will be destoryed (user can re-open mid closing)
	--self.closing = function() end
	
	-- tooltip being drawn
	-- t=
	--	
	-- return FALSE to stop the tooltip from showing
	--self.tooltip = function( t ) end
	
	-- slot header (where the titles/slot number are located in)
	-- return FALSE to prevent the background from drawing (things such as titles, and slot number will still draw)
	-- panel	= the entire slot panel
	-- slotId	= the slot number
	--self.slotHeader = function( panel, slotId ) end
	
	-- painting the selected box
	-- return FALSE to prevent the background from drawing (things such as titles, and slot number will still draw)
	-- panel	= the entire slot panel
	-- slotId	= the slot number
	--self.selectedPaint = function( panel, x, y, w, h, slotId ) end
	
	-- the PaintOver() function from the main parent panel
	-- this allow you to draw on top of everything
	-- totalWidth 	= total width of all the slots + padding
	-- beginXPos	= the beginning x position for the first slot, useful when wanting to align images
	--self.masterPaint = function( panel, width, height, totalWidth, beginXPos ) end
	
	--self.closing = function( panel ) print("closing, with animation") end
	--self.tooltip = function( tipTable ) print("drawing tool tip") end
end

tbl.themes.add( "default", theme );