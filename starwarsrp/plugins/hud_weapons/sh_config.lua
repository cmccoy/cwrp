--[[
	Configure your custom settings here
	You can also configure these settings with the in-game menu. When doing so, it will overwrite the configuration here.
--]]
ix.weaponHud.config = {
	-- maximum number of slots
	-- hl2 by default uses 6, so this should never be changed, unless using custom menus
	-- be sure to match your maxSlots with how many slots your custom menu has
	maxSlots = 6,
	
	-- adjust the total scale of everything (smaller number = smaller size)
	-- PLEASE NOTE:
	--			What looks good on YOUR resolution, might not look good on others!
	--	Recommend leaving this at one. If something isn't fitting, please submit a screenshot support ticket
	scale = 1,
	
	-- if a slot has too many weapons to fit on the screen
	-- should we position the weapon into the next slot?
	autoSlot = true,
	
	-- when a weapon doesn't have a layout order, or SlotPos
	-- it will automatically use this slot (by default (0) means the last / largest slot)
	slotOverflow = 0,
	
	-- this forces all HL2 (default weapons) to stay in their original slots
	-- even with a custom layout, hl2 weapons will still position themselves in the default order (if enabled)
	hl2Slots = false,
	
	-- enable themes
	-- if running on a server with many addons, may recommend disabling themes for a performance boost
	useThemes = false,
	
	-- force a theme on players
	-- leave empty to allow players to select their own theme(s) (make sure useThemes is TRUE!)
	forceTheme = "",
	
	-- should we be using the custom menu table?
	-- if you configured your menu in-game, make sure "ix_weaponhud_serverslots" is set to 1
	-- that will ignore useCustom
	useCustom = true,
	
	-- display context menu settings icon?
	-- as well as display the admin slot editor within the sandbox menu
	-- this does nothing on gamemodes that don't have the spawnmenu
	sandboxMods = true,
	
	-- the mouse wheel sensitivity
	-- by default it's easy to quickly scroll past the weapon you wish to select
	-- "high" 	= real time, no delay
	-- "medium" = slightly slower response
	-- "low" 	= even slower
	mouseSensitivity = "high",
	
	-- can we show the menu while being in a vehicle?
	inVehicle = true,
	
	-- how long (in seconds) to keep the drawer displayed
	-- set to zero (0.0) to keep it open until closed
	showTime = 1.5,
	
	-- should the weapon names be bold (capitalized)
	bold = true,
	
	-- titles for the default (hl2) weapon drawer
	-- layout is slot order (1, 2, 3, 4, 5, 6)
	-- this MUST match the amount set from "maxSlots"
	-- if using a custom menu, this table will be ignored. You can set your titles within the customMenu table
	titles = { "MELEE", "PRIMARY", "SECONDARY", "GRENADE", "UTILITY", "TOOLS" },
	
	-- EXAMPLE (if you wish to hide the titles, use showTitles=false)
	--titles = { "", "", "", "", "", "" },
	
	-- EXAMPLE
	--titles = { "-", "-", "-", "-", "-", "-" },
	
	-- show titles?
	-- this shows your custom menu title, or the default titles above
	showTitles = true,
	
	-- show information (author, purpose, etc..) about SWEPs
	showTooltip = true,
	
	-- show weapon icons
	-- this will also draw the default HL2 weapon sprites
	showIcons = true,
	
	-- indicates if we should show the ammo count
	showAmmo = true,
	
	-- show error and other useful messages in the console?
	-- this includes both on the server and client. Recommend to leave enabled for any issues
	showMessages = true,
	
	-- how far from the top of the screen, to position the drawer
	offset = 25,
	
	-- padding between slot elements (0-25 is recommended)
	padding = 10,
	
	-- collapsed height
	-- affected by screen resolution, recommend to leave alone
	collapsedHeight = 50,
	
	-- expanded height
	-- affected by screen resolution, recommend to leave alone
	expandedHeight = 160,
	
	-- the slot "headers" color
	slotColor = Color( 0, 0, 0, 150 ),
	
	-- main color of the drawer
	mainColor = Color( 0, 0, 0, 150 ),
	
	-- color of the current selection
	selectColor = Color( 0, 0, 0, 225 ),
	
	-- ammo text color
	ammoColor = Color( 255, 255, 51, 200 ),
	
	-- error text color
	-- such as "NO AMMO"
	errorColor = Color( 255, 51, 51, 255 ),
	
	-- title text color
	titleColor = Color( 255, 255, 51, 255 ),
	
	-- color of the hl2 weapon sprites/icons
	hl2WeaponColor = Color( 255, 255, 255, 255 ),
	
	-- how long it takes for the drawer to close/fade out
	fadeOutTime = 0.25,
	
	-- how long it takes for the drawer to close/fade out
	fadeInTime = 0.25,
	
	-- in the first verison(s) (1.0-2.9)
	-- there was a "pull down" animation, set to true to re-enable it
	legacyPulldown = false,
	
	-- sound played when navigation weapons
	-- use "" for no sound
	navSound = "common/wpn_moveselect.wav",
	
	-- sound played when a weapon is selected
	-- use "" for no sound
	selectedSound = "common/wpn_select.wav",
	
	-- the privilege a user needs to use the admin features
	-- if not using an Admin Mod that supports CAMI, this defaults to IsSuperAdmin()
	priv = "ix_weaponhud",
}

--[[
	Translations
--]]
ix.weaponHud.lang = {
	["asking_server"] 	= "Requesting information from server, please wait...",
	["access_denied"] 	= "You do not have sufficient access",
	["too_soon"]		= "Please wait before requesting slot configuration again..",
	["sending"]			= "Submitting configuration to server..",
	["saved"]			= "Weapon Drawer settings saved!"
}

--[[
	Custom Icons for Wepaons
	---------------------
	This allows you to setup custom icons (ignoring the default SWEP settings) for each weapon
	["weapon_class_name_entity"] = "path/to/material"
--]]
ix.weaponHud.icons = {
	--["weapon_crowbar"] = "gmod/scope", 					-- VTF/VMT MATERIAL
	--["weapon_crowbar"] = "entities/weapon_crowbar.png", 	-- PNG
}

-- Your custom menu, if required
-- Leave empty to use the standard HL2 layout
ix.weaponHud.customMenu = {
}
--[[
	Custom Menu Layout
	-----------------
	You can configure your slot names, and what weapons will be located in that slot.
	The order in which you add the weapons, is the order that it will appear in the weapon drawer
	
	Useful for other gamemodes besides Sandbox (like DarkRP).
	
	Any weapons that are not listed in a slot, will automatically populate the last/largest slot.
	Make sure you have enough slots to match "maxSlots" count!
--]]

ix.weaponHud.customMenu = {
	{ 
		title = "MELEE",
		"weapon_fists",
		"weapon_vibrosword"
	},
	{
		title = "PRIMARY",
		"tfa_swch_dc15s",
		"tfa_swch_dc15a",
		"tfa_sw_repshot",
		"tfa_swch_dc17m_br",
		"tfa_swch_z6",
		"tfa_sw_repsnip",
		"tfa_swch_alphablaster"
	},
	{
		title = "SECONDARY",
		"tfa_swch_dc17",
		"tfa_sw_dc17dual",
		"tfa_swch_de10",
		"tfa_sw_dual_de10"
	},
	{
		title = "GRENADE",
		"weapon_frag_grenade",
		"weapon_bacta_grenade"
	},
	{
		title = "UTILITY",
		"ix_gonk_gun",
		"weapon_policebaton",
		"ix_unarrest_stick",
		"taser",
		"weapon_jew_stimkit",
		"weapon_swrc_det"
	},
	{
		title = "TOOLS",
		"weapon_physcannon",
		"gmod_tool"
	}
}

--[[
-- EXAMPLE 1

ix.weaponHud.customMenu = {
	{ 
		title = "SLOT 1",
		"weapon_crowbar",
		"weapon_physgun",
		"weapon_physcannon"
	},
	
	{
		title = "SLOT2",
		"weapon_pistol",
		"weapon_smg1",
		"weapon_ar2"
	},
	
	{
		title = "SLOT3"
	},
	
	{
		title = "SLOT4"
	},
	
	{
		title = "SLOT5"
	},
	{
		title = "SLOT6"
	}
}

-- DarkRP Example

ix.weaponHud.customMenu = {
	{ 
		title = "ROLE-PLAY",
		"weapon_physcannon",
		"weapon_physgun",
		"arrest_stick",
		"door_ram",
		"keys",
		"lockpick"
	},
	{
		title = "WEAPONS",
		"weapon_ak472",
		"weapon_deagle2",
		"weapon_fiveseven2",
		"weapon_glock2",
		"weapon_m42",
		"weapon_mac102",
		"weapon_mp52",
		"weapon_p2282",
		"weapon_pumpshotgun2",
	},
	{
		title = "TOOLS",
		"gmod_tool",
		"gmod_camera",
		"weaponchecker"
	},
	{
		title = "MISC",
		"med_kit"
	}
}
]]--