local PLUGIN = PLUGIN

PLUGIN.name = "Payday"
PLUGIN.author = "Vac"
PLUGIN.description = "Allows people to be paid x amount every y minutes."

ix.util.Include("sv_payday.lua")

ix.config.Add("moneyPaymentAmount", 20, "How much people get paid", nil, {
	data = {min = 0, max = 100},
	category = "economy"
})

ix.config.Add("moneyPaymentInterval", 300, "How long often people get paid", function(old, new)
		if (SERVER) then
			if(timer.Exists("[IG] Payment Timer")) then
				timer.Remove("[IG] Payment Timer")
				timer.Create("[IG] Payment Timer", new || 300, 0, function()
					for k, v in pairs(player.GetAll()) do
						local character = v:GetCharacter()
						if (character) then
							if(!character:IsArrested()) then
								character:GiveMoney(ix.config.Get("moneyPaymentAmount"))
								v:NotifyLocalized("You were paid ₹" .. ix.config.Get("moneyPaymentAmount") .."!" )
							else
								v:NotifyLocalized("You weren't paid because your arrested!" )
							end
						end
					end
				end)
			end
		end
	end, {
	data = {min = 180, max = 600},
	category = "economy"
})
