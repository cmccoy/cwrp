hook.Add("InitPostEntity", "[IG] Start auto payment timer", function()

    timer.Create("[IG] Payment Timer", ix.config.Get("moneyPaymentInterval", 300), 0, function()
        for k, v in pairs(player.GetAll()) do
            local character = v:GetCharacter()
            if (character) then
                if (v:IsVip() || v:IsStaff()) then
                    local extra = ix.config.Get("moneyPaymentAmount") + ix.config.Get("moneyPaymentAmount") * .25
                    character:GiveMoney(extra)
                    v:NotifyLocalized("You were paid ₹" .. extra .."!" )
                else
                    character:GiveMoney(ix.config.Get("moneyPaymentAmount"))
                    v:NotifyLocalized("You were paid ₹" .. ix.config.Get("moneyPaymentAmount") .."!" )
                end
            end
        end
    end)
    
end)