local PLUGIN = PLUGIN

PLUGIN.name = "Defcon System"
PLUGIN.author = "Vac"
PLUGIN.description = "Sets up a defcon system for the server."

ix.util.Include("sv_defcon.lua")

ix.command.Add("defcon", {
    arguments = {ix.type.number},
    privilege = "",
	description = "Sets the current defcon to the specified value.",

	OnRun = function(self, client, number)
		if (client:IsSuperAdmin() || client:IsGameMaster()) then
			if(number < 1 || number > 5) then
				client:NotifyLocalized("You didn't input a valid DEFCON level!")
			else
				setDefcon(number)
				hook.Run("SwrpOnDefcon", client, number)
			end
		end
    end
})