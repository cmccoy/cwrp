hook.Add("InitPostEntity", "[IG] Setup defcon global", function()

    SetGlobalInt("defcon", 5)

end)

function setDefcon(value)
    SetGlobalInt("defcon", value)
    BroadcastLua([[surface.PlaySound("npc/attack_helicopter/aheli_damaged_alarm1.wav")]])
    for k, v in pairs(player.GetAll()) do
        v:NotifyLocalized("The DEFCON level has been set to ".. value .."!")
    end
end