--Server
local PLUGIN = PLUGIN
util.AddNetworkString("ixWeaponsLocker")

--Locker handling
	function PLUGIN.LockerInteract(ply, char, ent)
		-- if char:IsClone() then char:UnlockLoadoutWeapon("weapon_crowbar") end
		net.Start("ixWeaponsLocker")
			net.WriteEntity(self)
		net.Send(ply)
	end

	function PLUGIN:SaveData()
		local data = {}

		for _, entity in ipairs(ents.FindByClass("ix_weapons_locker")) do
			data[#data + 1] = {
				name = entity:GetDisplayName(),
				description = entity:GetDescription(),
				pos = entity:GetPos(),
				angles = entity:GetAngles(),
				model = entity:GetModel(),
			}
		end

		self:SetData(data)
	end

	function PLUGIN:LoadData()
		for _, v in ipairs(self:GetData() or {}) do
			local entity = ents.Create("ix_weapons_locker")
			entity:SetPos(v.pos)
			entity:SetAngles(v.angles)
			entity:Spawn()

			entity:SetModel(v.model)
			entity:SetSolid(SOLID_BBOX)
			entity:PhysicsInit(SOLID_BBOX)

			local physObj = entity:GetPhysicsObject()

			if (IsValid(physObj)) then
				physObj:EnableMotion(false)
				physObj:Sleep()
			end

			entity:SetDisplayName(v.name)
			entity:SetDescription(v.description)
		end
	end
--End locker handling

--Character Handling	
	function PLUGIN:OnCharacterCreated(client, character)

		if (character) then
			local loadout = {}

			if(character:IsClone()) then
				loadout = PLUGIN.defaultCloneLoadout
			elseif(character:IsJedi()) then
				loadout = PLUGIN.defaultJediLoadout
			else
				loadout = PLUGIN.defaultRecruitLoadout
			end

	        local query = mysql:Update("ix_characters")
				query:Where("id", character:GetID())
				query:Update("loadout", util.TableToJSON(loadout))
	        query:Execute()

	        for category, entclass in pairs(loadout) do
	        	character:UnlockLoadoutWeapon(entclass)
	        end
	    end

	end

	function PLUGIN:PostPlayerLoadout(client)
		local character = client:GetCharacter()

		if(character) then
			character:EquipNewLoadout(client, nil, character:GetLoadout())
		end
	end
--End Character Handling

--Char Meta
	local CHAR = ix.meta.character

	function CHAR:EquipNewLoadout(client, old, new)
		if old then
			for cat, ent in pairs(old) do
				if ent != "" then
					client:StripWeapon(ent)
				end
			end
		end

		if new then
			for cat, ent in pairs(new) do
				if ent != "" then
					client:Give(ent)
				end
			end
		end

		hook.Run("SwrpOnLoadoutChanged", entClass)
	end

	function CHAR:UnlockLoadoutWeapon(entClass)
		local wepInfo = PLUGIN.LoadoutGearInfo[entClass]
		if entClass == "" then return false end
		if not wepInfo then print( "Attempting to 'unlock' a bad loadout weapon on player", self, self:GetPlayer(), entClass ) return end

		local currentUnlocks = self:GetData("loadout_unlocks", {})

		currentUnlocks[entClass] = true
		self:SetData("loadout_unlocks", currentUnlocks)

		hook.Run("SwrpOnLoadoutUnlocked", client, entClass)
	end

	function CHAR:LockLoadoutWeapon(entClass)
		local wepInfo = PLUGIN.LoadoutGearInfo[entClass]
		if not wepInfo then print( "Attempting to 'lock' a bad loadout weapon on player", self, self:GetPlayer(), entClass ) return end

		local currentUnlocks = self:GetData("loadout_unlocks", {})
		if currentUnlocks[entClass] then
			currentUnlocks[entClass] = nil
			self:SetData("loadout_unlocks", currentUnlocks)
		end
	end

--Char Meta End

--Start Network handling
		function PLUGIN.LockerSetLoadout(len, ply)
		local char = ply:GetCharacter()

		if char then
			local requestedLoadout = net.ReadTable()
			--client will send only what changed (if not exploiting)
			if table.IsEmpty(requestedLoadout) then return end
			if requestedLoadout == char:GetLoadout() then return end


			local newLoadout = table.Copy(char:GetLoadout())
			local gearinfoTbl = PLUGIN.LoadoutGearInfo
			local unlockedWeapons = ply:GetCharacter():GetData("loadout_unlocks", {})
			local missingUnlock
			for cat, ent in pairs(requestedLoadout) do
				if not unlockedWeapons[ent] then missingUnlock = ent break end
				--Verify the ents in the table are something they can unlock
				local gearinfo = gearinfoTbl[ent]
				if not gearinfo or gearinfo.slot != cat then continue end
				newLoadout[cat] = ent
			end
			if missingUnlock then 
				print( string.format("Player %s (%s) tried to equip locked weapon (%s) on character %s.", tostring(ply), tostring(ply:SteamID()), tostring(ent), tostring(char) ) ) 
				return 
			end
			if newLoadout == char:GetLoadout() then 
				--Do nothing if loadout matches current. Probably an exploit if we got here or an unforseen bug.
				return 
			else
				--
				local oldLoadout = char:GetLoadout()
				char:SetLoadout(newLoadout)
				--Equip and de-equip weapons from loadout
				char:EquipNewLoadout(char:GetPlayer(), oldLoadout, char:GetLoadout())
			end
		end
	end
	net.Receive( "ixWeaponsLocker", PLUGIN.LockerSetLoadout )
--End Network handling

