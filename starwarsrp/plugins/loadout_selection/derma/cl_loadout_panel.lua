local PLUGIN = PLUGIN
surface.CreateFont("ixWeaponLockerFont", {
	font = font,
	size = 16,
	extended = true,
	weight = 1000
})

surface.CreateFont("ixWeaponLockerFont_Small", {
	font = font,
	size = 20,
	extended = true,
	weight = 1000
})
local function getActiveLoadout() --Helper function for easier access
	return LocalPlayer():GetCharacter():GetLoadout()
end

local PANEL = {}

function PANEL:Init()
	self:Dock(TOP)
	self:SetTall(128)
	self.IconSizes = 83
	self.Interior = self:Add("DIconLayout")
	self.Interior:Dock(FILL)
	self.Interior:SetSpaceY(4)
	self.Interior:SetSpaceX(8)

	self.Interior:DockMargin(8,2,0,2)
end 

function PANEL:SetContents(contentTable, dependant)
	local activeLoadout = getActiveLoadout()
	local unlockedWeapons = LocalPlayer():GetCharacter():GetData("loadout_unlocks", {})
	local selection

	for k, v in pairs(contentTable) do
		selection = self.Interior:Add("DImageButton")
		selection:SetTall(self.IconSizes)
		selection:SetWide(self.IconSizes)
		if v.icon then
			selection:SetImage(v.icon)
		end
		selection:SetColor(Color(255, 255, 255, 100))
		selection.ent = v.ent
		if v.ent and v.ent != "" and not unlockedWeapons[v.ent] then 
			selection.NotUnlocked = true 
		end

		selection.SetIsActive = function(self, bool)
			if bool then
				if self:GetParent().Selected then
					self:GetParent().Selected.IsSelected = false
				end
				self:GetParent().Selected = self
				self.IsSelected = true
			else
				if self:GetParent().Selected and self:GetParent().Selected == self then
					self:GetParent().Selected = false
				end
				self.IsSelected = false
			end
			--Update righthand side
			local slot = PLUGIN.LoadoutGearInfo[self.ent].slot
			--If you happen to see this, behold the all glorious super parent ༼ つ ◕_◕ ༽つ.
			--If unmodified, this should always end up on the main frame panel to get what it needs
			self:GetParent():GetParent():GetParent():GetParent():GetParent():GetParent().ActiveWepPanels[slot]:UpdateInfo(self.ent)
		end

		if not selection:GetParent().Selected and (not v.ent or activeLoadout[PLUGIN.LoadoutGearInfo[v.ent].slot] == v.ent) then
			selection:GetParent().Selected = selection
			selection.IsSelected = true
			selection.Active = true
		else
			selection.IsSelected = false
		end

		if not dependant then
			--[[selection.DoRightClick = function(self)
				self:SetIsActive(true)
				surface.PlaySound("UI/buttonclickrelease.wav")
			end]]
			selection.DoClick = function(self)
				if self.NotUnlocked then
					ix.util.NotifyLocalized(string.format( "You have not yet unlocked %s to use it in your loadout.", PLUGIN.LoadoutGearInfo[v.ent].name) )
				else
					self:SetIsActive(true)
					surface.PlaySound("UI/buttonclick.wav")
				end
			end
		end

		selection.PaintOver = function(self, w, h)
			if self.NotUnlocked or self.Active or self.IsSelected then
				local recur = 8
				local offset = 10

				for i=0, recur do
					if self.Active then
						surface.SetDrawColor(25, 125, 175, 155/i-1 )
					elseif self.NotUnlocked then
						surface.SetDrawColor(255, 25, 25, 155/i-1 )
					else
						surface.SetDrawColor(25, 175, 125, 155/i-1 )
					end
					surface.DrawOutlinedRect( i + offset/2, i + offset/2, w-(i+offset/2)*2, h-(i+offset/2)*2 )
					-- surface.DrawOutlinedRect( i, i , w-(i)*2, h-(i)*2 )
				end
			end
		end
	end

	if dependant then
		self.Interior:SetLayoutDir(LEFT) --Horizontally align panel to prevent formatting

		selection.InfoBox = vgui.Create("DLabel", self.Interior)

		selection.InfoBox:SetWide(self.IconSizes * 3)
		selection.InfoBox:SetFont("ixWeaponLockerFont_Small")

		selection.UpdateInfo = function(self, entClass)
			local gearinfo = PLUGIN.LoadoutGearInfo[entClass] or {}
			self.ent = entClass
			if getActiveLoadout()[gearinfo.slot] == entClass then
				self.Active = true
			else
				self.Active = false
			end

			if gearinfo.icon then
				self:SetImageVisible(true)
				self:SetImage(gearinfo.icon)
			else
				self:SetImageVisible(false)
			end

			if table.IsEmpty(gearinfo) then self.InfoBox:SetText("") return end
			local entInfo = weapons.Get(entClass)
			if entInfo then --Currently only accounts for TFA base weapons, try custom description instead
				local formattedInfo = string.format("Name: %s \nDamage: %s \nMagazine Size: %s \nFire-rate: %s",
					gearinfo.name or entInfo.PrintName or "?", 
					entInfo.Primary and entInfo.Primary.Damage and tostring(entInfo.Primary.Damage) or "?", 
					entInfo.Primary and entInfo.Primary.ClipSize and tostring(entInfo.Primary.ClipSize) or "?",
					entInfo.Primary and entInfo.Primary.RPM and tostring(math.Round(entInfo.Primary.RPM, 0)) or "?")

				self.InfoBox:SetText(formattedInfo)
			elseif gearinfo.customDescription then
				self.InfoBox:SetText( gearinfo.name .. "\n" .. gearinfo.customDescription)
			else
				self.InfoBox:SetText( gearinfo.name .. "\n" .."ERROR: FAILED TO RETRIEVE ITEM DEFINITIONS\nINFORMATION MISSING FROM DATABASE")
			end

			selection.InfoBox:SetWrap(true)
			selection.InfoBox:SetAutoStretchVertical(true)
		end
		selection:UpdateInfo(selection.ent)

		return selection 
	end
end

vgui.Register("ixLoadoutMenu_Section", PANEL, "DScrollPanel")

local PANEL = {}


local backgroundLeftPaint = Color(57, 57, 57, 255)
local labelLeftPaint = Color(24, 24, 24, 255)

local backgroundRightPaint = Color(35, 35, 35, 255)
local labelRightPaint = Color(12, 12, 12, 255)


function PANEL:Init()
	self:SetSize(800, 545)
	self:Center()
	self:MakePopup()
	self:SetTitle("")

	--Holder panel for confirmation button
	self.bottom = self:Add("DPanel")
		self.bottom:SetTall(50)
		self.bottom:Dock(BOTTOM)
		self.bottom.Paint = nil
		self.bottom:DockPadding(0,4,0,4)
		self.bottom:DockMargin(0,4,0,0) 
	local default = vgui.Create("DButton", self.bottom)
		default:SetFont("ixWeaponLockerFont_Small")
		default:SetText("Use Default Loadout")
		default:SizeToContentsX(8)
		default:SizeToContentsY(12)
		default:DockMargin(4,0,0,0)
		default:Dock(LEFT)
		default.confirmStage = 0
		default.curStageTimeout = CurTime()
		default.curStageStart = CurTime()
		default.ParentPan = self
	default.DoClick = function(self)
		if PLUGIN:RequestNewLoadout() then
			self.ParentPan:Close()
			ix.util.NotifyLocalized("Default loadout has been applied!")
		else
			ix.util.NotifyLocalized("Current loadout is the same. No changes made.")
		end
	end

	local apply = vgui.Create("DButton", self.bottom)
		apply:SetFont("ixWeaponLockerFont_Small")
		apply:SetText("Apply Loadout")
		apply:SizeToContentsX(8)
		apply:SizeToContentsY(12)
		apply:DockMargin(0,0,16,0)
		apply:Dock(RIGHT)
		apply.ParentPan = self
	apply.DoClick = function(self)
		if PLUGIN:RequestNewLoadout(self.ParentPan.ActiveWepPanels) then
			self.ParentPan:Close()
			ix.util.NotifyLocalized("Selected loadout has been applied!")
		else
			ix.util.NotifyLocalized("Current loadout is the same. No changes made.")
		end
	end

	self.bottom:SetTall(apply:GetTall() )
	
	self.leftPanel = self:Add("DScrollPanel")
		self.leftPanel:Dock(LEFT)
		self.leftPanel:SetWide(self:GetWide() * .5 - 5)
		--[[ not gona bother finishing or trying to push vbar to the left side, can't seem to get it to work
		self.leftPanel:GetVBar():Dock(LEFT)
		self.leftPanel:GetCanvas():Dock(RIGHT)
		self.leftPanel.Paint = function(self, w, h)
			surface.SetDrawColor(Color(0,255,0))
			surface.DrawRect(0,0,w,h)
		end]]

	self.leftPanel:GetCanvas().Paint = function(self, w, h)
		surface.SetDrawColor(backgroundLeftPaint)
		surface.DrawRect(0,0,w,h)
	end

	for catName, catInfo in pairs(PLUGIN.LoadoutChoices) do
		local catLabel = self.leftPanel:Add("DLabel")
			catLabel:Dock(TOP)
			catLabel:SetText(catInfo.name)
			catLabel:DockMargin(0, 0, 0, 2)
			catLabel:SetFont("ixWeaponLockerFont")
			catLabel:SetContentAlignment(5)
		catLabel.Paint = function(self, w, h)
			draw.RoundedBox(0, 0, 0, w, h, labelLeftPaint)
		end

		local cat = self.leftPanel:Add("ixLoadoutMenu_Section")
		cat:SetContents(catInfo.content)
	end

	self.rightPanel = self:Add("DScrollPanel")
		self.rightPanel:Dock(RIGHT)
		self.rightPanel:SetWide(self:GetWide() * .5 - 5)
	self.rightPanel:GetCanvas().Paint = function(self, w, h)
		surface.SetDrawColor(backgroundRightPaint)
		surface.DrawRect(0,0,w,h)
	end

	self.ActiveWepPanels = {}
	local ourLoadout = getActiveLoadout()
	--Use loadout gear order to create sections in order
	for position, cat in pairs(PLUGIN.LoadoutGearOrder) do 
		local entName = ourLoadout[cat]

		local catInfo = PLUGIN.LoadoutGearInfo[entName]
		if not catInfo then catInfo = {  name = "", slot = cat } end
		local catLabel = self.rightPanel:Add("DLabel")
			catLabel:Dock(TOP)
			catLabel:SetText(cat)
			catLabel:DockMargin(0, 0, 0, 2)
			catLabel:SetFont("ixWeaponLockerFont")
			catLabel:SetContentAlignment(5)
		catLabel.Paint = function(self, w, h)
			draw.RoundedBox(0, 0, 0, w, h, labelLeftPaint)
		end

		local catpan = self.rightPanel:Add("ixLoadoutMenu_Section")
		--True second argument to make unclickable and some other adjustments, then return us the made panel afterwards
		local wepPan = catpan:SetContents({catInfo}, true)
		-- for i=0, 10 do cat:SetContents({catInfo}, true) end
		self.ActiveWepPanels[cat] = wepPan
	end


end

vgui.Register("ixLoadoutMenu", PANEL, "DFrame")
