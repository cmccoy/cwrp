local PLUGIN = PLUGIN

PLUGIN.name = "Loadout"
PLUGIN.author = "Vac/Mr. Sandbox"
PLUGIN.description = "Spawns/saves loadout boxes that players can use to adjust their default gear loadout."

ix.util.Include("sv_plugin.lua")

--Loadout Choices
	local Loadout_Categories = {}
	--Setup with table.insert to have sequential keys regardless of name used
	table.insert(Loadout_Categories, {name = "Primary", content = {
			{name = "DC-15s", ent = "tfa_swch_dc15s", icon = "materials/ig_cwrp/icons/bordered/primary_dc15s.png", unlockprice = 15000},
			{name = "Shotgun", ent = "tfa_sw_repshot", icon = "materials/ig_cwrp/icons/bordered/primary_dc15a.png", unlockprice = 35000},
			{name = "DC-17m", ent = "tfa_swch_dc17m_br", icon = "materials/ig_cwrp/icons/bordered/primary_dc17m.png", unlockprice = 45000},
			{name = "Z6", ent = "tfa_swch_z6", icon = "materials/ig_cwrp/icons/bordered/primary_z6.png", unlockprice = 200000},
			{name = "Sniper", ent = "tfa_sw_repsnip", icon = "materials/ig_cwrp/icons/bordered/primary_sniper.png", unlockprice = 95000},
			{name = "Westar", ent = "tfa_swch_alphablaster", icon = "materials/ig_cwrp/icons/bordered/primary_westar.png", unlockprice = 65000},
			{name = "Vibrosword", ent = "weapon_vibrosword", icon = "materials/ig_cwrp/icons/bordered/primary_sword.png", unlockprice = 500000},

			-- {name = "SMG", ent = "weapon_smg1", icon = "entities/weapon_smg1.png"},
			-- {name = "Ar2", ent = "weapon_ar2", icon = "entities/weapon_ar2.png"},
			-- {name = "Mini Gun", ent = "bf2017_z6_summe", icon = "entities/gmod_tool.png"},
		},
	})

	table.insert(Loadout_Categories, {name = "Secondary", content = {
			{name = "DC-17", ent = "tfa_swch_dc17", icon = "materials/ig_cwrp/icons/bordered/secondary_dc17.png"},
			{name = "Dual DC-17", ent = "tfa_sw_dc17dual", icon = "materials/ig_cwrp/icons/bordered/secondary_dual_dc17.png", unlockprice = 20000},
			{name = "DE-10", ent = "tfa_swch_de10", icon = "materials/ig_cwrp/icons/bordered/secondary_de10.png", unlockprice = 15000},
			{name = "Dual DE-10", ent = "tfa_sw_dual_de10", icon = "materials/ig_cwrp/icons/bordered/secondary_dual_de10.png", unlockprice = 30000},

			-- {name = "Pistol", ent = "weapon_pistol", icon = "entities/weapon_pistol.png", customDescription = "haha pistol go pew pew"},
		},
	})

	table.insert(Loadout_Categories, {name = "Grenade", content = {
			{name = "Frag Grenade", ent = "weapon_frag_grenade", icon = "materials/ig_cwrp/icons/bordered/grenade.png", unlockprice = 15000},
			{name = "Bacta Grenade", ent = "weapon_bacta_grenade", icon = "materials/ig_cwrp/icons/bordered/grenade_health.png", unlockprice = 20000},

			-- {name = "HL2 Grenade", ent = "weapon_grenade", icon = "entities/weapon_frag.png"},
		},
	})

	table.insert(Loadout_Categories, {name = "Utility #1", content = {
			{name = "Stimkit", ent = "weapon_jew_stimkit", icon = "materials/ig_cwrp/icons/bordered/utilityone_stimkit.png", unlockprice = 25000},
		},
	})

	table.insert(Loadout_Categories, {name = "Utility #2", content = {
			{name = "Breaching Charge", ent = "weapon_swrc_det", icon = "materials/ig_cwrp/icons/bordered/utilitytwo_breachingcharge.png", unlockprice = 75000},

			-- {name = "SLAM", ent = "weapon_slam", icon = "entities/weapon_slam.png"},
		},
	})

	table.insert(Loadout_Categories, {name = "Utility #3", content = {
			{name = "Fists", ent = "weapon_fists", icon = "materials/ig_cwrp/icons/bordered/utilitythree_fists.png"},

			-- {name = "Crowbar", ent = "weapon_crowbar", icon = "entities/weapon_crowbar.png"},
		},
	})

	PLUGIN.LoadoutChoices = Loadout_Categories

	PLUGIN.LoadoutGearInfo = {} --Table with key being the entity class and info inside value as a table
	PLUGIN.LoadoutGearOrder = {} --Sequential table with values being the order gear slots should be

	--Setup gear info based on the entity name for reference
	for _, cat in pairs(PLUGIN.LoadoutChoices) do
		PLUGIN.LoadoutGearOrder[_] = cat.name
		for k, guninfo in pairs(cat.content) do
			-- PLUGIN.LoadoutGearInfo[guninfo.ent] = { icon = guninfo.icon, ent = guninfo.ent, name = guninfo.name, slot = cat.name }
			PLUGIN.LoadoutGearInfo[guninfo.ent] = table.Copy(guninfo)
			PLUGIN.LoadoutGearInfo[guninfo.ent].slot = cat.name
		end
	end

--End Loadout Choices

--Meta
	local CHAR = ix.meta.character

	--Plugins load after faction/classes are loaded so indexing like this will work
	local cloneFactions = { 
	    [FACTION_41ST] = true,
	    [FACTION_104TH] = true,
	    [FACTION_212TH] = true,
	    [FACTION_327TH] = true,
		[FACTION_501ST] = true,
		[FACTION_MEDICAL] = true,
		[FACTION_COMBAT_DROID] = true,
		[FACTION_CLONE] = true
	}

	function CHAR:IsClone()
	    local faction = self:GetFaction()
	    if cloneFactions[faction] then
	        return true
	    end
	    return false
	end

	function CHAR:IsJedi()
	    local faction = self:GetFaction()

	    if (faction == FACTION_JEDI) then
	        return true
	    end
	    return false
	end
--End Meta

PLUGIN.defaultRecruitLoadout = {
	["Primary"] = "",
	["Secondary"] = "",
	["Grenade"] = "",
	["Utility #1"] = "",
	["Utility #2"] = "",
	["Utility #3"] = "weapon_fists",
}

PLUGIN.defaultCloneLoadout = {
	["Primary"] = "",
	["Secondary"] = "tfa_swch_dc17",
	["Grenade"] = "",
	["Utility #1"] = "",
	["Utility #2"] = "",
	["Utility #3"] = "weapon_fists",
}

PLUGIN.defaultJediLoadout = {
	["Primary"] = "weapon_lightsaber_initiate",
	["Secondary"] = "",
	["Grenade"] = "",
	["Utility #1"] = "",
	["Utility #2"] = "",
	["Utility #3"] = "",
}


ix.char.RegisterVar("loadout", {
    field = "loadout",
    fieldType = ix.type.text,
	default = {},
	isLocal = true,
    bNoDisplay = true,

	OnGet = function(self, default)
		local currentLoadout = self.vars.loadout
		if (currentLoadout == nil || currentLoadout == "") then
			if (self:IsClone()) then
				return PLUGIN.defaultCloneLoadout
			elseif (self:IsJedi()) then
				return PLUGIN.defaultJediLoadout
			else
				return PLUGIN.defaultRecruitLoadout 
			end
		end
		return currentLoadout
    end
})
--Network
	if CLIENT then
		function PLUGIN:RequestNewLoadout(selectedLoadout)
			local finalLoadout = {}
			local activeLoadout = LocalPlayer():GetCharacter():GetLoadout()

			if not selectedLoadout then 
				local char = LocalPlayer():GetCharacter()
				local default = char:IsClone() and PLUGIN.defaultCloneLoadout or char:IsJedi() and PLUGIN.defaultJediLoadout or PLUGIN.defaultRecruitLoadout 
				local match = true

				for cat, class in pairs(default) do
					if class == "" then continue end
					if activeLoadout[cat] != class then 
						match = false 
						break 
					end
				end
				if match then return false end

				finalLoadout = {default = true}
			else
				local gearinfo = PLUGIN.LoadoutGearInfo

				for category, panelIcon in pairs(selectedLoadout) do
					local ent = panelIcon.ent
					if not ent or ent == activeLoadout[category] then continue end 
					finalLoadout[category] = ent
				end
			end
			if table.IsEmpty(finalLoadout) then return false end

			net.Start("ixWeaponsLocker")
				net.WriteTable(finalLoadout)
			net.SendToServer()
			
			return true
		end
	end
--End Network