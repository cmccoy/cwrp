include('shared.lua')
ENT.Category			= "IG"
ENT.Spawnable			= true
ENT.AdminOnly			= true
ENT.AdminSpawnable		= true
ENT.RenderGroup 		= RENDERGROUP_BOTH

--[[hook.Add( "HUDPaint", "Health_NPC", function()
	for _, ent in pairs(ents.FindByClass("loadout_vendor_npc")) do
		if ent:GetPos():Distance( LocalPlayer():GetPos() ) > 350 then continue end
		local pos = (ent:GetPos() + Vector(0, 0, 90)):ToScreen()
		
		surface.SetFont("Trebuchet24")
		local text = "Gear License Salesman"
		local w,h = surface.GetTextSize(text)
		draw.RoundedBox(8, (pos.x - w/2)-8, (pos.y + h/2)-1, w+16, h+2, Color(150,50,0) )
		
		--draw.RoundedBoxEx(0, (pos.x - w/2)-8, (pos.y + h/2)-1, w+16, h+4, Color(155,155,155), false, false, false, false )
		surface.SetTextPos(pos.x - w/2, pos.y+h/2)
		surface.SetTextColor( Color(0,255,0) )
		surface.DrawText(text)
	end
end)]]

function ENT:OnPopulateEntityInfo(container)
	local name = container:AddRow("name")
	name:SetImportant()
	name:SetText("SD-88 Loadout Upgrades")
	name:SetBackgroundColor(Color(100, 100, 255, 255))
	name:SizeToContents()

	local description = container:AddRow("description")
	description:SetText("Unlocks weapons in the weapons locker... if the price is right.")
	description:SizeToContents()
end

net.Receive("Loadout_Salesman", function()
	local frame = vgui.Create("DFrame")
		frame:ShowCloseButton(true)
		frame:SetTitle("Loadout Upgrades")
		frame:SetSize(425, 725)
		frame:Center()
		frame:MakePopup()

	local scroll = vgui.Create("DScrollPanel", frame)
		scroll:Dock(FILL)

	local PLUGIN = ix.plugin.Get("loadout_selection")
	local character = LocalPlayer():GetCharacter()
	local ourUnlocked = character:GetData("loadout_unlocks", {})

	local anyonethere = false
	-- for entClass, data in SortedPairsByMemberValue(PLUGIN.LoadoutGearInfo, "unlockprice", true) do
	for entClass, data in SortedPairsByMemberValue(PLUGIN.LoadoutGearInfo, "slot", false) do
	-- for entClass, data in pairs(PLUGIN.LoadoutGearInfo) do
		if ourUnlocked[entClass] then continue end
		if not data.unlockprice then continue end
		anyonethere = true

		local pan = vgui.Create("DPanel")
		pan:SetTall(180)
		pan:Dock(TOP)
		scroll:AddItem(pan)
		local panButton = vgui.Create("ixLoadoutMenu_Section", pan)
		panButton:Dock(FILL)
		local wepPan = panButton:SetContents({[1] = data}, true)
		if not character:HasMoney(data.unlockprice or 0) then 
			wepPan.NotUnlocked = true 
		else 
			wepPan.NotUnlocked = false 
			wepPan.IsSelected = true
		end
		wepPan.InfoBox:SetText( string.format("Slot: %s \nPrice: $%s\n\n%s", data.slot, data.unlockprice or "0", wepPan.InfoBox:GetText() ) )
		wepPan.DoClick = function(self)
			if character:HasMoney(data.unlockprice or 0) then
				frame:Close()
				net.Start("Loadout_Salesman")
				net.WriteString(self.ent)
				net.SendToServer()
			end
		end
	end

	if not anyonethere then 
		local button = vgui.Create("DButton", frame)
		button:Dock(FILL)
		-- button:SetTall(200)
		button:SetText("Looks like you own everything!\n I've nothing else to offer you...")
		button:SizeToContentsY(4)
		button.DoClick = function(self)
		self:GetParent():Close()
		end
		scroll:Remove()
		frame:SetTall(100)
		frame:Center()
	end

end)