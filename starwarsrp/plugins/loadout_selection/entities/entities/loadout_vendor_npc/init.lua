AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
include('shared.lua')

util.AddNetworkString("Loadout_Salesman")

function ENT:Initialize()

	self:SetModel("models/galactic/supnpc/shopdroid/shopdroid.mdl")

	self:SetHullSizeNormal()
 
	self:SetSolid( SOLID_BBOX ) 
	self:SetMoveType( MOVETYPE_STEP )

	self:SetUseType( SIMPLE_USE )

end


function ENT:AcceptInput(name,activator,caller)
	if !activator:IsPlayer() then return end
	if name == "Use" and caller:IsPlayer() then
		-- activator:ChatPrint("[Vendor] Here's what I can do for you.")
		net.Start("Loadout_Salesman")
		net.Send(activator)

	end
end

local function PlayerUnlockGear(len, ply)
	local entClass = net.ReadString()
	local char = ply:GetCharacter()
	local PLUGIN = ix.plugin.Get("loadout_selection")
	local targetWep = PLUGIN.LoadoutGearInfo[entClass]

	if not targetWep then 
		print( string.format("%s attempting to buy invalid loadout weapon of class %s", tostring(ply), tostring(entClass) ) )
		return 
	end

	if not targetWep.unlockprice then 
		print( string.format("%s attempting to buy unpurchasable loadout weapon of class %s", tostring(ply), tostring(entClass) ) )
		return 
	end

	local ourUnlocked = char:GetData("loadout_unlocks", {})
	if ourUnlocked[entClass] then 
		print( string.format("%s attempting to unlock already unlocked loadout weapon of class %s", tostring(ply), tostring(entClass) ) )
		return 
	end

	if char:HasMoney(targetWep.unlockprice or 0) then
		char:TakeMoney(targetWep.unlockprice, true) --True for no log
		char:UnlockLoadoutWeapon(entClass)
	end
end
net.Receive("Loadout_Salesman", PlayerUnlockGear)

function ENT:GetRelationship(ent)
end
function ENT:OnTakeDamage(dmginfo)
end
function ENT:StartTouch(ent) 
end
function ENT:EndTouch(ent)
end
function ENT:Use(activator, caller)
end
function ENT:KeyValue(key,value)
end
function ENT:OnRemove()
end
function ENT:OnRestore()
end
function ENT:Touch(hitEnt) 
end
function ENT:Think()
end