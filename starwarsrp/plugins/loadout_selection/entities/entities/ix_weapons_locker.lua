
ENT.Type = "anim"
ENT.PrintName = "Weapons Locker"
ENT.Category = "IG"
ENT.Spawnable = true
ENT.AdminOnly = true
ENT.bNoPersist = true

function ENT:SetupDataTables()
	self:NetworkVar("String", 0, "DisplayName")
	self:NetworkVar("String", 1, "Description")
end

function ENT:Initialize()
	if (SERVER) then
		self:SetModel("models/props/supweaponlocker.mdl")
		self:SetUseType(SIMPLE_USE)
		self:SetMoveType(MOVETYPE_NONE)
		self:DrawShadow(true)
		self:SetSolid(SOLID_BBOX)
		self:PhysicsInit(SOLID_BBOX)

		self:SetDisplayName("Weapons locker")
		self:SetDescription("Change the loadout you spawn with.")

		local physObj = self:GetPhysicsObject()

		if (IsValid(physObj)) then
			physObj:EnableMotion(false)
			physObj:Sleep()
		end
	end
end

if (SERVER) then
	local PLUGIN = PLUGIN

	function ENT:SpawnFunction(client, trace)
		local angles = (trace.HitPos - client:GetPos()):Angle()
		angles.r = 0
		angles.p = 0
		angles.y = angles.y + 180

		local entity = ents.Create("ix_weapons_locker")
		entity:SetPos(trace.HitPos)
		entity:SetAngles(angles)
		entity:Spawn()

		PLUGIN:SaveData()

		return entity
	end

	function ENT:Use(activator)
		if (activator:IsPlayer()) then
			local character = activator:GetCharacter()
			if(character) then
				PLUGIN.LockerInteract(activator, character, self)
			end
		end
	end
else
	function ENT:Draw()
		self:DrawModel()
	end

	function ENT:Think()		
	end

	function ENT:OnRemove()	
	end

	net.Receive("ixWeaponsLocker", function(len, pl)
		ix.gui.openWeaponsLocker = vgui.Create("ixLoadoutMenu")
	end)

	ENT.PopulateEntityInfo = true

	function ENT:OnPopulateEntityInfo(container)
		local name = container:AddRow("name")
		name:SetImportant()
		name:SetText(self:GetDisplayName())
		name:SetBackgroundColor(Color(100, 100, 255, 255))
		name:SizeToContents()

		local descriptionText = self:GetDescription()

		if (descriptionText != "") then
			local description = container:AddRow("description")
			description:SetText(self:GetDescription())
			description:SizeToContents()
		end
	end
end
