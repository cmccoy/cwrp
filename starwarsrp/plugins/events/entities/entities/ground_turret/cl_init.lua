include('shared.lua')

local revUp = false
local revSide = false

function ENT:Draw()

    self:DrawModel()
 
end


function ENT:GetBarrelPos()
        return self:GetBoneMatrix(2):GetTranslation()
end

local counter = 0

function ENT:Think()

	local target = self:GetTarget()
	
	if target then
		
		if counter < 360 then
			counter = counter + 1
			self:ManipulateBoneAngles(4, Angle(0, 0, counter+2))
		else
			counter = 0
		end
	
			local targetPos = self:GetTargetPos(target)
			local ang = self:GetAngles()
			
			local boneMatrix = self:GetBoneMatrix(2)
		   
			if boneMatrix then

					local bonePos = boneMatrix:GetTranslation()
					local desiredAng = (targetPos-bonePos):Angle()
					
					local curAng = self:GetManipulateBoneAngles(1)
					curAng = math.Round(curAng.y, 0)
					local newAng = math.Round(desiredAng.y, 0)
					if (curAng != newAng) then
						if(curAng > newAng) then
							self:ManipulateBoneAngles(1, Angle(0, newAng-1, 0))
						elseif curAng < newAng then
							self:ManipulateBoneAngles(1, Angle(0, newAng+1, 0))
						end
					end
					
					--self:ManipulateBoneAngles(1, Angle(-desiredAng.x + ang.x , 0, 0))
					
			end
		   
			local boneMatrix = self:GetBoneMatrix(0)
			if boneMatrix then
					local bonePos = boneMatrix:GetTranslation()
					local desiredAng = (targetPos-bonePos):Angle()
					
					local curAng = self:GetManipulateBoneAngles(0)
					--print(curAng)
					
					local cur = math.Round(curAng.y, 0)
					local toAng = math.Round(desiredAng.y, 0)
					
					--print(cur)
					--print(toAng)
					
					if cur < toAng  then
						self:ManipulateBoneAngles(0, Angle(0, 0, cur + 1))
					elseif cur > toAng then
						self:ManipulateBoneAngles(0, Angle(0, 0, cur - 1))
					end
					
					--self:ManipulateBoneAngles(0, Angle(0, 0, -desiredAng.y + ang.y )) -- FLIP BACK TO NO NEG AND MINUS FOR NOT UPSIDEDOWN
			end
		   
			--self:ManipulateBonePosition(4, Vector(-1, 0, 0))
			--self:ManipulateBoneAngles(4, Angle(20, 0, 0))
		   
			--self:ManipulateBonePosition(3, Vector(0, 0, 8))
	else
		
		
		-- local curAng = self:GetManipulateBoneAngles(1)
		-- curAng.x = math.Round(curAng.x, 0)
		-- if curAng.x < 40 then
		-- 	self:ManipulateBoneAngles(1, Angle(curAng.x+1, 0, 0))
		-- elseif curAng.x > 40 then
		-- 	self:ManipulateBoneAngles(1, Angle(curAng.x-1, 0, 0))
		-- end
		
		
		
		local curAng = self:GetManipulateBoneAngles(2)
		
		if curAng.z > 150 then
			revUp = true
		elseif curAng.z < -50 then
			revUp = false
		end

		--print(curAng.z)
		
		if revUp then
			self:ManipulateBoneAngles(2, Angle(0, 0, curAng.z - 1))
		else
			self:ManipulateBoneAngles(2, Angle(0, 0, curAng.z + 1))
		end
		
		local curAngSide = self:GetManipulateBoneAngles(0)
		
		if curAngSide.y > 90 then
			revSide = true
		elseif curAngSide.y < -90 then
			revSide = false
		end
		
		if revSide then
		 	self:ManipulateBoneAngles(0, Angle(0, curAngSide.y - 1, 0))
		else
			self:ManipulateBoneAngles(0, Angle(0, curAngSide.y + 1, 0))
		end
		
		
		
	
	end
       
end