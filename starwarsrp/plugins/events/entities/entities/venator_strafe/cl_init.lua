include("shared.lua")
local matPlasma	= Material( "effects/strider_muzzle" )

function ENT:Initialize()	
	local sound1 = "vanilla/hyperspace/vanilla_hyperspace_01.wav"
    local sound2 = "vanilla/hyperspace/vanilla_hyperspace_02.wav"

	local choose = math.random(0,1)
	if choose == 0 then
		surface.PlaySound(sound1)
	else
		surface.PlaySound(sound2)
	end
	
	self.FXEmitter = ParticleEmitter(self:GetPos())
	self:SetRenderAngles(self:GetAngles() + Angle(0,180,0))
end

function ENT:Think()
	self:EngineEffects()
end

ENT.EnginePosBig = {
	Vector(731, -3487, 363),
	Vector(364, -3786, 314),
	Vector(-370, -3784, 316),
	Vector(-728, -3493, 341)
}

ENT.EnginePosMedium = {
	Vector(-284, -3565, 520),
	Vector(-131, -3706, 213),
	Vector(121, -3553, 214),
	Vector(294, -3401, 553)
}

ENT.EnginePosSmall = {
	Vector(617, -3024, 508),
	Vector(-615, -3173, 515)
}

function ENT:EngineEffects()
	local normal = (self:GetForward() * -1):GetNormalized()
	local roll = math.Rand(-90,90)
	local FWD = self:GetForward();
	local id = self:EntIndex();

	for k,v in pairs(self.EnginePosBig) do
		local blue = self.FXEmitter:Add("sprites/bluecore",v)
		blue:SetVelocity(normal)
		blue:SetDieTime(0.025)
		blue:SetStartAlpha(255)
		blue:SetEndAlpha(255)
		blue:SetStartSize(15)
		blue:SetEndSize(13)
		blue:SetRoll(roll)
		blue:SetColor(255,255,255)
		
		local dynlight = DynamicLight(id + 4096*k);
		dynlight.Pos = v;
		dynlight.Brightness = 5;
		dynlight.Size = 100;
		dynlight.Decay = 1024;
		dynlight.R = 100;
		dynlight.G = 100;
		dynlight.B = 255;
		dynlight.DieTime = CurTime()+1;

	end

end

function ENT:Draw()
	self:DrawModel()

	self.LastPaint = RealTime()
	
	self:FrameAdvance(RealTime() - self.LastPaint)

	for i=1,2 do
		local vOffset = self.EnginePosBig[i] 
		local vOffset = self:GetPos()
		local scroll = CurTime() * -20
			
		render.SetMaterial( matPlasma )
		scroll = scroll * 0.9
		
		render.StartBeam( 3 )
			render.AddBeam( vOffset, 40, scroll, Color( 0, 255, 255, 255) )
			render.AddBeam( vOffset + self:GetForward()*-5, 36, scroll + 0.01, Color( 255, 255, 255, 255) )
			render.AddBeam( vOffset + self:GetForward()*-40, 32, scroll + 0.02, Color( 0, 255, 255, 0) )
		render.EndBeam()
		
		scroll = scroll * 0.9
		
		render.StartBeam( 3 )
			render.AddBeam( vOffset, 40, scroll, Color( 0, 255, 255, 255) )
			render.AddBeam( vOffset + self:GetForward()*-5, 36, scroll + 0.01, Color( 255, 255, 255, 255) )
			render.AddBeam( vOffset + self:GetForward()*-40, 32, scroll + 0.02, Color( 0, 255, 255, 0) )
		render.EndBeam()
		
		scroll = scroll * 0.9
		
		render.StartBeam( 3 )
			render.AddBeam( vOffset, 40, scroll, Color( 0, 255, 255, 255) )
			render.AddBeam( vOffset + self:GetForward()*-5, 36, scroll + 0.01, Color( 255, 255, 255, 255) )
			render.AddBeam( vOffset + self:GetForward()*-40, 32, scroll + 0.02, Color( 0, 255, 255, 0) )
		render.EndBeam()
	end
end