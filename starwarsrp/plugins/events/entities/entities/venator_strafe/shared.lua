ENT.Type = "anim"

util.PrecacheModel("models/sweaw/ships/rep_venator.mdl")

DEFINE_BASECLASS("base_anim")

ENT.PrintName           = "Skybox Ship"
ENT.Author              = "Vac"
ENT.Information         = ""
ENT.Category            = ""
 
ENT.Editable        = false
ENT.Spawnable       = true
ENT.AdminOnly       = true
ENT.RenderGroup     = RENDERGROUP_OPAQUE
 
 
AccessorFunc(ENT, "engageDistance", "EngageDistance")
AccessorFunc(ENT, "damage", "Damage")
AccessorFunc(ENT, "spread", "Spread")
AccessorFunc(ENT, "fireDelay", "FireDelay")
AccessorFunc(ENT, "targetClasses", "TargetClasses")

local function IsSandbox()
    return gmod.GetGamemode().IsSandboxDerived
end

function ENT:GetTargetPos(target)
        local targetPos = target:GetPos()
       
        if target:GetBoneName(0) != "__INVALIDBONE__" then
                targetPos = target:GetBonePosition(0)
        end
       
        local head = target:LookupBone("ValveBiped.Bip01_L_Clavicle")
        if head then
                targetPos = target:GetBonePosition(head)
        end
       
        return targetPos
end

function ENT:GetTarget()
        local target
        if SERVER then
                target = self.target
        else
                target = self:GetNWEntity("target")
        end
       
        if not self:IsValidTarget(target) then return nil end
       
        return target
end

function ENT:IsValidTarget(target)
        if not IsValid(target) then return false end
       
        if target:IsPlayer() then
                if not target:Alive() then return false end
        end
       
        if target:IsNPC() then
                if SERVER and target:Health() < 1 then return false end
        end
       
        return true
end

function ENT:FindTarget()
        local closestTarget
        local closestDistance
		
        for k, v in pairs( ents.FindInSphere( self.Entity:GetPos(), 10000 ) ) do
                if table.HasValue(self.targetClasses, v:GetClass()) and self:IsValidTarget(v) then
                        local entPos = self:GetTargetPos(v)
                        local barrelPos = self:GetBarrelPos()
                        local dist = (entPos-barrelPos):Length()
						
                        if (not closestTarget or dist < closestDistance) and dist < self.engageDistance and v:Visible(self) then
                                closestTarget = v
                                closestDistance = dist
                        end
                end
        end
		
        return closestTarget
end