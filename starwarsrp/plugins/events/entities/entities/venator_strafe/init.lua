AddCSLuaFile("shared.lua")
AddCSLuaFile("cl_init.lua")

include("shared.lua")

function ENT:Initialize()
	self:SetModel("models/sweaw/ships/rep_venator.mdl")

	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetCollisionGroup(COLLISION_GROUP_IN_VEHICLE)
	self:SetTrigger(true)
	self:SetModelScale(self:GetModelScale() * .25, 0 )
	self:Activate()
	-- util.SpriteTrail(self, 0, Color(255,0,0), false, 15, 1, 4, 1/(15+1)*0.5, "innovativegamers/ig_logo.vmt")
	

	local phys = self:GetPhysicsObject()
	if (IsValid(phys)) then
		phys:Wake()
		phys:EnableGravity(false)
	end
	
	self.SpawnTime = CurTime()
	
	timer.Simple(300, function()
	    
	    if IsValid(self) then
            self:Remove()
        end
    
	end)
	
	self:SetEngageDistance(15000)
	self:SetDamage(10000)
	self:SetSpread(1)
	self:SetFireDelay(1)
	
	self:SetTargetClasses({
		"npc_vj_swbd_commander",
		"npc_vj_swbd_commando",
		"npc_vj_swbd_genosis",
		"npc_vj_swbd_standard",
		"npc_vj_swbd_super",
		"npc_vj_swbd_super_rocket",
		"npc_vj_swbd_tactical",
		"npc_combine_grenadier",
		"npc_combine_guard",
		"npc_combine_soldier",
		"npc_combine_super_soldier",
		"npc_combine_s",
		"npc_aat_body",
		"npc_vj_swbd_tri_droid",
		"npc_vj_swbd_droideka",
		"npc_vj_swbd_dwarf_spider",
		"npc_vj_rancor_huge",
		"npc_vj_rancor_medium"
	})
	--Vector(728, -3367, 347)
	-- ParticleEffectAttach("choreo_launch_rocket_glow2", PATTACH_ABSORIGIN_FOLLOW, self, 1)
	-- ParticleEffect("choreo_launch_rocket_glow2", self:GetPos(), Angle(0,0,0), self)
	
end

function ENT:PhysicsCollide(physdata)
	if ( physdata.HitEntity == game.GetWorld() ) then
		timer.Simple(15, function()
			self:SetNotSolid(true)
			if IsValid(self) then self:Remove() end
		end)
	end
end

function ENT:OnRemove()
	print("removed")
end

local bt = {0,17,18}

function ENT:GetBarrelPos()
	return self:GetBoneMatrix(table.Random(bt)):GetTranslation()
end

local bullet = {}
bullet.Src = nil
bullet.Dir = nil
bullet.Spread = nil
bullet.Damage = nil
bullet.Num = 1
bullet.Force = 100
bullet.AmmoType = "rifle"
bullet.Attacker = nil
bullet.Hull = HULL_TINY
bullet.Tracer = 3
bullet.TracerName = "LaserTracer"
bullet.Callback = nil

function ENT:Think()

	local speed = math.random(250,500)

	local phys = self:GetPhysicsObject()
	phys:SetMass(100000)
	if ( IsValid( phys ) ) then
		phys:SetVelocity( self.Direction:Forward() * 200 )
	end

	-- shooting shit below

	if not self.nextSearch or CurTime() >= self.nextSearch then
		self.target = self:FindTarget()
		self:SetNWEntity("target", self.target or Entity(-1))
		self.nextSearch = CurTime() + 0.3
	end

	local target = self:GetTarget()
	if target then
			if not self.nextShot or CurTime() >= self.nextShot then
			
				local barrelPos = self:GetBarrelPos()
				local targetPos = self:GetTargetPos(target)
			
				self.nextShot = CurTime() + self.fireDelay
				
				bullet.Src = barrelPos
				bullet.Dir = (targetPos-barrelPos):GetNormalized()
				bullet.Damage = self.damage
				bullet.Spread = self.spread
				bullet.Attacker = self
				bullet.Tracer = 1
				bullet.TracerName  = "vj_turbolaser_blue"

				self:FireBullets(bullet)
				
				local explode = ents.Create( "env_explosion" ) -- creates the explosion
				explode:SetPos( targetPos )
				explode:Spawn()
				explode:SetKeyValue( "iMagnitude", "220" )
				explode:Fire( "Explode", 0, 0 )
				explode:EmitSound( "weapon_AWP.Single", 400, 400 )
				
			end
	end

	self:NextThink(CurTime()+self.fireDelay/2)

end

