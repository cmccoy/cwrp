AddCSLuaFile("shared.lua")
AddCSLuaFile("cl_init.lua")

include("shared.lua")

function ENT:Initialize()
	-- self:SetModel("models/props_wasteland/cargo_container01b.mdl")
	self:SetModel("models/csp_invhand/csp_invhand.mdll")
	
	-- self:SetSolid(SOLID_NONE)
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetCollisionGroup(COLLISION_GROUP_DEBRIS_TRIGGER)
	-- self:SetNotSolid(true)
	self:SetTrigger(true)
	self:Activate()
	

	local phys = self:GetPhysicsObject()
	phys:SetMass(100000)
	if (IsValid(phys)) then
		phys:Wake()
		phys:EnableGravity(false)
	end
	
end

function ENT:PhysicsCollide(physdata)
	if ( self.done and physdata.HitEntity == game.GetWorld() ) then
		self:Remove()
	end
end

function ENT:Think()

end