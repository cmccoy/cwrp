include("shared.lua")

function ENT:Initialize()	
	--self.RotorSound = CreateSound(self, "NPC_AttackHelicopter.Rotors")
	--self.RotorSound:Play()
	self:SetRenderAngles(self:GetAngles() + Angle(0,180,0))
end

function ENT:OnRemove()
	--self.RotorSound:Stop()
	--self.RotorSound = nil
end

function ENT:Draw()
	self:DrawModel()

	self.LastPaint = RealTime()
	
	self:FrameAdvance(RealTime() - self.LastPaint)
end