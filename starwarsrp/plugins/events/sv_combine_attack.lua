local dropZones = {
    Vector(-8589, -764, 60),
    Vector(-6989, -1220, 60),
    Vector(-7061, -3888, 60),
    Vector(-8586, -3948, 60),
    Vector(-6674, 591, 60),
    Vector(-9823, 1388, 60),
    Vector(-7913, 3252, 60)
}
local combineSpawns = {
    {
        EntityName = "Combine Soldier",
        SpawnPosition = Vector(100.000000, 100.000000, 1.000000),
        Entities = "npc_combine_soldier",
        WeaponsList= "weapon_vj_hl2_ar2"
    },
    {
        EntityName = "Combine Soldier",
        SpawnPosition = Vector(-100.000000, 100.000000, 1.000000),
        Entities = "npc_combine_soldier",
        WeaponsList= "weapon_vj_hl2_ar2"
    },
    {
        EntityName = "Combine Grenadier",
        SpawnPosition = Vector(100.000000, -100.000000, 1.000000),
        Entities = "npc_combine_grenadier",
        WeaponsList= "weapon_vj_hl2_ar2"
    },
    {
        EntityName = "Combine Grenadier",
        SpawnPosition = Vector(-100.000000, -100.000000, 1.000000),
        Entities = "npc_combine_grenadier",
        WeaponsList= "weapon_vj_hl2_ar2"
    },
    {
        EntityName = "Combine Super Solider",
        SpawnPosition = Vector(150.000000, 50.000000, 1.000000),
        Entities = "npc_combine_super_soldier",
        WeaponsList= "weapon_vj_hl2_ar2"
    }
}

local guardDrops = {
    Vector(-7721, -4740, 60),
    Vector(-8251, -4746, 60)
}

local guardSpawns = {
    {
        EntityName = "Combine Guard",
        SpawnPosition = Vector(100.000000, 100.000000, 1.000000),
        Entities = "npc_combine_guard"
    },
    {
        EntityName = "Combine Guard",
        SpawnPosition = Vector(-100.000000, 100.000000, 1.000000),
        Entities = "npc_combine_guard"
    },
    {
        EntityName = "Combine Guard",
        SpawnPosition = Vector(100.000000, -100.000000, 1.000000),
        Entities = "npc_combine_guard"
    },
    {
        EntityName = "Combine Guard",
        SpawnPosition = Vector(-100.000000, -100.000000, 1.000000),
        Entities = "npc_combine_guard"
    },
}

local combineKills = 0
local trackKills = false
local percentTracker = 0.25
local totalKills = 0

function combineRaid()
    BroadcastLua([[chat.AddText(Color(255,0,0), "[LISTENING POST] ", Color(100,255,100), "Hyperspace signature detected ETA 5 minutes")]])
    BroadcastLua([[chat.AddText(Color(255,0,0), "[LISTENING POST] ", Color(100,255,100), "Ship class: Combine")]])
    setDefcon(4)
    if(#player.GetAll() <= 10) then
        totalKills = 50
    else
        totalKills = #player.GetAll() * 5
    end
    timer.Simple(300, function()
        crCreateShip()
    end)

    timer.Simple(310, function()
        setDefcon(3)
        crSpawnDropPods()
    end)

    timer.Simple(780, function()
        cleanupCombineRaid()
    end)
end

function crCreateShip()
    BroadcastLua([[chat.AddText(Color(255,0,0), "[Conglomerate Forces] ", Color(100,255,100), "We'll make short work of this outpost")]])
    local ent = ents.Create("vanilla_hyperspace_ship")
    ent:SetKeyValue("AI", 0)
    ent:SetKeyValue("Freeze", 1)
    ent:SetKeyValue("Flip", 1)
    ent:SetKeyValue("Shake", 1)
    ent:SetKeyValue("Sound", 1)
    ent:SetKeyValue("SpawnModel", 1)
    ent:SetKeyValue("ActualModel", "models/csp_bothan/csp_bothan.mdl")
    ent:SetPos(Vector(-7706, -3891, 6018))
    ent:SetAngles(Angle(0,180,0))
    ent:Spawn()
    ent:SetMoveType(MOVETYPE_NONE)
end

function crSpawnDropPods()

    BroadcastLua([[chat.AddText(Color(255,0,0), "[Conglomerate Forces] ", Color(100,255,100), "Deploying drop pods")]])

    combineKills = 0
    trackKills = true
    for i = 1, #dropZones do
        local canisterTarget = ents.Create( "info_target" )
        local entCanister = ents.Create( "env_headcrabcanister" )
        entCanister:SetPos( dropZones[i] )
        canisterTarget:SetKeyValue( "targetname", "d" )
        canisterTarget:SetPos( Vector(-7706, -3891, 6018) )
        canisterTarget:SetParent(ents.FindByClass("vanilla_modelship")[1])
        canisterTarget:Spawn( )
        canisterTarget:Activate()
        entCanister:SetAngles( (canisterTarget:GetPos( ) - dropZones[i] ):Angle( ) )
        entCanister:SetKeyValue( "LaunchPositionName", "d" )
        entCanister:SetKeyValue( "FlightSpeed", 25 )
        entCanister:SetKeyValue( "FlightTime", 2 )
        entCanister:SetKeyValue( "Damage", 1000 )
        entCanister:SetKeyValue( "DamageRadius", 1000 )
        entCanister:SetKeyValue( "SmokeLifetime", 60 )
        entCanister:Fire( "Spawnflags", "16384", 0 )
        entCanister:Fire( "FireCanister", "", 0 )
        entCanister:Fire( "AddOutput", "OnImpacted OpenCanister", 0 )
        entCanister:SetParent(ents.FindByClass("vanilla_modelship")[1])
        entCanister:Spawn( )
        entCanister:Activate( )
        timer.Simple(600, function()
            if(IsValid(entCanister)) then
                entCanister:Remove()
            end
            if(IsValid(canisterTarget)) then
                canisterTarget:Remove()
            end
        end)

        timer.Simple(2.5, function()
            local spawner = ents.Create("obj_vj_spawner_base")
            spawner.EntitiesToSpawn = {}
            spawner:SetPos(dropZones[i])
            spawner:SetParent(ents.FindByClass("vanilla_modelship")[1])
			local angs = Angle(0,0,0)
            angs.pitch = 0
            angs.roll = 0
            angs.yaw = angs.yaw + 180
            spawner:SetAngles(angs)
            for k, v in pairs(combineSpawns) do
                table.insert(spawner.EntitiesToSpawn,{EntityName = "NPC"..math.random(1,99999999),SpawnPosition = {vForward=v.SpawnPosition.x,vRight=v.SpawnPosition.y,vUp=v.SpawnPosition.z},Entities = {v.Entities},WeaponsList={v.WeaponsList}})
            end
			spawner.TimedSpawn_Time = 3
			spawner:Spawn()
			spawner:Activate()
        end)
    end

end

function crShipLeave()
    BroadcastLua([[chat.AddText(Color(255,0,0), "[Conglomerate Forces] ", Color(100,255,100), "We'll be back.....")]])
    trackKills = false
    percentTracker = 0.25
    combineKills = 0
    totalKills = 0
    for k, v in ipairs(ents.FindByClass("vanilla_modelship")) do
        local ent = v
        local sound = ents.Create("vanilla_highwake")
        t = tostring(math.random(0,50000))

        if not IsValid(ent) then return end
        sound:SetPos(ent:GetPos())
        sound:Spawn()
        sound:SetNoDraw(true)

        if IsValid(ent) then
            timer.Simple(3,function()
                if not IsValid(ent) then return end
                timer.Create(t,0,0.2,function()
                    if not ent:IsValid() then return end
                    ent:SetPos(ent:GetPos() - ent:GetForward() * 700)
                end)
            end)
            timer.Create(t .. "Ender",5+ 1,1,function()
                timer.Remove(t)
                if not IsValid(ent) then return end
                ent:Remove()
            end)
        end
    end
end

function guardDrop()
    for i = 1, #guardDrops do
        local canisterTarget = ents.Create( "info_target" )
        local entCanister = ents.Create( "env_headcrabcanister" )
        entCanister:SetPos( guardDrops[i] )
        canisterTarget:SetKeyValue( "targetname", "guarddrop" .. i )
        canisterTarget:SetPos( Vector(-7706, -3891, 6018) )
        canisterTarget:SetParent(ents.FindByClass("vanilla_modelship")[1])
        canisterTarget:Spawn( )
        canisterTarget:Activate()
        entCanister:SetAngles( (canisterTarget:GetPos( ) - guardDrops[i] ):Angle( ) )
        entCanister:SetKeyValue( "LaunchPositionName", "guarddrop".. i )
        entCanister:SetKeyValue( "FlightSpeed", 25 )
        entCanister:SetKeyValue( "FlightTime", 2 )
        entCanister:SetKeyValue( "Damage", 1000 )
        entCanister:SetKeyValue( "DamageRadius", 1000 )
        entCanister:SetKeyValue( "SmokeLifetime", 60 )
        entCanister:Fire( "Spawnflags", "16384", 0 )
        entCanister:Fire( "FireCanister", "", 0 )
        entCanister:Fire( "AddOutput", "OnImpacted OpenCanister", 0 )
        entCanister:SetParent(ents.FindByClass("vanilla_modelship")[1])
        entCanister:Spawn( )
        entCanister:Activate( )
        timer.Simple(600, function()
            if(IsValid(entCanister)) then
                entCanister:Remove()
            end
            if(IsValid(canisterTarget)) then
                canisterTarget:Remove()
            end
        end)

        timer.Simple(2.5, function()
            local spawner = ents.Create("obj_vj_spawner_base")
            spawner.EntitiesToSpawn = {}
            spawner:SetPos(guardDrops[i])
            spawner:SetParent(ents.FindByClass("vanilla_modelship")[1])
            local angs = Angle(0,0,0)
            angs.pitch = 0
            angs.roll = 0
            angs.yaw = angs.yaw + 180
            spawner:SetAngles(angs)
            for k, v in pairs(guardSpawns) do
                table.insert(spawner.EntitiesToSpawn,{EntityName = "NPC"..math.random(1,99999999),SpawnPosition = {vForward=v.SpawnPosition.x,vRight=v.SpawnPosition.y,vUp=v.SpawnPosition.z},Entities = {v.Entities}})
            end
            spawner.TimedSpawn_Time = 20
            spawner:Spawn()
            spawner:Activate()
        end)
    end
end

hook.Add("OnNPCKilled", "[IG] Combine raid npc kill tracker", function(npc, attacker)
    if trackKills then
        if (attacker:IsPlayer() && string.match(npc:GetClass(), "^npc_combine_.+")) then
            combineKills = combineKills + 1
        end

        if( combineKills / totalKills > percentTracker) then
            BroadcastLua([[chat.AddText(Color(255,0,0), "[Conglomerate Forces] ", Color(100,255,100), "Let's see how they deal with these")]])
            guardDrop()
            percentTracker = 1.5
        end

        if (combineKills >= totalKills) then
            setDefcon(5)
            crShipLeave()
        end
    end
end)

function cleanupCombineRaid()
    for k, v in ipairs(ents.FindByClass("vanilla_modelship")) do
        if IsValid(v) then
            setDefcon(5)
            crShipLeave()
        end
    end
end

hook.Add("OnNPCKilled", "[IG] Pay combine kills", function(npc, attacker, inflictor)
    
    if (attacker:IsPlayer() && string.match(npc:GetClass(), "^npc_combine_.+")) then
        local character = attacker:GetCharacter()
        if (character) then
            if(attacker:IsVip() || attacker:IsStaff()) then
                local money = math.Round(npc:GetMaxHealth() / 10, 0)
                character:GiveMoney(money + math.Round(money * .1, 0) )
            else
                character:GiveMoney(math.Round(npc:GetMaxHealth() / 10, 0))
            end
        end
    end

end)