ix.config.events = {}

ix.config.events.rancorSpawns = {
    Vector(78, 11353, -474),
    Vector(1241, 14222, -367),
    Vector(2065, 10380, -415)
}

ix.config.events.wildSpawns = {
    Vector(7234, -1262, -116),
    Vector(10980, 823, -108),
    Vector(9949, 7580, 614),
    Vector(14120, -7545, -40),
    Vector(12437, -14812, 89),
    Vector(7289, -9929, 56),
    --Vector(-7461, -8580, 360),
    Vector(7188, 3573, -443)
}

ix.config.Add("rancorsActive", false, "Whether or not rancors should spawn in the cave", 
    function(oldValue, newValue)
        if (newValue) then
            if(string.match(game.GetMap(), "^rp_anaxes_i.+")) then
                if SERVER then
                    for k, v in ipairs(ix.config.events.rancorSpawns) do
                        local ent = ents.Create("npc_vj_rancor_medium")
                        ent:SetPos(v)
                        ent:Spawn()
                    end
                end
            else
                if SERVER then
                    ix.config.Set("rancorsActive", false)
                end
            end
        else
            if SERVER then
                for k, v in pairs(ents.GetAll()) do
                    if (string.match(v:GetClass(), "^npc_vj_rancor_.+")) then
                        if (IsValid(v)) then
                            v:Remove()
                        end
                    end
                end
            end
        end 
    end,
    { category = "events" }
)

ix.config.Add("randomEvents", false, "Whether or not random events should happen on the server", 
    function(oldValue, newValue)
        if (newValue) then
            if(string.match(game.GetMap(), "^rp_anaxes_i.+")) then
                if SERVER then
                    if(timer.Exists("[IG] Event Timer")) then
                        timer.Remove("[IG] Event Timer")
                    end
                    timer.Create("[IG] Event Timer", 3600, 0, function()
                        local query = mysql:Select("ix_events")
                            query:WhereGT("status", "0")
                            query:Callback(function(result)
                                if (istable(result) and #result > 0) then
                                    if (result[1].status != 1) then
                                        if (#player.GetAll() % 2 == 0) then
                                            print("[EVENTS] Running droid raid")
                                            droidRaid()
                                        else
                                            print("[EVENTS] Running combine raid")
                                            combineRaid()
                                        end
                                    end
                                else
                                    if (#player.GetAll() % 2 == 0) then
                                        print("[EVENTS] Running droid raid")
                                        droidRaid()
                                    else
                                        print("[EVENTS] Running combine raid")
                                        combineRaid()
                                    end
                                end
                            end)
                        query:Execute()
                    end)
                end
            else
                ix.config.Set("randomEvents", false)
            end
        else

        end
    end,
    {category = "events"}
)

ix.config.Add("wildNPCs", false, "Whether or not wild should spawn around the surface", 
    function(oldValue, newValue)
        if (newValue) then
            if(string.match(game.GetMap(), "^rp_anaxes_i.+")) then
                if SERVER then
                    for k, v in ipairs(ix.config.events.wildSpawns) do
                        local ent = ents.Create("sent_vj_as_randdronebsp")
                        ent:SetPos(v)
                        ent:Spawn()
                    end
                end
            else
                if SERVER then
                    ix.config.Set("wildNPCs", false)
                end
            end
        else
            if SERVER then
                for k, v in pairs(ents.GetAll()) do
                    if (string.match(v:GetClass(), "^sent_vj_as_.+")) then
                        if (IsValid(v)) then
                            v:Remove()
                        end
                    end
                end
            end
        end 
    end,
    { category = "events" }
)

hook.Add("OnNPCKilled", "[IG] Pay wild npc kills", function(npc, attacker, inflictor)
    
    if (attacker:IsPlayer() && string.match(npc:GetClass(), "^npc_vj_as_.+")) then
        local character = attacker:GetCharacter()
        if (character) then
            if(attacker:IsVip() || attacker:IsStaff()) then
                local money = math.random(2, 4)
                character:GiveMoney(money + math.Round(money * .5, 0) )
            else
                character:GiveMoney(math.random(2, 4))
            end
        end
    end

end)

hook.Add("ScaleNPCDamage", "[IG] Scale wild dmg taken", function(npc, hitgroup, dmginfo)
    
    if (string.match(npc:GetClass(), "^npc_vj_as_.+")) then
        dmginfo:ScaleDamage( .25 )
    end

end)

ix.config.Add("autoTurrets", true, "Whether or not auto turrets should be on", nil, {category = "events"}
)

hook.Add("InitPostEntity", "[IG] Enable events on main server", function()

    if (string.match(game.GetMap(), "^rp_anaxes_i.+")) then
        ix.config.Set("rancorsActive", true)
        ix.config.Set("randomEvents", true)
    end

end)