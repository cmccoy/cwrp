local PLUGIN = PLUGIN

PLUGIN.name = "Event Module"
PLUGIN.author = "Vac"
PLUGIN.description = "Sets up automated events"

ix.util.Include("sv_droid_attack.lua")
ix.util.Include("sv_combine_attack.lua")
ix.util.Include("sv_rancor.lua")
ix.util.Include("sh_config.lua")

if SERVER then
    util.AddNetworkString("ixVenatorText")
end

ix.command.Add("venatorStrafe", {
	description = "Spawns a venator that does a strafe run of the map, optional message to send on it's spawn",
	arguments = {ix.type.text},
	OnRun = function(self, client, message)
        if(client:IsGameMaster() || client:IsSuperAdmin()) then
            if (message) then
                net.Start("ixVenatorText")
                    net.WriteString(message)
                net.Broadcast()
            end
            local mid = Vector(14239, -14202, 6189)
	
            local rand = Angle( 0, math.random(90, 180), 0 )
            local randmath = math.random(1,5)

            local plane = ents.Create("venator_strafe")
            plane:SetPos(Vector(mid.x, mid.y,mid.z))
            plane:SetAngles( rand + Angle(0,90,0) )
            plane:Spawn()
            plane.Direction = rand
        end
	end
})

if (CLIENT) then
    net.Receive("ixVenatorText", function(len,pl)
        local text = net.ReadString()
        local name = "[Venator-" ..math.random(1000, 9999) .."] "
        chat.AddText(Color( 100, 100, 255 ), name, Color( 100, 255, 100 ), text)
    end)
end