ix.command.Add("leave", {
    privilege = "Leave your current battalion",
    description = "Removes your character from the current battalion and removes your whitelist to the battalion.",

    OnRun = function(self, client)
        local character = client:GetCharacter()

        if (character) then

            if(character:GetFaction() == "" || character:GetFaction() == nil) then
                return "You're not in a faction"
            end

            local faction = ix.faction.indices[character:GetFaction()]

            if (client.isDroid) then
                return "You can not leave a droid faction."
            end

            local steamID64 = client:SteamID64()
            local query = mysql:Select("ix_players")
                query:Select("data")
                query:Where("steamid", steamID64)
                query:Limit(1)
                query:Callback(function(result)
                    if (istable(result) and #result > 0) then
                        local data = util.JSONToTable(result[1].data or "[]")
                        local whitelists = data.whitelists and data.whitelists[Schema.folder]

                        if (!whitelists or !whitelists[faction.uniqueID]) then
                            return
                        end

                        whitelists[faction.uniqueID] = nil

                        local updateQuery = mysql:Update("ix_players")
                            updateQuery:Update("data", util.TableToJSON(data))
                            updateQuery:Where("steamid", steamID64)
                        updateQuery:Execute()

                        if(character:GetFaction() == FACTION_CG) then
                            client:StripCgWeps()
                        end

                        character:SetFaction(FACTION_CLONE)
                        character:SetModel(table.Random(ix.faction.indices[FACTION_CLONE].models))
                        character:SetRank(1)
                        character:SetClass(nil)
                        SetupName(character)

                        client:NotifyLocalized("You left the " .. faction.name .. " battalion!")
                    end
                end)
            query:Execute()
        end
    end
})