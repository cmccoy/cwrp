local PLAYER = FindMetaTable("Player")

function PLAYER:Demote()
    local character = self:GetCharacter()

    if (character) then
        local currentRank = character:GetRank()
        local currentFaction = ix.faction.indices[character:GetFaction()]

        if (currentFaction.index == FACTION_RECRUIT || currentFaction.index == FACTION_CLONE) then
            self:NotifyLocalized("You were unable to be demoted.")
            return
        end

        if(currentRank <= 1) then
            return false;
        end

        for k, v in pairs(player.GetAll()) do
            local tcharacter = v:GetCharacter()

            if (tcharacter && tcharacter:GetFaction() == character:GetFaction()) then
                v:NotifyLocalized(character:GetName() .. " was demoted!")
            end
        end

        local r = rankToClass(currentRank - 1, currentFaction.index)
        character:SetClass(r.index)
        character:SetRank(currentRank - 1)
        character:SetModel(r.model)        

        self:NotifyLocalized("You've been demoted!")
    end

    return false
end


ix.command.Add("demote", {
    arguments = {ix.type.player},
    privilege = "Demotes a player in their battalion.",
    description = "Demotes a player in their current battalion.",

    OnRun = function(self, client, target)
        local character = client:GetCharacter()
        local tcharacter = target:GetCharacter()

        if (character && tcharacter) then
            if (character:GetFaction() == FACTION_CLONE) then
                return "You're not in a battalion."
            elseif (character:GetFaction() == FACTION_RECRUIT) then
                return "You're not in a battalion."
            end
            
            if(character:GetFaction() != tcharacter:GetFaction() && !client:IsAdmin()) then
                return "You can't demote someone in another battalion."
            end

            local class = rankToClass(character:GetRank(), character:GetFaction())

            if (class.canPromote || client:IsAdmin()) then
                if(character:GetRank() > tcharacter:GetRank() || client:IsAdmin()) then
                    if (tcharacter:GetRank() > 1) then
                        target:Demote()
                        SetupName(tcharacter)

                        local fac = ix.faction.indices[tcharacter:GetFaction()]
                        hook.Run("SwrpOnCharacterDemoted", client, target, tcharacter:GetRank(), fac.name)
                    else
                        client:NotifyLocalized(tcharacter:GetName() .. " is already at the lowest rank!")
                    end
                else
                    client:NotifyLocalized("The target is a higher rank than you.")
                end
            else
                client:NotifyLocalized("You are not allowed to demote people.")
            end
        end
    end
})