local PLUGIN = PLUGIN

PLUGIN.name = "Ranks and Specialties"
PLUGIN.author = "Vac"
PLUGIN.description = "Sets up ranks and specialty variables"

ix.util.Include("sh_demote.lua")
ix.util.Include("sh_join.lua")
ix.util.Include("sh_invite.lua")
ix.util.Include("sh_kick.lua")
ix.util.Include("sh_leave.lua")
ix.util.Include("sh_promote.lua")

game.AddParticles("particles/achievement.pcf")
PrecacheParticleSystem("achieved")

function PLUGIN:OnCharacterCreated(client, character)

    if (character) then
        local query = mysql:Update("ix_characters")
		    query:Where("id", character:GetID())
            query:Update("specialty", character:GetSpecialty() or "none")
            query:Update("rank", character:GetRank() || 1)
        query:Execute()
        character:SetRank(character:GetRank() || 1)
        character:SetSpecialty(character:GetSpecialty() || "none")
    end

end

function PLUGIN:OnSpawn(client)
    local character = client:GetCharacter()

    if (character) then
        character:SetRank(character:GetRank() || 1)
        character:SetSpecialty(character:GetSpecialty() || "none")
    end
end

ix.char.RegisterVar("rank", {
    field = "rank",
    fieldType = ix.type.number,
    default = 1,
    bNoDisplay = true,
    OnSet = function(self, value)
         local client = self:GetPlayer()
         if(IsValid(client)) then
            self.vars.rank = tonumber(value) || 1
            -- @todo refactor networking of character vars so this doesn't need to be repeated on every OnSet override
            net.Start("ixCharacterVarChanged")
                net.WriteUInt(self:GetID(), 32)
                net.WriteString("rank")
                net.WriteType(self.vars.rank)
            net.Broadcast()
         end
    end,
    OnGet = function(self, default)
        local currentRank = self.vars.rank
        if(currentRank == nil || currentRank == "") then
            return default;
        end
        return currentRank
    end
})

ix.char.RegisterVar("specialty", {
    field = "specialty",
    fieldType = ix.type.string,
    default = "none",
    bNoDisplay = true,
    OnSet = function(self, value)
        local client = self:GetPlayer()
        if(IsValid(client)) then
            self.vars.specialty = value || "none"
            -- @todo refactor networking of character vars so this doesn't need to be repeated on every OnSet override
            net.Start("ixCharacterVarChanged")
                net.WriteUInt(self:GetID(), 32)
                net.WriteString("specialty")
                net.WriteType(self.vars.specialty)
            net.Broadcast()
        end
    end,
    OnGet = function(self, default)
        local currentSpecialty = self.vars.specialty
        if(currentSpecialty == nil || currentSpecialty == "") then
            return default;
        end
        return currentSpecialty
    end
})