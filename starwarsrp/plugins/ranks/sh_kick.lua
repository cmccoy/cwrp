ix.command.Add("kick", {
    arguments = {ix.type.player},
    privilege = "Kicks a player from the battalion",
    description = "Kicks a player from their current battalion",

    OnRun = function(self, client, target)
        local character = client:GetCharacter()
        local tcharacter = target:GetCharacter()

        if (character && tcharacter) then
            if (character:GetFaction() == FACTION_CLONE) then
                return "You're not in a battalion"
            elseif (character:GetFaction() == FACTION_RECRUIT) then
                return "You're not in a battalion"
            end
            
            if(character:GetFaction() != tcharacter:GetFaction()) then
                return "You can't kick someone out of another battalion"
            end

            local class = rankToClass(character:GetRank(), character:GetFaction())

            if (class.canPromote || client:IsAdmin()) then
                if(character:GetRank() > tcharacter:GetRank() || client:IsAdmin()) then

                    local tfac = tcharacter:GetFaction()
                    local faction = ix.faction.indices[tfac]

                    if(tcharacter:GetFaction() == FACTION_CG) then
                        target:StripCgWeps()
                    end

                    tcharacter:SetRank(1)
                    tcharacter:SetFaction(FACTION_CLONE)
                    tcharacter:SetClass(nil)
                    tcharacter:SetModel(table.Random(ix.faction.indices[FACTION_CLONE].models))
                    target:SetWhitelisted(tfac, false)
                    SetupName(tcharacter)

                    local tname = tcharacter:GetName()
                    for k, v in pairs(player.GetAll()) do
                        v:NotifyLocalized(tname .. " was kicked from their battalion!")
                    end

                    hook.Run("SwrpOnCharacterKicked", client, target, faction.name)
                else
                    client:NotifyLocalized("The target is a higher rank than you.")
                end
            else
                client:NotifyLocalized("You are not allowed to kick people")
            end
        end
    end
})

ix.command.Add("kickByID", {
    arguments = {ix.type.string},
    privilege = "Kicks a player from the battalion by steamID",
    description = "Kicks a player from their current battalion",

    OnRun = function(self, client, ID)
        local character = client:GetCharacter()

        if (character) then
            if (character:GetFaction() == FACTION_CLONE) then
                return "You're not in a battalion"
            elseif (character:GetFaction() == FACTION_RECRUIT) then
                return "You're not in a battalion"
            end

            local class = rankToClass(character:GetRank(), character:GetFaction())

            if (class.canPromote || client:IsAdmin()) then

                for k, v in pairs(player.GetAll()) do
                    if(v:GetCharacter() && v:GetCharacter():GetID() == ID) then
                        if(character:GetFaction() == v:GetCharacter():GetFaction()) then
                            if(character:GetRank() > v:GetCharacter():GetRank()) then
                                ix.command.Run(player, "kick", {v})
                                client:NotifyLocalized("Player is online, kicked using normal command")
                                return
                            end
                        end
                    end
                end

                local query = mysql:Select("ix_characters")
                    query:Where("id", tonumber(ID))
                    query:Where("faction", ix.faction.indices[character:GetFaction()].name)
                    query:Limit(1)
                    query:Callback(function(result)
                        if (istable(result) and #result > 0) then
                            if (result[1].rank < character:GetRank()) then
                                for key, value in pairs(ix.char.loaded) do
                                    if (value.vars) then
                                        if(value.vars.steamID == ID) then
                                            value:SetFaction(FACTION_CLONE)
                                            value:SetRank(1)
                                            value:SetModel(table.Random(ix.faction.indices[FACTION_CLONE].models))
                                            value:SetClass(nil)
                                            newName = SetupName(value)
                                        end
                                    end
                                end
                                
                                local n = result[1].name
                                n = string.Explode(" ", n)
                                n = "CT " .. result[1].id .. " ".. n[3]

                                local query = mysql:Update("ix_characters")
                                    query:Where("id", ID)

                                    query:Update("name", n)
                                    query:Update("rank", 1)
                                    query:Update("faction", CT)
                                query:Execute()    


                                hook.Run("SwrpOnCharacterKickedID", client, result[1].name, result[1].faction)
                            end
                        end
                    end)
                query:Execute()
            end
        end
    end
})