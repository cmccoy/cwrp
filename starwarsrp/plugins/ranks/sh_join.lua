ix.command.Add("join", {
    arguments = {ix.type.text},
    privilege = "Join a faction",
    description = "Join a faction that you were invited to",

    OnRun = function(self, client, fac)

        if(fac == "") then
            return "No faction was typed"
        end

        if(ix.faction.teams[fac] == nil) then
            return "Faction " .. fac .. " does not exist"
        end

        local character = client:GetCharacter()

        if (character) then
            local faction = ix.faction.teams[fac]
            local steamID64 = client:SteamID64()

            if (character:GetFaction() == faction.index) then
                return "You're already in the " .. faction.name .. "!"
            end

            if (faction.index == FACTION_UTILITY_DROID || faction.index == FACTION_COMBOT_DROID) then
                return "You must make a new character to be a droid."
            end

            local query = mysql:Select("ix_players")
                query:Select("data")
                query:Where("steamid", steamID64)
                query:Limit(1)
                query:Callback(function(result)
                    if (istable(result) and #result > 0) then
                        local data = util.JSONToTable(result[1].data or "[]")
                        local whitelists = data.whitelists and data.whitelists[Schema.folder]
                        for k, v in pairs(whitelists) do
                            if (k == fac) then
                                character:SetFaction(faction.index)
                                character:SetRank(1)
                                character:SetModel(table.Random(faction.models))
                                character:SetClass(faction.defaultClass)
                                SetupName(character)
                                if(character:GetFaction() == FACTION_CG) then
                                    client:GiveCgWeps()
                                elseif(character:GetFaction() == FACTION_JEDI) then
                                    if (client:HasWeapon("tfa_swch_dc15a")) then
                                        client:StripWeapon("tfa_swch_dc15a")
                                    end
                                    character:EquipNewLoadout(client, character:GetLoadout(), ix.plugin.Get("loadout_selection").defaultJediLoadout )
                                end
                                client:NotifyLocalized("You joined the " .. fac .. " battalion!")

                                hook.Run("SwrpOnPlayerJoin", client, fac)
                                return
                            end
                        end
                        client:NotifyLocalized("You are not whitelisted for the " .. fac .. " battalion!")
                    end
                end)
            query:Execute()
        end
    end
})