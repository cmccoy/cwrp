local PLAYER = FindMetaTable("Player")

local canPromoteTBL = {
    "2LT",
    "1LT",
    "CPT",
    "LTC",
    "COL",
    "XO",
    "COM"
}

function PLAYER:Promote()

    local character = self:GetCharacter()

    if(character) then
        local currentRank = character:GetRank()
        local currentFaction = ix.faction.indices[character:GetFaction()]

        if (currentFaction.index == FACTION_RECRUIT || currentFaction.index == FACTION_CLONE) then
            self:NotifyLocalized("You were unable to be promoted.")
            return
        end

        if(currentRank == currentFaction.maxPromoteRank) then
            return
        end
        local r = rankToClass(currentRank + 1, currentFaction.index)
        --TODO refactor this
        character:SetClass(r.index)
        character:SetRank(currentRank + 1)
        -- When you come back from drinking go through and add a model parameter to each class...
        character:SetModel(r.model)

        self:NotifyLocalized("You've been promoted to ".. rankToClass(currentRank, character:GetFaction()).nextRank .. "!")
        
        -- local vPoint = self:GetPos() + Vector(0,0,64)
        -- local effectdata = EffectData()
        -- effectdata:SetOrigin( vPoint )
        -- util.Effect( "rank_up_effect", effectdata )
        ParticleEffect( "achieved", self:GetPos() + Vector(0,0,50), Angle( 0, 0, 0 ) )
    end
    
end

function PLAYER:CanPromote()
    local character = self:GetCharacter()

    if (character) then
        local currentRank = character:GetRank()
        local class = rankToClass(currentRank, character:GetFaction())

        if(character:GetFaction() == FACTION_CLONE || character:GetFaction() == FACTION_RECRUIT) then
            return false
        end

        if(class.canPromote) then
            return true
        end

        return false;
    end

    return false;
end

function PLAYER:CanInvite()
    local character = self:GetCharacter()

    if (character) then
        local currentRank = character:GetRank()
        local class = rankToClass(currentRank, character:GetFaction())

        if(character:GetFaction() == FACTION_CLONE || character:GetFaction() == FACTION_RECRUIT) then
            return false
        end

        if(class.canPromote) then
            return true
        end

        return false;
    end

    return false;
end

ix.command.Add("promote", {
    arguments = {ix.type.player},
    privilege = "Promotes a player from the battalion",
    description = "Promotes a player from their current battalion",

    OnRun = function(self, client, target)
        local character = client:GetCharacter()
        local tcharacter = target:GetCharacter()

        if (character && tcharacter) then
            if (character:GetFaction() == FACTION_CLONE) then
                return "You're not in a battalion."
            elseif (character:GetFaction() == FACTION_RECRUIT) then
                return "You're not in a battalion."
            end
            
            if(character:GetFaction() != tcharacter:GetFaction() && !client:IsAdmin()) then
                return "You can't promote someone in another battalion."
            end

            local class = rankToClass(character:GetRank(), character:GetFaction())
            local faction = ix.faction.indices[character:GetFaction()]

            if (class.canPromote || client:IsAdmin()) then
                if(character:GetRank() > tcharacter:GetRank() + 1 || client:IsAdmin()) then
                    if (tcharacter:GetRank() < faction.maxPromoteRank) then
                        target:Promote()
                        SetupName(tcharacter)
                        hook.Run("SwrpOnCharacterPromoted", client, target, tcharacter:GetRank(), faction.name)
                    else
                        client:NotifyLocalized(tcharacter:GetName() .. " is already at the highest rank!")
                    end
                else
                    client:NotifyLocalized("The target is a higher rank than you.")
                end
            else
                client:NotifyLocalized("You are not allowed to promote people.")
            end
        end
    end
})

ix.command.Add("xo", {
    arguments = {ix.type.player},
    privilege = "Promotes a player to XO of their battalion",
    description = "Promotes a player to XO of their battalion",

    OnRun = function(self, client, target)
        local character = client:GetCharacter()
        local tcharacter = target:GetCharacter()

        if (character && tcharacter) then
            if (character:GetFaction() == FACTION_CLONE) then
                return "You're not in a battalion."
            elseif (character:GetFaction() == FACTION_RECRUIT) then
                return "You're not in a battalion."
            end
            
            if(character:GetFaction() != tcharacter:GetFaction() && !client:IsAdmin()) then
                return "You can't promote someone in another battalion."
            end

            local faction = ix.faction.indices[character:GetFaction()]
            local class = rankToClass(faction.commanderRank - 1, character:GetFaction())

            if (faction.commanderRank == character:GetRank() || client:IsAdmin()) then
                tcharacter:SetRank(faction.xoRank)
                tcharacter:SetFaction(faction.index)
                tcharacter:SetClass(faction.xoClass)
                tcharacter:SetModel(ix.class.list[faction.xoClass].model)
                SetupName(tcharacter)
                target:NotifyLocalized("You were promoted to XO of the " .. faction.name .." battalion!")

                hook.Run("SwrpOnXoPromotion", client, target, faction.name)
            else
                client:NotifyLocalized("You are not allowed to promote people to XO.")
            end
        end
    end
})

ix.command.Add("commander", {
    arguments = {ix.type.player, ix.type.string},
    privilege = "Promotes a player to Commander of a battalion",
    description = "Promotes a player to Commander of a battalion",

    OnRun = function(self, client, target, battalion)

        if(!client:IsAdmin()) then
            return "You must be admin to set a commander!"
        end

        if(battalion == "") then
            return "You must specify a battalion!"
        end

        if (ix.faction.teams[battalion] == nil) then
            return "Battalion " .. battalion .. " doesn't exist!"
        end

        local tcharacter = target:GetCharacter()

        if (tcharacter) then

            ix.command.Run( client, "clearcommander", {battalion})

            local faction = ix.faction.teams[battalion]
            local class = rankToClass(faction.commanderRank, ix.faction.teams[battalion].index)
            
            if(tcharacter:GetRank() == class) then
                return tcharacter:GetName() .. " is already the commander of the " .. battalion .. " battalion!"
            end
            target:SetWhitelisted(faction.index, true)
            tcharacter:SetRank(faction.commanderRank)
            tcharacter:SetFaction(faction.index)
            tcharacter:SetClass(faction.commanderClass)
            tcharacter:SetModel(ix.class.list[faction.commanderClass].model)
            local newName = SetupName(tcharacter)

            if (client == target) then
                client:NotifyLocalized("You promoted yourself to commander of the " .. battalion .. " battalion!")
            else
                client:NotifyLocalized("You promoted " .. tcharacter:GetName() .. " to commander of the " .. battalion .." battalion!")
                target:NotifyLocalized("You were promoted to commander of the" .. battalion .. " battalion!")
                for k, v in pairs(player.GetAll()) do
                    if (v != client && v != target) then
                        v:NotifyLocalized(tcharacter:GetName() .. " was promoted to commander of the" .. battalion .. " battalion!")
                    end
                end
            end

            hook.Run("SwrpOnCommanderPromotion", client, target, faction.name)
            local query = mysql:Update("ix_characters")
                query:Update("rank", faction.commanderRank)
                query:Update("faction", battalion)
                query:Update("name", newName)
                query:Where("id", tcharacter:GetID())
            query:Execute()
        end
    end
})

ix.command.Add("clearCommander", {
    arguments = {ix.type.string},
    privilege = "Sets anyone who is commander of this battalion to CT",
    description = "Sets anyone who is commander of this battalion to CT",

    OnRun = function(self, client, battalion)
        if(!client:IsAdmin()) then
            return "You must be admin to set a commander!"
        end

        if(battalion == "") then
            return "You must specify a battalion!"
        end

        if (ix.faction.teams[battalion] == nil) then
            return "Battalion " .. battalion .. " doesn't exist!"
        end

        local newName

        for key, value in pairs(ix.char.loaded) do
            if (value.vars) then
                if(value.vars.faction == battalion) then
                    if(value.vars.rank == ix.faction.teams[battalion].commanderRank) then
                        value:SetFaction(FACTION_CLONE)
                        value:SetModel(table.Random(ix.faction.indices[FACTION_CLONE].models))
                        value:SetRank(1)
                        value:SetClass(nil)
                        newName = SetupName(value)
                    end
                end
            end
        end

        local query = mysql:Select("ix_characters")
            query:Where("rank", ix.faction.teams[battalion].commanderRank)
            query:Where("faction", battalion)
            query:Callback(function(result)
                if (istable(result) and #result > 0) then
                    for k, v in pairs(result) do
                        local n = result[k].name
                        n = string.Explode(" ", n)
                        n = "CT " .. result[k].id .. " " .. n[3]
                        local query = mysql:Update("ix_characters")
                            query:Update("rank", 1)
                            query:Update("faction", "CT")
                            query:Update("name", n)
                            query:Where("rank", ix.faction.teams[battalion].commanderRank)
                            query:Where("faction", battalion)
                        query:Execute()
                    end
                end
            end)
        query:Execute()
        
        hook.Run("SwrpOnCommanderPromotion", client, battalion)
        client:NotifyLocalized("Commanders for the " .. battalion .. " battalion were cleared!")
    end
})

ix.command.Add("promoteCommanderByID", {
    arguments = {ix.type.string, ix.type.string},
    privilege = "Promotes a player to Commander of a battalion",
    description = "Promotes a player to Commander of a battalion",

    OnRun = function(self, client, steam64ID, battalion)
        if(!client:IsAdmin()) then
            return "You must be admin to set a commander!"
        end

        if (string.len(steam64ID) != 17) then
            return "You did not enter a valid steam64ID!"
        end

        if(battalion == "") then
            return "You must specify a battalion!"
        end

        if (ix.faction.teams[battalion] == nil) then
            return "Battalion " .. battalion .. " doesn't exist!"
        end

        for k, v in pairs(player.GetAll()) do
            if (v:SteamID64() == steam64ID) then
                return "Player is on server use regular /commander"
            end
        end

        -- Clear out the old commander
        ix.command.Run( client, "clearcommander", {battalion})

        local query = mysql:Select("ix_characters")
            query:Where("steamid", steam64ID)
            query:Limit(1)
            query:Callback(function(result)
                if (istable(result) and #result > 0) then
                    local name = result[1].name
                    local rank = result[1].rank
                    local name = string.Explode(" ", name)

                    if (#name < 3) then
                        name = (battalion .. " CMDR " .. name[1])
                    else
                        name = (battalion .. " CMDR " .. name[3])
                    end
                    
                    local query = mysql:Update("ix_characters")
                        query:Update("name", name)
                        query:Update("rank", rank)
                        query:Update("faction", battalion)
                        query:Where("steamid", steam64ID)
                        query:Limit(1)
                        query:Callback(function(result)
                            client:NotifyLocalized("Promoted player " .. name .. " to commander!")
                            hook.Run("SwrpOnCommanderPromotionID", client, steam64ID, battalion)
                        end)
                    query:Execute()

                    for key, value in pairs(ix.char.loaded) do
                        if (value.vars) then
                            if(value.vars.steamid == steam64ID) then
                                value:SetFaction(battalion)
                                value:SetModel(ix.class.list[ix.faction.teams[battalion].commanderClass].model)
                                value:SetRank(ix.faction.teams[battalion].commanderRank)
                                value:SetClass(ix.faction.teams[battalion].commanderClass)
                                newName = SetupName(value)  
                            end
                        end
                    end

                end
            end)
        query:Execute()
        
        local query = mysql:Select("ix_players")
            query:Select("data")
            query:Where("steamid", steam64ID)
            query:Limit(1)
            query:Callback(function(result)
                if (istable(result) and #result > 0) then
                    local data = util.JSONToTable(result[1].data or "[]")
                    local whitelists = data.whitelists and data.whitelists[Schema.folder]

                    if(whitelists) then
                        for k, v in pairs(whitelists) do
                            if k == battalion then
                                return
                            end
                        end
                        whitelists[battalion] = true
                    else
                        whitelists = {}
                        whitelists[battalion] = true
                    end

                    local updateQuery = mysql:Update("ix_players")
                        updateQuery:Update("data", util.TableToJSON(data))
                        updateQuery:Where("steamid", steam64ID)
                    updateQuery:Execute()
                end
            end)
        query:Execute()

    end
})