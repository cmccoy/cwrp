ix.command.Add("invite", {
    arguments = {ix.type.player},
    privilege = "",
    description = "Invite a player to your battalion",

    OnRun = function(self, client, target)
        local character = client:GetCharacter()
        local tcharacter = target:GetCharacter()
        if (character && tcharacter) then
            local characterClass = ix.class.list[character:GetClass()]

            if (characterClass && characterClass.canPromote) then
                local battalion = ix.faction.indices[character:GetFaction()]
                target:SetWhitelisted(character:GetFaction(), true)
                client:NotifyLocalized("You invited " .. target:GetName() .. " to your battalion.")
                net.Start("ixOpenJoinBattalionMenu")
                    net.WriteString(string.lower(battalion.name))
                net.Send(target)
                
                hook.Run("SwrpOnPlayerInvited", client, target, battalion.name)
            else
                client:NotifyLocalized("You are not able to invite people")
            end
        end
    end
})