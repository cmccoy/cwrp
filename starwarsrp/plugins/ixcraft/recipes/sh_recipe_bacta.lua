
RECIPE.name = "Bacta Grenade"
RECIPE.description = "Crafts a one time use bacta grenade."
RECIPE.model = "models/items/grenadeammo.mdl"
RECIPE.category = "Grenades"
RECIPE.requirements = {
	["scrap_metal"] = 5,
	["alien_goo"] = 3
}
RECIPE.results = {
	["bacta_grenade"] = 1
}
RECIPE.tools = {
	"assembler"
}
