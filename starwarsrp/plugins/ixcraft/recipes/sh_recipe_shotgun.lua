
RECIPE.name = "Shotgun"
RECIPE.description = "Crafts a one time use shotgun."
RECIPE.model = "models/swbf3/rep/shotgun.mdl"
RECIPE.category = "Weapons"
RECIPE.requirements = {
	["scrap_metal"] = 2,
	["broken_crystal"] = 4,
	["refined_metal"] = 2
}
RECIPE.results = {
	["shotgun"] = 1
}
RECIPE.tools = {
	"assembler"
}
