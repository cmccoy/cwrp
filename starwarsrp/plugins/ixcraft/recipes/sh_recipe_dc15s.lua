
RECIPE.name = "DC-15s"
RECIPE.description = "Crafts a one time use DC-15s."
RECIPE.model = "models/weapons/w_dc15s.mdl"
RECIPE.category = "Weapons"
RECIPE.requirements = {
	["scrap_metal"] = 4,
	["broken_crystal"] = 2,
	["refined_metal"] = 1
}
RECIPE.results = {
	["dc15s"] = 1
}
RECIPE.tools = {
	"assembler"
}
