RECIPE.name = "Standard Turret"
RECIPE.description = "Crafts a standard turret."
RECIPE.model = "models/combine_turrets/floor_turret.mdl"
RECIPE.category = "Tools"
RECIPE.requirements = {
	["scrap_metal"] = 3,
	["refined_metal"] = 2,
	["broken_crystal"] = 4,
	["targeting_system"] = 1,
	["cooling_unit"] = 1
}
RECIPE.results = {
	["standard_turret"] = 1
}
RECIPE.tools = {
	"assembler"
}
