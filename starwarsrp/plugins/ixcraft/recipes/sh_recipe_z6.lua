
RECIPE.name = "Z6 Rotary Blaster"
RECIPE.description = "Crafts a one time use Z6."
RECIPE.model = "models/weapons/w_z6_rotary_blaster.mdl"
RECIPE.category = "Weapons"
RECIPE.requirements = {
	["scrap_metal"] = 10,
	["broken_crystal"] = 6,
	["refined_metal"] = 8,
	["cooling_unit"] = 1,
}
RECIPE.results = {
	["z6"] = 1
}
RECIPE.tools = {
	"assembler"
}
