RECIPE.name = "Ion Turret"
RECIPE.description = "Crafts a ion turret."
RECIPE.model = "models/vj_combine/combine_cannon_gun.mdl"
RECIPE.category = "Tools"
RECIPE.requirements = {
	["scrap_metal"] = 6,
	["refined_metal"] = 4,
	["broken_crystal"] = 4,
	["targeting_system"] = 2,
	["cooling_unit"] = 1
}
RECIPE.results = {
	["ion_turret"] = 1
}
RECIPE.tools = {
	"assembler"
}
