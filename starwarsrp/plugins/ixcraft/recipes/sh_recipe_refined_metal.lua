
RECIPE.name = "Refined Metal"
RECIPE.description = "Crafts a ingot of refined metal."
RECIPE.model = "models/props_phx2/garbage_metalcan001a.mdl"
RECIPE.category = "Materials"
RECIPE.requirements = {
	["scrap_metal"] = 4
}
RECIPE.results = {
	["refined_metal"] = 1
}
RECIPE.tools = {
	"assembler"
}
