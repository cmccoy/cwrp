
RECIPE.name = "Sniper"
RECIPE.description = "Crafts a one time use sniper."
RECIPE.model = "models/swbf3/rep/sniperrifle.mdl"
RECIPE.category = "Weapons"
RECIPE.requirements = {
	["scrap_metal"] = 5,
	["broken_crystal"] = 8,
	["refined_metal"] = 5,
	["weapon_sight"] = 1
}
RECIPE.results = {
	["sniper"] = 1
}
RECIPE.tools = {
	"assembler"
}
