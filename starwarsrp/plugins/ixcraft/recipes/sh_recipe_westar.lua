
RECIPE.name = "Westar-M5"
RECIPE.description = "Crafts a one time use Westar-M5."
RECIPE.model = "models/weapons/w_alphablaster.mdl"
RECIPE.category = "Weapons"
RECIPE.requirements = {
	["scrap_metal"] = 5,
	["broken_crystal"] = 4,
	["refined_metal"] = 5,
	["weapon_sight"] = 1
}
RECIPE.results = {
	["westar"] = 1
}
RECIPE.tools = {
	"assembler"
}
