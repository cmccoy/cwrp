RECIPE.name = "Thermal Detonator"
RECIPE.description = "Crafts a thermal detonator."
RECIPE.model = "models/star wars the force unleashed/thermal_detonator.mdl"
RECIPE.category = "Grenades"
RECIPE.requirements = {
	["scrap_metal"] = 2,
	["refined_metal"] = 1,
	["explosive_adhesive"] = 4,
}
RECIPE.results = {
	["thermal_detonator"] = 1
}
RECIPE.tools = {
	"assembler"
}
