RECIPE.name = "Ion Grenade"
RECIPE.description = "Crafts a ion grenade."
RECIPE.model = "models/weapons/w_eq_fraggrenade.mdl"
RECIPE.category = "Grenades"
RECIPE.requirements = {
	["scrap_metal"] = 2,
	["refined_metal"] = 1,
	["broken_crystal"] = 4,
}
RECIPE.results = {
	["ion_grenade"] = 1
}
RECIPE.tools = {
	"assembler"
}
