
RECIPE.name = "DC-17m"
RECIPE.description = "Crafts a one time use DC-17m."
RECIPE.model = "models/weapons/w_dc17m_br.mdl"
RECIPE.category = "Weapons"
RECIPE.requirements = {
	["scrap_metal"] = 4,
	["broken_crystal"] = 2,
	["refined_metal"] = 2
}
RECIPE.results = {
	["dc17m"] = 1
}
RECIPE.tools = {
	"assembler"
}
