do
	local COMMAND = {}

	function COMMAND:OnRun(client)
		local character = client:GetCharacter()
		if (character) then
			if(character:IsClone()) then
				local helmet = client:FindBodygroupByName("helmet")
				local antenna = client:FindBodygroupByName("antenna")
				if(helmet) then
					if (client:GetBodygroup(helmet) == 1) then
						client:SetBodygroup(helmet, 0)
						if (client.antennaValue) then
							client:SetBodygroup(antenna, client.antennaValue)
							client.antennaValue = nil
						end
					elseif(antenna) then
						client:SetBodygroup(helmet, 1)
						client.antennaValue = client:GetBodygroup(antenna)
						client:SetBodygroup(antenna, 2)
					else
						client:SetBodygroup(helmet, 1)
					end
				end
			end
		end
	end

	ix.command.Add("helmet", COMMAND)
end

do
	local COMMAND = {}
	COMMAND.arguments = ix.type.text

	function COMMAND:OnRun(client, message)
		local character = client:GetCharacter()
		local radios = character:GetInventory():GetItemsByUniqueID("handheld_radio", true)
		local item

		for k, v in ipairs(radios) do
			if (v:GetData("enabled", false)) then
				item = v
				break
			end
		end

		if (item) then
			if (!client:IsRestricted()) then
				ix.chat.Send(client, "radio", message)
				ix.chat.Send(client, "radio_eavesdrop", message)
			else
				return "@notNow"
			end
		elseif (#radios > 0) then
			return "@radioNotOn"
		else
			return "@radioRequired"
		end
	end

	ix.command.Add("Radio", COMMAND)
end

do
	local COMMAND = {}
	COMMAND.arguments = ix.type.number

	function COMMAND:OnRun(client, frequency)
		local character = client:GetCharacter()
		local inventory = character:GetInventory()
		local itemTable = inventory:HasItem("handheld_radio")

		if (itemTable) then
			if (string.find(frequency, "^%d%d%d%.%d$")) then
				character:SetData("frequency", frequency)
				itemTable:SetData("frequency", frequency)

				client:Notify(string.format("You have set your radio frequency to %s.", frequency))
			end
		end
	end

	ix.command.Add("SetFreq", COMMAND)
end

do
	local COMMAND = {}
	COMMAND.arguments = ix.type.text

	function COMMAND:OnRun(client, message)
		local character = client:GetCharacter()
		local inventory = character:GetInventory()

		if (inventory:HasItem("request_device") or client:IsNaval() or client:Team() == FACTION_ADMIN) then
			if (!client:IsRestricted()) then
				Schema:AddCombineDisplayMessage("@cRequest")

				ix.chat.Send(client, "request", message)
				ix.chat.Send(client, "request_eavesdrop", message)
			else
				return "@notNow"
			end
		else
			return "@needRequestDevice"
		end
	end

	ix.command.Add("Request", COMMAND)
end

do
	local COMMAND = {}
	COMMAND.arguments = ix.type.text

	function COMMAND:OnRun(client, message)

		local character = client:GetCharacter()

		ix.chat.Send(client, "teamChat", message)
			
	end
	ix.command.Add("battalionChat", COMMAND)
end