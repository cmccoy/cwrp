local meta = FindMetaTable("Vector")

function meta:WithinDistance(vec, dist)
	return self:DistToSqr(vec) <= (dist * dist)
end