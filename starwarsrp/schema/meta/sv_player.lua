local CLIENT = FindMetaTable("Player")

function CLIENT:IsScanner()
	return IsValid(self.ixScanner)
end

function CLIENT:AddCombineDisplayMessage(text, color, ...)
	if (self:IsNaval()) then
		netstream.Start(self, "CombineDisplayMessage", text, color or false, {...})
	end
end

function CLIENT:GiveCgWeps()

	self:Give("taser")

	self:Give("weapon_policebaton")

	self:Give("ix_unarrest_stick")

	self:Give("weapon_policeshield")

	self:Give("weapon_cuff_tactical")

end

function CLIENT:StripCgWeps()

	if (self:HasWeapon("taser")) then
		self:StripWeapon("taser")
	end

	if (self:HasWeapon("weapon_policebaton")) then
		self:StripWeapon("weapon_policebaton")
	end

	if (self:HasWeapon("ix_unarrest_stick")) then
		self:StripWeapon("ix_unarrest_stick")
	end

	if (self:HasWeapon("weapon_policeshield")) then
		self:StripWeapon("weapon_policeshield")
	end

	if (self:HasWeapon("weapon_cuff_tactical")) then
		self:StripWeapon("weapon_cuff_tactical")
	end
	
end

function CLIENT:GiveMedicWeps()
	self:Give("weapon_jew_stimkit")
	self:Give("weapon_bacta_grenade")
	self:Give("weapon_defib")
end

function CLIENT:StripMedicWeps()
	if (self:HasWeapon("weapon_jew_stimkit")) then
		self:StripWeapon("weapon_jew_stimkit")
	end
	if (self:HasWeapon("weapon_bacta_grenade")) then
		self:StripWeapon("weapon_bacta_grenade")
	end
	if (self:HasWeapon("weapon_defib")) then
		self:StripWeapon("weapon_defib")
	end
end

function CLIENT:WhitelistVipJobs()
	self:SetWhitelisted(FACTION_JEDI, true)
	self:SetWhitelisted(FACTION_COMBAT_DROID, true)
	self:SetWhitelisted(FACTION_UTILITY_DROID, true)
end

function CLIENT:UnWhitelistVipJobs()
	self:SetWhitelisted(FACTION_JEDI, false)
	self:SetWhitelisted(FACTION_COMBAT_DROID, false)
	self:SetWhitelisted(FACTION_UTILITY_DROID, false)
end