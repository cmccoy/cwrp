local PLAYER = FindMetaTable("Player")

local staffValues ={
    "owner",
    "council",
    "headadmin",
    "superadmin",
    "admin",
    "moderator",
    "trialmod"
}

function contains(String, toFind)
    local has =  string.find(String:lower(), toFind)

    if has then
        return true
    end

    return false
end

function PLAYER:IsStaff()
    return table.HasValue(staffValues, self:GetUserGroup())
end


function PLAYER:IsRC()

    local char = self:GetCharacter()
    if(char) then
	    if (char:GetFaction() == FACTION_RC) then
		    return true
	    end
        return false
    end
end

function PLAYER:IsNaval()
	local char = self:GetCharacter()
    if(char) then
	    if (char:GetFaction() == FACTION_NAVAL) then
		    return true
	    end
        return false
    end
end

function PLAYER:WithinDistance(ent, dist)
	return self:GetPos():WithinDistance(ent:GetPos(), dist)
end