
local CHAR = ix.meta.character

function CHAR:IsNaval()
	local faction = self:GetFaction()
	return faction == FACTION_MPF or faction == FACTION_OTA
end
