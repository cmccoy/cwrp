ITEM.name = "Light Armor Plating"
ITEM.model = Model("models/gibs/shield_scanner_gib2.mdl")
ITEM.description = "When equipped, this items will grant 25 armor"
ITEM.category = "Armor"
ITEM.price = 200

ITEM.functions.Equip = {
	OnRun = function(itemTable)
        local client = itemTable.player
        client:EmitSound("items/battery_pickup.wav")
        if(client:Armor() > 75) then
            client:SetArmor(100)
        else
            client:SetArmor(client:Armor() + 25)
        end
	end
}

--models/combine_helicopter/bomb_debris_2.mdl