
ITEM.name = "Standard Turret"
ITEM.description = "A standard issue republic turret that fires slug projectiles."
ITEM.model = Model("models/combine_turrets/floor_turret.mdl")
ITEM.width = 2
ITEM.height = 3
ITEM.iconCam = {
	pos = Vector(2, 80, 30),
	ang = Angle(0, 270, 0),
	fov = 25.29
}

ITEM.functions.Place = {
	icon = "icon16/wrench.png",
	OnRun = function(itemTable)
		local client = itemTable.player
		net.Start("ixDrawStandardTurretMesh")
		net.Send(client)
		return false
	end
}

if SERVER then
	util.AddNetworkString("ixDrawStandardTurretMesh")

	net.Receive("ixDrawStandardTurretMesh", function(len, client)
		local character = client:GetCharacter()
		if (character) then
			local inventory = character:GetInventory()

			local clientForward = net.ReadVector()

			if (inventory:HasItem("standard_turret")) then
				local item = inventory:GetItemsByUniqueID("standard_turret")
				local tr = client:GetEyeTrace()
				local forward = tr.StartPos + Vector(client:GetForward().x, client:GetForward().y, 0) * 100
				local down = util.TraceLine({
					start = forward,
					endpos = forward - Vector(0,0,100)
				})
				if (!down.HitWorld) then 
					client:Notify("Not valid turret placement")
					return 
				end

				local turret = ents.Create("npc_vj_cmb_turret_f")
				turret:SetPos(down.HitPos)
				turret:SetAngles(Angle(0, client:GetAngles().y, 0))
				turret:Spawn()

				inventory:Remove(item[1].id)
			end
		end
	end)
end


if CLIENT then

	net.Receive("ixDrawStandardTurretMesh", function(len, pl)

		local text = "LMB: Place | RMB: Cancel"
		local model = "models/vj_combine/floor_turret.mdl"
		local client = LocalPlayer()

		hook.Add("HUDPaint", "[Impact] Draw turret text", function()

			draw.SimpleText(text, "ix3D2DSmallFont", ScrW() * .5, ScrH() * .8, Color(255,255,255), TEXT_ALIGN_CENTER)

		end)

		hook.Add("Think", "[Impact] Draw Standard Turret Mesh", function()
			local tr = client:GetEyeTrace()
			local forward = tr.StartPos + Vector(client:GetForward().x, client:GetForward().y, 0) * 100
			local down = util.TraceLine({
				start = forward,
				endpos = forward - Vector(0,0,100)
			})
			if (!down.HitWorld) then 
				if(IsValid(client.turretPlacement)) then
					client.turretPlacement:Remove()
					client.turretPlacement = nil 
				end
				return 
			end
			if !client.turretPlacement then
				local turret = ents.CreateClientProp( model )
				turret:SetPos( down.HitPos )
				turret:SetAngles(Angle(0, client:GetAngles().y, 0))
				turret:Spawn()

				turret:SetMaterial("models/wireframe")
				turret:SetMoveType( MOVETYPE_NONE )
				turret:SetNotSolid( true )
				turret:SetRenderMode( RENDERMODE_TRANSALPHA )

				client.turretPlacement = turret
			else
				client.turretPlacement:SetPos( down.HitPos )
				client.turretPlacement:SetAngles(Angle(0, client:GetAngles().y, 0))
			end
			
		end)

		hook.Add("KeyPress", "[Impact] Standard Turret Key Press", function(client, pressed)
			if (pressed == IN_ATTACK) then
				net.Start("ixDrawStandardTurretMesh")
					net.WriteVector(Vector(client:GetForward().x, client:GetForward().y, 0) * 100)
				net.SendToServer()

				if (IsValid(client.turretPlacement)) then
					client.turretPlacement:Remove()
					client.turretPlacement = nil
				end
				hook.Remove("Think", "[Impact] Draw Standard Turret Mesh")
				hook.Remove("HUDPaint", "[Impact] Draw turret text")
				hook.Remove("KeyPress", "[Impact] Standard Turret Key Press")
			elseif(pressed == IN_ATTACK2) then
				if (IsValid(client.turretPlacement)) then
					client.turretPlacement:Remove()
					client.turretPlacement = nil
				end
				hook.Remove("Think", "[Impact] Draw Standard Turret Mesh")
				hook.Remove("HUDPaint", "[Impact] Draw turret text")
				hook.Remove("KeyPress", "[Impact] Standard Turret Key Press")
			end
		end)

	end)

end