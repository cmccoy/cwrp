ITEM.name = "Thermal Detonator"
ITEM.description = "Illegal in 5 systems, this grenade will vaporize pretty much anything in the blast zone"
ITEM.model = "models/star wars the force unleashed/thermal_detonator.mdl"
ITEM.width = 1
ITEM.height = 1

ITEM.functions.Equip = {
    OnRun = function(item)
        item.player:Give("weapon_thermal_det")
		return
	end
}