ITEM.name = "Ion Grenade"
ITEM.description = "Releases an electric arc that will overload any nearby droids."
ITEM.model = "models/weapons/w_eq_fraggrenade.mdl"
ITEM.width = 1
ITEM.height = 1

ITEM.functions.Equip = {
    OnRun = function(item)
        item.player:Give("weapon_ion_grenade")
		return
	end
}