ITEM.name = "DC-15s"
ITEM.description = "A compact, fully automatic rifle."
ITEM.model = Model("models/weapons/w_dc15s.mdl")
ITEM.width = 2
ITEM.height = 2

ITEM.functions.Equip = {
    OnRun = function(item)
        item.player:Give("tfa_swch_dc15s")
		return
	end
}