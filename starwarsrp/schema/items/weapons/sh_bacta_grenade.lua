ITEM.name = "Bacata Grenade"
ITEM.description = "Releases a cloud of healing bacata."
ITEM.model = "models/items/grenadeammo.mdl"
ITEM.width = 1
ITEM.height = 1

ITEM.functions.Equip = {
    OnRun = function(item)
        item.player:Give("weapon_bacta_grenade")
		return
	end
}