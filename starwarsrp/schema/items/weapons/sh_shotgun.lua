ITEM.name = "Shotgun"
ITEM.description = "Great for close quarters combat."
ITEM.model = Model("models/swbf3/rep/shotgun.mdl")
ITEM.width = 3
ITEM.height = 2

ITEM.functions.Equip = {
    OnRun = function(item)
        item.player:Give("tfa_sw_repshot")
		return
	end
}