ITEM.name = "Sniper"
ITEM.description = "Great for close quarters combat."
ITEM.model = Model("models/swbf3/rep/sniperrifle.mdl")
ITEM.width = 4
ITEM.height = 2

ITEM.functions.Equip = {
    OnRun = function(item)
        item.player:Give("tfa_sw_repsnip")
		return
	end
}