ITEM.name = "Heavy Armor Plating"
ITEM.model = Model("models/combine_helicopter/bomb_debris_2.mdl")
ITEM.description = "When equipped, this items will grant 50 armor"
ITEM.category = "Armor"
ITEM.price = 300

ITEM.functions.Equip = {
	OnRun = function(itemTable)
        local client = itemTable.player
        client:EmitSound("items/battery_pickup.wav")
        if(client:Armor() > 50) then
            client:SetArmor(100)
        else
            client:SetArmor(client:Armor() + 50)
        end
	end
}