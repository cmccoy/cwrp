game.AddParticles( "particles/jb_weapon_shockrifle.pcf" )
PrecacheParticleSystem( "jb_proj_shock" )
PrecacheParticleSystem( "shock_explode_ch_after" )
PrecacheParticleSystem( "shock_explode_ch_glow" )
PrecacheParticleSystem( "shock_explode_ch_sparks" )

game.AddParticles( "particles/electrical_fx.pcf" )
PrecacheParticleSystem ("st_elmos_fire_cp0")
PrecacheParticleSystem ("electrical_arc_01_system")

game.AddParticles( "particles/aurora_sphere2.pcf" )
PrecacheParticleSystem ("aurora_shockwave")

game.AddParticles( "particles/choreo_launch.pcf" )
PrecacheParticleSystem ("choreo_launch_rocket_glow2")