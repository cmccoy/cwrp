local holsteredgunsconvar = CreateConVar( "cl_holsteredguns", "1", { FCVAR_ARCHIVE, }, "Enable/Disable the rendering of the weapons on any player" )
 
local NEXT_WEAPONS_UPDATE=CurTime();
 
local weaponsinfos={}
weaponsinfos["weapon_physcannon"]={}
weaponsinfos["weapon_physcannon"].Model=""
weaponsinfos["weapon_physcannon"].Bone="ValveBiped.Bip01_Spine1"
weaponsinfos["weapon_physcannon"].BoneOffset={Vector(6,15,0),Angle(90,180,0)} 
weaponsinfos["weapon_physcannon"].Priority="weapon_physgun" --this means that if the weapon_physgun can be drawn,we will not
 
weaponsinfos["weapon_physgun"]={}
weaponsinfos["weapon_physgun"].Model=""
weaponsinfos["weapon_physgun"].Bone="ValveBiped.Bip01_Spine1"
weaponsinfos["weapon_physgun"].DrawFunction=function(ent) end -- draw custom core to make it look like it's on
weaponsinfos["weapon_physgun"].BoneOffset={Vector(6,15,0),Angle(90,180,0)} 
weaponsinfos["weapon_physgun"].Skin=1;  --we can set custom skin too,but only once,remember that
 
 
weaponsinfos["weapon_physgun"].DrawFunction=function(ent)
    local attachment=ent:GetAttachment( 1)
    local StartPos = attachment.Pos + attachment.Ang:Forward()*4
    render.SetMaterial(physgunmat)
    render.DrawSprite(attachment.Pos,20,20,Color(255,255,255,255));
    render.SetMaterial(physgunmat1)
    render.DrawSprite(StartPos,20,20,Color(255,255,255,255));   
end
 
weaponsinfos["tfa_swch_dc17"]={}
weaponsinfos["tfa_swch_dc17"].Model="models/weapons/w_dc17.mdl"
weaponsinfos["tfa_swch_dc17"].Bone="ValveBiped.Bip01_Pelvis"
weaponsinfos["tfa_swch_dc17"].BoneOffset={Vector(0,-8,0),Angle(0,-90,0)} 

weaponsinfos["tfa_sw_dc17dual"]={}
weaponsinfos["tfa_sw_dc17dual"].Model="models/weapons/w_dc17.mdl"
weaponsinfos["tfa_sw_dc17dual"].Bone="ValveBiped.Bip01_Pelvis"
weaponsinfos["tfa_sw_dc17dual"].BoneOffset={Vector(0,-8,0),Angle(0,-90,0)} 

weaponsinfos["tfa_swch_de10"]={}
weaponsinfos["tfa_swch_de10"].Model="models/weapons/w_de10.mdl"
weaponsinfos["tfa_swch_de10"].Bone="ValveBiped.Bip01_Pelvis"
weaponsinfos["tfa_swch_de10"].BoneOffset={Vector(0,-8,0),Angle(0,-90,0)} 

weaponsinfos["tfa_sw_dual_de10"]={}
weaponsinfos["tfa_sw_dual_de10"].Model="models/weapons/w_de10.mdl"
weaponsinfos["tfa_sw_dual_de10"].Bone="ValveBiped.Bip01_Pelvis"
weaponsinfos["tfa_sw_dual_de10"].BoneOffset={Vector(0,-8,0),Angle(0,-90,0)} 
 
weaponsinfos["weapon_frag_grenade"]={}
weaponsinfos["weapon_frag_grenade"].Model="models/weapons/w_eq_fraggrenade.mdl"
weaponsinfos["weapon_frag_grenade"].Bone="ValveBiped.Bip01_Pelvis"
weaponsinfos["weapon_frag_grenade"].BoneOffset={Vector(3,-5,6),Angle(-95,0,0)} 

weaponsinfos["weapon_bacta_grenade"]={}
weaponsinfos["weapon_bacta_grenade"].Model="models/weapons/w_eq_smokegrenade.mdl"
weaponsinfos["weapon_bacta_grenade"].Bone="ValveBiped.Bip01_Pelvis"
weaponsinfos["weapon_bacta_grenade"].BoneOffset={Vector(3,5,6),Angle(-95,0,0)} 
 
weaponsinfos["weapon_swrc_det"]={}
weaponsinfos["weapon_swrc_det"].Model="models/weapons/w_swrcdeton.mdl"
weaponsinfos["weapon_swrc_det"].Bone="ValveBiped.Bip01_Spine2"
weaponsinfos["weapon_swrc_det"].BoneOffset={Vector(3,0,0),Angle(90,0,0)} 

weaponsinfos["tfa_swch_dc15a"]={}
weaponsinfos["tfa_swch_dc15a"].Model="models/weapons/w_dc15a_neue.mdl"
weaponsinfos["tfa_swch_dc15a"].Bone="ValveBiped.Bip01_L_Clavicle"
weaponsinfos["tfa_swch_dc15a"].BoneOffset={Vector(0,4,-4),Angle(0,30,90)} 
 
weaponsinfos["tfa_swch_alphablaster"]={}
weaponsinfos["tfa_swch_alphablaster"].Model="models/weapons/w_alphablaster.mdl"
weaponsinfos["tfa_swch_alphablaster"].Bone="ValveBiped.Bip01_R_Clavicle"
weaponsinfos["tfa_swch_alphablaster"].BoneOffset={Vector(10,8,4),Angle(0,30,90)} 

weaponsinfos["tfa_sw_repshot"]={}
weaponsinfos["tfa_sw_repshot"].Model="models/swbf3/rep/shotgun.mdl"
weaponsinfos["tfa_sw_repshot"].Bone="ValveBiped.Bip01_R_Clavicle"
weaponsinfos["tfa_sw_repshot"].BoneOffset={Vector(10,10,4),Angle(90,0,70)} 

weaponsinfos["tfa_swch_dc17m_br"]={}
weaponsinfos["tfa_swch_dc17m_br"].Model="models/weapons/w_dc17m_br.mdl"
weaponsinfos["tfa_swch_dc17m_br"].Bone="ValveBiped.Bip01_R_Clavicle"
weaponsinfos["tfa_swch_dc17m_br"].BoneOffset={Vector(10,8,4),Angle(0,30,90)} 

weaponsinfos["tfa_swch_z6"]={}
weaponsinfos["tfa_swch_z6"].Model="models/weapons/w_z6_rotary_blaster.mdl"
weaponsinfos["tfa_swch_z6"].Bone="ValveBiped.Bip01_R_Clavicle"
weaponsinfos["tfa_swch_z6"].BoneOffset={Vector(10,8,8),Angle(0,300,0)} 

weaponsinfos["tfa_sw_repsnip"]={}
weaponsinfos["tfa_sw_repsnip"].Model="models/swbf3/rep/sniperrifle.mdl"
weaponsinfos["tfa_sw_repsnip"].Bone="ValveBiped.Bip01_R_Clavicle"
weaponsinfos["tfa_sw_repsnip"].BoneOffset={Vector(4,10,4),Angle(90,0,70)} 

weaponsinfos["weapon_vibrosword"]={}
weaponsinfos["weapon_vibrosword"].Model="models/weapons/w_vibrosword.mdl"
weaponsinfos["weapon_vibrosword"].Bone="ValveBiped.Bip01_R_Clavicle"
weaponsinfos["weapon_vibrosword"].BoneOffset={Vector(0,-3,5),Angle(90,0,320)} 
	 
     
function LPGB(dotrace)
    if !dotrace then
    for i=0,LocalPlayer():GetBoneCount()-1 do
        print(LocalPlayer():GetBoneName(i))
    end
    else
    local entity=LocalPlayer():GetEyeTrace().Entity
    if !IsValid(entity) then return end
    for i=0,entity:GetBoneCount()-1 do
        print(entity:GetBoneName(i))
    end
    end
end
 
local function CalcOffset(pos,ang,off)
        return pos + ang:Right() * off.x + ang:Forward() * off.y + ang:Up() * off.z;
end
     
local function clhasweapon(pl,weaponclass)
    for i,v in pairs(pl:GetWeapons()) do
        if string.lower(v:GetClass())==string.lower(weaponclass) then return true end
    end
     
    return false;
end
 
local function clgetweapon(pl,weaponclass)
    for i,v in pairs(pl:GetWeapons()) do
        if string.lower(v:GetClass())==string.lower(weaponclass) then return v end
    end
     
    return nil;
end
 
local function playergettf2class(ply)
    return ply:GetPlayerClass()
end
 
local function IsTf2Class(ply)
    return LocalPlayer().IsHL2 && !LocalPlayer():IsHL2()
end
 
local function GetHolsteredWeaponTable(ply,indx)
    local class=IsTf2Class(ply) and playergettf2class(ply) or nil
    if !class then  return weaponsinfos[indx]
    else return (weaponsinfos[indx] && weaponsinfos[indx][class]) and weaponsinfos[indx][class] or nil
    end
end
 
local function thinkdamnit()
    if !holsteredgunsconvar:GetBool() then return end
    for _,pl in pairs(player.GetAll()) do
        if !IsValid(pl) then continue end
         
        if !pl.CL_CS_WEPS then
            pl.CL_CS_WEPS={}
        end
         
        if !pl:Alive() then pl.CL_CS_WEPS={} continue end

        if !(LocalPlayer():GetPos():DistToSqr(pl:GetPos()) < (500*500)) then
            continue
        end
        
        if NEXT_WEAPONS_UPDATE<CurTime() then
        
            for k, ent in pairs(pl.CL_CS_WEPS) do --Remove existing CS ents
                ent:Remove()
            end
        
            pl.CL_CS_WEPS={} 
            NEXT_WEAPONS_UPDATE=CurTime()+5
        end
         
        for i,v in pairs(pl:GetWeapons())do
            if !IsValid(v) then continue; end
             
            if pl.CL_CS_WEPS[v:GetClass()] then continue end
             
            if !pl.CL_CS_WEPS[v:GetClass()] then
                local worldmodel=v.WorldModelOverride or v.WorldModel //attempt to pick the model from a swep
                local attachedwmodel=v.AttachedWorldModel;
                 
                if GetHolsteredWeaponTable(pl,v:GetClass()) && GetHolsteredWeaponTable(pl,v:GetClass()).Model then --damnit,it's not a swep,then try to get it from our local table
                    worldmodel=GetHolsteredWeaponTable(pl,v:GetClass()).Model
                end
                if !worldmodel || worldmodel=="" then continue end; --allright,this weapon is not supposed to show up
                 
                 
                pl.CL_CS_WEPS[v:GetClass()]=ClientsideModel(worldmodel,RENDERGROUP_OPAQUE)
                pl.CL_CS_WEPS[v:GetClass()]:SetNoDraw(true)
                pl.CL_CS_WEPS[v:GetClass()]:SetSkin(v:GetSkin())
                pl.CL_CS_WEPS[v:GetClass()]:SetColor(v:GetColor())
                 
                if GetHolsteredWeaponTable(pl,v:GetClass()) && GetHolsteredWeaponTable(pl,v:GetClass()).Scale then
                    pl.CL_CS_WEPS[v:GetClass()]:SetModelScale(GetHolsteredWeaponTable(pl,v:GetClass()).Scale);
                end
                 
                if GetHolsteredWeaponTable(pl,v:GetClass()) && GetHolsteredWeaponTable(pl,v:GetClass()).BBP then
                    pl.CL_CS_WEPS[v:GetClass()].BuildBonePositions=GetHolsteredWeaponTable(pl,v:GetClass()).BBP;
                end
                 
                if v.MaterialOverride || v:GetMaterial() then
                    pl.CL_CS_WEPS[v:GetClass()]:SetMaterial(v.MaterialOverride || v:GetMaterial())
                end
                if worldmodel == "models/weapons/w_models/w_shotgun.mdl" then
                    pl.CL_CS_WEPS[v:GetClass()]:SetMaterial("models/weapons/w_shotgun_tf/w_shotgun_tf")
                end
                 
                pl.CL_CS_WEPS[v:GetClass()].WModelAttachment=v.WModelAttachment
                pl.CL_CS_WEPS[v:GetClass()].WorldModelVisible=v.WorldModelVisible
                 
                 
                if attachedwmodel then
                    pl.CL_CS_WEPS[v:GetClass()].AttachedModel=ClientsideModel(attachedwmodel,RENDERGROUP_OPAQUE)
                    pl.CL_CS_WEPS[v:GetClass()].AttachedModel:SetNoDraw(true)
                    pl.CL_CS_WEPS[v:GetClass()].AttachedModel:SetSkin(v:GetSkin())
                    pl.CL_CS_WEPS[v:GetClass()].AttachedModel:SetParent(pl.CL_CS_WEPS[v:GetClass()])
                    pl.CL_CS_WEPS[v:GetClass()].AttachedModel:AddEffects(bit.bor(EF_BONEMERGE,EF_BONEMERGE_FASTCULL,EF_PARENT_ANIMATES))
                end
            end
        end
    end
end
 
local function playerdrawdamnit(pl,legs)
    if !holsteredgunsconvar:GetBool() then return end
    if !IsValid(pl) then return end
    if !pl.CL_CS_WEPS then return end
    for i,v in pairs(pl.CL_CS_WEPS) do
 
             
        if GetHolsteredWeaponTable(pl,i) && (pl:GetActiveWeapon()==NULL || pl:GetActiveWeapon():GetClass()~=i) && clhasweapon(pl,i) then
            if GetHolsteredWeaponTable(pl,i).Priority then
                local priority=GetHolsteredWeaponTable(pl,i).Priority
                local bol=GetHolsteredWeaponTable(pl,priority) && (pl:GetActiveWeapon()==NULL || pl:GetActiveWeapon():GetClass()!=priority) && clhasweapon(pl,priority)
                if bol then continue; end
            end
             
            local oldpl=pl;
            local wep=clgetweapon(oldpl,i)
             
            if legs && IsValid(legs) then
            pl=legs;
            end
             
            if legs && IsValid(legs) && (string.find(string.lower(GetHolsteredWeaponTable(oldpl,i).Bone),"spine") or string.find(string.lower(GetHolsteredWeaponTable(oldpl,i).Bone),"clavi") ) then
            pl=oldpl;
            continue;
            end
             
            local bone=pl:LookupBone(GetHolsteredWeaponTable(oldpl,i).Bone or "")
            if !bone then pl=oldpl;continue; end
 
             
            local matrix = pl:GetBoneMatrix(bone)
            if !matrix then pl=oldpl;continue; end
            local pos = matrix:GetTranslation()
            local ang = matrix:GetAngles()
            local pos=CalcOffset(pos,ang,GetHolsteredWeaponTable(oldpl,i).BoneOffset[1])
            if GetHolsteredWeaponTable(oldpl,i).Skin then v:SetSkin(GetHolsteredWeaponTable(oldpl,i).Skin) end
             
            v:SetRenderOrigin(pos)
             
            ang:RotateAroundAxis(ang:Forward(),GetHolsteredWeaponTable(oldpl,i).BoneOffset[2].p)
            ang:RotateAroundAxis(ang:Up(),GetHolsteredWeaponTable(oldpl,i).BoneOffset[2].y)
            ang:RotateAroundAxis(ang:Right(),GetHolsteredWeaponTable(oldpl,i).BoneOffset[2].r)
             
            v:SetRenderAngles(ang)
            if v.WorldModelVisible==nil || (v.WorldModelVisible!=false) then
                v:DrawModel();
            end
             
            if IsValid(v.AttachedModel) then
                v.AttachedModel:DrawModel();
            end
            if v.WModelAttachment && multimodel then
                multimodel.Draw(v.WModelAttachment, wep, {origin=pos, angles=ang})
                multimodel.DoFrameAdvance(v.WModelAttachment, CurTime(),wep)
            end
             
            if GetHolsteredWeaponTable(oldpl,i).DrawFunction then
                GetHolsteredWeaponTable(oldpl,i).DrawFunction(v,oldpl)
            end
            pl=oldpl;
        end
    end
end
 
local function drawlegsdamnit(legs)
    playerdrawdamnit(LocalPlayer(),legs)
end
 
hook.Add("PostLegsDraw","HG_DrawOnLegs",drawlegsdamnit)
hook.Add("Think","HG_Think",thinkdamnit)
hook.Add("PostPlayerDraw","HG_Draw",playerdrawdamnit)