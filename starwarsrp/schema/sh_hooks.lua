
function Schema:CanPlayerUseBusiness(client, uniqueID)
	
end

-- called when the client wants to view the combine data for the given target
function Schema:CanPlayerViewData(client, target)
	
end

-- called when the client wants to edit the combine data for the given target
function Schema:CanPlayerEditData(client, target)
	
end

function Schema:CanPlayerViewObjectives(client)
	
end

function Schema:CanPlayerEditObjectives(client)
	
end

function Schema:CanDrive()
	
end
