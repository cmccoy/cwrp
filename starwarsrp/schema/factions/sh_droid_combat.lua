FACTION.name = "Combat"
FACTION.description = "A Droid ready for combat"
FACTION.logo = "materials/ig_cwrp/ranks/droid.png"
FACTION.color = Color(16, 16, 16, 0)
FACTION.isGloballyRecognized = true
FACTION.isDroid = true
FACTION.isActive = false
FACTION.models = {
    "models/galactic/colordroid/colordroid.mdl"
}
FACTION.isDefault = false

function FACTION:OnSpawn(client)
    local character = client:GetCharacter()
    if (character) then
        character:SetRank(1)
    end
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
    client:Give("tfa_swch_dc15a")
end

FACTION_COMBAT_DROID = FACTION.index