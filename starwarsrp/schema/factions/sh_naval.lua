FACTION.name = "Naval"
FACTION.description = "Naval directs the fleet"
FACTION.logo = "materials/ig_cwrp/battalions/501st.png"
FACTION.color = Color(55, 90, 141)
FACTION.isDefault = false
FACTION.isActive = false
FACTION.isGloballyRecognized = true
FACTION.models = {
    "models/player/gman_high.mdl"
}

FACTION_NAVAL = FACTION.index