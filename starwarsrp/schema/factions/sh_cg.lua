FACTION.name = "CG"
FACTION.description = "Clone Trooper for the Coruscant Guards"
FACTION.logo = "materials/ig_cwrp/battalions/cg.png"
FACTION.color = Color(108, 49, 54)
FACTION.maxPromoteRank = 17
FACTION.xoRank = 18
FACTION.commanderRank = 19
FACTION.xoClass = CLASS_CG_XO
FACTION.commanderClass = CLASS_CG_COMMANDER
FACTION.isDefault = false
FACTION.isActive = true
FACTION.isGloballyRecognized = true
FACTION.defaultClass = CLASS_CG_PRIVATE
FACTION.models = {
    "models/galactic/clones/cg/trooper.mdl"
}
function FACTION:OnSpawn(client)
    local character = client:GetCharacter()
    if (character) then
        local class = rankToClass(character:GetRank(), character:GetFaction())
        character:SetClass(class.index)
        client:GiveCgWeps()
        client:Give("tfa_swch_dc15a")
    end
end

FACTION_CG = FACTION.index