FACTION.name = "Medical"
FACTION.description = "Trains clones on how to be medics"
FACTION.logo = "materials/ig_cwrp/battalions/cg.png"
FACTION.color = Color(108, 49, 54)
FACTION.maxPromoteRank = 1
FACTION.xoRank = 1
FACTION.commanderRank = 1
FACTION.xoClass = CLASS_MEDIC_COMMANDER
FACTION.commanderClass = CLASS_MEDIC_COMMANDER
FACTION.isDefault = false
FACTION.isActive = false
FACTION.isGloballyRecognized = true
FACTION.defaultClass = CLASS_MEDIC_COMMANDER
FACTION.models = {
    "models/galactic/clones/rm/cdr.mdl"
}
function FACTION:OnSpawn(client)
    local character = client:GetCharacter()
    if (character) then
        local class = rankToClass(character:GetRank(), character:GetFaction())
        character:SetClass(class.index)
        client:Give("weapon_jew_stimkit")
        client:Give("weapon_bacta_grenade")
        client:Give("tfa_swch_dc15a")
    end
end

FACTION_MEDICAL = FACTION.index