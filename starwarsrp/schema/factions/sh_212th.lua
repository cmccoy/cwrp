FACTION.name = "212th"
FACTION.description = "Clone Trooper for the 212th Battalion!"
FACTION.logo = "materials/ig_cwrp/battalions/212th.png"
FACTION.color = Color(184, 139, 22)
FACTION.maxPromoteRank = 17
FACTION.xoRank = 18
FACTION.commanderRank = 19
FACTION.xoClass = CLASS_212TH_XO
FACTION.commanderClass = CLASS_212TH_COMMANDER
FACTION.isDefault = false
FACTION.isActive = true
FACTION.isGloballyRecognized = true
FACTION.defaultClass = CLASS_212TH_PRIVATE
FACTION.models = {
    "models/galactic/clones/212/trooper.mdl"
}

function FACTION:OnSpawn(client)
    local character = client:GetCharacter()
    if (character) then
        local class = rankToClass(character:GetRank(), character:GetFaction())
        character:SetClass(class.index)
        client:Give("tfa_swch_dc15a")
    end
end

FACTION_212TH = FACTION.index