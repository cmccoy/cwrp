FACTION.name = "Utility"
FACTION.description = "A utility droid"
FACTION.logo = "materials/ig_cwrp/ranks/droid.png"
FACTION.color = Color(16, 16, 16, 0)
FACTION.isGloballyRecognized = true
FACTION.isDroid = true
FACTION.isActive = false
FACTION.models = {
    "models/galactic/citizendroid1/citizendroid1.mdl",
    "models/galactic/citizendroid2/citizendroid2.mdl",
    "models/galactic/repairdroid1/repairdroid1.mdl"
}
FACTION.isDefault = false

function FACTION:OnSpawn(client)
    local character = client:GetCharacter()
    if (character) then
        character:SetRank(1)
    end
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
end

FACTION_UTILITY_DROID = FACTION.index