FACTION.name = "104th"
FACTION.description = "Clone Trooper for the 104th Battalion!"
FACTION.logo = "materials/ig_cwrp/battalions/104th.png"
FACTION.color = Color(69, 80, 79)
FACTION.maxPromoteRank = 17
FACTION.xoRank = 18
FACTION.commanderRank = 19
FACTION.xoClass = CLASS_104TH_XO
FACTION.commanderClass = CLASS_104TH_COMMANDER
FACTION.isDefault = false
FACTION.isActive = true
FACTION.isGloballyRecognized = true
FACTION.defaultClass = CLASS_104TH_PRIVATE
FACTION.models = {
    "models/galactic/clones/104/trooper.mdl"
}

function FACTION:OnSpawn(client)
    local character = client:GetCharacter()
    if (character) then
        local class = rankToClass(character:GetRank(), character:GetFaction())
        character:SetClass(class.index)
        client:Give("tfa_swch_dc15a")
    end
end

FACTION_104TH = FACTION.index