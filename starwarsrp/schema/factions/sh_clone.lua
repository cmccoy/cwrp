FACTION.name = "CT"
FACTION.description = "Clone Trooper for the Grand Army of the Republic!"
FACTION.logo = "materials/ig_cwrp/battalions/no_bat.png"
FACTION.color = Color(16, 16, 16, 0)
FACTION.isDefault = false
FACTION.isActive = false
FACTION.isGloballyRecognized = true
FACTION.models = {
    "models/player/trooper/cctrooper.mdl"
}

function FACTION:OnSpawn(client)
    local character = client:GetCharacter()
    if (character) then
        character:SetRank(1)
        client:Give("tfa_swch_dc15a")
    end
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
end

FACTION_CLONE = FACTION.index