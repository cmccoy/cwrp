FACTION.name = "Jedi"
FACTION.description = "Guardians of the Galaxy"
FACTION.logo = "materials/ig_cwrp/battalions/501st.png"
FACTION.color = Color(55, 90, 141)
FACTION.maxPromoteRank = 4
FACTION.xoRank = 5
FACTION.commanderRank = 6
FACTION.xoClass = CLASS_JEDI_COUNCIL
FACTION.commanderClass = CLASS_JEDI_MASTER_OF_THE_ORDER
FACTION.defaultClass = CLASS_JEDI_INITIATE
FACTION.isDefault = false
FACTION.isActive = true
FACTION.isGloballyRecognized = true
FACTION.models = {
    "models/grealms/characters/casualjedi/casualjedi_07.mdl",
    "models/grealms/characters/casualjedi/casualjedi_09.mdl"
}
function FACTION:OnSpawn(client)
    local character = client:GetCharacter()
    if (character) then
        local class = rankToClass(character:GetRank(), character:GetFaction())
        character:SetClass(class.index)
    end
end

FACTION_JEDI = FACTION.index