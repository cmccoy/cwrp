FACTION.name = "RC"
FACTION.description = "The best of the best"
FACTION.logo = "materials/ig_cwrp/battalions/501st.png"
FACTION.color = Color(55, 90, 141)
FACTION.maxPromoteRank = 17
FACTION.xoRank = 18
FACTION.commanderRank = 19
FACTION.isDefault = false
FACTION.isActive = false
FACTION.isGloballyRecognized = true
FACTION.models = {
    "models/player/gman_high.mdl"
}

FACTION_RC = FACTION.index