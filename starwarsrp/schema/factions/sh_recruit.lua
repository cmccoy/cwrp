FACTION.name = "Recruit"
FACTION.description = "Clone Recruit for the Grand Army of the Republic!"
FACTION.logo = "materials/ig_cwrp/battalions/no_bat.png"
FACTION.color = Color(16, 16, 16, 0)
FACTION.isGloballyRecognized = true
FACTION.isActive = false
FACTION.models = {
    "models/player/trooper/cctrooper.mdl"
}
FACTION.isDefault = true

function FACTION:OnSpawn(client)
    local character = client:GetCharacter()
    if (character) then
        character:SetRank(1)
    end
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
end

FACTION_RECRUIT = FACTION.index