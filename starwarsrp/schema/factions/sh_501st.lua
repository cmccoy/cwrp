FACTION.name = "501st"
FACTION.description = "Clone Trooper for the 501st Battalion!"
FACTION.logo = "materials/ig_cwrp/battalions/501st.png"
FACTION.color = Color(55, 90, 141)
FACTION.maxPromoteRank = 17
FACTION.xoRank = 18
FACTION.commanderRank = 19
FACTION.xoClass = CLASS_501ST_XO
FACTION.commanderClass = CLASS_501ST_COMMANDER
FACTION.isDefault = false
FACTION.isActive = true
FACTION.isGloballyRecognized = true
FACTION.defaultClass = CLASS_501ST_PRIVATE
FACTION.models = {
    "models/galactic/clones/501/trooper.mdl"
}

function FACTION:OnSpawn(client)
    local character = client:GetCharacter()
    if (character) then
        local class = rankToClass(character:GetRank(), character:GetFaction())
        character:SetClass(class.index)
        client:Give("tfa_swch_dc15a")
    end
end

FACTION_501ST = FACTION.index