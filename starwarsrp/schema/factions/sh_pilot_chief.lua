FACTION.name = "Pilot"
FACTION.description = "Trains clones on how to be pilots"
FACTION.logo = "materials/ig_cwrp/battalions/cg.png"
FACTION.color = Color(239, 222, 99)
FACTION.maxPromoteRank = 1
FACTION.xoRank = 1
FACTION.commanderRank = 1
FACTION.xoClass = CLASS_PILOT_COMMANDER
FACTION.commanderClass = CLASS_PILOT_COMMANDER
FACTION.isDefault = false
FACTION.isActive = false
FACTION.isGloballyRecognized = true
FACTION.defaultClass = CLASS_PILOT_COMMANDER
FACTION.models = {
    "models/galactic/clones/rp/redofc.mdl"
}
function FACTION:OnSpawn(client)
    local character = client:GetCharacter()
    if (character) then
        local class = rankToClass(character:GetRank(), character:GetFaction())
        character:SetClass(class.index)
        client:Give("tfa_swch_dc15a")
    end
end

FACTION_PILOT = FACTION.index