FACTION.name = "41st"
FACTION.description = "Clone Trooper for the 41st Battalion!"
FACTION.logo = "materials/ig_cwrp/battalions/41st.png"
FACTION.color = Color(82, 110, 81)
FACTION.maxPromoteRank = 17
FACTION.xoRank = 18
FACTION.commanderRank = 19
FACTION.xoClass = CLASS_41ST_XO
FACTION.commanderClass = CLASS_41ST_COMMANDER
FACTION.isDefault = false
FACTION.isActive = true
FACTION.isGloballyRecognized = true
FACTION.defaultClass = CLASS_41ST_PRIVATE
FACTION.models = {
    "models/galactic/clones/41/trooper.mdl"
}

function FACTION:OnSpawn(client)
    local character = client:GetCharacter()
    if (character) then
        local class = rankToClass(character:GetRank(), character:GetFaction())
        character:SetClass(class.index)
        client:Give("tfa_swch_dc15a")
    end
end

FACTION_41ST = FACTION.index