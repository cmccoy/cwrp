FACTION.name = "327th"
FACTION.description = "Clone Trooper for the 327th Battalion!"
FACTION.logo = "materials/ig_cwrp/battalions/327th.png"
FACTION.color = Color(161, 154, 60)
FACTION.maxPromoteRank = 17
FACTION.xoRank = 18
FACTION.commanderRank = 19
FACTION.xoClass = CLASS_327TH_XO
FACTION.commanderClass = CLASS_327TH_COMMANDER
FACTION.isDefault = false
FACTION.isActive = true
FACTION.isGloballyRecognized = true
FACTION.defaultClass = CLASS_327TH_PRIVATE
FACTION.models = {
    "models/galactic/clones/327/trooper.mdl"
}

function FACTION:OnSpawn(client)
    local character = client:GetCharacter()
    if (character) then
        local class = rankToClass(character:GetRank(), character:GetFaction())
        character:SetClass(class.index)
        client:Give("tfa_swch_dc15a")
    end
end

FACTION_327TH = FACTION.index