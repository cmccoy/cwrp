ix.anim.gonk = {
    melee = {
		[ACT_MP_STAND_IDLE] = {"idle", "idle"},
        [ACT_MP_CROUCH_IDLE] = {"idle", "idle"},
        [ACT_MP_CROUCHWALK] = {"idle", "walk"},
        [ACT_MP_WALK] = {"idle", "walk"},
        [ACT_MP_RUN] = {"idle", "walk"},
        [ACT_VM_HOLSTER_EMPTY] = {"idle", "walk"},
        [ACT_VM_IDLE_LOWERED] = {"idle", "walk"},
        [ACT_LAND] = {"idle", "idle"}
	},
	grenade = {
		[ACT_MP_STAND_IDLE] = {"idle", "idle"},
        [ACT_MP_CROUCH_IDLE] = {"idle", "idle"},
        [ACT_MP_CROUCHWALK] = {"idle", "walk"},
        [ACT_MP_WALK] = {"idle", "walk"},
        [ACT_MP_RUN] = {"idle", "walk"},
        [ACT_VM_HOLSTER_EMPTY] = {"idle", "walk"},
        [ACT_VM_IDLE_LOWERED] = {"idle", "walk"},
        [ACT_LAND] = {"idle", "idle"}
	},
	normal = {
		[ACT_MP_STAND_IDLE] = {"idle", "idle"},
        [ACT_MP_CROUCH_IDLE] = {"idle", "idle"},
        [ACT_MP_CROUCHWALK] = {"idle", "walk"},
        [ACT_MP_WALK] = {"idle", "walk"},
        [ACT_MP_RUN] = {"idle", "walk"},
        [ACT_VM_HOLSTER_EMPTY] = {"idle", "walk"},
        [ACT_VM_IDLE_LOWERED] = {"idle", "walk"},
        [ACT_LAND] = {"idle", "idle"}
	},
	pistol = {
		[ACT_MP_STAND_IDLE] = {"idle", "idle"},
        [ACT_MP_CROUCH_IDLE] = {"idle", "idle"},
        [ACT_MP_CROUCHWALK] = {"idle", "walk"},
        [ACT_MP_WALK] = {"idle", "walk"},
        [ACT_MP_RUN] = {"idle", "walk"},
        [ACT_VM_HOLSTER_EMPTY] = {"idle", "walk"},
        [ACT_VM_IDLE_LOWERED] = {"idle", "walk"},
        [ACT_LAND] = {"idle", "idle"}
	},
	shotgun = {
		[ACT_MP_STAND_IDLE] = {"idle", "idle"},
        [ACT_MP_CROUCH_IDLE] = {"idle", "idle"},
        [ACT_MP_CROUCHWALK] = {"idle", "walk"},
        [ACT_MP_WALK] = {"idle", "walk"},
        [ACT_MP_RUN] = {"idle", "walk"},
        [ACT_VM_HOLSTER_EMPTY] = {"idle", "walk"},
        [ACT_VM_IDLE_LOWERED] = {"idle", "walk"},
        [ACT_LAND] = {"idle", "idle"}
	},
	smg = {
		[ACT_MP_STAND_IDLE] = {"idle", "idle"},
        [ACT_MP_CROUCH_IDLE] = {"idle", "idle"},
        [ACT_MP_CROUCHWALK] = {"idle", "walk"},
        [ACT_MP_WALK] = {"idle", "walk"},
        [ACT_MP_RUN] = {"idle", "walk"},
        [ACT_VM_HOLSTER_EMPTY] = {"idle", "walk"},
        [ACT_VM_IDLE_LOWERED] = {"idle", "walk"},
        [ACT_LAND] = {"idle", "idle"}
	},
	beam = {
		[ACT_MP_STAND_IDLE] = {"idle", "idle"},
        [ACT_MP_CROUCH_IDLE] = {"idle", "idle"},
        [ACT_MP_CROUCHWALK] = {"idle", "walk"},
        [ACT_MP_WALK] = {"idle", "walk"},
        [ACT_MP_RUN] = {"idle", "walk"},
        [ACT_VM_HOLSTER] = {"idle", "walk"},
        [ACT_VM_IDLE_LOWERED] = {"idle", "walk"},
        [ACT_LAND] = {"idle", "idle"}
    }
}

ix.anim.rancor = {
    melee = {
		[ACT_MP_STAND_IDLE] = {"idle", "idle"},
        [ACT_MP_CROUCH_IDLE] = {"idle", "idle"},
        [ACT_MP_CROUCHWALK] = {"idle", "walk"},
        [ACT_MP_WALK] = {"idle", "walk"},
        [ACT_MP_RUN] = {"idle", "run"},
        [ACT_VM_HOLSTER_EMPTY] = {"idle", "run"},
        [ACT_VM_IDLE_LOWERED] = {"idle", "run"},
        [ACT_LAND] = {"idle", "idle"}
	},
	grenade = {
		[ACT_MP_STAND_IDLE] = {"idle", "idle"},
        [ACT_MP_CROUCH_IDLE] = {"idle", "idle"},
        [ACT_MP_CROUCHWALK] = {"idle", "walk"},
        [ACT_MP_WALK] = {"idle", "walk"},
        [ACT_MP_RUN] = {"idle", "run"},
        [ACT_VM_HOLSTER_EMPTY] = {"idle", "run"},
        [ACT_VM_IDLE_LOWERED] = {"idle", "run"},
        [ACT_LAND] = {"idle", "idle"}
	},
	normal = {
		[ACT_MP_STAND_IDLE] = {"idle", "idle"},
        [ACT_MP_CROUCH_IDLE] = {"idle", "idle"},
        [ACT_MP_CROUCHWALK] = {"idle", "walk"},
        [ACT_MP_WALK] = {"idle", "walk"},
        [ACT_MP_RUN] = {"idle", "run"},
        [ACT_VM_HOLSTER_EMPTY] = {"idle", "run"},
        [ACT_VM_IDLE_LOWERED] = {"idle", "run"},
        [ACT_LAND] = {"idle", "idle"}
	},
	pistol = {
		[ACT_MP_STAND_IDLE] = {"idle", "idle"},
        [ACT_MP_CROUCH_IDLE] = {"idle", "idle"},
        [ACT_MP_CROUCHWALK] = {"idle", "walk"},
        [ACT_MP_WALK] = {"idle", "walk"},
        [ACT_MP_RUN] = {"idle", "run"},
        [ACT_VM_HOLSTER_EMPTY] = {"idle", "run"},
        [ACT_VM_IDLE_LOWERED] = {"idle", "run"},
        [ACT_LAND] = {"idle", "idle"}
	},
	shotgun = {
		[ACT_MP_STAND_IDLE] = {"idle", "idle"},
        [ACT_MP_CROUCH_IDLE] = {"idle", "idle"},
        [ACT_MP_CROUCHWALK] = {"idle", "walk"},
        [ACT_MP_WALK] = {"idle", "walk"},
        [ACT_MP_RUN] = {"idle", "run"},
        [ACT_VM_HOLSTER_EMPTY] = {"idle", "run"},
        [ACT_VM_IDLE_LOWERED] = {"idle", "run"},
        [ACT_LAND] = {"idle", "idle"}
	},
	smg = {
		[ACT_MP_STAND_IDLE] = {"idle", "idle"},
        [ACT_MP_CROUCH_IDLE] = {"idle", "idle"},
        [ACT_MP_CROUCHWALK] = {"idle", "walk"},
        [ACT_MP_WALK] = {"idle", "walk"},
        [ACT_MP_RUN] = {"idle", "run"},
        [ACT_VM_HOLSTER_EMPTY] = {"idle", "run"},
        [ACT_VM_IDLE_LOWERED] = {"idle", "run"},
        [ACT_LAND] = {"idle", "idle"}
	},
	beam = {
		[ACT_MP_STAND_IDLE] = {"idle", "idle"},
        [ACT_MP_CROUCH_IDLE] = {"idle", "idle"},
        [ACT_MP_CROUCHWALK] = {"idle", "walk"},
        [ACT_MP_WALK] = {"idle", "walk"},
        [ACT_MP_RUN] = {"idle", "run"},
        [ACT_VM_HOLSTER_EMPTY] = {"idle", "run"},
        [ACT_VM_IDLE_LOWERED] = {"idle", "run"},
        [ACT_LAND] = {"idle", "idle"}
    }
}

ix.anim.SetModelClass("models/props/starwars/tech/gonk_droid_new.mdl", "gonk")
ix.anim.SetModelClass("models/creeps/roshan/rancor.mdl", "rancor")