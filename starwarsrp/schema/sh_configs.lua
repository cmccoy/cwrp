ix.currency.symbol = ""
-- ₹
ix.currency.singular = "Republic Credit"
ix.currency.plural = "Republic Credits"

ix.config.SetDefault("scoreboardRecognition", true)
--ix.config.SetDefault("music", "music/hl2_song19.mp3")
ix.config.SetDefault("maxAttributes", 60)

ALWAYS_RAISED["weapon_vibrosword"] = true
ALWAYS_RAISED["weapon_lightsaber_initiate"] = true
ALWAYS_RAISED["weapon_lightsaber_padawan"] = true
ALWAYS_RAISED["weapon_lightsaber_knight"] = true
ALWAYS_RAISED["weapon_lightsaber_sentinel"] = true
ALWAYS_RAISED["weapon_lightsaber_consular"] = true 
ALWAYS_RAISED["weapon_policebaton"] = true 
ALWAYS_RAISED["ix_unarrest_stick"] = true 
ALWAYS_RAISED["taser"] = true 

-- ALWAYS_RAISED["weapon_lightsaber_masterguardian"] = true
-- ALWAYS_RAISED["weapon_lightsaber_masterconsular"] = true
-- ALWAYS_RAISED["weapon_lightsaber_mastersentinel"] = true
-- ALWAYS_RAISED["weapon_lightsaber_council"] = true
-- ALWAYS_RAISED["weapon_lightsaber_moto"] = true