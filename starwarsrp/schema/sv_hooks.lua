
function Schema:LoadData()
	-- self:LoadRationDispensers()
	-- self:LoadVendingMachines()
	-- self:LoadCombineLocks()
	self:LoadForceFields()

	-- Schema.CombineObjectives = ix.data.Get("combineObjectives", {}, false, true)
end

function Schema:SaveData()
	-- self:SaveRationDispensers()
	-- self:SaveVendingMachines()
	-- self:SaveCombineLocks()
	self:SaveForceFields()
end

function Schema:PlayerSwitchFlashlight(client, enabled)
	return true
end

local dispose = {
	2948,
	2087,
	2070,
	2067,
	2083,
	2061,
	2078,
	2057,
	2073
}


local dispose2 = {
	3578,
	3572,
	2925,
	2931,
	3380,
	3381,
	3448,
	2093,
	2089,
	2948,
	2087,
	2070,
	2067,
	2083,
	2061,
	2078,
	2057,
	2073
}

local cellButtons = {}
local watchButtons = {}

for k, v in pairs(dispose) do
	cellButtons[v] = true
end

for k, v in pairs(dispose2) do
	watchButtons[v] = true
end

dispose = nil
dispose2 = nil

function Schema:PlayerUse(client, ent)
	if (ent:GetClass() == "func_button") then
		client.nextPlayerUse = client.nextPlayerUse || 0

		if(client.nextPlayerUse <= CurTime()) then 
			client.nextPlayerUse = CurTime() + 1
		else
			return false
		end

		if (watchButtons[ent:MapCreationID()]) then
			
			local character = client:GetCharacter()

			if(ent:MapCreationID() == 3578) then
				hook.Run("SwrpOnAlarmPress", client)

			elseif(ent:MapCreationID() == 3572) then
				hook.Run("SwrpOnResetPress", client)

			elseif(ent:MapCreationID() == 2925) then
				hook.Run("SwrpOnPowerPress", client)

			elseif(ent:MapCreationID() == 2931) then
				hook.Run("SwrpOnPowerGatePress", client)

			elseif(ent:MapCreationID() == 3380) then
				hook.Run("SwrpOnGatePress", client)

			elseif(ent:MapCreationID() == 3381) then
				hook.Run("SwrpOnGatePress", client)

			elseif(ent:MapCreationID() == 3449) then
				hook.Run("SwrpOnHangerPress", client)
			elseif(ent:MapCreationID() == 2093) then

				if(character:GetFaction() != FACTION_CG && !client:IsStaff() && !client:IsGameMaster()) then
					client:NotifyLocalized("You can't press this button")
					client.nextPlayerUse = CurTime() + 2
					return false
				end
				hook.Run("SwrpOnBrigLockdown", client)

			elseif(ent:MapCreationID() == 2089) then

				if(character:GetFaction() != FACTION_CG && !client:IsStaff() && !client:IsGameMaster()) then
					client:NotifyLocalized("You can't press this button")
					client.nextPlayerUse = CurTime() + 2
					return false
				end
				hook.Run("SwrpOnBrigPress", client)

			elseif(cellButtons[ent:MapCreationID()]) then

				if(character:GetFaction() != FACTION_CG && !client:IsStaff() && !client:IsGameMaster()) then
					client:NotifyLocalized("You can't press this button")
					client.nextPlayerUse = CurTime() + 2
					return false
				end
				hook.Run("SwrpOnBrigCellOpen", client, ent)

			end

		end

		ent:Fire("use")

	end
	
end

function Schema:PlayerUseDoor(client, door)
	
end

function Schema:PlayerLoadout(client)
	
end

function Schema:PostPlayerLoadout(client)
	if(client:HasWeapon("ix_hands")) then
		client:StripWeapon("ix_hands")
	end
end

function Schema:PrePlayerLoadedCharacter(client, character, oldCharacter)
	
end

function Schema:PlayerLoadedCharacter(client, character, oldCharacter)
	
	local fac = GetFactionObject(character:GetFaction())

	if (fac.isDroid) then
		client.isDroid = true
	else
		client.isDroid = nil
	end

	-- Characters load too fast
	timer.Simple(.2, function()
		if (!client:OnGround()) then
			client:SetPos(character:GetSpawnPos())
		end
	end)
end

function Schema:CharacterVarChanged(character, key, oldValue, value)
	
end

function Schema:PlayerFootstep(client, position, foot, soundName, volume)
	client:EmitSound(soundName)
	return true
end

function Schema:PlayerSpawn(client)
	if (!client:IsStaff() || !client:IsGameMaster() || !client.hasClaimed) then
		local character = client:GetCharacter()
		if (character) then
			if (character:HasFlags("p")) then
				character:TakeFlags("p")
			end
			if (character:HasFlags("e")) then
				character:TakeFlags("e")
			end
			if (character:HasFlags("n")) then
				character:TakeFlags("n")
			end
			if (character:HasFlags("t")) then
				character:TakeFlags("t")
			end
		end
	end
	if (event_spawner_active) then
		client:SetPos(event_spawner_point + Vector(math.random(-200,200), math.random(-200,200), 0))
	end
end
--TODO Add notification for CG when someone dies in the base
function Schema:PlayerDeath(client, inflicter, attacker)
	-- if (client:IsCG()) then
	-- 	local location = client:GetArea() or "unknown location"

	-- 	self:AddCombineDisplayMessage("@cLostBiosignal")
	-- 	self:AddCombineDisplayMessage("@cLostBiosignalLocation", Color(255, 0, 0, 255), location)

	-- 	if (IsValid(client.ixScanner) and client.ixScanner:Health() > 0) then
	-- 		client.ixScanner:TakeDamage(999)
	-- 	end

	-- 	local sounds = {"npc/overwatch/radiovoice/on1.wav", "npc/overwatch/radiovoice/lostbiosignalforunit.wav"}
	-- 	local chance = math.random(1, 7)

	-- 	if (chance == 2) then
	-- 		sounds[#sounds + 1] = "npc/overwatch/radiovoice/remainingunitscontain.wav"
	-- 	elseif (chance == 3) then
	-- 		sounds[#sounds + 1] = "npc/overwatch/radiovoice/reinforcementteamscode3.wav"
	-- 	end

	-- 	sounds[#sounds + 1] = "npc/overwatch/radiovoice/off4.wav"

	-- 	for k, v in ipairs(player.GetAll()) do
	-- 		if (v:IsNaval()) then
	-- 			ix.util.EmitQueuedSounds(v, sounds, 2, nil, v == client and 100 or 80)
	-- 		end
	-- 	end
	-- end
end

function Schema:PlayerNoClip(client)
	
end

function Schema:EntityTakeDamage(entity, dmgInfo)
	
end

function Schema:PlayerHurt(client, attacker, health, damage)
	if (health <= 0) then
		return
	end

	if (client:IsNaval() and (client.ixTraumaCooldown or 0) < CurTime()) then
		local text = "External"

		if (damage > 50) then
			text = "Severe"
		end

		client:AddCombineDisplayMessage("@cTrauma", Color(255, 0, 0, 255), text)

		if (health < 25) then
			client:AddCombineDisplayMessage("@cDroppingVitals", Color(255, 0, 0, 255))
		end

		client.ixTraumaCooldown = CurTime() + 15
	end
end

--TODO Add droid pain sounds
function Schema:GetPlayerPainSound(client)
	-- if (client:IsNaval()) then
	-- 	local sound = "NPC_MetroPolice.Pain"

	-- 	if (Schema:IsNavalRank(client:Name(), "SCN")) then
	-- 		sound = "NPC_CScanner.Pain"
	-- 	elseif (Schema:IsNavalRank(client:Name(), "SHIELD")) then
	-- 		sound = "NPC_SScanner.Pain"
	-- 	end

	-- 	return sound
	-- end
end

--TODO Add droid death sounds
function Schema:GetPlayerDeathSound(client)
	-- if (client:IsNaval()) then
	-- 	local sound = "NPC_MetroPolice.Die"

	-- 	if (Schema:IsNavalRank(client:Name(), "SCN")) then
	-- 		sound = "NPC_CScanner.Die"
	-- 	elseif (Schema:IsNavalRank(client:Name(), "SHIELD")) then
	-- 		sound = "NPC_SScanner.Die"
	-- 	end

	-- 	for k, v in ipairs(player.GetAll()) do
	-- 		if (v:IsNaval()) then
	-- 			v:EmitSound(sound)
	-- 		end
	-- 	end

	-- 	return sound
	-- end
end

function Schema:OnNPCKilled(npc, attacker, inflictor)
	
end

function Schema:PlayerMessageSend(speaker, chatType, text, anonymous, receivers, rawText)
	
end

function Schema:CanPlayerJoinClass(client, class, info)
	if (client:GetCharacter():IsArrested()) then
		client:Notify("You cannot change classes when you are restrained!")

		return false
	end
end

function Schema:KeyPress(client, key)
	
end

function Schema:PlayerSpawnObject(client)
	if (client:IsRestricted() or IsValid(client.ixScanner)) then
		return false
	end
end

function Schema:PlayerSpray(client)
	return true
end

hook.Add("OnNPCKilled", "[Impact] Remove npc weapon on death", function(npc, attacker, inflictor)

	if (IsValid(npc:GetActiveWeapon())) then
		npc:GetActiveWeapon():Remove()
	end

end)

function Schema:PlayerSpawnSENT(client, class)
	if (client:GetCharacter()) then
		return client:GetCharacter():HasFlags("E")
	end

	return false
end

function Schema:PlayerSpawnSWEP(client, weapon, swep)
	if (client:GetCharacter()) then
		return client:GetCharacter():HasFlags("w")
	end
	return false
end