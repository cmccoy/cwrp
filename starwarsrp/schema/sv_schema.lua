
Schema.CombineObjectives = Schema.CombineObjectives or {}

function Schema:AddCombineDisplayMessage(text, color, exclude, ...)
	color = color or color_white

	local arguments = {...}
	local receivers = {}

	-- we assume that exclude will be part of the argument list if we're using
	-- a phrase and exclude is a non-player argument
	if (type(exclude) != "Player") then
		table.insert(arguments, 1, exclude)
	end

	for _, v in ipairs(player.GetAll()) do
		if (v:IsNaval() and v != exclude) then
			receivers[#receivers + 1] = v
		end
	end

	netstream.Start(receivers, "CombineDisplayMessage", text, color, arguments)
end

-- data saving
function Schema:SaveCombineLocks()
	local data = {}

	for _, v in ipairs(ents.FindByClass("ix_combinelock")) do
		if (IsValid(v.door)) then
			data[#data + 1] = {
				v.door:MapCreationID(),
				v.door:WorldToLocal(v:GetPos()),
				v.door:WorldToLocalAngles(v:GetAngles()),
				v:GetLocked()
			}
		end
	end

	ix.data.Set("combineLocks", data)
end

function Schema:SaveForceFields()
	local data = {}

	for _, v in ipairs(ents.FindByClass("ix_forcefield")) do
		data[#data + 1] = {v:GetPos(), v:GetAngles(), v:GetMode()}
	end

	ix.data.Set("forceFields", data)
end

-- data loading
function Schema:LoadCombineLocks()
	for _, v in ipairs(ix.data.Get("combineLocks") or {}) do
		local door = ents.GetMapCreatedEntity(v[1])

		if (IsValid(door) and door:IsDoor()) then
			local lock = ents.Create("ix_combinelock")

			lock:SetPos(door:GetPos())
			lock:Spawn()
			lock:SetDoor(door, door:LocalToWorld(v[2]), door:LocalToWorldAngles(v[3]))
			lock:SetLocked(v[4])
		end
	end
end

function Schema:LoadForceFields()
	for _, v in ipairs(ix.data.Get("forceFields") or {}) do
		local field = ents.Create("ix_forcefield")

		field:SetPos(v[1])
		field:SetAngles(v[2])
		field:Spawn()
		field:SetMode(v[3])
	end
end

function Schema:PlayerCanPickupWeapon(client, wep)

	local character = client:GetCharacter()
	if (character) then
		local fac = GetFactionObject(character:GetFaction())

		if (character:GetFaction() == FACTION_UTILITY_DROID) then
			return false
		end
	end

end

hook.Add("InitPostEntity", "[IG] Delete laser doors", function()

	for k, v in pairs(ents.GetAll()) do
		if((v:MapCreationID() == 1750) || (v:MapCreationID() == 1746) || (v:MapCreationID() == 3579) || (v:MapCreationID() == 3581)) then
			v:Remove()
		end
	end

end)

WeirdMoneyTotals = {}

do
	if (!timer.Exists("[IG] Track Alot of money")) then
		timer.Create("[IG] Track Alot of money", 3600, 0, function()
			for k, v in pairs(WeirdMoneyTotals) do
				if(v > 2000) then
					hook.Run("SwrpPossibleAbusers", k, v)
				end
			end
			WeirdMoneyTotals = {}
		end)
	end
end

hook.Add("SwrpGiveMoney", "[IG] Money Earned", function(pl, amount)
	if (WeirdMoneyTotals[pl:SteamID()]) then
		WeirdMoneyTotals[pl:SteamID()] = WeirdMoneyTotals[pl:SteamID()] + amount
	else
		WeirdMoneyTotals[pl:SteamID()] = amount
	end
end)

local possibleDrops = {
	{name = "Weapon Sight", class = "weapon_sight"},
	{name = "Alien Goo", class = "alien_goo"},
	{name = "Cooling Unit", class = "cooling_unit"},
	{name = "Refined Metal", class = "refined_metal"},
	{name = "Targeting System", class = "targeting_system"},
	{name = "Explosive Adhesive", class = "explosive_adhesive"}
}

hook.Add("OnNPCKilled", "[Impact] Random item give", function(npc, attacker, inflictor)

	if (attacker:IsPlayer()) then
		
		if (math.random(0, 50) == 50) then

			local item = table.Random(possibleDrops)

			attacker:GetCharacter():GetInventory():Add(item.class)

			attacker:ChatPrint("[ITEM DROP] " .. item.name)

		end

	end

end)