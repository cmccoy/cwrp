CLASS.name = "212th Commander"
CLASS.faction = FACTION_212TH
CLASS.previousRank = "XO"
CLASS.previousRankLiteral = CLASS_212TH_XO
CLASS.currentRankFull = "Commander" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/rcdr.png"
CLASS.rank = 19
CLASS.currentRankAbv = "CMDR"
CLASS.canPromote = true
CLASS.nextRank = nil
CLASS.nextRankLiteral = nil
CLASS.model = "models/galactic/clones/212/cdr.mdl"
function CLASS:OnSpawn(client) 
  client:SetMaxHealth(200)
  client:SetHealth(client:GetMaxHealth())
end
CLASS_212TH_COMMANDER = CLASS.index