CLASS.name = "212th Staff Sergeant"
CLASS.faction = FACTION_212TH
CLASS.previousRank = "SGT"
CLASS.previousRankLiteral = CLASS_212TH_SERGEANT
CLASS.currentRankFull = "Staff Sergeant" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/ssg.png"
CLASS.rank = 6
CLASS.currentRankAbv = "SSGT"
CLASS.nextRank = "SFC"
CLASS.nextRankLiteral = CLASS_212TH_SERGEANT_FIRST_CLASS
CLASS.model = "models/galactic/clones/212/nco.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_212TH_STAFF_SERGEANT = CLASS.index