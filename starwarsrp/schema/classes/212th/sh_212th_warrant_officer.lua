CLASS.name = "212th Warrant Officer"
CLASS.faction = FACTION_212TH
CLASS.previousRank = "SGM"
CLASS.previousRankLiteral = CLASS_212TH_SERGEANT_MAJOR
CLASS.currentRankFull = "Warrant Officer" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/oc.png"
CLASS.rank = 11
CLASS.currentRankAbv = "WO"
CLASS.nextRank = "2LT"
CLASS.nextRankLiteral = CLASS_212TH_SECOND_LIEUTENANT
CLASS.model = "models/galactic/clones/212/nco.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_212TH_WARRANT_OFFICER = CLASS.index