CLASS.name = "212th Private"
CLASS.faction = FACTION_212TH
CLASS.previousRank = nil;
CLASS.previousRankLiteral = nil
CLASS.currentRankFull = "Private" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/pvt.png"
CLASS.rank = 1
CLASS.currentRankLiteral = CLASS_212TH_PRIVATE
CLASS.currentRankAbv = "PVT";
CLASS.nextRank = "PFC"
CLASS.nextRankLiteral = CLASS_212TH_PRIVATEFIRSTCLASS
CLASS.model = "models/galactic/clones/212/trooper.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_212TH_PRIVATE = CLASS.index