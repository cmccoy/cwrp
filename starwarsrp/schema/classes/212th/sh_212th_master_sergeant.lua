CLASS.name = "212th Master Sergeant"
CLASS.faction = FACTION_212TH
CLASS.previousRank = "SFC"
CLASS.previousRankLiteral = CLASS_212TH_SERGEANT_FIRST_CLASS
CLASS.currentRankFull = "Master Sergeant" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/msg.png"
CLASS.rank = 8
CLASS.currentRankAbv = "MSGT"
CLASS.nextRank = "1SGT"
CLASS.nextRankLiteral = CLASS_212TH_FIRST_SERGEANT
CLASS.model = "models/galactic/clones/212/nco.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_212TH_MASTER_SERGEANT = CLASS.index