CLASS.name = "212th Sergeant First CLASS"
CLASS.faction = FACTION_212TH
CLASS.previousRank = "SSGT"
CLASS.previousRankLiteral = CLASS_212TH_STAFF_SERGEANT
CLASS.currentRankFull = "Sergeant First Class" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/sfc.png"
CLASS.rank = 7
CLASS.currentRankAbv = "SFC"
CLASS.nextRank = "MSGT"
CLASS.nextRankLiteral = CLASS_212TH_MASTER_SERGEANT
CLASS.model = "models/galactic/clones/212/nco.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_212TH_SERGEANT_FIRST_CLASS = CLASS.index