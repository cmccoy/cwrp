CLASS.name = "212th Sergeant"
CLASS.faction = FACTION_212TH
CLASS.previousRank = "CPL"
CLASS.previousRankLiteral = CLASS_212TH_CORPORAL
CLASS.currentRankFull = "Sergant" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/sgt.png"
CLASS.rank = 5
CLASS.currentRankAbv = "SGT"
CLASS.nextRank = "SSGT"
CLASS.nextRankLiteral = CLASS_212TH_STAFF_SERGEANT
CLASS.model = "models/galactic/clones/212/nco.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_212TH_SERGEANT = CLASS.index