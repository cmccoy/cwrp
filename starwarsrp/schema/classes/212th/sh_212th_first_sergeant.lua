CLASS.name = "212th First Sergeant"
CLASS.faction = FACTION_212TH
CLASS.previousRank = "MSGT"
CLASS.previousRankLiteral = CLASS_212TH_MASTER_SERGEANT
CLASS.currentRankFull = "First Sergeant" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/1sg.png"
CLASS.rank = 9
CLASS.currentRankAbv = "1SGT"
CLASS.nextRank = "SGM"
CLASS.nextRankLiteral = CLASS_212TH_SERGEANT_MAJOR
CLASS.model = "models/galactic/clones/212/nco.mdl"
function CLASS:OnSpawn(client) 
  client:SetMaxHealth(200)
  client:SetHealth(client:GetMaxHealth())
end
CLASS_212TH_FIRST_SERGEANT = CLASS.index