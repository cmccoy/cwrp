CLASS.name = "212th Lieutenant Colonel"
CLASS.faction = FACTION_212TH
CLASS.previousRank = "MAJ"
CLASS.previousRankLiteral = CLASS_212TH_MAJOR
CLASS.currentRankFull = "Lieutenant Colonel" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/ltc.png"
CLASS.rank = 16
CLASS.currentRankAbv = "LTC"
CLASS.canPromote = true
CLASS.nextRank = "COL"
CLASS.nextRankLiteral = CLASS_212TH_COLONEL
CLASS.model = "models/galactic/clones/212/highofc.mdl"
function CLASS:OnSpawn(client) 
  client:SetMaxHealth(200)
  client:SetHealth(client:GetMaxHealth())
end
CLASS_212TH_LIEUTENANT_COLONEL = CLASS.index