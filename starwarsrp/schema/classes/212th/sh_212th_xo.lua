CLASS.name = "212th Executive Officer"
CLASS.faction = FACTION_212TH
CLASS.previousRank = "COL"
CLASS.previousRankLiteral = CLASS_212TH_COLONEL
CLASS.currentRankFull = "Executive Officer"  
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/cdr.png"
CLASS.rank = 18
CLASS.currentRankAbv = "XO"
CLASS.canPromote = true
CLASS.nextRank = "COMM"
CLASS.nextRankLiteral = CLASS_212TH_COMMANDER
CLASS.model = "models/galactic/clones/212/highofc.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_212TH_XO = CLASS.index