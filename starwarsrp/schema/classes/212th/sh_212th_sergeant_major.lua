CLASS.name = "212th Sergeant Major"
CLASS.faction = FACTION_212TH
CLASS.previousRank = "1SGT"
CLASS.previousRankLiteral = CLASS_212TH_FIRST_SERGEANT
CLASS.currentRankFull = "Sergeant Major" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/sgm.png"
CLASS.rank = 10
CLASS.currentRankAbv = "SGM"
CLASS.nextRank = "WO"
CLASS.nextRankLiteral = CLASS_212TH_WARRANT_OFFICER
CLASS.model = "models/galactic/clones/212/nco.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_212TH_SERGEANT_MAJOR = CLASS.index