CLASS.name = "Pilot Chief"
CLASS.faction = FACTION_PILOT
CLASS.previousRank = "Chief"
CLASS.previousRankLiteral = FACTION_PILOT
CLASS.currentRankFull = "Chief"
CLASS.rank = 1
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/rcdr.png"
CLASS.currentRankAbv = "Chief"
CLASS.canPromote = true
CLASS.nextRank = nil
CLASS.nextRankLiteral = nil
CLASS.model = "models/galactic/clones/rp/redofc.mdl"

function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end

CLASS_PILOT = CLASS.index