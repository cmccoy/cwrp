CLASS.name = "CG First Sergeant"
CLASS.faction = FACTION_CG
CLASS.previousRank = "MSGT"
CLASS.previousRankLiteral = CLASS_CG_MASTER_SERGEANT
CLASS.currentRankFull = "First Sergeant" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/1sg.png"
CLASS.rank = 9
CLASS.currentRankAbv = "1SGT"
CLASS.nextRank = "SGM"
CLASS.nextRankLiteral = CLASS_CG_SERGEANT_MAJOR
CLASS.model = "models/galactic/clones/cg/nco.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_CG_FIRST_SERGEANT = CLASS.index