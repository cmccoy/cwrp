CLASS.name = "CG Sergeant"
CLASS.faction = FACTION_CG
CLASS.previousRank = "CPL"
CLASS.previousRankLiteral = CLASS_CG_CORPORAL
CLASS.currentRankFull = "Sergant" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/sgt.png"
CLASS.rank = 5
CLASS.currentRankAbv = "SGT"
CLASS.nextRank = "SSGT"
CLASS.nextRankLiteral = CLASS_CG_STAFF_SERGEANT
CLASS.model = "models/galactic/clones/cg/nco.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_CG_SERGEANT = CLASS.index