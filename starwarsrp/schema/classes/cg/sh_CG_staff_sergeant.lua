CLASS.name = "CG Staff Sergeant"
CLASS.faction = FACTION_CG
CLASS.previousRank = "SGT"
CLASS.previousRankLiteral = CLASS_CG_SERGEANT
CLASS.currentRankFull = "Staff Sergeant" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/ssg.png"
CLASS.rank = 6
CLASS.currentRankAbv = "SSGT"
CLASS.nextRank = "SFC"
CLASS.nextRankLiteral = CLASS_CG_SERGEANT_FIRST_CLASS
CLASS.model = "models/galactic/clones/cg/nco.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_CG_STAFF_SERGEANT = CLASS.index