CLASS.name = "CG Corporal"
CLASS.faction = FACTION_CG
CLASS.previousRank = "LCPL"
CLASS.previousRankLiteral = CLASS_CG_LANCE_CORPORAL
CLASS.currentRankFull = "Corporal" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/1cpl.png"
CLASS.rank = 4
CLASS.currentRankAbv = "CPL"
CLASS.nextRank = "SGT"
CLASS.nextRankLiteral = CLASS_CG_SERGEANT
CLASS.model = "models/galactic/clones/cg/trooper.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_CG_CORPORAL = CLASS.index