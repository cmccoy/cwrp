CLASS.name = "CG Master Sergeant"
CLASS.faction = FACTION_CG
CLASS.previousRank = "SFC"
CLASS.previousRankLiteral = CLASS_CG_SERGEANT_FIRST_CLASS
CLASS.currentRankFull = "Master Sergeant" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/msg.png"
CLASS.rank = 8
CLASS.currentRankAbv = "MSGT"
CLASS.nextRank = "1SGT"
CLASS.nextRankLiteral = CLASS_CG_FIRST_SERGEANT
CLASS.model = "models/galactic/clones/cg/nco.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_CG_MASTER_SERGEANT = CLASS.index