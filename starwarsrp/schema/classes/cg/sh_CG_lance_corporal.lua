CLASS.name = "CG Lance Corporal"
CLASS.faction = FACTION_CG
CLASS.previousRank = "PFC"
CLASS.previousRankLiteral = CLASS_CG_PRIVATEFIRSTCLASS
CLASS.currentRankFull = "Lance Corporal" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/cpl.png"
CLASS.rank = 3
CLASS.currentRankAbv = "LCPL"
CLASS.nextRank = "CPL"
CLASS.nextRankLiteral = CLASS_CG_CORPORAL
CLASS.model = "models/galactic/clones/cg/trooper.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_CG_LANCE_CORPORAL = CLASS.index