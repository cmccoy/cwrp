CLASS.name = "CG Commander"
CLASS.faction = FACTION_CG
CLASS.previousRank = "XO"
CLASS.previousRankLiteral = CLASS_CG_XO
CLASS.currentRankFull = "Commander" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/rcdr.png"
CLASS.rank = 19
CLASS.currentRankAbv = "CMDR"
CLASS.canPromote = true
CLASS.nextRank = nil
CLASS.nextRankLiteral = nil
CLASS.model = "models/galactic/clones/cg/cdr.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_CG_COMMANDER = CLASS.index