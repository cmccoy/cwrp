CLASS.name = "CG Warrant Officer"
CLASS.faction = FACTION_CG
CLASS.previousRank = "SGM"
CLASS.previousRankLiteral = CLASS_CG_SERGEANT_MAJOR
CLASS.currentRankFull = "Warrant Officer" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/oc.png"
CLASS.rank = 11
CLASS.currentRankAbv = "WO"
CLASS.nextRank = "2LT"
CLASS.nextRankLiteral = CLASS_CG_SECOND_LIEUTENANT
CLASS.model = "models/galactic/clones/cg/nco.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_CG_WARRANT_OFFICER = CLASS.index