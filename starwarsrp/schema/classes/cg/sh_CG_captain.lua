CLASS.name = "CG Captain"
CLASS.faction = FACTION_CG
CLASS.previousRank = "1LT"
CLASS.previousRankLiteral = CLASS_CG_FIRST_LIEUTENANT
CLASS.currentRankFull = "Captain" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/cpt.png"
CLASS.rank = 14
CLASS.currentRankAbv = "CPT"
CLASS.canPromote = true
CLASS.nextRank = "MAJ"
CLASS.nextRankLiteral = CLASS_CG_MAJOR
CLASS.model = "models/galactic/clones/cg/lowofc.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_CG_CAPTAIN = CLASS.index