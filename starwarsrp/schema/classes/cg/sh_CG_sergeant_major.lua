CLASS.name = "CG Sergeant Major"
CLASS.faction = FACTION_CG
CLASS.previousRank = "1SGT"
CLASS.previousRankLiteral = CLASS_CG_FIRST_SERGEANT
CLASS.currentRankFull = "Sergeant Major" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/sgm.png"
CLASS.rank = 10
CLASS.currentRankAbv = "SGM"
CLASS.nextRank = "WO"
CLASS.nextRankLiteral = CLASS_CG_WARRANT_OFFICER
CLASS.model = "models/galactic/clones/cg/nco.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_CG_SERGEANT_MAJOR = CLASS.index