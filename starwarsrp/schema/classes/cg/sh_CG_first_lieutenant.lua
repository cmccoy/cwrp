CLASS.name = "CG 1st Lieutenant"
CLASS.faction = FACTION_CG
CLASS.previousRank = "2LT"
CLASS.previousRankLiteral = CLASS_CG_SECOND_LIEUTENANT
CLASS.currentRankFull = "1st Lieutenant" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/lt.png"
CLASS.rank = 13
CLASS.currentRankAbv = "1LT"
CLASS.canPromote = true
CLASS.nextRank = "CPT"
CLASS.nextRankLiteral = CLASS_CG_CAPTAIN
CLASS.model = "models/galactic/clones/cg/lowofc.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_CG_FIRST_LIEUTENANT = CLASS.index