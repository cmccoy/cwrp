CLASS.name = "CG Sergeant First CLASS"
CLASS.faction = FACTION_CG
CLASS.previousRank = "SSGT"
CLASS.previousRankLiteral = CLASS_CG_STAFF_SERGEANT
CLASS.currentRankFull = "Sergeant First Class" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/sfc.png"
CLASS.rank = 7
CLASS.currentRankAbv = "SFC"
CLASS.nextRank = "MSGT"
CLASS.nextRankLiteral = CLASS_CG_MASTER_SERGEANT
CLASS.model = "models/galactic/clones/cg/nco.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_CG_SERGEANT_FIRST_CLASS = CLASS.index