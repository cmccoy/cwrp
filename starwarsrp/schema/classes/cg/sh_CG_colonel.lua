CLASS.name = "CG Colonel"
CLASS.faction = FACTION_CG
CLASS.previousRank = "LTC"
CLASS.previousRankLiteral = CLASS_CG_LIEUTENANT_COLONEL
CLASS.currentRankFull = "Colonel" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/col.png"
CLASS.rank = 17
CLASS.currentRankAbv = "COL"
CLASS.canPromote = true
CLASS.nextRank = nil
CLASS.nextRankLiteral = nil
CLASS.model = "models/galactic/clones/cg/highofc.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_CG_COLONEL = CLASS.index