CLASS.name = "CG Major"
CLASS.faction = FACTION_CG
CLASS.previousRank = "CPT"
CLASS.previousRankLiteral = CLASS_CG_CAPTAIN
CLASS.currentRankFull = "Major"
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/maj.png"
CLASS.rank = 15
CLASS.currentRankAbv = "MAJ"
CLASS.canPromote = true
CLASS.nextRank = "LTC"
CLASS.nextRankLiteral = CLASS_CG_LIEUTENANT_COLONEL
CLASS.model = "models/galactic/clones/cg/highofc.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_CG_MAJOR = CLASS.index