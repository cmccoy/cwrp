CLASS.name = "CG 2nd Lieutenant"
CLASS.faction = FACTION_CG
CLASS.previousRank = "WO"
CLASS.previousRankLiteral = CLASS_CG_WARRANT_OFFICER
CLASS.currentRankFull = "2nd Lieutenant" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/2lt.png"
CLASS.rank = 12
CLASS.currentRankAbv = "2LT"
CLASS.canPromote = true
CLASS.nextRank = "1LT"
CLASS.nextRankLiteral = CLASS_CG_FIRST_LIEUTENANT
CLASS.model = "models/galactic/clones/cg/lowofc.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_CG_SECOND_LIEUTENANT = CLASS.index