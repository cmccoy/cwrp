CLASS.name = "CG Private First Class"
CLASS.faction = FACTION_CG
CLASS.previousRank = "PVT";
CLASS.previousRankLiteral = CLASS_CG_PRIVATE
CLASS.currentRankFull = "Private First Class" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/pfc.png"
CLASS.rank = 2
CLASS.currentRankAbv = "PFC";
CLASS.nextRank = "LCPL"
CLASS.nextRankLiteral = CLASS_CG_LANCE_CORPORAL
CLASS.model = "models/galactic/clones/cg/trooper.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_CG_PRIVATEFIRSTCLASS = CLASS.index