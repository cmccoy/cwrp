CLASS.name = "CG Lieutenant Colonel"
CLASS.faction = FACTION_CG
CLASS.previousRank = "MAJ"
CLASS.previousRankLiteral = CLASS_CG_MAJOR
CLASS.currentRankFull = "Lieutenant Colonel" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/ltc.png"
CLASS.rank = 16
CLASS.currentRankAbv = "LTC"
CLASS.canPromote = true
CLASS.nextRank = "COL"
CLASS.nextRankLiteral = CLASS_CG_COLONEL
CLASS.model = "models/galactic/clones/cg/highofc.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_CG_LIEUTENANT_COLONEL = CLASS.index