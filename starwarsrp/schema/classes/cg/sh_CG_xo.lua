CLASS.name = "CG Executive Officer"
CLASS.faction = FACTION_CG
CLASS.previousRank = "COL"
CLASS.previousRankLiteral = CLASS_CG_COLONEL
CLASS.currentRankFull = "Executive Officer"  
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/cdr.png"
CLASS.rank = 18
CLASS.currentRankAbv = "XO"
CLASS.canPromote = true
CLASS.nextRank = "COMM"
CLASS.nextRankLiteral = CLASS_CG_COMMANDER
CLASS.model = "models/galactic/clones/cg/highofc.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_CG_XO = CLASS.index