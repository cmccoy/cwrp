CLASS.name = "CG Private"
CLASS.faction = FACTION_CG
CLASS.previousRank = nil;
CLASS.previousRankLiteral = nil
CLASS.currentRankFull = "Private" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/pvt.png"
CLASS.rank = 1
CLASS.currentRankLiteral = CLASS_CG_PRIVATE
CLASS.currentRankAbv = "PVT";
CLASS.nextRank = "PFC"
CLASS.nextRankLiteral = CLASS_CG_PRIVATEFIRSTCLASS
CLASS.model = "models/galactic/clones/cg/trooper.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_CG_PRIVATE = CLASS.index