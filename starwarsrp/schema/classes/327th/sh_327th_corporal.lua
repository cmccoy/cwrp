CLASS.name = "327th Corporal"
CLASS.faction = FACTION_327TH
CLASS.previousRank = "LCPL"
CLASS.previousRankLiteral = CLASS_327TH_LANCE_CORPORAL
CLASS.currentRankFull = "Corporal" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/1cpl.png"
CLASS.rank = 4
CLASS.currentRankAbv = "CPL"
CLASS.nextRank = "SGT"
CLASS.nextRankLiteral = CLASS_327TH_SERGEANT
CLASS.model = "models/galactic/clones/327/trooper.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_327TH_CORPORAL = CLASS.index