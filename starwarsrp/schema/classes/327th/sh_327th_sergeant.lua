CLASS.name = "327th Sergeant"
CLASS.faction = FACTION_327TH
CLASS.previousRank = "CPL"
CLASS.previousRankLiteral = CLASS_327TH_CORPORAL
CLASS.currentRankFull = "Sergant" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/sgt.png"
CLASS.rank = 5
CLASS.currentRankAbv = "SGT"
CLASS.nextRank = "SSGT"
CLASS.nextRankLiteral = CLASS_327TH_STAFF_SERGEANT
CLASS.model = "models/galactic/clones/327/nco.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_327TH_SERGEANT = CLASS.index