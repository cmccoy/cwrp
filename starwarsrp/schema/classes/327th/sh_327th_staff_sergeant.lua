CLASS.name = "327th Staff Sergeant"
CLASS.faction = FACTION_327TH
CLASS.previousRank = "SGT"
CLASS.previousRankLiteral = CLASS_327TH_SERGEANT
CLASS.currentRankFull = "Staff Sergeant" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/ssg.png"
CLASS.rank = 6
CLASS.currentRankAbv = "SSGT"
CLASS.nextRank = "SFC"
CLASS.nextRankLiteral = CLASS_327TH_SERGEANT_FIRST_CLASS
CLASS.model = "models/galactic/clones/327/nco.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_327TH_STAFF_SERGEANT = CLASS.index