CLASS.name = "327th Private First Class"
CLASS.faction = FACTION_327TH
CLASS.previousRank = "PVT";
CLASS.previousRankLiteral = CLASS_327TH_PRIVATE
CLASS.currentRankFull = "Private First Class" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/pfc.png"
CLASS.rank = 2
CLASS.currentRankAbv = "PFC";
CLASS.nextRank = "LCPL"
CLASS.nextRankLiteral = CLASS_327TH_LANCE_CORPORAL
CLASS.model = "models/galactic/clones/327/trooper.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_327TH_PRIVATEFIRSTCLASS = CLASS.index