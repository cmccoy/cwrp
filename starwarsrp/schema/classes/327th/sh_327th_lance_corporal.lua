CLASS.name = "327th Lance Corporal"
CLASS.faction = FACTION_327TH
CLASS.previousRank = "PFC"
CLASS.previousRankLiteral = CLASS_327TH_PRIVATEFIRSTCLASS
CLASS.currentRankFull = "Lance Corporal" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/cpl.png"
CLASS.rank = 3
CLASS.currentRankAbv = "LCPL"
CLASS.nextRank = "CPL"
CLASS.nextRankLiteral = CLASS_327TH_CORPORAL
CLASS.model = "models/galactic/clones/327/trooper.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_327TH_LANCE_CORPORAL = CLASS.index