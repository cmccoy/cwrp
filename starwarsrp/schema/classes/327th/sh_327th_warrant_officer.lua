CLASS.name = "327th Warrant Officer"
CLASS.faction = FACTION_327TH
CLASS.previousRank = "SGM"
CLASS.previousRankLiteral = CLASS_327TH_SERGEANT_MAJOR
CLASS.currentRankFull = "Warrant Officer" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/oc.png"
CLASS.rank = 11
CLASS.currentRankAbv = "WO"
CLASS.nextRank = "2LT"
CLASS.nextRankLiteral = CLASS_327TH_SECOND_LIEUTENANT
CLASS.model = "models/galactic/clones/327/nco.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_327TH_WARRANT_OFFICER = CLASS.index