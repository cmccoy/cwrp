CLASS.name = "327th Commander"
CLASS.faction = FACTION_327TH
CLASS.previousRank = "XO"
CLASS.previousRankLiteral = CLASS_327TH_XO
CLASS.currentRankFull = "Commander" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/rcdr.png"
CLASS.rank = 19
CLASS.currentRankAbv = "CMDR"
CLASS.canPromote = true
CLASS.nextRank = nil
CLASS.nextRankLiteral = nil
CLASS.model = "models/galactic/clones/327/cdr.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_327TH_COMMANDER = CLASS.index