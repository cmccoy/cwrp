CLASS.name = "327th Sergeant Major"
CLASS.faction = FACTION_327TH
CLASS.previousRank = "1SGT"
CLASS.previousRankLiteral = CLASS_327TH_FIRST_SERGEANT
CLASS.currentRankFull = "Sergeant Major" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/sgm.png"
CLASS.rank = 10
CLASS.currentRankAbv = "SGM"
CLASS.nextRank = "WO"
CLASS.nextRankLiteral = CLASS_327TH_WARRANT_OFFICER
CLASS.model = "models/galactic/clones/327/highofc.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_327TH_SERGEANT_MAJOR = CLASS.index