CLASS.name = "327th Executive Officer"
CLASS.faction = FACTION_327TH
CLASS.previousRank = "COL"
CLASS.previousRankLiteral = CLASS_327TH_COLONEL
CLASS.currentRankFull = "Executive Officer"  
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/cdr.png"
CLASS.rank = 18
CLASS.currentRankAbv = "XO"
CLASS.canPromote = true
CLASS.nextRank = "COMM"
CLASS.nextRankLiteral = CLASS_327TH_COMMANDER
CLASS.model = "models/galactic/clones/327/highofc.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_327TH_XO = CLASS.index