CLASS.name = "327th Captain"
CLASS.faction = FACTION_327TH
CLASS.previousRank = "1LT"
CLASS.previousRankLiteral = CLASS_327TH_FIRST_LIEUTENANT
CLASS.currentRankFull = "Captain" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/cpt.png"
CLASS.rank = 14
CLASS.currentRankAbv = "CPT"
CLASS.canPromote = true
CLASS.nextRank = "MAJ"
CLASS.nextRankLiteral = CLASS_327TH_MAJOR
CLASS.model = "models/galactic/clones/327/lowofc.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_327TH_CAPTAIN = CLASS.index