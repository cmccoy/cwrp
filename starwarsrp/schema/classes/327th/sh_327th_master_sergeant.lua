CLASS.name = "327th Master Sergeant"
CLASS.faction = FACTION_327TH
CLASS.previousRank = "SFC"
CLASS.previousRankLiteral = CLASS_327TH_SERGEANT_FIRST_CLASS
CLASS.currentRankFull = "Master Sergeant" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/msg.png"
CLASS.rank = 8
CLASS.currentRankAbv = "MSGT"
CLASS.nextRank = "1SGT"
CLASS.nextRankLiteral = CLASS_327TH_FIRST_SERGEANT
CLASS.model = "models/galactic/clones/327/nco.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_327TH_MASTER_SERGEANT = CLASS.index