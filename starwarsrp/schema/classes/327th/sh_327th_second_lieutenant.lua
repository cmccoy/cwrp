CLASS.name = "327th 2nd Lieutenant"
CLASS.faction = FACTION_327TH
CLASS.previousRank = "WO"
CLASS.previousRankLiteral = CLASS_327TH_WARRANT_OFFICER
CLASS.currentRankFull = "2nd Lieutenant" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/2lt.png"
CLASS.rank = 12
CLASS.currentRankAbv = "2LT"
CLASS.canPromote = true
CLASS.nextRank = "1LT"
CLASS.nextRankLiteral = CLASS_327TH_FIRST_LIEUTENANT
CLASS.model = "models/galactic/clones/327/lowofc.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_327TH_SECOND_LIEUTENANT = CLASS.index