CLASS.name = "41st Staff Sergeant"
CLASS.faction = FACTION_41ST
CLASS.previousRank = "SGT"
CLASS.previousRankLiteral = CLASS_41ST_SERGEANT
CLASS.currentRankFull = "Staff Sergeant"
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/ssg.png"
CLASS.rank = 6
CLASS.currentRankAbv = "SSGT"
CLASS.nextRank = "SFC"
CLASS.nextRankLiteral = CLASS_41ST_SERGEANT_FIRST_CLASS
CLASS.model = "models/galactic/clones/41/nco.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_41ST_STAFF_SERGEANT = CLASS.index