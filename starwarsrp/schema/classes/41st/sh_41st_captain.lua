CLASS.name = "41st Captain"
CLASS.faction = FACTION_41ST
CLASS.previousRank = "1LT"
CLASS.previousRankLiteral = CLASS_41ST_FIRST_LIEUTENANT
CLASS.currentRankFull = "Captain" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/cpt.png"
CLASS.rank = 14
CLASS.currentRankAbv = "CPT"
CLASS.canPromote = true
CLASS.nextRank = "MAJ"
CLASS.nextRankLiteral = CLASS_41ST_MAJOR
CLASS.model = "models/galactic/clones/41/lowofc.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_41ST_CAPTAIN = CLASS.index