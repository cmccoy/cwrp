CLASS.name = "41st Commander"
CLASS.faction = FACTION_41ST
CLASS.previousRank = "XO"
CLASS.previousRankLiteral = CLASS_41ST_XO
CLASS.currentRankFull = "Commander" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/rcdr.png"
CLASS.rank = 19
CLASS.currentRankAbv = "CMDR"
CLASS.canPromote = true
CLASS.nextRank = nil
CLASS.nextRankLiteral = nil
CLASS.model = "models/galactic/clones/41gc/highofc.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_41ST_COMMANDER = CLASS.index