CLASS.name = "41st Sergeant"
CLASS.faction = FACTION_41ST
CLASS.previousRank = "CPL"
CLASS.previousRankLiteral = CLASS_41ST_CORPORAL
CLASS.currentRankFull = "Sergant" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/sgt.png"
CLASS.rank = 5
CLASS.currentRankAbv = "SGT"
CLASS.nextRank = "SSGT"
CLASS.nextRankLiteral = CLASS_41ST_STAFF_SERGEANT
CLASS.model = "models/galactic/clones/41/nco.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_41ST_SERGEANT = CLASS.index