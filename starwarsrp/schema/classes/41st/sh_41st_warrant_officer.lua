CLASS.name = "41st Warrant Officer"
CLASS.faction = FACTION_41ST
CLASS.previousRank = "SGM"
CLASS.previousRankLiteral = CLASS_41ST_SERGEANT_MAJOR
CLASS.currentRankFull = "Warrant Officer" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/oc.png"
CLASS.rank = 11
CLASS.currentRankAbv = "WO"
CLASS.nextRank = "2LT"
CLASS.nextRankLiteral = CLASS_41ST_SECOND_LIEUTENANT
CLASS.model = "models/galactic/clones/41/nco.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_41ST_WARRANT_OFFICER = CLASS.index