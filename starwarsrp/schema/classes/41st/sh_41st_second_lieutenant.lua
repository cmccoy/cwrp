CLASS.name = "41st 2nd Lieutenant"
CLASS.faction = FACTION_41ST
CLASS.previousRank = "WO"
CLASS.previousRankLiteral = CLASS_41ST_WARRANT_OFFICER
CLASS.currentRankFull = "2nd Lieutenant" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/2lt.png"
CLASS.rank = 12
CLASS.currentRankAbv = "2LT"
CLASS.canPromote = true
CLASS.nextRank = "1LT"
CLASS.nextRankLiteral = CLASS_41ST_FIRST_LIEUTENANT
CLASS.model = "models/galactic/clones/41/lowofc.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_41ST_SECOND_LIEUTENANT = CLASS.index