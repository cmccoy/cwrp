CLASS.name = "41st Sergeant First CLASS"
CLASS.faction = FACTION_41ST
CLASS.previousRank = "SSGT"
CLASS.previousRankLiteral = CLASS_41ST_STAFF_SERGEANT
CLASS.currentRankFull = "Sergeant First Class" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/sfc.png"
CLASS.rank = 7
CLASS.currentRankAbv = "SFC"
CLASS.nextRank = "MSGT"
CLASS.nextRankLiteral = CLASS_41ST_MASTER_SERGEANT
CLASS.model = "models/galactic/clones/41/nco.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_41ST_SERGEANT_FIRST_CLASS = CLASS.index