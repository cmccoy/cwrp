CLASS.name = "41st Lieutenant Colonel"
CLASS.faction = FACTION_41ST
CLASS.previousRank = "MAJ"
CLASS.previousRankLiteral = CLASS_41ST_MAJOR
CLASS.currentRankFull = "Lieutenant Colonel" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/ltc.png"
CLASS.rank = 16
CLASS.currentRankAbv = "LTC"
CLASS.canPromote = true
CLASS.nextRank = "COL"
CLASS.nextRankLiteral = CLASS_41ST_COLONEL
CLASS.model = "models/galactic/clones/41/highofc.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_41ST_LIEUTENANT_COLONEL = CLASS.index