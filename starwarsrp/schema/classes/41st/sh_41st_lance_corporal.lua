CLASS.name = "41st Lance Corporal"
CLASS.faction = FACTION_41ST
CLASS.previousRank = "PFC"
CLASS.previousRankLiteral = CLASS_41ST_PRIVATEFIRSTCLASS
CLASS.currentRankFull = "Lance Corporal" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/cpl.png"
CLASS.rank = 3
CLASS.currentRankAbv = "LCPL"
CLASS.nextRank = "CPL"
CLASS.nextRankLiteral = CLASS_41ST_CORPORAL
CLASS.model = "models/galactic/clones/41/trooper.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_41ST_LANCE_CORPORAL = CLASS.index