CLASS.name = "41st Master Sergeant"
CLASS.faction = FACTION_41ST
CLASS.previousRank = "SFC"
CLASS.previousRankLiteral = CLASS_41ST_SERGEANT_FIRST_CLASS
CLASS.currentRankFull = "Master Sergeant" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/msg.png"
CLASS.rank = 8
CLASS.currentRankAbv = "MSGT"
CLASS.nextRank = "1SGT"
CLASS.nextRankLiteral = CLASS_41ST_FIRST_SERGEANT
CLASS.model = "models/galactic/clones/41/nco.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_41ST_MASTER_SERGEANT = CLASS.index