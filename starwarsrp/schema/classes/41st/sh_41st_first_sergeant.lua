CLASS.name = "41st First Sergeant"
CLASS.faction = FACTION_41ST
CLASS.previousRank = "MSGT"
CLASS.previousRankLiteral = CLASS_41ST_MASTER_SERGEANT
CLASS.currentRankFull = "First Sergeant"
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/1sg.png"
CLASS.rank = 9
CLASS.currentRankAbv = "1SGT"
CLASS.nextRank = "SGM"
CLASS.nextRankLiteral = CLASS_41ST_SERGEANT_MAJOR
CLASS.model = "models/galactic/clones/41/nco.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_41ST_FIRST_SERGEANT = CLASS.index