CLASS.name = "41st Private First Class"
CLASS.faction = FACTION_41ST
CLASS.previousRank = "PVT";
CLASS.previousRankLiteral = CLASS_41ST_PRIVATE
CLASS.currentRankFull = "Private First Class" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/pfc.png"
CLASS.rank = 2
CLASS.currentRankAbv = "PFC";
CLASS.nextRank = "LCPL"
CLASS.nextRankLiteral = CLASS_41ST_LANCE_CORPORAL
CLASS.model = "models/galactic/clones/41/trooper.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_41ST_PRIVATEFIRSTCLASS = CLASS.index