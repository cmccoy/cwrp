CLASS.name = "41st Executive Officer"
CLASS.faction = FACTION_41ST
CLASS.previousRank = "COL"
CLASS.previousRankLiteral = CLASS_41ST_COLONEL
CLASS.currentRankFull = "Executive Officer"  
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/cdr.png"
CLASS.rank = 18
CLASS.currentRankAbv = "XO"
CLASS.canPromote = true
CLASS.nextRank = "COMM"
CLASS.nextRankLiteral = CLASS_41ST_COMMANDER
CLASS.model = "models/galactic/clones/41/highofc.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_41ST_XO = CLASS.index