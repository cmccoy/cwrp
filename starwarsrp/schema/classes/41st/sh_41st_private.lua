CLASS.name = "41st Private"
CLASS.faction = FACTION_41ST
CLASS.previousRank = nil;
CLASS.previousRankLiteral = nil
CLASS.currentRankFull = "Private" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/pvt.png"
CLASS.rank = 1
CLASS.currentRankLiteral = CLASS_41ST_PRIVATE
CLASS.currentRankAbv = "PVT";
CLASS.nextRank = "PFC"
CLASS.nextRankLiteral = CLASS_41ST_PRIVATEFIRSTCLASS
CLASS.model = "models/galactic/clones/41/trooper.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_41ST_PRIVATE = CLASS.index