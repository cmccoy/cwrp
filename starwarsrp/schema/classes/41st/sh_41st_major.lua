CLASS.name = "41st Major"
CLASS.faction = FACTION_41ST
CLASS.previousRank = "CPT"
CLASS.previousRankLiteral = CLASS_41ST_CAPTAIN
CLASS.currentRankFull = "Major" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/maj.png"
CLASS.rank = 15
CLASS.currentRankAbv = "MAJ"
CLASS.canPromote = true
CLASS.nextRank = "LTC"
CLASS.nextRankLiteral = CLASS_41ST_LIEUTENANT_COLONEL
CLASS.model = "models/galactic/clones/41/highofc.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_41ST_MAJOR = CLASS.index