CLASS.name = "41st Corporal"
CLASS.faction = FACTION_41ST
CLASS.previousRank = "LCPL"
CLASS.previousRankLiteral = CLASS_41ST_LANCE_CORPORAL
CLASS.currentRankFull = "Corporal" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/1cpl.png"
CLASS.rank = 4
CLASS.currentRankAbv = "CPL"
CLASS.nextRank = "SGT"
CLASS.nextRankLiteral = CLASS_41ST_SERGEANT
CLASS.model = "models/galactic/clones/41/trooper.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_41ST_CORPORAL = CLASS.index