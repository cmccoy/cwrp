CLASS.name = "Medical Chief"
CLASS.faction = FACTION_MEDICAL
CLASS.previousRank = "Chief"
CLASS.previousRankLiteral = FACTION_MEDICAL
CLASS.currentRankFull = "Chief"
CLASS.rank = 1
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/rcdr.png"
CLASS.currentRankAbv = "Chief"
CLASS.canPromote = true
CLASS.nextRank = nil
CLASS.nextRankLiteral = nil
CLASS.model = "models/galactic/clones/rm/cdr.mdl"

function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end

CLASS_MEDICAL = CLASS.index