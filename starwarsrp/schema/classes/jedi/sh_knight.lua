CLASS.name = "Jedi Knight"
CLASS.faction = FACTION_JEDI
CLASS.previousRank = "Padawan"
CLASS.previousRankLiteral = CLASS_JEDI_PADAWAN
CLASS.currentRankFull = "Knight"
CLASS.rank = 3
CLASS.rankLogo = "materials/ig_cwrp/ranks/jedi/knight.png"
CLASS.currentRankAbv = "Knight"
CLASS.nextRank = "Master"
CLASS.nextRankLiteral = CLASS_JEDI_MASTER
CLASS.model = "models/grealms/characters/jedirobes/jedirobes_02.mdl"
function CLASS:OnSpawn(client)
end

CLASS_JEDI_KNIGHT = CLASS.index