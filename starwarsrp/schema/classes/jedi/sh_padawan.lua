CLASS.name = "Jedi Padawan"
CLASS.faction = FACTION_JEDI
CLASS.previousRank = "Initiate"
CLASS.previousRankLiteral = CLASS_JEDI_INITIATE
CLASS.currentRankFull = "Padawan"
CLASS.rank = 2
CLASS.rankLogo = "materials/ig_cwrp/ranks/jedi/padawan.png"
CLASS.currentRankAbv = "Padawan"
CLASS.nextRank = "Knight"
CLASS.nextRankLiteral = CLASS_JEDI_KNIGHT
CLASS.model = "models/grealms/characters/casualjedi/casualjedi_07.mdl"
function CLASS:OnSpawn(client)
end

CLASS_JEDI_PADAWAN = CLASS.index