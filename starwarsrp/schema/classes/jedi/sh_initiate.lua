CLASS.name = "Jedi Initiate"
CLASS.faction = FACTION_JEDI
CLASS.previousRank = nil
CLASS.previousRankLiteral = nil
CLASS.currentRankFull = "Initiate"
CLASS.rankLogo = "materials/ig_cwrp/ranks/jedi/initiate.png"
CLASS.rank = 1
CLASS.currentRankAbv = "Initiate"
CLASS.nextRank = "Padawan"
CLASS.nextRankLiteral = CLASS_JEDI_PADAWAN
CLASS.model = "models/grealms/characters/casualjedi/casualjedi_07.mdl"
function CLASS:OnSpawn(client)
end

CLASS_JEDI_INITIATE = CLASS.index