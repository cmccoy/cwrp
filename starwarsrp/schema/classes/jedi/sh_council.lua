CLASS.name = "Jedi Council"
CLASS.faction = FACTION_JEDI
CLASS.previousRank = "Master"
CLASS.previousRankLiteral = CLASS_JEDI_MASTER
CLASS.currentRankFull = "Council"
CLASS.rank = 5
CLASS.rankLogo = "materials/ig_cwrp/ranks/jedi/master.png"
CLASS.currentRankAbv = "Council"
CLASS.canPromote = true
CLASS.nextRank = 6
CLASS.nextRankLiteral = CLASS_JEDI_MASTER_OF_THE_ORDER
CLASS.model = "models/grealms/characters/jedibattlelord/jedibattlelord.mdl"
function CLASS:OnSpawn(client)
end

CLASS_JEDI_COUNCIL = CLASS.index