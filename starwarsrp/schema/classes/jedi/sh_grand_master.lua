CLASS.name = "Grand Master of the Order"
CLASS.faction = FACTION_JEDI
CLASS.previousRank = "Council"
CLASS.previousRankLiteral = CLASS_JEDI_COUNCIL
CLASS.currentRankFull = "MOTO"
CLASS.rank = 6
CLASS.rankLogo = "materials/ig_cwrp/ranks/jedi/master.png"
CLASS.currentRankAbv = "MOTO"
CLASS.canPromote = true
CLASS.nextRank = nil
CLASS.nextRankLiteral = nil
CLASS.model = "models/tobester/jedi/moto.mdl"
function CLASS:OnSpawn(client)
end

CLASS_JEDI_MASTER_OF_THE_ORDER = CLASS.index