CLASS.name = "Jedi Master"
CLASS.faction = FACTION_JEDI
CLASS.previousRank = "Knight"
CLASS.previousRankLiteral = CLASS_JEDI_KNIGHT
CLASS.currentRankFull = "Master"
CLASS.rank = 4
CLASS.rankLogo = "materials/ig_cwrp/ranks/jedi/master.png"
CLASS.currentRankAbv = "Master"
CLASS.canPromote = true
CLASS.nextRank = "Council"
CLASS.nextRankLiteral = CLASS_JEDI_COUNCIL
CLASS.model = "models/grealms/characters/jedibattlelord/jedibattlelord.mdl"
function CLASS:OnSpawn(client)
end

CLASS_JEDI_MASTER = CLASS.index