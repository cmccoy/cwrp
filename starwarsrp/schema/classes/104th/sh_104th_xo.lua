CLASS.name = "104th Executive Officer"
CLASS.faction = FACTION_104TH
CLASS.previousRank = "COL"
CLASS.previousRankLiteral = CLASS_104TH_COLONEL
CLASS.currentRankFull = "Executive Officer"  
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/cdr.png"
CLASS.rank = 18
CLASS.currentRankAbv = "XO"
CLASS.canPromote = true
CLASS.nextRank = "COMM"
CLASS.nextRankLiteral = CLASS_104TH_COMMANDER
CLASS.model = "models/galactic/clones/104/highofc.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_104TH_XO = CLASS.index