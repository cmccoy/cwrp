CLASS.name = "104th Lance Corporal"
CLASS.faction = FACTION_104TH
CLASS.previousRank = "PFC"
CLASS.previousRankLiteral = CLASS_104TH_PRIVATEFIRSTCLASS
CLASS.currentRankFull = "Lance Corporal" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/cpl.png"
CLASS.rank = 3
CLASS.currentRankAbv = "LCPL"
CLASS.nextRank = "CPL"
CLASS.nextRankLiteral = CLASS_104TH_CORPORAL
CLASS.model = "models/galactic/clones/104/trooper.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_104TH_LANCE_CORPORAL = CLASS.index