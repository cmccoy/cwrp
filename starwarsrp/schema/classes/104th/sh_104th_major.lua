CLASS.name = "104th Major"
CLASS.faction = FACTION_104TH
CLASS.previousRank = "CPT"
CLASS.previousRankLiteral = CLASS_104TH_CAPTAIN
CLASS.currentRankFull = "Major" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/maj.png"
CLASS.rank = 15
CLASS.currentRankAbv = "MAJ"
CLASS.canPromote = true
CLASS.nextRank = "LTC"
CLASS.nextRankLiteral = CLASS_104TH_LIEUTENANT_COLONEL
CLASS.model = "models/galactic/clones/104/highofc.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_104TH_MAJOR = CLASS.index