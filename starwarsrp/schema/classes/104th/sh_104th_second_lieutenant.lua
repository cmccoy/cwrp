CLASS.name = "104th 2nd Lieutenant"
CLASS.faction = FACTION_104TH
CLASS.previousRank = "WO"
CLASS.previousRankLiteral = CLASS_104TH_WARRANT_OFFICER
CLASS.currentRankFull = "2nd Lieutenant" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/2lt.png"
CLASS.rank = 12
CLASS.currentRankAbv = "2LT"
CLASS.canPromote = true
CLASS.nextRank = "1LT"
CLASS.nextRankLiteral = CLASS_104TH_FIRST_LIEUTENANT
CLASS.model = "models/galactic/clones/104/lowofc.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_104TH_SECOND_LIEUTENANT = CLASS.index