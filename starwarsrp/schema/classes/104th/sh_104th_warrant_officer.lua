CLASS.name = "104th Warrant Officer"
CLASS.faction = FACTION_104TH
CLASS.previousRank = "SGM"
CLASS.previousRankLiteral = CLASS_104TH_SERGEANT_MAJOR
CLASS.currentRankFull = "Warrant Officer" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/oc.png"
CLASS.rank = 11
CLASS.currentRankAbv = "WO"
CLASS.nextRank = "2LT"
CLASS.nextRankLiteral = CLASS_104TH_SECOND_LIEUTENANT
CLASS.model = "models/galactic/clones/104/nco.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_104TH_WARRANT_OFFICER = CLASS.index