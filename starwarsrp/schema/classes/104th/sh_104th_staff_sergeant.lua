CLASS.name = "104th Staff Sergeant"
CLASS.faction = FACTION_104TH
CLASS.previousRank = "SGT"
CLASS.previousRankLiteral = CLASS_104TH_SERGEANT
CLASS.currentRankFull = "Staff Sergeant" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/ssg.png"
CLASS.rank = 6
CLASS.currentRankAbv = "SSGT"
CLASS.nextRank = "SFC"
CLASS.nextRankLiteral = CLASS_104TH_SERGEANT_FIRST_CLASS
CLASS.model = "models/galactic/clones/104/nco.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_104TH_STAFF_SERGEANT = CLASS.index