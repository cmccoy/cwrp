CLASS.name = "104th Sergeant First CLASS"
CLASS.faction = FACTION_104TH
CLASS.previousRank = "SSGT"
CLASS.previousRankLiteral = CLASS_104TH_STAFF_SERGEANT
CLASS.currentRankFull = "Sergeant First Class" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/sfc.png"
CLASS.rank = 7
CLASS.currentRankAbv = "SFC"
CLASS.nextRank = "MSGT"
CLASS.nextRankLiteral = CLASS_104TH_MASTER_SERGEANT
CLASS.model = "models/galactic/clones/104/nco.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_104TH_SERGEANT_FIRST_CLASS = CLASS.index