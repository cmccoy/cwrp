CLASS.name = "104th Private"
CLASS.faction = FACTION_104TH
CLASS.previousRank = nil;
CLASS.previousRankLiteral = nil
CLASS.currentRankFull = "Private" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/pvt.png"
CLASS.rank = 1
CLASS.currentRankLiteral = CLASS_104TH_PRIVATE
CLASS.currentRankAbv = "PVT";
CLASS.nextRank = "PFC"
CLASS.nextRankLiteral = CLASS_104TH_PRIVATEFIRSTCLASS
CLASS.model = "models/galactic/clones/104/trooper.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_104TH_PRIVATE = CLASS.index