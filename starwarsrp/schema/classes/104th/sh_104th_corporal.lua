CLASS.name = "104th Corporal"
CLASS.faction = FACTION_104TH
CLASS.previousRank = "LCPL"
CLASS.previousRankLiteral = CLASS_104TH_LANCE_CORPORAL
CLASS.currentRankFull = "Corporal" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/1cpl.png"
CLASS.rank = 4
CLASS.currentRankAbv = "CPL"
CLASS.nextRank = "SGT"
CLASS.nextRankLiteral = CLASS_104TH_SERGEANT
CLASS.model = "models/galactic/clones/104/trooper.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_104TH_CORPORAL = CLASS.index