CLASS.name = "104th First Sergeant"
CLASS.faction = FACTION_104TH
CLASS.previousRank = "MSGT"
CLASS.previousRankLiteral = CLASS_104TH_MASTER_SERGEANT
CLASS.currentRankFull = "First Sergeant" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/1sg.png"
CLASS.rank = 9
CLASS.currentRankAbv = "1SGT"
CLASS.nextRank = "SGM"
CLASS.nextRankLiteral = CLASS_104TH_SERGEANT_MAJOR
CLASS.model = "models/galactic/clones/104/nco.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_104TH_FIRST_SERGEANT = CLASS.index