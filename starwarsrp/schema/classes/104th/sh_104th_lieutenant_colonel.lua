CLASS.name = "104th Lieutenant Colonel"
CLASS.faction = FACTION_104TH
CLASS.previousRank = "MAJ"
CLASS.previousRankLiteral = CLASS_104TH_MAJOR
CLASS.currentRankFull = "Lieutenant Colonel" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/ltc.png"
CLASS.rank = 16
CLASS.currentRankAbv = "LTC"
CLASS.canPromote = true
CLASS.nextRank = "COL"
CLASS.nextRankLiteral = CLASS_104TH_COLONEL
CLASS.model = "models/galactic/clones/104/highofc.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_104TH_LIEUTENANT_COLONEL = CLASS.index