CLASS.name = "104th Commander"
CLASS.faction = FACTION_104TH
CLASS.previousRank = "XO"
CLASS.previousRankLiteral = CLASS_104TH_XO
CLASS.currentRankFull = "Commander" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/rcdr.png"
CLASS.rank = 19
CLASS.currentRankAbv = "CMDR"
CLASS.canPromote = true
CLASS.nextRank = nil
CLASS.nextRankLiteral = nil
CLASS.model = "models/galactic/clones/104/cdr.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_104TH_COMMANDER = CLASS.index