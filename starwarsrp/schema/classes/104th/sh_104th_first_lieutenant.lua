CLASS.name = "104th 1st Lieutenant"
CLASS.faction = FACTION_104TH
CLASS.previousRank = "2LT"
CLASS.previousRankLiteral = CLASS_104TH_SECOND_LIEUTENANT
CLASS.currentRankFull = "1st Lieutenant" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/lt.png"
CLASS.rank = 13
CLASS.currentRankAbv = "1LT"
CLASS.canPromote = true
CLASS.nextRank = "CPT"
CLASS.nextRankLiteral = CLASS_104TH_CAPTAIN
CLASS.model = "models/galactic/clones/104/lowofc.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_104TH_FIRST_LIEUTENANT = CLASS.index