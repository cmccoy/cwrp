CLASS.name = "104th Captain"
CLASS.faction = FACTION_104TH
CLASS.previousRank = "1LT"
CLASS.previousRankLiteral = CLASS_104TH_FIRST_LIEUTENANT
CLASS.currentRankFull = "Captain" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/cpt.png"
CLASS.rank = 14
CLASS.currentRankAbv = "CPT"
CLASS.canPromote = true
CLASS.nextRank = "MAJ"
CLASS.nextRankLiteral = CLASS_104TH_MAJOR
CLASS.model = "models/galactic/clones/104/lowofc.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_104TH_CAPTAIN = CLASS.index