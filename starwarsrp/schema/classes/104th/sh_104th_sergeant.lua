CLASS.name = "104th Sergeant"
CLASS.faction = FACTION_104TH
CLASS.previousRank = "CPL"
CLASS.previousRankLiteral = CLASS_104TH_CORPORAL
CLASS.currentRankFull = "Sergant" 
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/sgt.png"
CLASS.rank = 5
CLASS.currentRankAbv = "SGT"
CLASS.nextRank = "SSGT"
CLASS.nextRankLiteral = CLASS_104TH_STAFF_SERGEANT
CLASS.model = "models/galactic/clones/104/nco.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_104TH_SERGEANT = CLASS.index