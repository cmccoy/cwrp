CLASS.name = "501st Warrant Officer"
CLASS.faction = FACTION_501ST
CLASS.previousRank = "SGM"
CLASS.previousRankLiteral = CLASS_501ST_SERGEANT_MAJOR
CLASS.currentRankFull = "Warrant Officer"
CLASS.rank = 11
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/oc.png"
CLASS.currentRankAbv = "WO"
CLASS.nextRank = "2LT"
CLASS.nextRankLiteral = CLASS_501ST_SECOND_LIEUTENANT
CLASS.model = "models/galactic/clones/501tc/nco.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_501ST_WARRANT_OFFICER = CLASS.index