CLASS.name = "501st Sergeant First Class"
CLASS.faction = FACTION_501ST
CLASS.previousRank = "SSGT"
CLASS.previousRankLiteral = CLASS_501ST_STAFF_SERGEANT
CLASS.currentRankFull = "Sergeant First Class"
CLASS.rank = 7
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/sfc.png"
CLASS.currentRankAbv = "SFC"
CLASS.nextRank = "MSGT"
CLASS.nextRankLiteral = CLASS_501ST_MASTER_SERGEANT
CLASS.model = "models/galactic/clones/501tc/nco.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_501ST_SERGEANT_FIRST_CLASS = CLASS.index