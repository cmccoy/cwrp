CLASS.name = "501st Private First Class"
CLASS.faction = FACTION_501ST
CLASS.previousRank = "PVT";
CLASS.previousRankLiteral = CLASS_501ST_PRIVATE
CLASS.currentRankFull = "Private First Class"
CLASS.rank = 2
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/pfc.png"
CLASS.currentRankAbv = "PFC";
CLASS.nextRank = "LCPL"
CLASS.nextRankLiteral = CLASS_501ST_LANCE_CORPORAL
CLASS.model = "models/galactic/clones/501/trooper.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_501ST_PRIVATEFIRSTCLASS = CLASS.index