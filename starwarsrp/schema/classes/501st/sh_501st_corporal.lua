CLASS.name = "501st Corporal"
CLASS.faction = FACTION_501ST
CLASS.previousRank = "LCPL"
CLASS.previousRankLiteral = CLASS_501ST_LANCE_CORPORAL
CLASS.currentRankFull = "Corporal"
CLASS.rank = 4
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/1cpl.png"
CLASS.currentRankAbv = "CPL"
CLASS.nextRank = "SGT"
CLASS.nextRankLiteral = CLASS_501ST_SERGEANT
CLASS.model = "models/galactic/clones/501/trooper.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_501ST_CORPORAL = CLASS.index