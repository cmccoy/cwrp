CLASS.name = "501st Major"
CLASS.faction = FACTION_501ST
CLASS.previousRank = "CPT"
CLASS.previousRankLiteral = CLASS_501ST_CAPTAIN
CLASS.currentRankFull = "Major"
CLASS.rank = 15
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/maj.png"
CLASS.currentRankAbv = "MAJ"
CLASS.canPromote = true
CLASS.nextRank = "LTC"
CLASS.nextRankLiteral = CLASS_501ST_LIEUTENANT_COLONEL
CLASS.model = "models/galactic/clones/501tc/lowofc.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_501ST_MAJOR = CLASS.index