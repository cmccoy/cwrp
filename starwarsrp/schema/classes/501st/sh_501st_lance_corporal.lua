CLASS.name = "501st Lance Corporal"
CLASS.faction = FACTION_501ST
CLASS.previousRank = "PFC"
CLASS.previousRankLiteral = CLASS_501ST_PRIVATEFIRSTCLASS
CLASS.currentRankFull = "Lance Corporal"
CLASS.rank = 3
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/cpl.png"
CLASS.currentRankAbv = "LCPL"
CLASS.nextRank = "CPL"
CLASS.nextRankLiteral = CLASS_501ST_CORPORAL
CLASS.model = "models/galactic/clones/501/trooper.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_501ST_LANCE_CORPORAL = CLASS.index