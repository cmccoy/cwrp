CLASS.name = "501st Staff Sergeant"
CLASS.faction = FACTION_501ST
CLASS.previousRank = "SGT"
CLASS.previousRankLiteral = CLASS_501ST_SERGEANT
CLASS.currentRankFull = "Staff Sergeant"
CLASS.rank = 6
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/ssg.png"
CLASS.currentRankAbv = "SSGT"
CLASS.nextRank = "SFC"
CLASS.nextRankLiteral = CLASS_501ST_SERGEANT_FIRST_CLASS
CLASS.model = "models/galactic/clones/501tc/nco.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_501ST_STAFF_SERGEANT = CLASS.index