CLASS.name = "501st Commander"
CLASS.faction = FACTION_501ST
CLASS.previousRank = "XO"
CLASS.previousRankLiteral = CLASS_501ST_XO
CLASS.currentRankFull = "Commander"
CLASS.rank = 19
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/rcdr.png"
CLASS.currentRankAbv = "CMDR"
CLASS.canPromote = true
CLASS.nextRank = nil
CLASS.nextRankLiteral = nil
CLASS.model = "models/galactic/clones/501/cdr.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_501ST_COMMANDER = CLASS.index