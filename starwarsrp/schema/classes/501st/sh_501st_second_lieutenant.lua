CLASS.name = "501st 2nd Lieutenant"
CLASS.faction = FACTION_501ST
CLASS.previousRank = "WO"
CLASS.previousRankLiteral = CLASS_501ST_WARRANT_OFFICER
CLASS.currentRankFull = "2nd Lieutenant"
CLASS.rank = 12
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/2lt.png"
CLASS.currentRankAbv = "2LT"
CLASS.canPromote = true
CLASS.nextRank = "1LT"
CLASS.nextRankLiteral = CLASS_501ST_FIRST_LIEUTENANT
CLASS.model = "models/galactic/clones/501tc/lowofc.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_501ST_SECOND_LIEUTENANT = CLASS.index