CLASS.name = "501st Master Sergeant"
CLASS.faction = FACTION_501ST
CLASS.previousRank = "SFC"
CLASS.previousRankLiteral = CLASS_501ST_SERGEANT_FIRST_CLASS
CLASS.currentRankFull = "Master Sergeant"
CLASS.rank = 8
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/msg.png"
CLASS.currentRankAbv = "MSGT"
CLASS.nextRank = "1SGT"
CLASS.nextRankLiteral = CLASS_501ST_FIRST_SERGEANT
CLASS.model = "models/galactic/clones/501tc/nco.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_501ST_MASTER_SERGEANT = CLASS.index