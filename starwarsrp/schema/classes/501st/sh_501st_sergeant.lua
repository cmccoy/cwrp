CLASS.name = "501st Sergeant"
CLASS.faction = FACTION_501ST
CLASS.previousRank = "CPL"
CLASS.previousRankLiteral = CLASS_501ST_CORPORAL
CLASS.currentRankFull = "Sergant"
CLASS.rank = 5
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/sgt.png"
CLASS.currentRankAbv = "SGT"
CLASS.nextRank = "SSGT"
CLASS.nextRankLiteral = CLASS_501ST_STAFF_SERGEANT
CLASS.model = "models/galactic/clones/501tc/nco.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_501ST_SERGEANT = CLASS.index