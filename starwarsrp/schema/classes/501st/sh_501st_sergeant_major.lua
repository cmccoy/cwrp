CLASS.name = "501st Sergeant Major"
CLASS.faction = FACTION_501ST
CLASS.previousRank = "1SGT"
CLASS.previousRankLiteral = CLASS_501ST_FIRST_SERGEANT
CLASS.currentRankFull = "Sergeant Major"
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/sgm.png"
CLASS.rank = 10
CLASS.currentRankAbv = "SGM"
CLASS.nextRank = "WO"
CLASS.nextRankLiteral = CLASS_501ST_WARRANT_OFFICER
CLASS.model = "models/galactic/clones/501tc/nco.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_501ST_SERGEANT_MAJOR = CLASS.index