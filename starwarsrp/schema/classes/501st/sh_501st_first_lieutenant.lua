CLASS.name = "501st 1st Lieutenant"
CLASS.faction = FACTION_501ST
CLASS.previousRank = "2LT"
CLASS.previousRankLiteral = CLASS_501ST_SECOND_LIEUTENANT
CLASS.currentRankFull = "1st Lieutenant"
CLASS.rank = 13
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/lt.png"
CLASS.currentRankAbv = "1LT"
CLASS.canPromote = true
CLASS.nextRank = "CPT"
CLASS.nextRankLiteral = CLASS_501ST_CAPTAIN
CLASS.model = "models/galactic/clones/501tc/lowofc.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_501ST_FIRST_LIEUTENANT = CLASS.index