CLASS.name = "501st Captain"
CLASS.faction = FACTION_501ST
CLASS.previousRank = "1LT"
CLASS.previousRankLiteral = CLASS_501ST_FIRST_LIEUTENANT
CLASS.currentRankFull = "Captain"
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/cpt.png"
CLASS.rank = 14
CLASS.currentRankAbv = "CPT"
CLASS.canPromote = true
CLASS.nextRank = "MAJ"
CLASS.nextRankLiteral = CLASS_501ST_MAJOR
CLASS.model = "models/galactic/clones/501tc/lowofc.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_501ST_CAPTAIN = CLASS.index