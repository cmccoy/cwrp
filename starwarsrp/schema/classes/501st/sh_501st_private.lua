CLASS.name = "501st Private"
CLASS.faction = FACTION_501ST
CLASS.previousRank = nil;
CLASS.previousRankLiteral = nil
CLASS.currentRankFull = "Private"
CLASS.rank = 1
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/pvt.png"
CLASS.model = "models/galactic/clones/501/trooper.mdl"
CLASS.currentRankLiteral = CLASS_501ST_PRIVATE
CLASS.currentRankAbv = "PVT";
CLASS.nextRank = "PFC"
CLASS.nextRankLiteral = CLASS_501ST_PRIVATEFIRSTCLASS
CLASS.model = "models/galactic/clones/501/trooper.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_501ST_PRIVATE = CLASS.index