CLASS.name = "501st Executive Officer"
CLASS.faction = FACTION_501ST
CLASS.previousRank = "COL"
CLASS.previousRankLiteral = CLASS_501ST_COLONEL
CLASS.currentRankFull = "Executive Officer"
CLASS.rank = 18
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/cdr.png"
CLASS.currentRankAbv = "XO"
CLASS.canPromote = true
CLASS.nextRank = "COMM"
CLASS.nextRankLiteral = CLASS_501ST_COMMANDER
CLASS.model = "models/galactic/clones/501tc/highofc.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_501ST_XO = CLASS.index