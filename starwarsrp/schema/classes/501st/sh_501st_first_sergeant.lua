CLASS.name = "501st First Sergeant"
CLASS.faction = FACTION_501ST
CLASS.previousRank = "MSGT"
CLASS.previousRankLiteral = CLASS_501ST_MASTER_SERGEANT
CLASS.currentRankFull = "First Sergeant"
CLASS.rank = 9
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/1sg.png"
CLASS.currentRankAbv = "1SGT"
CLASS.nextRank = "SGM"
CLASS.nextRankLiteral = CLASS_501ST_SERGEANT_MAJOR
CLASS.model = "models/galactic/clones/501/trooper.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_501ST_FIRST_SERGEANT = CLASS.index