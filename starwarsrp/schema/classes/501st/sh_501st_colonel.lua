CLASS.name = "501st Colonel"
CLASS.faction = FACTION_501ST
CLASS.previousRank = "LTC"
CLASS.previousRankLiteral = CLASS_501ST_LIEUTENANT_COLONEL
CLASS.currentRankFull = "Colonel"
CLASS.rank = 17
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/col.png"
CLASS.currentRankAbv = "COL"
CLASS.canPromote = true
CLASS.nextRank = nil
CLASS.nextRankLiteral = nil
CLASS.model = "models/galactic/clones/501tc/highofc.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_501ST_COLONEL = CLASS.index