CLASS.name = "501st Lieutenant Colonel"
CLASS.faction = FACTION_501ST
CLASS.previousRank = "MAJ"
CLASS.previousRankLiteral = CLASS_501ST_MAJOR
CLASS.currentRankFull = "Lieutenant Colonel"
CLASS.rank = 16
CLASS.rankLogo = "materials/ig_cwrp/ranks/trp/ltc.png"
CLASS.currentRankAbv = "LTC"
CLASS.canPromote = true
CLASS.nextRank = "COL"
CLASS.nextRankLiteral = CLASS_501ST_COLONEL
CLASS.model = "models/galactic/clones/501tc/highofc.mdl"
function CLASS:OnSpawn(client) 
    client:SetMaxHealth(200)
    client:SetHealth(client:GetMaxHealth())
  end
CLASS_501ST_LIEUTENANT_COLONEL = CLASS.index