
Schema.name = "CWRP"
Schema.author = "Impact Networks"
Schema.description = "A roleplay based on the clone wars era."
Schema.logo = "materials/ig_cwrp/logo_v2.png"
Schema.version = "1.0"

-- Include netstream


ix.util.Include("libs/thirdparty/sh_netstream2.lua")
ix.util.Include("libs/sh_anims_swrp.lua")

ix.util.Include("sh_configs.lua")
ix.util.Include("sh_commands.lua")

ix.util.Include("cl_draw_back_weps.lua")
ix.util.Include("cl_schema.lua")
ix.util.Include("cl_hooks.lua")
ix.util.Include("sh_hooks.lua")
ix.util.Include("sh_voices.lua")
ix.util.Include("sh_particles.lua")
ix.util.Include("sh_overwrites.lua")
ix.util.Include("sh_staff_commands.lua")
ix.util.Include("sv_schema.lua")
ix.util.Include("sv_hooks.lua")
ix.util.Include("sv_droid_hooks.lua")
ix.util.Include("sv_log_types.lua")

ix.util.Include("quick_commands/cl_quick_commands.lua")
ix.util.Include("quick_commands/sv_quick_commands.lua")

ix.util.Include("broadcast_system/cl_broadcast.lua")
ix.util.Include("broadcast_system/sv_broadcast.lua")

ix.util.Include("meta/sh_player.lua")
ix.util.Include("meta/sv_player.lua")
ix.util.Include("meta/sh_character.lua")
ix.util.Include("meta/sh_vector.lua")

ix.flag.Add("v", "Access to light blackmarket goods.")
ix.flag.Add("V", "Access to heavy blackmarket goods.")

ix.anim.SetModelClass("models/eliteghostcp.mdl", "metrocop")
ix.anim.SetModelClass("models/eliteshockcp.mdl", "metrocop")
ix.anim.SetModelClass("models/leet_police2.mdl", "metrocop")
ix.anim.SetModelClass("models/sect_police2.mdl", "metrocop")
ix.anim.SetModelClass("models/policetrench.mdl", "metrocop")

ix.class.LoadFromDir("starwarsrp/schema/classes/jedi")
ix.class.LoadFromDir("starwarsrp/schema/classes/501st")
ix.class.LoadFromDir("starwarsrp/schema/classes/212th")
ix.class.LoadFromDir("starwarsrp/schema/classes/327th")
ix.class.LoadFromDir("starwarsrp/schema/classes/104th")
ix.class.LoadFromDir("starwarsrp/schema/classes/41st")
ix.class.LoadFromDir("starwarsrp/schema/classes/CG")
ix.class.LoadFromDir("starwarsrp/schema/classes/ARC")
ix.class.LoadFromDir("starwarsrp/schema/classes/RC")
ix.class.LoadFromDir("starwarsrp/schema/classes/medical")
ix.class.LoadFromDir("starwarsrp/schema/classes/pilot")
--TODO make load from dir recursivly grab faction class fodlers
hook.Add("DoPluginIncludes", "[IG] Load classes after factions", function()
	ix.faction.LoadFromDir("starwarsrp/schema/factions")
end)

local function currentPath()
	return (debug.getinfo((level or 1)+1).source:gsub("\\", "/"):sub(2):gsub("//", "/"))
end
local function getFolder(i)
	i = i or 0
	local path = currentPath()
	return path:match("(.*/)" .. (i == 0 and "" or (".*/"):rep(i or 1))) or ""
end

function addModelAnimations(path, info)
	if path[#path] == "/" then path = path:sub(1, #path-1) end
	local files, folders = file.Find(path.."/*", "MOD")
	for _,f in ipairs(files)do
		local fpath = path.."/"..f
		local path_model    = fpath:match("(models/.+mdl)")
		if path_model then
			ix.anim.SetModelClass(path_model, "player")
			if info then print("[utils]: Added Model Animations to: "..path_model) end
		end
	end
	
	for _,f in ipairs(folders)do
		addModelAnimations(path.."/"..f, info)
	end
end

hook.Add("InitPostEntity", "[IG] Load model animations", function()
	-- addModelAnimations("addons/models_jedi", false)
	-- addModelAnimations("addons/models_gamemaster", false)
	-- addModelAnimations("addons/models_cwrp_battalions", false)
end)

hook.Add("InitPostEntity", "[IG] Fix default chat commands", function()

	ix.chat.Register("ic", {
		indicator = "chatTalking",
		OnChatAdd = function(self, speaker, text)
			-- @todo remove and fix actual cause of speaker being nil
			if (!IsValid(speaker)) then
				return
			end
	
			local icon = "icon16/user.png"
	
			if (speaker:IsSuperAdmin()) then
				icon = "icon16/shield.png"
			elseif (speaker:IsAdmin()) then
				icon = "icon16/star.png"
			elseif (speaker:IsUserGroup("moderator") or speaker:IsUserGroup("operator")) then
				icon = "icon16/wrench.png"
			elseif (speaker:IsUserGroup("vip") or speaker:IsUserGroup("donator") or speaker:IsUserGroup("donor")) then
				icon = "icon16/heart.png"
			end
	
			icon = Material(hook.Run("GetPlayerIcon", speaker) or icon)
	
			chat.AddText(icon, Color(255, 50, 50), "", speaker, color_white, ": "..text)
		end,
		CanHear = ix.config.Get("chatRange", 280)
	})
	
	ix.chat.Register("y", {
		indicator = "chatYelling",
		OnChatAdd = function(self, speaker, text)
			-- @todo remove and fix actual cause of speaker being nil
			if (!IsValid(speaker)) then
				return
			end
	
			local icon = "icon16/user.png"
	
			if (speaker:IsSuperAdmin()) then
				icon = "icon16/shield.png"
			elseif (speaker:IsAdmin()) then
				icon = "icon16/star.png"
			elseif (speaker:IsUserGroup("moderator") or speaker:IsUserGroup("operator")) then
				icon = "icon16/wrench.png"
			elseif (speaker:IsUserGroup("vip") or speaker:IsUserGroup("donator") or speaker:IsUserGroup("donor")) then
				icon = "icon16/heart.png"
			end
	
			icon = Material(hook.Run("GetPlayerIcon", speaker) or icon)
	
			chat.AddText(icon, Color(255, 50, 50), "[YELL] ", speaker, color_white, ": "..text)
		end,
		CanHear = ix.config.Get("chatRange", 280),
		prefix = {"/Y", "/Yell"},
		description = "@cmdY",
		indicator = "chatYelling"
	})
	
	ix.config.Add("commsDelay", 3, "The delay before a player can use comms chat again in seconds.", nil, {
		data = {min = 0, max = 10000},
		category = "chat"
	})
	
	ix.chat.Register("comms", {
		CanSay = function(self, speaker, text)
			
			local delay = ix.config.Get("commsDelay", 3)
	
			-- Only need to check the time if they have spoken in OOC chat before.
			if (delay > 0 and speaker.lastComms) then
				local lastComms = CurTime() - speaker.lastComms
	
				-- Use this method of checking time in case the lastComms config changes.
				if (lastComms <= delay and !CAMI.PlayerHasAccess(speaker, "Helix - Bypass OOC Timer", nil)) then
					speaker:NotifyLocalized("lastComms", delay - math.ceil(lastComms))
	
					return false
				end
			end
	
			-- Save the last time they spoke in OOC.
			speaker.lastComms = CurTime()
		end,
		OnChatAdd = function(self, speaker, text)
			-- @todo remove and fix actual cause of speaker being nil
			if (!IsValid(speaker)) then
				return
			end
	
			local icon = "icon16/user.png"
	
			if (speaker:IsSuperAdmin()) then
				icon = "icon16/shield.png"
			elseif (speaker:IsAdmin()) then
				icon = "icon16/star.png"
			elseif (speaker:IsUserGroup("moderator") or speaker:IsUserGroup("operator")) then
				icon = "icon16/wrench.png"
			elseif (speaker:IsUserGroup("vip") or speaker:IsUserGroup("donator") or speaker:IsUserGroup("donor")) then
				icon = "icon16/heart.png"
			end
	
			icon = Material(hook.Run("GetPlayerIcon", speaker) or icon)
	
			chat.AddText(icon, Color(255, 50, 50), "[COMMS] ", speaker, color_white, ": "..text)
		end,
		prefix = {"/comms"},
		description = "Allows you to send a message over comms",
		noSpaceAfter = true
	})

	ix.chat.Register("w", {
		OnChatAdd = function(self, speaker, text)
			-- @todo remove and fix actual cause of speaker being nil
			if (!IsValid(speaker)) then
				return
			end
	
			local icon = "icon16/user.png"
	
			if (speaker:IsSuperAdmin()) then
				icon = "icon16/shield.png"
			elseif (speaker:IsAdmin()) then
				icon = "icon16/star.png"
			elseif (speaker:IsUserGroup("moderator") or speaker:IsUserGroup("operator")) then
				icon = "icon16/wrench.png"
			elseif (speaker:IsUserGroup("vip") or speaker:IsUserGroup("donator") or speaker:IsUserGroup("donor")) then
				icon = "icon16/heart.png"
			end
	
			icon = Material(hook.Run("GetPlayerIcon", speaker) or icon)
	
			chat.AddText(icon, Color(255, 50, 50), "[WHISPER] ", speaker, color_white, ": "..text)
		end,
		CanHear = ix.config.Get("chatRange", 280) * 0.25,
		prefix = {"/W", "/Whisper"},
		description = "@cmdW",
		indicator = "chatWhispering"
	})
	
	ix.config.Add("broadcastDelay", 3, "The delay before a player can use comms chat again in seconds.", nil, {
		data = {min = 0, max = 10000},
		category = "chat"
	})
	
	ix.chat.Register("broadcast", {
		CanSay = function(self, speaker, text)
			local character = speaker:GetCharacter()
	
			local class = rankToClass(character:GetRank(), character:GetFaction())

			if (class && class.canPromote) then
				local delay = ix.config.Get("broadcastDelay", 3)
	
				-- Only need to check the time if they have spoken in OOC chat before.
				if (delay > 0 and speaker.lastBroadcast) then
					local lastBroadcast = CurTime() - speaker.lastBroadcast
	
					-- Use this method of checking time in case the lastBroadcast config changes.
					if (lastBroadcast <= delay and !CAMI.PlayerHasAccess(speaker, "Helix - Bypass OOC Timer", nil)) then
						speaker:NotifyLocalized("lastBroadcast", delay - math.ceil(lastBroadcast))
	
						return false
					end
				end
	
				-- Save the last time they spoke in OOC.
				ixSendBroadcastedMessage(speaker, text)
				speaker.lastBroadcast = CurTime()
			else
				speaker:NotifyLocalized("You are not allowed to broadcast messages.")
				return false
			end
		end,
		OnChatAdd = function(self, speaker, text)
			-- @todo remove and fix actual cause of speaker being nil
			if (!IsValid(speaker)) then
				return
			end
	
			local icon = "icon16/user.png"
	
			if (speaker:IsSuperAdmin()) then
				icon = "icon16/shield.png"
			elseif (speaker:IsAdmin()) then
				icon = "icon16/star.png"
			elseif (speaker:IsUserGroup("moderator") or speaker:IsUserGroup("operator")) then
				icon = "icon16/wrench.png"
			elseif (speaker:IsUserGroup("vip") or speaker:IsUserGroup("donator") or speaker:IsUserGroup("donor")) then
				icon = "icon16/heart.png"
			end
	
			icon = Material(hook.Run("GetPlayerIcon", speaker) or icon)
	
			chat.AddText(icon, Color(255, 50, 50), "BROADCAST FROM ", color_white, speaker)
		end,
		prefix = {"/broadcast"},
		description = "Allows you to send a message over broadcast",
		noSpaceAfter = true
	})

end)

if SERVER then
	function Schema:ShowSpare1(client)
		if (client:GetCharacter()) then
			client:ConCommand("ctp")
		end
	end
end

function Schema:ZeroNumber(number, length)
	local amount = math.max(0, length - string.len(number))
	return string.rep("0", amount)..tostring(number)
end

function Schema:IsNavalRank(text, rank)
	return string.find(text, "[%D+]"..rank.."[%D+]")
end

function GetFactionObject(factionIndex)

	return ix.faction.indices[factionIndex]

end

function rankToClass(r, faction)

    local classList = ix.class.list

    for k, v in pairs(classList) do
        if (v.rank == r && v.faction == faction) then
            return v
        end
    end

    return nil

end

function rankToClassIndex(r, faction)

    local classList = ix.class.list

    for k, v in pairs(classList) do
        if (v.rank == r && v.faction == faction) then
            if (v.index) then
                return v.index
            end
        end
    end

    return nil

end

do
	local CLASS = {}
	CLASS.color = Color(150, 100, 100)
	CLASS.format = "Dispatch broadcasts \"%s\""

	function CLASS:CanSay(speaker, text)
		if (!speaker:IsRC()) then
			speaker:NotifyLocalized("notAllowed")

			return false
		end
	end

	function CLASS:OnChatAdd(speaker, text)
		chat.AddText(self.color, string.format(self.format, text))
	end

	ix.chat.Register("dispatch", CLASS)
end

do
	local CLASS = {}
	CLASS.color = Color(75, 150, 50)
	CLASS.format = "%s radios in \"%s\""

	function CLASS:CanHear(speaker, listener)
		local character = listener:GetCharacter()
		local inventory = character:GetInventory()
		local bHasRadio = false

		for k, v in pairs(inventory:GetItemsByUniqueID("handheld_radio", true)) do
			if (v:GetData("enabled", false) and speaker:GetCharacter():GetData("frequency") == character:GetData("frequency")) then
				bHasRadio = true
				break
			end
		end

		return bHasRadio
	end

	function CLASS:OnChatAdd(speaker, text)
		text = speaker:IsNaval() and string.format("<:: %s ::>", text) or text
		chat.AddText(self.color, string.format(self.format, speaker:Name(), text))
	end

	ix.chat.Register("radio", CLASS)
end

do
	local CLASS = {}
	CLASS.color = Color(255, 255, 175)
	CLASS.format = "%s radios in \"%s\""

	function CLASS:GetColor(speaker, text)
		if (LocalPlayer():GetEyeTrace().Entity == speaker) then
			return Color(175, 255, 175)
		end

		return self.color
	end

	function CLASS:CanHear(speaker, listener)
		if (ix.chat.classes.radio:CanHear(speaker, listener)) then
			return false
		end

		local chatRange = ix.config.Get("chatRange", 280)

		return (speaker:GetPos() - listener:GetPos()):LengthSqr() <= (chatRange * chatRange)
	end

	function CLASS:OnChatAdd(speaker, text)
		text = speaker:IsNaval() and string.format("<:: %s ::>", text) or text
		chat.AddText(self.color, string.format(self.format, speaker:Name(), text))
	end

	ix.chat.Register("radio_eavesdrop", CLASS)
end

do
	local CLASS = {}
	CLASS.color = Color(175, 125, 100)
	CLASS.format = "%s requests \"%s\""

	function CLASS:CanHear(speaker, listener)
		return listener:IsNaval() or speaker:Team() == FACTION_ADMIN
	end

	function CLASS:OnChatAdd(speaker, text)
		chat.AddText(self.color, string.format(self.format, speaker:Name(), text))
	end

	ix.chat.Register("request", CLASS)
end

do
	local CLASS = {}
	CLASS.color = Color(175, 125, 100)
	CLASS.format = "%s requests \"%s\""

	function CLASS:CanHear(speaker, listener)
		if (ix.chat.classes.request:CanHear(speaker, listener)) then
			return false
		end

		local chatRange = ix.config.Get("chatRange", 280)

		return (speaker:Team() == FACTION_CITIZEN and listener:Team() == FACTION_CITIZEN)
		and (speaker:GetPos() - listener:GetPos()):LengthSqr() <= (chatRange * chatRange)
	end

	function CLASS:OnChatAdd(speaker, text)
		chat.AddText(self.color, string.format(self.format, speaker:Name(), text))
	end

	ix.chat.Register("request_eavesdrop", CLASS)
end

do
	local CLASS = {}

	function CLASS:GetColor(speaker, text)
		if (LocalPlayer():GetEyeTrace().Entity == speaker) then
			return Color(175, 255, 175)
		end

		return self.color
	end

	function CLASS:CanHear(speaker, listener)
		return speaker:GetCharacter():GetFaction() == listener:GetCharacter():GetFaction()
	end

	function CLASS:OnChatAdd(speaker, text)
		local faction = GetFactionObject(speaker:GetCharacter():GetFaction())
		chat.AddText(faction.color, "[" .. faction.name .."] ", Color(128, 128, 128), speaker:Name() .. ": ", Color(255,255,255), text)
		
		hook.Run("SwrpOnBattalionChat", speaker, text)
	end

	ix.chat.Register("teamChat", CLASS)
end

sound.Add( {
	name = "spawn_point_loop",
	channel = CHAN_STATIC,
	volume = 1.0,
	level = 60,
	pitch = {95, 110},
	sound = "ambient/levels/labs/teleport_active_loop1.wav"
} )

ix.flag.Add("E", "Access to spawn SENTs.")
ix.flag.Add("w", "Access to spawn weapons.")