ix.log.AddType("arrest", function(client, ...)
    local arg = {...}
    return client:Name() .. " has arrested ".. arg[1] .."."
end, FLAG_NORMAL)

ix.log.AddType("unarrest", function(client, ...)
    local arg = {...}
    return client:Name() .. " has unarrested ".. arg[1] .."."
end, FLAG_NORMAL)

ix.log.AddType("taser", function(client, ...)
    local arg = {...}
    return client:Name() .. " has tased ".. arg[1] .."."
end, FLAG_NORMAL)

ix.log.AddType("grenade", function(client)
    return client:Name() .. " has throw a grenade."
end, FLAG_NORMAL)