local alertUrl = "https://static.innovativegamers.net/sound/bizwarn.wav"

net.Receive("ixBroadcastMessage", function(length)
    local url = net.ReadString() 
    local alertName = "impact_alert.txt"
    local broadName = "impact_broadcast.txt"
    if(file.Exists(alertName, "DATA")) then
        http.Fetch( url,
        function( body, len, headers, code )
            file.Write(broadName, body)
            local emitEntity = ents.FindByClass("ix_speakers")
            
            for k, v in ipairs( emitEntity ) do

                if (v:GetPos():DistToSqr(LocalPlayer():GetPos()) > 1000 * 1000) then continue end

                sound.PlayFile( "data/" .. alertName, "noplay 3d", function( alert, errCode, errStr )
                    if ( IsValid( alert ) ) then
                        alert:SetPos(v:GetPos())
                        alert:Set3DFadeDistance(200, 600)
                        alert:Set3DCone(-1, -1, 0)
                        alert:Play()
                        alert:SetVolume(1.5)
                    else
                        print( "Error playing sound!", errCode, errStr )
                    end
                end )

                timer.Simple(.65, function()
                    sound.PlayFile( "data/" .. broadName, "noplay 3d", function( broadcast, errCode, errStr )
                        if ( IsValid( broadcast ) ) then
                            broadcast:SetPos(v:GetPos())
                            broadcast:Set3DFadeDistance(200, 600)
                            broadcast:Set3DCone(-1, -1, 0)
                            broadcast:Play()
                            broadcast:SetVolume(1.5)
                        else
                            print( "Error playing sound!", errCode, errStr )
                        end
                    end )
                end)

            end
        end,
        function( error )
            print("Broadcast error: " .. error)
        end)
    else
        http.Fetch(alertUrl, 
        function( body, len, headers, code )
            file.Write(alertName, body)
            http.Fetch( url,
            function( body, len, headers, code )
                file.Write(broadName, body)
                local emitEntity = ents.FindByClass("ix_speakers")
                
                for k, v in ipairs( emitEntity ) do

                    sound.PlayFile( "data/" .. alertName, "noplay 3d", function( alert, errCode, errStr )
                        if ( IsValid( alert ) ) then
                            alert:SetPos(v:GetPos())
                            alert:Play()
                        else
                            print( "Error playing sound!", errCode, errStr )
                        end
                    end )

                    timer.Simple(.5, function()
                        sound.PlayFile( "data/" .. broadName, "noplay 3d", function( broadcast, errCode, errStr )
                            if ( IsValid( broadcast ) ) then
                                broadcast:SetPos(v:GetPos())
                                broadcast:Play()
                            else
                                print( "Error playing sound!", errCode, errStr )
                            end
                        end )
                    end)

                end
            end,
            function( error )
                print("Broadcast error: " .. error)
            end)
        end)
    end
end)