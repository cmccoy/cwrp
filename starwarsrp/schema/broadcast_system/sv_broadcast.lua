util.AddNetworkString("ixBroadcastMessage")

local nextAnnounce = CurTime()
function ixSendBroadcastedMessage(client, text)
    local character = client:GetCharacter()
    local class = rankToClass(character:GetRank(), character:GetFaction())

    if nextAnnounce < CurTime() then
        if (class.canPromote) then
            text = string.Replace(text, " ", "%20")
            text = string.format("https://tetyys.com/SAPI4/SAPI4?text=%s&voice=Sam&pitch=100&speed=150", text)
            
            net.Start("ixBroadcastMessage")
                net.WriteString(text)
            net.Broadcast()
            
            nextBroadcast = CurTime() + 10
        end
    end

end