hook.Add("EntityTakeDamage", "[Impact] Scale Droid Damage", function(target, dmginfo)

	if (target:IsPlayer() && target.isDroid) then
		dmginfo:ScaleDamage(0.5)
	end

end)

hook.Add("PlayerCanPickupWeapons", "[Impact] Disable Utility Droid Weapons", function(client, wep)

	if client.isDroid then
		if(client:GetCharacter():GetFaction() == FACTION_UTILITY_DROID) then
			return false
		end
	end

end)

hook.Add( "PlayerDeath", "[Impact] Player Droid Death", function( victim, inflictor, attacker)
	if victim.isDroid then

		timer.Simple(0.01, function()
			if(victim:GetRagdollEntity() != nil and	victim:GetRagdollEntity():IsValid()) then
				victim:GetRagdollEntity():Remove()
			end
		end )
		
		ParticleEffect( "striderbuster_break", victim:GetPos() + Vector(0,0,50), Angle( 0, 0, 0 ), nil )
		sound.Play("ambient/explosions/explode_7.wav",victim:GetPos() + Vector(0,0,50),75,100)
		
	end
end)