util.AddNetworkString("animationwheel_bonegestures")

net.Receive("animationwheel_bonegestures", function(len, pl)
    local bones = net.ReadTable()
    local resetangles = net.ReadBool() --false if you are not resetting the bone angles

    for bonename, boneang in pairs(bones) do
        if resetangles then
            pl:ManipulateBoneAngles(pl:LookupBone(bonename), Angle(0, 0, 0))
        elseif not resetangles then
            pl:ManipulateBoneAngles(pl:LookupBone(bonename), boneang)
        end
    end
end)