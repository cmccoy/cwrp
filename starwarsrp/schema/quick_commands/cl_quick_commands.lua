local radialConfig = {}								-- Config object
local radialConfigTools = {}							-- Config object - Tools
local SToolButtons = {}								-- Search Menu
local Results = {}									-- Search menu - Results
local radialMenuShown = false				-- Is the menu visible?
local screenCenterX = ScrW() / 2		-- X center
local screenCenterY = ScrH() / 2		-- Y center
local pi = 4 * math.atan2(1, 1)			-- Pi (Yum :)
local radialEntrySize = 0						-- How many degrees does a menu entry span?
local radialMenuSelectedTool = 0		-- The currently selected tool

local radialMenuToggledAnimation = 0    -- The Toggled Animation
local radialMenuTAnimActiveColor = Color(50, 168, 82) --Color the thing different if it's selected

-- Check if we read and parsed the config, and if not, create a default valued one
if table.Count(radialConfig) == 0 then
    -- Default Config
    radialConfig["gbr-menudeadzone"] = 40									-- Defines the "dead area" around the screen center, where you can't select anything
    radialConfig["gbr-menufont"] = "DermaDefault"					-- Font used to print the radial menu
    radialConfig["gbr-menusplitterlength"] = 60						-- The lenght of the split-lines between menu items
    radialConfig["gbr-menushowsplitters"] = 1							-- Show or don't show the menu splitter lines
    radialConfig["gbr-menulabelssizedindividually"] = 1		-- Should all tool labels be the same size or not?
    radialConfig["gbr-menushowtraceline"] = 1							-- Draw a line from center screen to mouse cursor?

    -- Default Tools, only set these if they haven't been loaded before
    if table.Count(radialConfigTools) == 0 then
        --radialConfigTools[1] = { name = "Salute", rconcommand = "act salute; say /me salutes" }
        radialConfigTools[1] = { name = "Salute", bones = {
            ["ValveBiped.Bip01_R_UpperArm"] = Angle(80, -95, -77.5), 
            ["ValveBiped.Bip01_R_Forearm"] = Angle(35, -125, -5)
        }}
        --radialConfigTools[2] = { name = "Wave", rconcommand = "act wave; say /me waves" }
        radialConfigTools[2] = { name = "Surrender", bones = {
            ["ValveBiped.Bip01_L_Forearm"] = Angle(25,-65,25),
            ["ValveBiped.Bip01_R_Forearm"] = Angle(-25,-65,-25),
            ["ValveBiped.Bip01_L_UpperArm"] = Angle(-70,-180,70),
            ["ValveBiped.Bip01_R_UpperArm"] = Angle(70,-180,-70)
        }}
        radialConfigTools[3] = { name = "Agree", rconcommand = "act agree; say /me agrees" }
        radialConfigTools[4] = { name = "Disagree", rconcommand = "act disagree; say /me disagrees" }
        radialConfigTools[5] = { name = "Halt", rconcommand = "act halt; say Halt!" }
        radialConfigTools[6] = { name = "Group", rconcommand = "act group; say Group Up!" }
        radialConfigTools[7] = { name = "Forward", rconcommand = "act forward; say Push forward" }
        --radialConfigTools[8] = { name = "Cheer", rconcommand = "act cheer; say /me cheers" }
        radialConfigTools[8] = { name = "Cross", bones = {
            ["ValveBiped.Bip01_R_Forearm"] = Angle(-43.779933929443,-107.18412780762,15.918969154358),
            ["ValveBiped.Bip01_R_UpperArm"] = Angle(20.256689071655, -57.223915100098, -6.1269416809082),
            ["ValveBiped.Bip01_L_UpperArm"] = Angle(-28.913911819458, -59.408206939697, 1.0253102779388),
            ["ValveBiped.Bip01_R_Thigh"] = Angle(4.7250719070435, -6.0294013023376, -0.46876749396324),
            ["ValveBiped.Bip01_L_Thigh"] = Angle(-7.6583762168884, -0.21996378898621, 0.4060270190239),
            ["ValveBiped.Bip01_L_Forearm"] = Angle(51.038677215576, -120.44165039063, -18.86986541748),
            ["ValveBiped.Bip01_R_Hand"] = Angle(14.424224853516, -33.406204223633, -7.2624106407166),
            ["ValveBiped.Bip01_L_Hand"] = Angle(25.959447860718, 31.564517974854, -14.979378700256)
        }}
        --radialConfigTools[9] = { name = "Laugh", rconcommand = "act laugh; say /me laughs" }
        radialConfigTools[9] = { name = "Rest", bones = {
            ["ValveBiped.Bip01_R_UpperArm"] = Angle(3.809, 15.382, 2.654),
            ["ValveBiped.Bip01_R_Forearm"] = Angle(-63.658, 1.8 , -84.928),
            ["ValveBiped.Bip01_L_UpperArm"] = Angle(3.809, 15.382, 2.654),
            ["ValveBiped.Bip01_L_Forearm"] = Angle(53.658, -29.718, 31.455),
            
            ["ValveBiped.Bip01_R_Thigh"] = Angle(4.829, 0, 0),
            ["ValveBiped.Bip01_L_Thigh"] = Angle(-8.89, 0, 0)
        }}
        radialConfigTools[10] = { name = "Bow", rconcommand = "act bow; say /me bows" }
    end
end

-- Calculate positions for the various radial menu entries
local function CalculateMenuLayout()
    local angle = 0				-- Our starting angle
    local longestName = 0	-- The length of the longest name, used to calculate x/y position
    radialEntrySize = math.floor(360 / table.getn(radialConfigTools))

    -- Find the pixelsize of the largest word, to get an even circle
    
    for key,value in pairs(radialConfigTools) do
        -- Is this the longest name yet?
        surface.SetFont(tostring(radialConfig["gbr-menufont"]))
        tw, th = surface.GetTextSize(value.name)
        
        if tw > longestName then longestName = tw end
    end

    -- Loop through all tools, and calculate span-angle, x/y position and menu-splitter
    for key,value in pairs(radialConfigTools) do
        value.minangle = angle - (radialEntrySize / 2)
        if value.minangle < 0 then value.minangle = 360 + value.minangle end	-- First tool MinAngle will always dip below 0, and have to "wrap" down from 360

        value.maxangle = angle + (radialEntrySize / 2)

        value.xpos = screenCenterX - ((radialConfig["gbr-menudeadzone"] + (longestName / 2)) * math.sin((360 - angle) * (pi / 180)))	-- X position of the menu point text
        value.ypos = screenCenterY - ((radialConfig["gbr-menudeadzone"] + (longestName / 2)) * math.cos((360 - angle) * (pi / 180)))	-- Y position of the menu point text

        value.menusplitxinner = screenCenterX - (radialConfig["gbr-menudeadzone"] * math.sin((360 - value.minangle) * (pi / 180)))	-- Used to draw a split-line from center-screen, 100 pixels out
        value.menusplityinner = screenCenterY - (radialConfig["gbr-menudeadzone"] * math.cos((360 - value.minangle) * (pi / 180)))	-- -O-
        value.menusplitxouter = screenCenterX - ((radialConfig["gbr-menusplitterlength"] + radialConfig["gbr-menudeadzone"]) * math.sin((360 - value.minangle) * (pi / 180)))	-- -O-
        value.menusplityouter = screenCenterY - ((radialConfig["gbr-menusplitterlength"] + radialConfig["gbr-menudeadzone"]) * math.cos((360 - value.minangle) * (pi / 180)))	-- -O-

        -- Should labels be the same size or not?
        tw, th = surface.GetTextSize(value.name)
        
        if radialConfig["gbr-menulabelssizedindividually"] == 1 then
            -- Labels are individually sized
            value.labelwidth = tw
            value.labelheight = th
        else
            -- Labels are same size (ie, size of the longest name)
            value.labelwidth = longestName
            value.labelheight = th
        end

        -- Increase the angle
        angle = angle + radialEntrySize
    end
    
    -- Set the last entrys max to the first entrys min, so we don't get any "empty" menu-space
    radialConfigTools[table.getn(radialConfigTools)].maxangle = radialConfigTools[1].minangle
end

-- Calculate the menu layout
CalculateMenuLayout()

-- Functions to show and hide the menu
local function ShowRadialMenu()
    -- Enable mouse cursor
    gui.EnableScreenClicker(true)
    
    -- Set cursor centerscreen
    gui.SetMousePos(screenCenterX, screenCenterY)
    
    -- Show menu
    radialMenuShown = true		
end

local function HideRadialMenu()
    -- Is there an active tool?
    if radialMenuSelectedTool > 0 then
        -- Yes there is, select the tool
        
        if radialConfigTools[radialMenuSelectedTool].rconcommand then --regular rconcommand thing does this
            LocalPlayer():ConCommand(radialConfigTools[radialMenuSelectedTool].rconcommand)
        elseif radialConfigTools[radialMenuSelectedTool].bones then --weird animation shit here
            if radialMenuSelectedTool ~= radialMenuToggledAnimation then
                for bonename, boneang in pairs(radialConfigTools[radialMenuSelectedTool].bones) do
                    LocalPlayer():ManipulateBoneAngles( LocalPlayer():LookupBone(bonename), boneang )
                end
                net.Start("animationwheel_bonegestures")
                net.WriteTable(radialConfigTools[radialMenuSelectedTool].bones)
                net.WriteBool(false) --Tell server to not reset boneangles
                net.SendToServer()
                radialMenuToggledAnimation = radialMenuSelectedTool --So We know what we toggled
            elseif radialMenuSelectedTool == radialMenuToggledAnimation then
                for bonename, boneang in pairs(radialConfigTools[radialMenuSelectedTool].bones) do
                    LocalPlayer():ManipulateBoneAngles( LocalPlayer():LookupBone(bonename), Angle(0,0,0) ) --Reset to Default Angles
                end
                net.Start("animationwheel_bonegestures")
                net.WriteTable(radialConfigTools[radialMenuSelectedTool].bones)
                net.WriteBool(true) --Tell server to reset boneangles
                net.SendToServer()
                radialMenuToggledAnimation = 0 --So we know that nothing is toggled
            end
        end
        -- Now we dont have a selected tool
        radialMenuSelectedTool = 0
    end
    
    -- Disable mouse cursor
    gui.EnableScreenClicker(false)

    -- Hide menu
    radialMenuShown = false
end

-- Print text in a RoundedBox, centered at required coordinates, using given color
local function DrawRadialMenuItem(itemText, itemXpos, itemYpos, labelWidth, labelHeight, boxColor, textColor)
    if not itemText then return end
    
    -- Draw the roundedbox
    draw.RoundedBox(8, (itemXpos - (labelWidth / 2) - 5), (itemYpos - (labelHeight / 2) - 2), labelWidth + 10, labelHeight + 4, boxColor)
    
    -- Draw the text
    draw.SimpleText(itemText, radialMenuFont, itemXpos, itemYpos, textColor, 1, 1)
end

local function DrawRadialMenu()
    -- Do not draw the menu if it is not shown
    if not radialMenuShown then return end
    
    -- Draw a line from screen-center to mouse cursor, if enabled
    if radialConfig["gbr-menushowtraceline"] == 1 then
        surface.SetDrawColor(255, 255, 255, 80)
        surface.DrawLine(screenCenterX, screenCenterY, gui.MouseX(), gui.MouseY())					
    end
    
    -- Is the distance from screen-center to the cursor larger than the set deadspace?
    if math.Dist(screenCenterX, screenCenterY, gui.MouseX(), gui.MouseY()) > radialConfig["gbr-menudeadzone"] then
        -- We have moved out of the deadzone
        -- Calculate the angle from screen-center to the mousecursor, so we can determine which radial element the cursor is over
        local radialSelectAngle = 360 - (math.deg(math.atan2(gui.MouseX() - screenCenterX, gui.MouseY() - screenCenterY)) + 180)	-- Manipulate the degrees, so we get 0 to be upwards, and increasing clockwise

        -- Loop tools them and find out which one is active
        for key,value in pairs(radialConfigTools) do
            -- Check for a "normal" entry first (one that doesn't span 0)
            if (value.minangle <= radialSelectAngle) and (value.maxangle > radialSelectAngle) then
                -- We have found the active tool
                radialMenuSelectedTool = key
                
                -- Break out of the for loop
                break
            end

            if value.minangle > value.maxangle then
                -- MinAngle is larger than MaxAngle, we have the entry that spans 0 degrees (straight up)
                if (value.minangle <= radialSelectAngle) or (value.maxangle > radialSelectAngle) then
                    -- We have found the active tool
                    radialMenuSelectedTool = key

                    -- Break out of the for loop
                    break
                end
            end
        end			
    else
        -- We are in the deadzone, no tools are selected
        radialMenuSelectedTool = 0
    end
    
    -- Draw the tool menu entries
    for key,value in pairs(radialConfigTools) do
        if key == radialMenuSelectedTool then
            -- This is the selected tool
            DrawRadialMenuItem(value.name, value.xpos, value.ypos, value.labelwidth, value.labelheight, Color(60, 80, 120, 200), Color(255, 255, 255, 255))
        elseif key == radialMenuToggledAnimation then
            -- This is the Toggled animation
            DrawRadialMenuItem(value.name, value.xpos, value.ypos, value.labelwidth, value.labelheight, radialMenuTAnimActiveColor, Color(255, 255, 255, 255))
        else
            -- This is not the selected tool
            DrawRadialMenuItem(value.name, value.xpos, value.ypos, value.labelwidth, value.labelheight, Color(30, 50, 60, 200), Color(255, 255, 255, 255))
        end
        
        -- Draw the splitter-line, if the user wants to
        if radialConfig["gbr-menushowsplitters"] == 1 then
            surface.SetDrawColor(255, 255, 255, 80)
            surface.DrawLine(value.menusplitxinner, value.menusplityinner, value.menusplitxouter, value.menusplityouter)			
        end
    end				
end

-- Add hook to paint the radial menu
hook.Add("HUDPaint", "DrawRadialMenu", DrawRadialMenu)

-- Capture both left and right-click (to override left, and open settings menu on right
local function CaptureMouseClicks(mouseInfo)
    -- Do not do any of this if the menu is not shown
    if not radialMenuShown then return end
    
    -- Handle different clicktypes
    if mouseInfo == 107 then
        -- Left click, just make it impossible to left-click
        return true
    elseif mouseInfo == 108 then
        -- Right click, open the config menu
        HideRadialMenu()
        g_ContextMenu:Open()
        
        return true
    end
end
hook.Add("GUIMousePressed", "CaptureMouseClicks", CaptureMouseClicks)
    

-- Add con commands to show/hide the menu
concommand.Add("+gb-radial", ShowRadialMenu)
concommand.Add("-gb-radial", HideRadialMenu)

hook.Add("PlayerButtonDown", "Open gestures", function(pl, button)
	
	if button == KEY_B then
	    ShowRadialMenu()
	end

end)


hook.Add("PlayerButtonUp", "Close gestures", function(pl, button)

	if button == KEY_B then	
    	HideRadialMenu()
    end

end)