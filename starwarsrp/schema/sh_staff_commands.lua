local sitRoom = Vector(-13167, 13885, -932)
local noExitPos = Vector(-6006, 13324, -204)

ix.command.Add("sit", {
    description = "Teleports you to the sit room",
    OnCheckAccess = function(self, client)
        if client:IsStaff() then
            return true
        end
        return false
    end,

    OnRun = function(self, client)
        client.presitPos = client:GetPos()
        client:SetPos(sitRoom)
        hook.Run("SwrpOnStaffSit", client)
    end
})

ix.command.Add("leaveSit", {
    description = "Teleports you out of the sit room",
    OnCheckAccess = function(self, client)
        if client:IsStaff() then
            return true
        end
        return false
    end,

    OnRun = function(self, client)
        if(client.presitPos) then
            client:SetPos(client.presitPos)
            client.presitPos = nil
            hook.Run("SwrpOnLeaveSit", client)
        else
            client:SetPos(noExitPos)
            hook.Run("SwrpOnLeaveSitNoExit", client)
        end
    end
})